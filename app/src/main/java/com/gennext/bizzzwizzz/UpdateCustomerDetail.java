package com.gennext.bizzzwizzz;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzz.model.ProfileModel;
import com.gennext.bizzzwizzz.user.MyProfile;
import com.gennext.bizzzwizzz.user.customerDetail.BankDetail;
import com.gennext.bizzzwizzz.user.customerDetail.BusinessDetail;
import com.gennext.bizzzwizzz.user.customerDetail.KYCDetail;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.FragmentUtil;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;


/**
 * Created by Abhijit on 12-Jan-17.
 */

public class UpdateCustomerDetail extends BaseActivity implements View.OnClickListener{

    public static final int TAB_PROFILE = 0,TAB_BUSINESS = 1,TAB_KYC=2,TAB_BANK=3;

    private TextView tab0,tab1,tab2,tab3;

    private BusinessDetail businessDetail;
    private KYCDetail kycDetail;
    private BankDetail bankDetail;
    private MyProfile myProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_cusromer_detail_update);
        initToolBar(this,"Contact Details");
        InitUI();
        initTab();
        setTab(TAB_PROFILE);
    }

    private void initTab() {
        ProfileModel model = AppUser.getUserProfile(this);
        myProfile= MyProfile.newInstance(model,null);
        businessDetail=BusinessDetail.newInstance(null,null);
        kycDetail=KYCDetail.newInstance(null,null);
        bankDetail=BankDetail.newInstance(null, null);
    }

    private void InitUI() {
        tab0=(TextView)findViewById(R.id.tab_0);
        tab1=(TextView)findViewById(R.id.tab_1);
        tab2=(TextView)findViewById(R.id.tab_2);
        tab3=(TextView)findViewById(R.id.tab_3);

        tab0.setOnClickListener(this);
        tab1.setOnClickListener(this);
        tab2.setOnClickListener(this);
        tab3.setOnClickListener(this);

        if(AppUser.getPackageType(this).equalsIgnoreCase(AppUser.PERSONAL)){
            tab1.setVisibility(View.GONE);
        }
    }
    public void setTab(int tab) {
        switch (tab){
            case TAB_PROFILE:
                tab0.setSelected(true);
                tab1.setSelected(false);
                tab2.setSelected(false);
                tab3.setSelected(false);
                setProfileTab();
                break;
            case TAB_BUSINESS:
                tab0.setSelected(false);
                tab1.setSelected(true);
                tab2.setSelected(false);
                tab3.setSelected(false);
                setBusinessTab();
                break;
            case TAB_KYC:
                tab0.setSelected(false);
                tab1.setSelected(false);
                tab2.setSelected(true);
                tab3.setSelected(false);
                setKYCTab();
                break;
            case TAB_BANK:
                tab0.setSelected(false);
                tab1.setSelected(false);
                tab2.setSelected(false);
                tab3.setSelected(true);
                setBankTab();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tab_0:
                setTab(TAB_PROFILE);
                break;
            case R.id.tab_1:
                setTab(TAB_BUSINESS);
                break;
            case R.id.tab_2:
                setTab(TAB_KYC);
                break;
            case R.id.tab_3:
                setTab(TAB_BANK);
                break;
        }
    }


    private void setProfileTab() {
        setTabFragment(R.id.container,myProfile,"myProfile",businessDetail,kycDetail,bankDetail);
    }
    private void setBusinessTab() {
        setTabFragment(R.id.container,businessDetail,"businessDetail",myProfile,kycDetail,bankDetail);
    }
    private void setKYCTab() {
        setTabFragment(R.id.container,kycDetail,"kycDetail",myProfile,businessDetail,bankDetail);
    }
    private void setBankTab() {
        setTabFragment(R.id.container,bankDetail,"bankDetail",myProfile,kycDetail,businessDetail);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            if (requestCode == BankDetail.ATTACHMENT_BANK) {
                String filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
                // Do anything with file
                BankDetail bankDetail = (BankDetail) FragmentUtil.getFragmentByTag(this, "bankDetail");
                if (bankDetail != null) {
                    bankDetail.addThemToView(filePath);
                }

            } else if (requestCode == KYCDetail.ATTACHMENT_PAN) {
                String filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
                // Do anything with file
                KYCDetail kycDetail = (KYCDetail) FragmentUtil.getFragmentByTag(this, "kycDetail");
                if (kycDetail != null) {
                    kycDetail.addPANThemToView(filePath);
                }

            } else if (requestCode == KYCDetail.ATTACHMENT_ID_PROOF) {
                String filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
                // Do anything with file
                KYCDetail kycDetail = (KYCDetail) FragmentUtil.getFragmentByTag(this, "kycDetail");
                if (kycDetail != null) {
                    kycDetail.addIDThemToView(filePath);
                }

            } else {
                Toast.makeText(this, "ERROR-102 Image not found", Toast.LENGTH_LONG).show();
            }
        }else if(resultCode == ImageMaster.REQUEST_MYPROFILE){
            Uri uri = data.getData();
            if(uri!=null){
                MyProfile myProfile = (MyProfile) FragmentUtil.getFragmentByTag(this, "myProfile");
                if (myProfile != null) {
                    myProfile.setImageURI(uri);
                }
            }
        }else if(resultCode == ImageMaster.REQUEST_BUSINESS){
            Uri uri = data.getData();
            if(uri!=null){
                BusinessDetail businessDetail = (BusinessDetail) FragmentUtil.getFragmentByTag(this, "businessDetail");
                if (businessDetail != null) {
                    businessDetail.setImageURI(uri);
                }
            }
        }else {
            Toast.makeText(this, "ERROR-103 Image not found", Toast.LENGTH_LONG).show();
        }
    }

}