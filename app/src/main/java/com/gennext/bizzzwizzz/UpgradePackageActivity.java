package com.gennext.bizzzwizzz;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Toast;

import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.dialog.PaymentSuccessDialog;
import com.gennext.bizzzwizzz.model.PaymentModel;
import com.gennext.bizzzwizzz.user.mypackage.UpgradePackage;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.PaymentConstant;
import com.gennext.bizzzwizzz.util.RequestBuilder;
import com.payu.india.Extras.PayUChecksum;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.Payu;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.payuui.Activity.PayUBaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Iterator;

/**
 * Created by Abhijit-PC on 01-Mar-17.
 */

public class UpgradePackageActivity extends BaseActivity implements ApiCallError.ErrorPaymentListener, PaymentSuccessDialog.PayentSuccessListener {
    public static final int REQUEST_PAYMENT = 10001;
    public static final String PAYMENT_STATUS = "reqPayData";
    private static final int TASK_VALIDATE = 1, TASK_UPGRADE_PACKAGE = 2;
    private String packageId, packageAmt;
    private GetPaymentInfo paymentInfoTask;

    // required fields for Payment SDK
    private String merchantKey, userCredentials;
    // These will hold all the payment parameters
    private PaymentParams mPaymentParams;
    // This sets the configuration
    private PayuConfig payuConfig;
    // Used when generating hash from SDK
    private PayUChecksum checksum;

    private static final int TESTING = 1;
    private static final int PRODUCTION = 2;

    private int PAYEMNT_MODE;
    //test Card 5123456789012346
    //cvv 123 exp-may2017

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_upgrade);
        // Payment Gateway start
        Payu.setInstance(this);
        PAYEMNT_MODE = PRODUCTION;
        // Payment Gateway end
        addFragmentWithoutBackstack(UpgradePackage.newInstance(UpgradePackage.PACKAGE_UPGRADE),R.id.container_upgrade_package,"upgradePackage");
    }

    public void setData(String pkgId,String pkgAmt) {
        packageId = pkgId;
        packageAmt = pkgAmt;
        executeTask(TASK_VALIDATE);
    }


    private void executeTask(int task) {
        executeTask(task, null);
    }

    private void executeTask(int task, PaymentModel paymentModel) {
        if (task == TASK_VALIDATE) {
            paymentInfoTask = new GetPaymentInfo(this, task, packageId, packageAmt, null);
            paymentInfoTask.execute(AppSettings.GET_PAYMENT_INFO);
        } else  if (task == TASK_UPGRADE_PACKAGE) {
            if (paymentModel != null) {
                paymentInfoTask = new GetPaymentInfo(this, task, null, null, paymentModel);
                paymentInfoTask.execute(AppSettings.PAYMENT_SUCCESS);
            } else {
                showToast(AppSettings.ERROR105 + "Payment error");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            if (data != null) {
                Log.e("PAY", "Payu's Data : " + data.getStringExtra("payu_response"));
                String responseData = data.getStringExtra("payu_response");
                PaymentConstant paymentConstant = PaymentConstant.get(getApplicationContext());
                final PaymentModel responseModel = JsonParser.parseResponseFromPayuMoney(responseData,paymentConstant);
                if (responseModel != null) {
                    if (responseModel.getStatus().equalsIgnoreCase("success")) {
                        updatePaymentRecord(responseModel);
                    } else {
                        new AlertDialog.Builder(this)
                                .setCancelable(false)
                                .setMessage("TxnId : " + responseModel.getTxnId()
                                        + "\n" + "id : " + responseModel.getId()
                                        + "\n" + "Amount : " + responseModel.getAmount()
                                        + "\n" + "Status : " + responseModel.getStatus()
                                        + "\n" + "Error message : " + responseModel.getError_Message())
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                    }
                                }).show();
                    }
                }

            } else {
                Toast.makeText(this, getString(R.string.could_not_receive_data), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void updatePaymentRecord(PaymentModel responseModel) {
        executeTask(TASK_UPGRADE_PACKAGE, responseModel);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog,int task, PaymentModel paymentModel) {
        if(task==TASK_UPGRADE_PACKAGE) {
            updatePaymentRecord(paymentModel);
        }else {
            executeTask(task);
        }
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog,int task, PaymentModel paymentModel) {
        if(task==TASK_UPGRADE_PACKAGE && paymentModel!=null) {
            PopupAlert.newInstance(getString(R.string.payment_error_title), getString(R.string.payment_error_message)
                    + "Transaction Id :" + paymentModel.getTxnId() +
                    "\nPayment Id :" + paymentModel.getId(), PopupAlert.POPUP_DIALOG, 100000);
        }
    }

    @Override
    public void onPaymentSuccessClick(DialogFragment dialog) {
        AppUser.setPackageAvailability(UpgradePackageActivity.this, "success");
        Intent backIntent = new Intent(UpgradePackageActivity.this, MainActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(backIntent);
        finish();
    }

    public class GetPaymentInfo extends AsyncTask<String, Void, PaymentModel> {
        private final int task;
        private final PaymentModel paymentModel;
        ProgressDialog progressDialog;
        private Context context;
        private String pkgId, pkgAmt;

        public GetPaymentInfo(Context context, int task, String pkgId, String pkgAmt, PaymentModel paymentModel) {
            this.paymentModel = paymentModel;
            this.task = task;
            this.pkgId = pkgId;
            this.pkgAmt = pkgAmt;
            this.context = context;
        }

        public void onDetach() {
            this.context = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(UpgradePackageActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected PaymentModel doInBackground(String... params) {
            String response;
            String userId = AppUser.getUserId(context);
            if (task == TASK_VALIDATE) {
                response = ApiCall.POST(params[0], RequestBuilder.getbayersDetail(userId, pkgId));
                return JsonParser.parseBayerDetail(response);
            } else if (task == TASK_UPGRADE_PACKAGE) {
                response = ApiCall.POST(params[0], RequestBuilder.successfillBuyPackage(userId, paymentModel.getPackageId()
                        , paymentModel.getTxnId(), paymentModel.getId(), paymentModel.getPackageAmt(), paymentModel.getNetAmount(),
                        paymentModel.getCapping(), paymentModel.getRefrenceNo(), paymentModel.getPlacement(), paymentModel.getPurchaseType()));
                return JsonParser.parsePaymentSuccess(response);
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(PaymentModel result) {
            super.onPostExecute(result);
            if (context != null) {
                progressDialog.dismiss();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        switch (task) {
                            case TASK_VALIDATE:
                                PaymentConstant.set(context, pkgAmt, "", "", pkgId, "", result.getCapping());
                                proceedToPay(result.getAmount(), result.getOfferKey(), result.getTxnId(),
                                        result.getProductInfo(), result.getName(), result.getEmail());
//                                        result.getProductInfo(), result.getName(), result.getEmail(), placement, purchaseType, pkgId, refNo, result.getCapping());
                                break;
                            case TASK_UPGRADE_PACKAGE:
                                AppUser.isPayThroughMobile(context, "success");
                                PaymentSuccessDialog payDialog = PaymentSuccessDialog.newInstance("Payment Successful", paymentModel.getProductInfo() + "\nAmount : " + paymentModel.getTransaction_fee(), UpgradePackageActivity.this);
                                payDialog.setCancelable(false);
                                payDialog.show(getSupportFragmentManager(), "PaymentSuccessDialog");
                                break;

                        }
                    } else if (result.getOutput().equals("failure")) {
                        if (task == TASK_UPGRADE_PACKAGE) {
                            showPopupAlert(getSt(R.string.payment_error_title), "Error Message:" + result.getOutputMsg() + "\n" + getString(R.string.payment_error_message)
                                    + "Transaction Id :" + paymentModel.getTxnId() +
                                    "\nPayment Id :" + paymentModel.getId(), PopupAlert.POPUP_DIALOG);
                        } else {
                            showPopupAlert("Alert", result.getOutputMsg()
                                    , PopupAlert.POPUP_DIALOG);
                        }
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),task, paymentModel, UpgradePackageActivity.this)
                                .show(getSupportFragmentManager(), "apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,task, paymentModel, UpgradePackageActivity.this)
                                .show(getSupportFragmentManager(), "apiCallError");
                    }else {
                        if (task == TASK_UPGRADE_PACKAGE) {
                            showPopupAlert(getSt(R.string.payment_error_title), result.getOutputMsg() + "\n" + getString(R.string.payment_error_message)
                                    + "Transaction Id :" + paymentModel.getTxnId() +
                                    "\nPayment Id :" + paymentModel.getId(), PopupAlert.POPUP_DIALOG);
                        } else {
                            showPopupAlert("Alert", result.getOutputMsg()
                                    , PopupAlert.POPUP_DIALOG);
                        }
                    }
                }
            }
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (paymentInfoTask != null) {
            paymentInfoTask.onDetach();
        }
    }

    private void proceedToPay(String totalAmt, String offerKey, String txnId, String productInfo, String name, String emailId) {


        /************************  for Prod code start **********************/
        if (PAYEMNT_MODE == PRODUCTION) {
            merchantKey = "Xjs0hA";//prod
        } else {
            merchantKey = "gtKFFx";//test
        }
        String amount = totalAmt;
        String email = emailId;

        int environment;
        if (PAYEMNT_MODE == PRODUCTION) {
            environment = PayuConstants.PRODUCTION_ENV; //prod
        } else {
            environment = PayuConstants.STAGING_ENV;  //test
        }

        userCredentials = merchantKey + ":" + email;

        //TODO Below are mandatory params for hash genetation
        mPaymentParams = new PaymentParams();
        /**
         * For Test Environment, merchantKey = "gtKFFx"
         * For Production Environment, merchantKey should be your live key or for testing in live you can use "0MQaQP"
         */
        mPaymentParams.setKey(merchantKey);
        mPaymentParams.setAmount(amount);
        mPaymentParams.setProductInfo(productInfo);
        mPaymentParams.setFirstName(name);
        mPaymentParams.setEmail(email);
        /************************  for Prod code end **********************/

        /*
        * Transaction Id should be kept unique for each transaction.
        * */
//        mPaymentParams.setTxnId(txnId + System.currentTimeMillis());
        mPaymentParams.setTxnId(txnId);

        /**
         * Surl --> Success url is where the transaction response is posted by PayU on successful transaction
         * Furl --> Failre url is where the transaction response is posted by PayU on failed transaction
         */
        mPaymentParams.setSurl("https://payu.herokuapp.com/success");
        mPaymentParams.setFurl("https://payu.herokuapp.com/failure");

        /*
         * udf1 to udf5 are options params where you can pass additional information related to transaction.
         * If you don't want to use it, then send them as empty string like, udf1=""
         * */
        mPaymentParams.setUdf1("");
        mPaymentParams.setUdf2("");
        mPaymentParams.setUdf3("");
        mPaymentParams.setUdf4("");
        mPaymentParams.setUdf5("");

        /**
         * These are used for store card feature. If you are not using it then user_credentials = "default"
         * user_credentials takes of the form like user_credentials = "merchant_key : user_id"
         * here merchant_key = your merchant key,
         * user_id = unique id related to user like, email, phone number, etc.
         * */
        mPaymentParams.setUserCredentials(userCredentials);

        //TODO Pass this param only if using offer key
        mPaymentParams.setOfferKey(offerKey);

        //TODO Sets the payment environment in PayuConfig object
        payuConfig = new PayuConfig();
        payuConfig.setEnvironment(environment);

//        //TODO It is recommended to generate hash from server only. Keep your key and salt in server side hash generation code.
//        generateHashFromServer(mPaymentParams);

        /**
         * Below approach for generating hash is not recommended. However, this approach can be used to test in PRODUCTION_ENV
         * if your server side hash generation code is not completely setup. While going live this approach for hash generation
         * should not be used.
         * */
        String salt;
        if (PAYEMNT_MODE == PRODUCTION) {
            salt = "o2BeY1c2";//prod
        }else{
            salt = "eCwWELxi";//test
        }
        generateHashFromSDK(mPaymentParams, salt);

    }

    public void launchSdkUI(PayuHashes payuHashes) {

        Intent intent = new Intent(this, PayUBaseActivity.class);
        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
        intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);
        startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);

    }

    public void generateHashFromSDK(PaymentParams mPaymentParams, String salt) {
        PayuHashes payuHashes = new PayuHashes();
        PostData postData = new PostData();

        // payment Hash;
        checksum = null;
        checksum = new PayUChecksum();
        checksum.setAmount(mPaymentParams.getAmount());
        checksum.setKey(mPaymentParams.getKey());
        checksum.setTxnid(mPaymentParams.getTxnId());
        checksum.setEmail(mPaymentParams.getEmail());
        checksum.setSalt(salt);
        checksum.setProductinfo(mPaymentParams.getProductInfo());
        checksum.setFirstname(mPaymentParams.getFirstName());
        checksum.setUdf1(mPaymentParams.getUdf1());
        checksum.setUdf2(mPaymentParams.getUdf2());
        checksum.setUdf3(mPaymentParams.getUdf3());
        checksum.setUdf4(mPaymentParams.getUdf4());
        checksum.setUdf5(mPaymentParams.getUdf5());

        postData = checksum.getHash();
        if (postData.getCode() == PayuErrors.NO_ERROR) {
            payuHashes.setPaymentHash(postData.getResult());
        }

        // checksum for payemnt related details
        // var1 should be either user credentials or default
        String var1 = mPaymentParams.getUserCredentials() == null ? PayuConstants.DEFAULT : mPaymentParams.getUserCredentials();
        String key = mPaymentParams.getKey();

        if ((postData = calculateHash(key, PayuConstants.PAYMENT_RELATED_DETAILS_FOR_MOBILE_SDK, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) // Assign post data first then check for success
            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(postData.getResult());
        //vas
        if ((postData = calculateHash(key, PayuConstants.VAS_FOR_MOBILE_SDK, PayuConstants.DEFAULT, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
            payuHashes.setVasForMobileSdkHash(postData.getResult());

        // getIbibocodes
        if ((postData = calculateHash(key, PayuConstants.GET_MERCHANT_IBIBO_CODES, PayuConstants.DEFAULT, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
            payuHashes.setMerchantIbiboCodesHash(postData.getResult());

        if (!var1.contentEquals(PayuConstants.DEFAULT)) {
            // get user card
            if ((postData = calculateHash(key, PayuConstants.GET_USER_CARDS, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) // todo rename storedc ard
                payuHashes.setStoredCardsHash(postData.getResult());
            // save user card
            if ((postData = calculateHash(key, PayuConstants.SAVE_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setSaveCardHash(postData.getResult());
            // delete user card
            if ((postData = calculateHash(key, PayuConstants.DELETE_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setDeleteCardHash(postData.getResult());
            // edit user card
            if ((postData = calculateHash(key, PayuConstants.EDIT_USER_CARD, var1, salt)) != null && postData.getCode() == PayuErrors.NO_ERROR)
                payuHashes.setEditCardHash(postData.getResult());
        }

        if (mPaymentParams.getOfferKey() != null) {
            postData = calculateHash(key, PayuConstants.OFFER_KEY, mPaymentParams.getOfferKey(), salt);
            if (postData.getCode() == PayuErrors.NO_ERROR) {
                payuHashes.setCheckOfferStatusHash(postData.getResult());
            }
        }

        if (mPaymentParams.getOfferKey() != null && (postData = calculateHash(key, PayuConstants.CHECK_OFFER_STATUS, mPaymentParams.getOfferKey(), salt)) != null && postData.getCode() == PayuErrors.NO_ERROR) {
            payuHashes.setCheckOfferStatusHash(postData.getResult());
        }

        // we have generated all the hases now lest launch sdk's ui
        launchSdkUI(payuHashes);
    }

    // deprecated, should be used only for testing.
    private PostData calculateHash(String key, String command, String var1, String salt) {
        checksum = null;
        checksum = new PayUChecksum();
        checksum.setKey(key);
        checksum.setCommand(command);
        checksum.setVar1(var1);
        checksum.setSalt(salt);
        return checksum.getHash();
    }


    public void generateHashFromServer(PaymentParams mPaymentParams) {
        //nextButton.setEnabled(false); // lets not allow the user to click the button again and again.

        // lets create the post params
        StringBuffer postParamsBuffer = new StringBuffer();
        postParamsBuffer.append(concatParams(PayuConstants.KEY, mPaymentParams.getKey()));
        postParamsBuffer.append(concatParams(PayuConstants.AMOUNT, mPaymentParams.getAmount()));
        postParamsBuffer.append(concatParams(PayuConstants.TXNID, mPaymentParams.getTxnId()));
        postParamsBuffer.append(concatParams(PayuConstants.EMAIL, null == mPaymentParams.getEmail() ? "" : mPaymentParams.getEmail()));
        postParamsBuffer.append(concatParams(PayuConstants.PRODUCT_INFO, mPaymentParams.getProductInfo()));
        postParamsBuffer.append(concatParams(PayuConstants.FIRST_NAME, null == mPaymentParams.getFirstName() ? "" : mPaymentParams.getFirstName()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF1, mPaymentParams.getUdf1() == null ? "" : mPaymentParams.getUdf1()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF2, mPaymentParams.getUdf2() == null ? "" : mPaymentParams.getUdf2()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF3, mPaymentParams.getUdf3() == null ? "" : mPaymentParams.getUdf3()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF4, mPaymentParams.getUdf4() == null ? "" : mPaymentParams.getUdf4()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF5, mPaymentParams.getUdf5() == null ? "" : mPaymentParams.getUdf5()));
        postParamsBuffer.append(concatParams(PayuConstants.USER_CREDENTIALS, mPaymentParams.getUserCredentials() == null ? PayuConstants.DEFAULT : mPaymentParams.getUserCredentials()));

        // for offer_key
        if (null != mPaymentParams.getOfferKey())
            postParamsBuffer.append(concatParams(PayuConstants.OFFER_KEY, mPaymentParams.getOfferKey()));

        String postParams = postParamsBuffer.charAt(postParamsBuffer.length() - 1) == '&' ? postParamsBuffer.substring(0, postParamsBuffer.length() - 1).toString() : postParamsBuffer.toString();

        // lets make an api call
        GetHashesFromServerTask getHashesFromServerTask = new GetHashesFromServerTask(mPaymentParams);
        getHashesFromServerTask.execute(postParams);
    }


    protected String concatParams(String key, String value) {
        return key + "=" + value + "&";
    }

    /**
     * This AsyncTask generates hash from server.
     */
    private class GetHashesFromServerTask extends AsyncTask<String, String, PayuHashes> {
        private final PaymentParams mParams;
        private ProgressDialog progressDialog;

        public GetHashesFromServerTask(PaymentParams mPaymentParams) {
            this.mParams = mPaymentParams;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(UpgradePackageActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected PayuHashes doInBackground(String... postParams) {
            PayuHashes payuHashes = new PayuHashes();
            try {

                //TODO Below url is just for testing purpose, merchant needs to replace this with their server side hash generation url
                URL url = new URL("https://payu.herokuapp.com/get_hash");


                // get the payuConfig first
                String postParam = postParams[0];

                byte[] postParamsByte = postParam.getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postParamsByte);

                InputStream responseInputStream = conn.getInputStream();
                StringBuffer responseStringBuffer = new StringBuffer();
                byte[] byteContainer = new byte[1024];
                for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                    responseStringBuffer.append(new String(byteContainer, 0, i));
                }

                JSONObject response = new JSONObject(responseStringBuffer.toString());

                Iterator<String> payuHashIterator = response.keys();
                while (payuHashIterator.hasNext()) {
                    String key = payuHashIterator.next();
                    switch (key) {
                        //TODO Below three hashes are mandatory for payment flow and needs to be generated at merchant server
                        /**
                         * Payment hash is one of the mandatory hashes that needs to be generated from merchant's server side
                         * Below is formula for generating payment_hash -
                         *
                         * sha512(key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||SALT)
                         *
                         */
                        case "payment_hash":
                            payuHashes.setPaymentHash(response.getString(key));
                            break;
                        /**
                         * vas_for_mobile_sdk_hash is one of the mandatory hashes that needs to be generated from merchant's server side
                         * Below is formula for generating vas_for_mobile_sdk_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be "default"
                         *
                         */
                        case "vas_for_mobile_sdk_hash":
                            payuHashes.setVasForMobileSdkHash(response.getString(key));
                            break;
                        /**
                         * payment_related_details_for_mobile_sdk_hash is one of the mandatory hashes that needs to be generated from merchant's server side
                         * Below is formula for generating payment_related_details_for_mobile_sdk_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "payment_related_details_for_mobile_sdk_hash":
                            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(response.getString(key));
                            break;

                        //TODO Below hashes only needs to be generated if you are using Store card feature
                        /**
                         * delete_user_card_hash is used while deleting a stored card.
                         * Below is formula for generating delete_user_card_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "delete_user_card_hash":
                            payuHashes.setDeleteCardHash(response.getString(key));
                            break;
                        /**
                         * get_user_cards_hash is used while fetching all the cards corresponding to a user.
                         * Below is formula for generating get_user_cards_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "get_user_cards_hash":
                            payuHashes.setStoredCardsHash(response.getString(key));
                            break;
                        /**
                         * edit_user_card_hash is used while editing details of existing stored card.
                         * Below is formula for generating edit_user_card_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "edit_user_card_hash":
                            payuHashes.setEditCardHash(response.getString(key));
                            break;
                        /**
                         * save_user_card_hash is used while saving card to the vault
                         * Below is formula for generating save_user_card_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "save_user_card_hash":
                            payuHashes.setSaveCardHash(response.getString(key));
                            break;

                        //TODO This hash needs to be generated if you are using any offer key
                        /**
                         * check_offer_status_hash is used while using check_offer_status api
                         * Below is formula for generating check_offer_status_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be Offer Key.
                         *
                         */
                        case "check_offer_status_hash":
                            payuHashes.setCheckOfferStatusHash(response.getString(key));
                            break;
                        default:
                            break;
                    }
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return payuHashes;
        }

        @Override
        protected void onPostExecute(PayuHashes payuHashes) {
            super.onPostExecute(payuHashes);

            progressDialog.dismiss();
            launchSdkUI(payuHashes);
        }
    }


    @Override
    public void onBackPressed() {
        if (0 == 0) {
            Intent backIntent = new Intent();
            backIntent.putExtra(PAYMENT_STATUS, "failure");
            setResult(REQUEST_PAYMENT, backIntent);
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
