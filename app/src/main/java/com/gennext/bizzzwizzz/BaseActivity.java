package com.gennext.bizzzwizzz;

/**
 * Created by Abhijit on 14-Jul-16.
 */


import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.SideMenu;
import com.gennext.bizzzwizzz.model.SideMenuAdapter;
import com.gennext.bizzzwizzz.user.AppFeedback;
import com.gennext.bizzzwizzz.user.customerDetail.BusinessDetail;
import com.gennext.bizzzwizzz.user.customerDetail.CustomerDetail;
import com.gennext.bizzzwizzz.user.mypackage.UpgradePackage;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.PDialog;
import com.gennext.bizzzwizzz.util.RequestBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;


public class BaseActivity extends AppCompatActivity {
    boolean conn = false;
    Builder alertDialog;
    ProgressDialog progressDialog;
    AlertDialog dialog = null;
    ImageView ActionBack;
    TextView ActionBarHeading;

    DrawerLayout dLayout;
    ListView dList;
    List<SideMenu> sideMenuList;
    SideMenuAdapter slideMenuAdapter;

    static int COACH = 1, VENUE = 2;
    protected int FINISH_ACTIVITY = 1, FINISH_FRAGMENT = 2;
    private AlertDialog aDialog;
    public Uri profileImageUri;
    private ImageView ivProfile;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /*protected void setHeading(String title, final Activity act) {

        // getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.action_bar_bg));
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.action_bar);
        TextView appName = (TextView) findViewById(R.id.tv_action_appName);
        ImageView backPressed = (ImageView) findViewById(R.id.iv_action_back);
        backPressed.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                act.finish();
                showToast("Action Back");
            }
        });
        appName.setText(setFont(title));

    }*/

//    public SpannableString setActionBarTitle(String title) {
//        return setBoldFont(R.color.white,Typeface.BOLD,title);
//    }

    public void initToolBar(final Activity activity, String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.mipmap.ic_back);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.finish();
                    }
                }

        );
    }

    public boolean APICheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return true;
        } else {
            return false;
        }
    }

    public Toolbar setToolBar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.mipmap.ic_menu_white);
        return toolbar;
    }

    public void closeFragmentDialog(String dialogName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    public SpannableString setBoldFont(int colorId, int typeFaceStyle, String title) {
        SpannableString s = new SpannableString(title);
        Typeface externalFont = Typeface.createFromAsset(BaseActivity.this.getAssets(), "fonts/segoeui.ttf");
        s.setSpan(externalFont, 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(getResources().getColor(colorId)), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(typeFaceStyle), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return s;
    }


//    public void setActionBarOption(String Title, final int Option) {
//        LinearLayout ActionBack;
//        ActionBack = (LinearLayout) findViewById(R.id.ll_actionbar_back);
//        TextView tvTitle = (TextView) findViewById(R.id.actionbar_title);
////
//        tvTitle.setText(Title);
//        ActionBack.setOnClickListener(new View.OnClickListener() {
//
//
//            @Override
//            public void onClick(View arg0) {
//                //mannager.popBackStack();
//                switch (Option) {
//                    case 1://FINISH_ACTIVITY:
//                        finish();
//                        break;
//                    case 2://FINISH_FRAGMENT
//                        FragmentManager manager = getSupportFragmentManager();
//                        manager.popBackStack();
//                        break;
//                }
//            }
//        });
//
//    }

    public void showPDialog(Context context, String msg) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void hideActionBar() {
        getSupportActionBar().hide();
    }

//    public SpannableString setFont(String title) {
//        SpannableString s = new SpannableString(title);
////		s.setSpan(new TypefaceSpan("CircularStd-Book.otf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, s.length(),
//                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
//        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//        return s;
//    }

    public void hideKeybord(Activity act) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(act.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }


    public SpannableStringBuilder subScr(int text) {

        SpannableStringBuilder cs = new SpannableStringBuilder(String.valueOf(text));
        cs.setSpan(new SubscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        cs.setSpan(new RelativeSizeSpan(0.75f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return cs;
    }


    // ProgressDialog progressDialog; I have declared earlier.
    public void showPDialog(String msg) {
        progressDialog = new ProgressDialog(BaseActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.dialog_animation));
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissPDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }


    public String LoadPref(String key) {
        return LoadPref(BaseActivity.this, key);
    }

    public String LoadPref(Context context, String key) {
        if(context!=null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String data = sharedPreferences.getString(key, "");
            return data;
        }else {
            return "";
        }
    }

    public void SavePref(String key, String value) {
        SavePref(BaseActivity.this, key, value);
    }

    public void SavePref(Context context, String key, String value) {
        if(context!=null && value!=null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.commit();
        }
    }

    public String convert(String res) {
        String UrlEncoding = null;
        try {
            UrlEncoding = URLEncoder.encode(res, "UTF-8");

        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return UrlEncoding;
    }

    public boolean isOnline() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(BaseActivity.this.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        if (haveConnectedWifi == true || haveConnectedMobile == true) {
            conn = true;
        } else {
            conn = false;
        }

        return conn;
    }


    public String getSt(int id) {

        return getResources().getString(id);
    }

    private void EnableMobileIntent() {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.provider.Settings.ACTION_SETTINGS);
        startActivity(intent);

    }

    public void showPopupAlert(String title, String description, int finishType) {
        showPopupAlert(title, description, finishType, 0);
    }

    public void showPopupAlert(String title, String description, int finishType, int popupTimeOutInSec) {
        PopupAlert popupAlert = PopupAlert.newInstance(title, description, finishType, popupTimeOutInSec);
        AppAnimation.setDialogAnimation(getApplicationContext(), popupAlert);
        addFragment(popupAlert, "popupAlert");
    }

    public void showToast(String txt) {
        // Inflate the Layout
        Toast.makeText(BaseActivity.this, txt, Toast.LENGTH_SHORT).show();
//        Toast toast = Toast.makeText(BaseActivity.this, txt, Toast.LENGTH_SHORT);
//        toast.setGravity(Gravity.BOTTOM | Gravity.BOTTOM, 0, 0);
//        toast.show();
    }


    // SpannableStringBuilder cs = new SpannableStringBuilder("X3 + X2");
    // cs.setSpan(new SubscriptSpan(), 1, 3,
    // Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    // cs.setSpan(new RelativeSizeSpan(0.75f), 1, 2,
    // Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    // cs.setSpan(new SubscriptSpan(), 6, 7,
    // Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    // cs.setSpan(new RelativeSizeSpan(0.75f), 6, 7,
    // Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

    protected void updateDrawerProfile(final Activity act) {
        if(profileImageUri!=null && ivProfile!=null){
            Glide.with(act)
                    .load(profileImageUri)
                    .bitmapTransform(new CropCircleTransformation(act))
                    .into(ivProfile);
        }
    }

    protected void SetDrawer(final Activity act, Toolbar toolbar) {
        // TODO Auto-generated method stub

        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean drawerOpen = dLayout.isDrawerOpen(dList);
                        if (!drawerOpen) {
                            dLayout.openDrawer(dList);
                        } else {
                            dLayout.closeDrawer(dList);
                        }
                    }
                }

        );

        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dList = (ListView) findViewById(R.id.left_drawer);
        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.side_menu_header, null, false);
        TextView tvName = (TextView) listHeaderView.findViewById(R.id.tv_slide_menu_header_name);
        ivProfile = (ImageView) listHeaderView.findViewById(R.id.iv_slide_menu_header_profile);
        String name = AppUser.getName(act);
        String image = AppUser.getImage(act);
        tvName.setText(name);


        if (!TextUtils.isEmpty(image)) {
            Glide.with(act)
                    .load(image)
                    .bitmapTransform(new CropCircleTransformation(act))
                    .into(ivProfile);
        } else {
            Glide.with(act)
                    .load(R.drawable.profile)
                    .bitmapTransform(new CropCircleTransformation(act))
                    .into(ivProfile);
        }

        dList.addHeaderView(listHeaderView);

        SideMenu s1 = new SideMenu("My Detail", R.drawable.ic_nav_customer);
        SideMenu s2 = new SideMenu("Update Business Detail", R.drawable.ic_nav_business);
        SideMenu s3;
        if(!AppUser.isPackageAvailable(act)) {
            s3 = new SideMenu("Package", R.drawable.ic_nav_package);
        }else{
            s3 = new SideMenu("Upgrade Package", R.drawable.ic_nav_package);
        }
        SideMenu s4 = new SideMenu("Feedback", R.drawable.nav_feedback);
        SideMenu s5 = new SideMenu("Rate Us", R.drawable.nav_rating);
        SideMenu s6 = new SideMenu("Log out", R.drawable.nav_logout);

        sideMenuList = new ArrayList<>();
        sideMenuList.add(s1);
        final String pkgType=AppUser.getPackageType(act);
        if(pkgType.equalsIgnoreCase("business")) {
            sideMenuList.add(s2);
        }
        sideMenuList.add(s3);
        sideMenuList.add(s4);
        sideMenuList.add(s5);
        sideMenuList.add(s6);

        slideMenuAdapter = new SideMenuAdapter(act, R.layout.side_menu_list_slot, sideMenuList);

        dList.setAdapter(slideMenuAdapter);

        dList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {

                dLayout.closeDrawers();
                Intent intent;
                if(pkgType.equalsIgnoreCase("business")){
                    switch (position) {
                        case 1:
                            CustomerDetail customerDetail = CustomerDetail.newInstance();
                            AppAnimation.setDrawerAnimation(act, customerDetail);
                            addFragment(customerDetail, "customerDetail");
                            break;
                        case 2:
                            BusinessDetail businessDetail = BusinessDetail.newInstance(null,null);
                            AppAnimation.setDrawerAnimation(act, businessDetail);
                            addFragment(businessDetail, "businessDetail");
                            break;
                        case 3:
                            if(!AppUser.isPackageAvailable(act)) {
                                UpgradePackage upgradePackage = UpgradePackage.newInstance();
                                AppAnimation.setDrawerAnimation(act, upgradePackage);
                                addFragmentCommitAllowingStateLoss(upgradePackage, "upgradePackage");
                            }else{
                                startActivity(new Intent(act,UpgradePackageActivity.class));
                            }
                            break;
                        case 4:
                            AppFeedback appFeedback = AppFeedback.newInstance();
                            AppAnimation.setDrawerAnimation(act, appFeedback);
                            addFragment(appFeedback, "appFeedback");
                            break;
                        case 5:
                            try {
                                Uri uri = Uri.parse("market://details?id=" + act.getPackageName() + "");
                                Intent goMarket = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(goMarket);
                            } catch (ActivityNotFoundException e) {
                                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=" + act.getPackageName() + "");
                                Intent goMarket = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(goMarket);
                            }
                            break;
                        case 6:
                            intent = new Intent(act, LoginActivity.class);
                            startActivity(intent);
                            act.finish();
                            break;

                    }
                }else if(!pkgType.equalsIgnoreCase("business")){
                    switch (position) {
                        case 1:
                            CustomerDetail customerDetail = CustomerDetail.newInstance();
                            AppAnimation.setDrawerAnimation(act, customerDetail);
                            addFragment(customerDetail, "customerDetail");
                            break;
                        case 2:
                            if(!AppUser.isPackageAvailable(act)) {
                                UpgradePackage upgradePackage = UpgradePackage.newInstance();
                                AppAnimation.setDrawerAnimation(act, upgradePackage);
                                addFragmentCommitAllowingStateLoss(upgradePackage, "upgradePackage");
                            }else{
                                startActivity(new Intent(act,UpgradePackageActivity.class));
                            }
                            break;
                        case 3:
                            AppFeedback appFeedback = AppFeedback.newInstance();
                            AppAnimation.setDrawerAnimation(act, appFeedback);
                            addFragment(appFeedback, "appFeedback");
                            break;
                        case 4:
                            try {
                                Uri uri = Uri.parse("market://details?id=" + act.getPackageName() + "");
                                Intent goMarket = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(goMarket);
                            } catch (ActivityNotFoundException e) {
                                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=" + act.getPackageName() + "");
                                Intent goMarket = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(goMarket);
                            }
                            break;
                        case 5:
                            intent = new Intent(act, LoginActivity.class);
                            startActivity(intent);
                            act.finish();
                            break;

                    }
                }

            }

        });
    }



    public void showProgressDialog(Context context, String msg) {
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progressDialog.setMessage(msg);
//        progressDialog.setIndeterminate(false);
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        PDialog cDialog = PDialog.newInstance(context, msg);
        aDialog = cDialog.show();
    }

    public void hideProgressDialog() {
//        if (progressDialog != null)
//            progressDialog.dismiss();
        if (aDialog != null)
            aDialog.dismiss();
    }

    protected void replaceFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack("tag");
        transaction.commit();
    }

    protected void setFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void addFragment(Fragment fragment, String tag) {
        addFragment(fragment, android.R.id.content, tag);
    }

     protected void addFragmentCommitAllowingStateLoss(Fragment fragment, String tag) {
         FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
         transaction.add(android.R.id.content, fragment, tag);
         transaction.addToBackStack(tag);
         transaction.commitAllowingStateLoss();
    }

    protected void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void addFragmentWithoutBackstack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.commit();
    }

    // for 4 tabs
    public void setTabFragment(int containerId, Fragment addFragment, String fragTag
            , Fragment rFrag1, Fragment rFrag2, Fragment rFrag3) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // show 1st fragments
        if (addFragment != null) {
            if (addFragment.isAdded()) { // if the fragment is already in container
                ft.show(addFragment);
            } else { // fragment needs to be added to frame container
                ft.add(containerId, addFragment, fragTag);
            }
        }

        // Hide 2st fragments
        if (rFrag1 != null && rFrag1.isAdded()) {
            ft.hide(rFrag1);
        }
        // Hide 3nd fragments
        if (rFrag2 != null && rFrag2.isAdded()) {
            ft.hide(rFrag2);
        }
        // Hide 4nd fragments
        if (rFrag3 != null && rFrag3.isAdded()) {
            ft.hide(rFrag3);
        }
        ft.commit();
    }


    // for 4 tabs
    public void setTabFragment(int containerId, Fragment addFragment, String fragTag
            , Fragment rFrag1, Fragment rFrag2, Fragment rFrag3, Fragment rFrag4) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // show 1st fragments
        if (addFragment != null) {
            if (addFragment.isAdded()) { // if the fragment is already in container
                ft.show(addFragment);
            } else { // fragment needs to be added to frame container
                ft.add(containerId, addFragment, fragTag);
            }
        }

        // Hide 2st fragments
        if (rFrag1 != null && rFrag1.isAdded()) {
            ft.hide(rFrag1);
        }
        // Hide 3nd fragments
        if (rFrag2 != null && rFrag2.isAdded()) {
            ft.hide(rFrag2);
        }
        // Hide 4nd fragments
        if (rFrag3 != null && rFrag3.isAdded()) {
            ft.hide(rFrag3);
        }
        // Hide 5nd fragments
        if (rFrag4 != null && rFrag4.isAdded()) {
            ft.hide(rFrag4);
        }

        ft.commit();
    }


}