package com.gennext.bizzzwizzz.dashboard;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.bizzzwizzz.LoginActivity;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.ConsultantRating;
import com.gennext.bizzzwizzz.model.ConsultancyModel;
import com.gennext.bizzzwizzz.model.DashDirReferralAdapter;
import com.gennext.bizzzwizzz.model.DashPayoutsAdapter;
import com.gennext.bizzzwizzz.model.DashSalesAdapter;
import com.gennext.bizzzwizzz.model.DirectReferralModel;
import com.gennext.bizzzwizzz.model.MyPayoutModel;
import com.gennext.bizzzwizzz.model.MySalesModel;
import com.gennext.bizzzwizzz.user.consultancy.AllSlotBookings;
import com.gennext.bizzzwizzz.user.directRefrel.DirectRefrel;
import com.gennext.bizzzwizzz.user.helpdesk.Helpdesk;
import com.gennext.bizzzwizzz.user.mypackage.UpgradePackage;
import com.gennext.bizzzwizzz.user.payout.MyPayout;
import com.gennext.bizzzwizzz.user.sales.MySales;
import com.gennext.bizzzwizzz.user.team.MyTeam;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.GravitySnapHelper;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.CropCircleTransformation;


/**
 * Created by Abhijit on 10-Dec-16.
 */

public class Dashboard extends CompactFragment implements ApiCallError.ErrorFlagListener,View.OnClickListener {
    private static final int  LOAD_SALES_TASK = 3,LOAD_PAYOUT_TASK=4,DIRECT_REFERRAL_TASK=5,LOAD_CON_FEEDBACK=6;

    private AssignTask assignTask;
    private RecyclerView rvDirRefrell, rvSales,rvPayouts;
    //private TextView tvMyPackageTag;
    private LinearLayout progressDirReferral, progressSales,progressPayouts,
            llReferral,llSales,llPayout;
    private ArrayList<MySalesModel> salesList;
    private DashSalesAdapter salesAdapter;
    private ArrayList<MyPayoutModel> payoutList;
    private DashPayoutsAdapter payoutAdapter;
    private DashDirReferralAdapter dirReferralAdapter;
    private Button btnDirReferral,btnSales,btnPayouts;
    private ArrayList<DirectReferralModel> dirReferralList;
    private ImageView ivProfile;
    private TextView tvProfileName,tvUserId;
    private ArrayList<ConsultancyModel> feedList;

    @Override
    public void onAttach(Context activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }
    public static Dashboard newInstance() {
        Dashboard dashboard=new Dashboard();
        return dashboard;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_dashboard_menu, container, false);
        InitUI(v);
        setProfileDetail(getActivity());
        executeTask(DIRECT_REFERRAL_TASK);
        return v;
    }

    public void updateUi() {
        executeTask(DIRECT_REFERRAL_TASK);
    }


    private void openDialog(ArrayList<ConsultancyModel> feedList) {
        if(feedList!=null && feedList.size()>0) {
            for (ConsultancyModel model : feedList) {
                if(model.getStatus().equalsIgnoreCase("complete"))
                ConsultantRating.newInstance(model.getDomainId(), model.getDomainName())
                        .show(getFragmentManager(), "consultantRating");
            }
        }
    }

    private void setProfileDetail(Context act) {
        String name = AppUser.getName(act);
        String userId = AppUser.getUserId(act);
        String image = AppUser.getImage(act);
        tvProfileName.setText(name);
        tvUserId.setText(userId);

        if(!TextUtils.isEmpty(image)) {
            Glide.with(act)
                    .load(image)
                    .bitmapTransform(new CropCircleTransformation(act))
                    .into(ivProfile);
        }else {
            Glide.with(act)
                    .load(R.drawable.profile)
                    .bitmapTransform(new CropCircleTransformation(act))
                    .into(ivProfile);
        }
    }

    public void setProfileImage(Uri mImageUri) {
        Glide.with(getActivity())
                .load(mImageUri)
                .bitmapTransform(new CropCircleTransformation(getActivity()))
                .into(ivProfile);
    }


    private void InitUI(View v) {
        ivProfile = (ImageView) v.findViewById(R.id.iv_profile);
        tvProfileName = (TextView) v.findViewById(R.id.tv_name);
        tvUserId = (TextView) v.findViewById(R.id.tv_userId);
        progressDirReferral = (LinearLayout) v.findViewById(R.id.progressBarDirectReferral);
        progressSales = (LinearLayout) v.findViewById(R.id.progressBarSales);
        rvDirRefrell = (RecyclerView) v.findViewById(R.id.rv_direct_referral);
        rvSales = (RecyclerView) v.findViewById(R.id.rv_sales);
        rvPayouts = (RecyclerView) v.findViewById(R.id.rv_payouts);
        progressPayouts = (LinearLayout) v.findViewById(R.id.progressBarPayouts);

        btnDirReferral = (Button) v.findViewById(R.id.btn_more_direct_referral);
        btnSales = (Button) v.findViewById(R.id.btn_more_sales);
        btnPayouts = (Button) v.findViewById(R.id.btn_more_payouts);
        LinearLayout llMenu0 = (LinearLayout) v.findViewById(R.id.menu_0);
        LinearLayout llMenu1 = (LinearLayout) v.findViewById(R.id.menu_1);
        LinearLayout llMenu2 = (LinearLayout) v.findViewById(R.id.menu_2);
        LinearLayout llMenu3 = (LinearLayout) v.findViewById(R.id.menu_3);
        LinearLayout llMenu4 = (LinearLayout) v.findViewById(R.id.menu_4);
        LinearLayout llMenu5 = (LinearLayout) v.findViewById(R.id.menu_5);
        LinearLayout llMenu6 = (LinearLayout) v.findViewById(R.id.menu_6);
        llReferral = (LinearLayout) v.findViewById(R.id.ll_dashboard_myreferral);
        llSales = (LinearLayout) v.findViewById(R.id.ll_dashboard_mysales);
        llPayout = (LinearLayout) v.findViewById(R.id.ll_dashboard_mypayout);

        btnDirReferral.setOnClickListener(this);
        btnSales.setOnClickListener(this);
        btnPayouts.setOnClickListener(this);
        llMenu0.setOnClickListener(this);
        llMenu1.setOnClickListener(this);
        llMenu2.setOnClickListener(this);
        llMenu3.setOnClickListener(this);
        llMenu4.setOnClickListener(this);
        llMenu5.setOnClickListener(this);
        llMenu6.setOnClickListener(this);

        if(AppUser.isPackageAvailable(getActivity())){
            llMenu0.setVisibility(View.GONE);
        }else{
            llMenu0.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_more_direct_referral:
                openDirectReferral();
                break;
            case R.id.btn_more_sales:
               openMySales();
                break;
           case R.id.btn_more_payouts:
                openMyPayouts();
                break;
            case R.id.menu_0:
                openPackageOption();
                break;
            case R.id.menu_1:
                openConsultancy();
                break;
            case R.id.menu_2:
                openMyTeam();
                break;
            case R.id.menu_3:
                openMySales();
                break;
            case R.id.menu_4:
                openMyPayouts();
                break;
            case R.id.menu_5:
                openDirectReferral();
                break;
            case R.id.menu_6:
                openHelpDesk();
                break;

        }
    }

    private void openPackageOption() {
        UpgradePackage upgradePackage = UpgradePackage.newInstance();
        AppAnimation.setDialogAnimation(getActivity(),upgradePackage);
        addFragment(upgradePackage,"upgradePackage");
    }

    private void openConsultancy() {
        AllSlotBookings allSlotBookings = AllSlotBookings.newInstance();
        AppAnimation.setDrawerAnimation(getActivity(),allSlotBookings);
        addFragment(allSlotBookings, "allSlotBookings");

//        BookASlot bookASlot = BookASlot.newInstance();
//        AppAnimation.setDialogAnimation(getActivity(),bookASlot);
//        addFragment(bookASlot,"bookASlot");
    }

    private void openHelpDesk() {
        Helpdesk helpdesk = Helpdesk.newInstance();
        AppAnimation.setDialogAnimation(getActivity(),helpdesk);
        addFragment(helpdesk,"helpdesk");
    }

    private void openDirectReferral() {
        DirectRefrel directRefrel = DirectRefrel.newInstance();
        AppAnimation.setDialogAnimation(getActivity(),directRefrel);
        addFragment(directRefrel,"directRefrel");
    }

    private void openMyPayouts() {
        MyPayout myPayout = MyPayout.newInstance();
        AppAnimation.setDialogAnimation(getActivity(),myPayout);
        addFragment(myPayout,"myPayout");
    }

    private void openMySales() {
        MySales mySales = MySales.newInstance();
        AppAnimation.setDialogAnimation(getActivity(),mySales);
        addFragment(mySales,"mySales");
    }

    private void openMyTeam() {
        MyTeam myTeam = MyTeam.newInstance();
        AppAnimation.setDialogAnimation(getActivity(),myTeam);
        addFragment(myTeam,"myTeam");
    }


    private void executeTask(int task) {
        if (task == DIRECT_REFERRAL_TASK) {
            assignTask = new AssignTask(getActivity(), task);
            assignTask.execute(AppSettings.DIRECT_REFERRAL_DETAILS);
        } else if (task == LOAD_SALES_TASK) {
            assignTask = new AssignTask(getActivity(), task);
            assignTask.execute(AppSettings.MY_SALES);
        }else if (task == LOAD_PAYOUT_TASK) {
            assignTask = new AssignTask(getActivity(), task);
            assignTask.execute(AppSettings.MY_PAYOUTS);
        }else if (task == LOAD_CON_FEEDBACK) {
            assignTask = new AssignTask(getActivity(), task);
            assignTask.execute(AppSettings.AVAILABLE_SLOTS);
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, int flag) {
        executeTask(flag);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, int task) {
        hideAllProgress();
        if (task == DIRECT_REFERRAL_TASK) {
            llReferral.setVisibility(View.GONE);
            btnDirReferral.setText("Not found");
            executeTask(LOAD_SALES_TASK);
        } else if (task == LOAD_SALES_TASK) {
            llSales.setVisibility(View.GONE);
            btnSales.setText("Not found");
            executeTask(LOAD_PAYOUT_TASK);
        } else if (task == LOAD_PAYOUT_TASK) {
            llPayout.setVisibility(View.GONE);
            btnPayouts.setText("Not found");
            executeTask(LOAD_CON_FEEDBACK);
        } else if (task == LOAD_CON_FEEDBACK) {
        }
//        else if (task == LOAD_PACKAGE_TASK) {
//            btnPackages.setText("Not found");
//        }
    }

    public void showProgressDialogTree() {
    }

    public void hideProgressDialogTree() {
    }
    public void hideProgressDialogTreeError() {
    }



    private class AssignTask extends AsyncTask<String, Void, String> {
        Context activity;
        int task;
        private String response;

        public void onAttach(Context activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, int task) {
            this.activity = activity;
            this.task = task;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (task == DIRECT_REFERRAL_TASK) {
                progressDirReferral.setVisibility(View.VISIBLE);
            } else if (task == LOAD_SALES_TASK) {
                progressSales.setVisibility(View.VISIBLE);
            }else if (task == LOAD_PAYOUT_TASK) {
                progressPayouts.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected String doInBackground(String... urls) {
            if (task == DIRECT_REFERRAL_TASK) {
                String response;
                String userId= AppUser.getUserId(activity);
                response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
                JsonParser jsonParser = new JsonParser();
                DirectReferralModel sModel = jsonParser.parseDirectReferralDetail(response);
                if (sModel != null) {
                    dirReferralList=new ArrayList<>();
                    dirReferralList.addAll(sModel.getList());
                    return sModel.getOutput();
                } else {
                    return null;
                }
            }else if (task == LOAD_SALES_TASK) {
                String userId = AppUser.getUserId(activity);
                response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
                JsonParser jsonParser = new JsonParser();
                MySalesModel sModel = jsonParser.parseMySalesDetail(response);
                if (sModel != null) {
                    salesList=new ArrayList<>();
                    salesList.addAll(sModel.getList());
                    return sModel.getOutput();
                } else {
                    return null;
                }
            }else if (task == LOAD_PAYOUT_TASK) {
                String response;
                String userId= AppUser.getUserId(activity);
                response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
                JsonParser jsonParser = new JsonParser();
                MyPayoutModel payModel = jsonParser.parseMyPayoutDetail(response);
                if (payModel != null) {
                    payoutList=new ArrayList<>();
                    payoutList.addAll(payModel.getList());
                    return payModel.getOutput();
                } else {
                    return null;
                }
            }else if (task == LOAD_CON_FEEDBACK) {
                String userId = AppUser.getUserId(activity);
                response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
                JsonParser jsonParser = new JsonParser();
                ConsultancyModel sModel = jsonParser.parseAllSlotsBookingDetail(response);
                if (sModel != null) {
                    feedList=sModel.getList();
                    return sModel.getOutput();
                } else {
                    return null;
                }
            }

                return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(activity!=null) {
                if (result != null) {
                    if (result.equals("success")) {
                        if (task == DIRECT_REFERRAL_TASK) {
                            progressDirReferral.setVisibility(View.GONE);
                            setDirReferralInDashboard(dirReferralList);
                            executeTask(LOAD_SALES_TASK);
                        } else if (task == LOAD_SALES_TASK) {
                            progressSales.setVisibility(View.GONE);
                            setMySalesInDashboard(salesList);
                            executeTask(LOAD_PAYOUT_TASK);
                        } else if (task == LOAD_PAYOUT_TASK) {
                            progressPayouts.setVisibility(View.GONE);
                            setMyPayoutsInDashboard(payoutList);
                            executeTask(LOAD_CON_FEEDBACK);
                        } else if (task == LOAD_CON_FEEDBACK) {
                            openDialog(feedList);
                        }
                    } else if (result.equals("internet")) {
                        hideAllProgress();
                        ApiCallError.newInstance(JsonParser.ERRORMESSAGE, task, Dashboard.this)
                                .show(getFragmentManager(), "apiCallError");
                    } else if (result.equals("server")) {
                        hideAllProgress();
                        ApiCallError.newInstance(JsonParser.ERRORMESSAGE,true, task, Dashboard.this)
                                .show(getFragmentManager(), "apiCallError");
                    } else {
                        hideAllProgress();
                        if (task == DIRECT_REFERRAL_TASK) {
                            llReferral.setVisibility(View.GONE);
                            btnDirReferral.setText("Not found");
                            executeTask(LOAD_SALES_TASK);
                        } else if (task == LOAD_SALES_TASK) {
                            llSales.setVisibility(View.GONE);
                            btnSales.setText("Not found");
                            executeTask(LOAD_PAYOUT_TASK);
                        } else if (task == LOAD_PAYOUT_TASK) {
                            llPayout.setVisibility(View.GONE);
                            btnPayouts.setText("Not found");
                            executeTask(LOAD_CON_FEEDBACK);
                        }
                    }
                }else{
                    hideAllProgress();
                }
            }
        }
    }

    private void hideAllProgress() {
        progressDirReferral.setVisibility(View.GONE);
        progressPayouts.setVisibility(View.GONE);
        progressSales.setVisibility(View.GONE);
    }

    private void setDirReferralInDashboard(ArrayList<DirectReferralModel> dirReferralList) {
        /**
         * Start snapping
         */
        if(dirReferralList!=null) {
            SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
            snapHelper.attachToRecyclerView(rvDirRefrell);


            // HORIZONTAL for Gravity START/END and VERTICAL for TOP/BOTTOM
            rvDirRefrell.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            rvDirRefrell.setHasFixedSize(true);

            dirReferralAdapter = new DashDirReferralAdapter(getActivity(), dirReferralList, Dashboard.this, getFragmentManager());
            rvDirRefrell.setAdapter(dirReferralAdapter);
        }
    }
    private void setMySalesInDashboard(ArrayList<MySalesModel> salesList) {
        /**
         * Start snapping
         */
        if(salesList!=null) {
            SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
            snapHelper.attachToRecyclerView(rvSales);

            /**
             * End snapping
             */
            //SnapHelper snapHelper = new GravitySnapHelper(Gravity.END);
            //snapHelper.attachToRecyclerView(recyclerView);

            /**
             * Top snapping
             */
            //SnapHelper snapHelper = new GravitySnapHelper(Gravity.TOP);
            //snapHelper.attachToRecyclerView(recyclerView);

            /**
             * Bottom snapping
             */
//        SnapHelper snapHelper = new GravitySnapHelper(Gravity.BOTTOM);
//        snapHelper.attachToRecyclerView(recyclerView);

            // HORIZONTAL for Gravity START/END and VERTICAL for TOP/BOTTOM
            rvSales.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            rvSales.setHasFixedSize(true);

            salesAdapter = new DashSalesAdapter(getActivity(), salesList, Dashboard.this);
            rvSales.setAdapter(salesAdapter);
        }
    }
    private void setMyPayoutsInDashboard(ArrayList<MyPayoutModel> payoutList) {
        /**
         * Start snapping
         */
        if(payoutList!=null) {
            SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
            snapHelper.attachToRecyclerView(rvPayouts);


            // HORIZONTAL for Gravity START/END and VERTICAL for TOP/BOTTOM
            rvPayouts.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            rvPayouts.setHasFixedSize(true);

            payoutAdapter = new DashPayoutsAdapter(getActivity(), payoutList, Dashboard.this, getFragmentManager());
            rvPayouts.setAdapter(payoutAdapter);
        }
    }

//    private void setMyTeamInDashboard() {
//        DashboardTree dashboardTree = DashboardTree.newInstance(Dashboard.this);
//        addFragmentWithoutBackStack(dashboardTree, R.id.dashboard_tree_view, "dashboardTree");
//
//        myTeamFrame.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                MyTeam myTeam = MyTeam.newInstance();
//                AppAnimation.setDialogAnimation(getActivity(), myTeam);
//                addFragment(myTeam, "myTeam");
//            }
//        });
//    }


}