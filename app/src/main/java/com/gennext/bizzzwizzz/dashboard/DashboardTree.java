package com.gennext.bizzzwizzz.dashboard;


/**
 * Created by Abhijit on 08-Dec-16.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.MyTeamModel;
import com.gennext.bizzzwizzz.model.TreeModel;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;


public class DashboardTree extends CompactFragment implements ApiCallError.ErrorListener {
    private TextView tvParent, tvFatherLeft, tvFatherRight, tvFatherLeftChildLeft, tvFatherLeftChildRight, tvFatherRightChildLeft, tvFatherRightChildRight;
    private LinearLayout parentLayout, fatherLeftLayout, fatherRightLayout, llParent, llFatherLeft, llFatherRight, llFatherLeftChildLeft, llFatherLeftChildRight, llFatherRightChildLeft, llFatherRightChildRight;
    private TreeModel model;

    private AssignTask assignTask;
    private Dashboard dashboard;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static DashboardTree newInstance(Dashboard dashboard) {
        DashboardTree myTeam = new DashboardTree();
        myTeam.dashboard = dashboard;
        return myTeam;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_dashboard_tree, container, false);
        InitUI(v);
        executeTask();
        return v;
    }

    private void InitUI(View v) {
        tvParent = (TextView) v.findViewById(R.id.tv_tree_root);
        tvFatherLeft = (TextView) v.findViewById(R.id.tv_tree_root_left);
        tvFatherRight = (TextView) v.findViewById(R.id.tv_tree_root_right);
        tvFatherLeftChildLeft = (TextView) v.findViewById(R.id.tv_tree_root_left_child_left);
        tvFatherLeftChildRight = (TextView) v.findViewById(R.id.tv_tree_root_left_child_right);
        tvFatherRightChildLeft = (TextView) v.findViewById(R.id.tv_tree_root_right_child_left);
        tvFatherRightChildRight = (TextView) v.findViewById(R.id.tv_tree_root_right_child_right);
        llParent = (LinearLayout) v.findViewById(R.id.ll_root);


        parentLayout = (LinearLayout) v.findViewById(R.id.parent);
        fatherLeftLayout = (LinearLayout) v.findViewById(R.id.childLeft);
        fatherRightLayout = (LinearLayout) v.findViewById(R.id.childRight);
        llFatherLeft = (LinearLayout) v.findViewById(R.id.ll_tree_root_left);
        llFatherRight = (LinearLayout) v.findViewById(R.id.ll_tree_root_right);
        llFatherLeftChildLeft = (LinearLayout) v.findViewById(R.id.ll_tree_root_left_child_left);
        llFatherLeftChildRight = (LinearLayout) v.findViewById(R.id.ll_tree_root_left_child_right);
        llFatherRightChildLeft = (LinearLayout) v.findViewById(R.id.ll_tree_root_right_child_left);
        llFatherRightChildRight = (LinearLayout) v.findViewById(R.id.ll_tree_root_right_child_right);

        model = new TreeModel();


    }


    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GENERATE_TREE_VIEW);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }


    private class AssignTask extends AsyncTask<String, Void, MyTeamModel> {
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dashboard.showProgressDialogTree();
//            showProgressDialog(context, "Processing, please wait");
        }

        @Override
        protected MyTeamModel doInBackground(String... urls) {
            String response;
            response = ApiCall.POST(urls[0], RequestBuilder.Default(AppUser.getUserId(context)));
            JsonParser jsonParser = new JsonParser();
            MyTeamModel tempModel = jsonParser.parseTreeView2(response);
            return tempModel;
        }

        @Override
        protected void onPostExecute(MyTeamModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        populateTree2(result);
                        dashboard.hideProgressDialogTreeError();
                    } else if (result.getOutput().equals("failure")) {
                        dashboard.hideProgressDialogTreeError();
                    } else if (result.getOutput().equals("internet")) {
                        dashboard.hideProgressDialogTreeError();
                        ApiCallError.newInstance(result.getOutputMsg(), DashboardTree.this)
                                .show(getFragmentManager(), "apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        dashboard.hideProgressDialogTreeError();
                        ApiCallError.newInstance(result.getOutputMsg(),true, DashboardTree.this)
                                .show(getFragmentManager(), "apiCallError");
                    }else {
                        dashboard.hideProgressDialogTreeError();
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    }
                }
            }
        }
    }


    private void populateTree2(MyTeamModel result) {
        MyTeamModel parent = result.getParent();
        MyTeamModel fatherLeft = result.getFatherLeft();
        MyTeamModel fatherRight = result.getFatherRight();

        if (parent != null) {
            parent.getName();
            parent.getReferenceNumber();
            tvParent.setText(parent.getUserId() + "\nBV=" + parent.getBv() + "\n" + parent.getActivationDate());
            model.setParent(parent.getUserId());
            if (parent.getPackageType().equalsIgnoreCase("Business")) {
                tvParent.setBackgroundResource(R.color.tree_business);
            } else {
                tvParent.setBackgroundResource(R.color.tree_personal);
            }
            showParent();
        } else {
            hideParent();
        }
        if (fatherLeft != null || fatherRight != null) {
            showBothFather();
            if (fatherLeft != null) {
                tvFatherLeft.setText(fatherLeft.getUserId() + "\nBV=" + fatherLeft.getBv() + "\n" + fatherLeft.getActivationDate());
                model.setFatherLeft(fatherLeft.getUserId());
                if (fatherLeft.getPackageType().equalsIgnoreCase("Business")) {
                    tvFatherLeft.setBackgroundResource(R.color.tree_business);
                } else {
                    tvFatherLeft.setBackgroundResource(R.color.tree_personal);
                }
                showFatherLeft();
                setFatherLeftChildrens(result);
            } else {
                hideFatherLeft();
            }
            if (fatherRight != null) {
                tvFatherRight.setText(fatherRight.getUserId() + "\nBV=" + fatherRight.getBv() + "\n" + fatherRight.getActivationDate());
                model.setFatherRight(fatherRight.getUserId());
                if (fatherRight.getPackageType().equalsIgnoreCase("Business")) {
                    tvFatherRight.setBackgroundResource(R.color.tree_business);
                } else {
                    tvFatherRight.setBackgroundResource(R.color.tree_personal);
                }
                showFatherRight();
                setFatherRightChildrens(result);
            } else {
                hideFatherRight();
            }
        } else {
            hideBothFather();
        }

    }

    private void setFatherLeftChildrens(MyTeamModel result) {
        MyTeamModel fatherLeftChildLeft = result.getFatherLeftChildLeft();
        MyTeamModel fatherLeftChildRight = result.getFatherLeftChildRight();

        if (fatherLeftChildLeft != null || fatherLeftChildRight != null) {
            showFatherLeftBothChildrens();
            if (fatherLeftChildLeft != null) {
                tvFatherLeftChildLeft.setText(fatherLeftChildLeft.getUserId());
                model.setFatherLeftChildLeft(fatherLeftChildLeft.getUserId());
                if (fatherLeftChildLeft.getPackageType().equalsIgnoreCase("Business")) {
                    tvFatherLeftChildLeft.setBackgroundResource(R.color.tree_business);
                } else {
                    tvFatherLeftChildLeft.setBackgroundResource(R.color.tree_personal);
                }
                llFatherLeftChildLeft.setVisibility(View.VISIBLE);
            } else {
                llFatherLeftChildLeft.setVisibility(View.INVISIBLE);
            }
            if (fatherLeftChildRight != null) {
                tvFatherLeftChildRight.setText(fatherLeftChildRight.getUserId());
                model.setFatherLeftChildRight(fatherLeftChildRight.getUserId());
                if (fatherLeftChildRight.getPackageType().equalsIgnoreCase("Business")) {
                    tvFatherLeftChildRight.setBackgroundResource(R.color.tree_business);
                } else {
                    tvFatherLeftChildRight.setBackgroundResource(R.color.tree_personal);
                }
                llFatherLeftChildRight.setVisibility(View.VISIBLE);
            } else {
                llFatherLeftChildRight.setVisibility(View.INVISIBLE);
            }
        }else{
            hideFatherLeftBothChildrens();
        }
    }

    private void setFatherRightChildrens(MyTeamModel result) {
        MyTeamModel fatherRightChildLeft = result.getFatherRightChildLeft();
        MyTeamModel fatherRightChildRight = result.getFatherRightChildRight();

        if (fatherRightChildLeft != null || fatherRightChildRight != null) {
            showFatherRightBothChildrens();
            if (fatherRightChildLeft != null) {
                tvFatherRightChildLeft.setText(fatherRightChildLeft.getUserId());
                model.setFatherLeftChildLeft(fatherRightChildLeft.getUserId());
                if (fatherRightChildLeft.getPackageType().equalsIgnoreCase("Business")) {
                    tvFatherRightChildLeft.setBackgroundResource(R.color.tree_business);
                } else {
                    tvFatherRightChildLeft.setBackgroundResource(R.color.tree_personal);
                }
                llFatherRightChildLeft.setVisibility(View.VISIBLE);
            } else {
                llFatherRightChildLeft.setVisibility(View.INVISIBLE);
            }
            if (fatherRightChildRight != null) {
                tvFatherRightChildRight.setText(fatherRightChildRight.getUserId());
                model.setFatherLeftChildRight(fatherRightChildRight.getUserId());
                if (fatherRightChildRight.getPackageType().equalsIgnoreCase("Business")) {
                    tvFatherRightChildRight.setBackgroundResource(R.color.tree_business);
                } else {
                    tvFatherRightChildRight.setBackgroundResource(R.color.tree_personal);
                }
                llFatherRightChildRight.setVisibility(View.VISIBLE);
            } else {
                llFatherRightChildRight.setVisibility(View.INVISIBLE);
            }
        }else{
            hideFatherRightBothChildrens();
        }
    }
    private void showParent() {
        llParent.setVisibility(View.VISIBLE);
        parentLayout.setVisibility(View.VISIBLE);
    }

    private void hideParent() {
        llParent.setVisibility(View.INVISIBLE);
        parentLayout.setVisibility(View.INVISIBLE);
    }

    private void showFatherLeft() {
        fatherLeftLayout.setVisibility(View.VISIBLE);
        llFatherLeft.setVisibility(View.VISIBLE);
    }

    private void hideFatherLeft() {
        fatherLeftLayout.setVisibility(View.INVISIBLE);
        llFatherLeft.setVisibility(View.INVISIBLE);
    }

    private void showFatherRight() {
        fatherRightLayout.setVisibility(View.VISIBLE);
        llFatherRight.setVisibility(View.VISIBLE);
    }

    private void hideFatherRight() {
        fatherRightLayout.setVisibility(View.INVISIBLE);
        llFatherRight.setVisibility(View.INVISIBLE);
    }

    private void hideBothFather() {
        parentLayout.setVisibility(View.INVISIBLE);
        fatherLeftLayout.setVisibility(View.INVISIBLE);
        fatherRightLayout.setVisibility(View.INVISIBLE);
    }

    private void showBothFather() {
        parentLayout.setVisibility(View.VISIBLE);
        fatherLeftLayout.setVisibility(View.VISIBLE);
        fatherRightLayout.setVisibility(View.VISIBLE);
    }

    private void showFatherLeftBothChildrens() {
        fatherLeftLayout.setVisibility(View.VISIBLE);
    }
    private void hideFatherLeftBothChildrens() {
        fatherLeftLayout.setVisibility(View.INVISIBLE);
    }

    private void showFatherRightBothChildrens() {
        fatherRightLayout.setVisibility(View.VISIBLE);
    }
    private void hideFatherRightBothChildrens() {
        fatherRightLayout.setVisibility(View.INVISIBLE);
    }




//    private void populateTree(MyTeamModel result) {
//        MyTeamModel parent = result.getParent();
//        MyTeamModel fatherLeft = result.getFatherLeft();
//        MyTeamModel fatherRight = result.getFatherRight();
//
//
//
//        if (parent != null) {
//
//            ExpModel.RootDetails rootDetails;
//            ExpModel.EntityDetails entityDetails;
//            rootDetails = root.getRootDetails();
//            entityDetails = root.getEntityDetails();
//
//            if (entityDetails != null) {
//                String entry = rootDetails.getRootEntity();
//                String bv = entityDetails.getBv();
//                String referenceNumber = entityDetails.getReferenceNumber();
//                String activationDate = entityDetails.getActivationDate();
//                String packageType = entityDetails.getPackageType();
//                tvRoot.setText(entry + "\nBV=" + bv + "\n" + activationDate);
//                model.setParent(entry);
//                if(packageType.equalsIgnoreCase("Business")){
//                    tvRoot.setBackgroundColor(getResources().getColor(R.color.tree_business));
//                }else{
//                    tvRoot.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//                }
//            }
//            if (rootLeftNode == null && rootRightNode == null) {
//                parent.setVisibility(View.INVISIBLE);
//                childLeft.setVisibility(View.INVISIBLE);
//                childRight.setVisibility(View.INVISIBLE);
//            }else {
//                parent.setVisibility(View.VISIBLE);
//            }
//            if (rootLeftNode != null) {
//                childLeft.setVisibility(View.VISIBLE);
//                llRootLeft.setVisibility(View.VISIBLE);
//                setLeftChild(rootLeftNode);
//            } else {
//                childLeft.setVisibility(View.INVISIBLE);
//                llRootLeft.setVisibility(View.INVISIBLE);
//            }
//            if (rootRightNode != null) {
//                childRight.setVisibility(View.VISIBLE);
//                llRootRight.setVisibility(View.VISIBLE);
//                setRightChild(rootRightNode);
//            } else {
//                childRight.setVisibility(View.INVISIBLE);
//                llRootRight.setVisibility(View.INVISIBLE);
//            }
//
//            llRoot.setVisibility(View.VISIBLE);
//        } else {
//            llRoot.setVisibility(View.INVISIBLE);
//        }
//    }



//    private void setLeftChild(ExpModel rootLeftNode) {
//        ExpModel.RootDetails rootDetails = rootLeftNode.getRootDetails();
//        ExpModel.EntityDetails entityDetails = rootLeftNode.getEntityDetails();
//        ExpModel.LeftChildDetails leftChildDetails = rootLeftNode.getLeftChildDetails();
//        ExpModel.RightChildDetails rightChildDetails = rootLeftNode.getRightChildDetails();
//
//        String entry = rootDetails.getRootEntity();
//        String left = rootDetails.getLeftOfRoot();
//        String right = rootDetails.getRightOfRoot();
//        if (entityDetails != null) {
//            String bv = entityDetails.getBv();
//            String referenceNumber = entityDetails.getReferenceNumber();
//            String activationDate = entityDetails.getActivationDate();
//            String packageType = entityDetails.getPackageType();
//
//        }
//        if (leftChildDetails == null && rightChildDetails == null) {
//            childLeft.setVisibility(View.INVISIBLE);
//        } else {
//            childLeft.setVisibility(View.VISIBLE);
//        }
//        if (leftChildDetails != null) {
//            setLeftChildLeft(leftChildDetails, left);
//            llRootLeftChildLeft.setVisibility(View.VISIBLE);
//        } else {
//            llRootLeftChildLeft.setVisibility(View.INVISIBLE);
//        }
//        if (rightChildDetails != null) {
//            setLeftChildRight(rightChildDetails, right);
//            llRootLeftChildRight.setVisibility(View.VISIBLE);
//        } else {
//            llRootLeftChildRight.setVisibility(View.INVISIBLE);
//        }
//    }
//
//    private void setLeftChildLeft(ExpModel.LeftChildDetails leftChildDetails, String left) {
//        String bv = leftChildDetails.getBv();
//        String referenceNumber = leftChildDetails.getReferenceNumber();
//        String activationDate = leftChildDetails.getActivationDate();
//        String packageType = leftChildDetails.getPackageType();
//        tvRootLeftChildLeft.setText(left);
//        model.setFatherLeftChildLeft(left);
//        if (packageType.equalsIgnoreCase("Business")) {
//            tvRootLeftChildLeft.setBackgroundColor(getResources().getColor(R.color.tree_business));
//        } else {
//            tvRootLeftChildLeft.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//        }
//    }
//
//    private void setLeftChildRight(ExpModel.RightChildDetails rightChildDetails, String right) {
//        String bv = rightChildDetails.getBv();
//        String referenceNumber = rightChildDetails.getReferenceNumber();
//        String activationDate = rightChildDetails.getActivationDate();
//        String packageType = rightChildDetails.getPackageType();
//        tvRootLeftChildRight.setText(right);
//        model.setFatherLeftChildRight(right);
//        if (packageType.equalsIgnoreCase("Business")) {
//            tvRootLeftChildRight.setBackgroundColor(getResources().getColor(R.color.tree_business));
//        } else {
//            tvRootLeftChildRight.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//        }
//    }
//
//    private void setRightChild(ExpModel rootRightNode) {
//        ExpModel.RootDetails rootDetails = rootRightNode.getRootDetails();
//        ExpModel.EntityDetails entityDetails = rootRightNode.getEntityDetails();
//        ExpModel.LeftChildDetails leftChildDetails = rootRightNode.getLeftChildDetails();
//        ExpModel.RightChildDetails rightChildDetails = rootRightNode.getRightChildDetails();
//
//        String entry = rootDetails.getRootEntity();
//        String left = rootDetails.getLeftOfRoot();
//        String right = rootDetails.getRightOfRoot();
//        if (entityDetails != null) {
//            String bv = entityDetails.getBv();
//            String referenceNumber = entityDetails.getReferenceNumber();
//            String activationDate = entityDetails.getActivationDate();
//            String packageType = entityDetails.getPackageType();
//            tvRootRight.setText(entry + "\nBV=" + bv + "\n" + activationDate);
//            model.setFatherRight(entry);
//            if (packageType.equalsIgnoreCase("Business")) {
//                tvRootRight.setBackgroundColor(getResources().getColor(R.color.tree_business));
//            } else {
//                tvRootRight.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//            }
//        }
//        if (leftChildDetails == null && rightChildDetails == null) {
//            childRight.setVisibility(View.INVISIBLE);
//        } else {
//            childRight.setVisibility(View.VISIBLE);
//        }
//        if (leftChildDetails != null) {
//            setRightChildLeft(leftChildDetails, left);
//            llRootRightChildLeft.setVisibility(View.VISIBLE);
//        } else {
//            llRootRightChildLeft.setVisibility(View.INVISIBLE);
//        }
//        if (rightChildDetails != null) {
//            setRightChildRight(rightChildDetails, right);
//            llRootRightChildRight.setVisibility(View.VISIBLE);
//        } else {
//            llRootRightChildRight.setVisibility(View.INVISIBLE);
//        }
//    }
//
//    private void setRightChildLeft(ExpModel.LeftChildDetails leftChildDetails, String left) {
//        String bv = leftChildDetails.getBv();
//        String referenceNumber = leftChildDetails.getReferenceNumber();
//        String activationDate = leftChildDetails.getActivationDate();
//        String packageType = leftChildDetails.getPackageType();
//        tvRootRightChildLeft.setText(left);
//        model.setFatherRightChildLeft(left);
//        if (packageType.equalsIgnoreCase("Business")) {
//            tvRootRightChildLeft.setBackgroundColor(getResources().getColor(R.color.tree_business));
//        } else {
//            tvRootRightChildLeft.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//        }
//    }
//
//    private void setRightChildRight(ExpModel.RightChildDetails rightChildDetails, String right) {
//        String bv = rightChildDetails.getBv();
//        String referenceNumber = rightChildDetails.getReferenceNumber();
//        String activationDate = rightChildDetails.getActivationDate();
//        String packageType = rightChildDetails.getPackageType();
//        tvRootRightChildRight.setText(right);
//        model.setFatherRightChildRight(right);
//        if (packageType.equalsIgnoreCase("Business")) {
//            tvRootRightChildRight.setBackgroundColor(getResources().getColor(R.color.tree_business));
//        } else {
//            tvRootRightChildRight.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//        }
//    }




}

