package com.gennext.bizzzwizzz.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.BoolRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.MyPackageModel;
import com.gennext.bizzzwizzz.model.PackageDetalAdapter;

import java.util.ArrayList;


public class PackageDetail extends DialogFragment {
    private Dialog dialog;
    private PackageDetailListener mListener;
    private MyPackageModel myPackageModel;
    private ArrayList<MyPackageModel> packageList;
    private PackageDetalAdapter adapter;
    private LinearLayout titleBar;
    private LinearLayout baseBar;
    private Button btnPay;
    private TextView tvTitle,tvServiceTx;
    private ImageView ivRupee;
    private TextView tvPackagePrice;

    public interface PackageDetailListener {
        void onPackageDetailClick(DialogFragment dialog,MyPackageModel myPackageModel);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static PackageDetail newInstance(MyPackageModel myPackageModel, PackageDetailListener listener) {
        PackageDetail fragment = new PackageDetail();
        fragment.myPackageModel = myPackageModel;
        fragment.mListener = listener;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_package, null);
        dialogBuilder.setView(v);
        titleBar = (LinearLayout) v.findViewById(R.id.ll_title_bar);
        baseBar = (LinearLayout) v.findViewById(R.id.ll_package_base);
        btnPay = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        ListView lvMain = (ListView) v.findViewById(R.id.lv_main);
//        RecyclerView lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        tvServiceTx = (TextView) v.findViewById(R.id.tv_service_tx);
        tvPackagePrice = (TextView) v.findViewById(R.id.tv_package_price);
        ivRupee = (ImageView) v.findViewById(R.id.iv_rupee);

        tvTitle.setText(myPackageModel.getPackageName());
        tvPackagePrice.setText(myPackageModel.getPackageAmount());
        btnPay.setText("Pay Rs."+myPackageModel.getPackageAmount());
        setTitleAndBaseColorNew(getActivity(),myPackageModel.getColored());

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onPackageDetailClick(PackageDetail.this,myPackageModel);
                }
            }
        });


        if (myPackageModel.getContentList()!=null) {
            packageList=myPackageModel.getContentList();
            adapter = new PackageDetalAdapter(getActivity(), packageList);
            lvMain.setAdapter(adapter);

//            lvMain.setHasFixedSize(true);
//            lvMain.setItemAnimator(new DefaultItemAnimator());
//
//            LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//            lvMain.setLayoutManager(horizontalManager);
//
//            adapter = new PackageDetalAdapter(getActivity(), packageList);
//            lvMain.setAdapter(adapter);

        }

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }

    private void setTitleAndBaseColorNew(Context context, Boolean isColored) {

        if(isColored){
            titleBar.setBackgroundResource(R.color.package_gold);
            btnPay.setBackgroundResource(R.color.package_gold);
            baseBar.setBackgroundResource(R.color.package_gold_secondary);
            tvTitle.setTextColor(ContextCompat.getColor(context,R.color.black_color));
            btnPay.setTextColor(ContextCompat.getColor(context,R.color.black_color));
            tvServiceTx.setTextColor(ContextCompat.getColor(context,R.color.black_color));
            tvPackagePrice.setTextColor(ContextCompat.getColor(context,R.color.black_color));
            ivRupee.setColorFilter(ContextCompat.getColor(context,R.color.black_color));
        }else{
            titleBar.setBackgroundResource(R.color.colorPrimaryDark);
            btnPay.setBackgroundResource(R.color.colorPrimaryDark);
            baseBar.setBackgroundResource(R.color.colorPrimary);
            tvTitle.setTextColor(ContextCompat.getColor(context,R.color.white));
            btnPay.setTextColor(ContextCompat.getColor(context,R.color.white));
            tvServiceTx.setTextColor(ContextCompat.getColor(context,R.color.white));
            tvPackagePrice.setTextColor(ContextCompat.getColor(context,R.color.white));
            ivRupee.setColorFilter(ContextCompat.getColor(context,R.color.white));
        }
    }

    private void setTitleAndBaseColor(Context context, String pkgName) {
        pkgName=pkgName.toLowerCase();
        switch (pkgName){
            case "bronze":
                titleBar.setBackgroundResource(R.color.package_bronze);
                btnPay.setBackgroundResource(R.color.package_bronze);
                baseBar.setBackgroundResource(R.color.package_bronze_secondary);
                break;
            case "silver":
                titleBar.setBackgroundResource(R.color.package_silver);
                btnPay.setBackgroundResource(R.color.package_silver);
                baseBar.setBackgroundResource(R.color.package_silver_secondary);
                break;
            case "gold":
                titleBar.setBackgroundResource(R.color.package_gold);
                btnPay.setBackgroundResource(R.color.package_gold);
                baseBar.setBackgroundResource(R.color.package_gold_secondary);
                tvTitle.setTextColor(ContextCompat.getColor(context,R.color.black_color));
                btnPay.setTextColor(ContextCompat.getColor(context,R.color.black_color));
                tvServiceTx.setTextColor(ContextCompat.getColor(context,R.color.black_color));
                tvPackagePrice.setTextColor(ContextCompat.getColor(context,R.color.black_color));
                ivRupee.setColorFilter(ContextCompat.getColor(context,R.color.black_color));
                break;
            case "diamond":
                titleBar.setBackgroundResource(R.color.package_diamond);
                btnPay.setBackgroundResource(R.color.package_diamond);
                baseBar.setBackgroundResource(R.color.package_diamond_secondary);
                tvTitle.setTextColor(ContextCompat.getColor(context,R.color.black_color));
                btnPay.setTextColor(ContextCompat.getColor(context,R.color.black_color));
                tvServiceTx.setTextColor(ContextCompat.getColor(context,R.color.black_color));
                tvPackagePrice.setTextColor(ContextCompat.getColor(context,R.color.black_color));
                ivRupee.setColorFilter(ContextCompat.getColor(context,R.color.black_color));
                break;
            default:
                if(pkgName.contains("bronze")){
                    titleBar.setBackgroundResource(R.color.package_bronze);
                    btnPay.setBackgroundResource(R.color.package_bronze);
                    baseBar.setBackgroundResource(R.color.package_bronze_secondary);
                }else if(pkgName.contains("silver")){
                    titleBar.setBackgroundResource(R.color.package_silver);
                    btnPay.setBackgroundResource(R.color.package_silver);
                    baseBar.setBackgroundResource(R.color.package_silver_secondary);
                }else if(pkgName.contains("gold")){
                    titleBar.setBackgroundResource(R.color.package_gold);
                    btnPay.setBackgroundResource(R.color.package_gold);
                    baseBar.setBackgroundResource(R.color.package_gold_secondary);
                    tvTitle.setTextColor(ContextCompat.getColor(context,R.color.text));
                    btnPay.setTextColor(ContextCompat.getColor(context,R.color.text));
                }else if(pkgName.contains("diamond")){
                    titleBar.setBackgroundResource(R.color.package_diamond);
                    btnPay.setBackgroundResource(R.color.package_diamond);
                    baseBar.setBackgroundResource(R.color.package_diamond_secondary);
                    tvTitle.setTextColor(ContextCompat.getColor(context,R.color.text));
                    btnPay.setTextColor(ContextCompat.getColor(context,R.color.text));
                }
                break;
        }
    }
}
