package com.gennext.bizzzwizzz.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;


public class PaymentSuccessDialog extends DialogFragment {
    private TextView tvDescription;
    private Dialog dialog;
    private String mTitle;
    private String mMessage;
    private PayentSuccessListener mListener;

    public interface PayentSuccessListener {
        void onPaymentSuccessClick(DialogFragment dialog);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static PaymentSuccessDialog newInstance(String title, String message, PayentSuccessListener listener) {
        PaymentSuccessDialog fragment = new PaymentSuccessDialog();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.mListener = listener;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_payment_success, null);
        dialogBuilder.setView(v);
        setCancelable(false);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);

        tvTitle.setText(mTitle);
        tvDescription.setText(mMessage);
        button1.setText("Done");

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onPaymentSuccessClick(PaymentSuccessDialog.this);
                }
            }
        });


        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
