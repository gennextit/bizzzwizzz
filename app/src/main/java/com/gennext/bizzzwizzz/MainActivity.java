package com.gennext.bizzzwizzz;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Toast;

import com.gennext.bizzzwizzz.common.RatingPopupAlert;
import com.gennext.bizzzwizzz.dashboard.Dashboard;
import com.gennext.bizzzwizzz.dialog.PaymentSuccessDialog;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.user.MyProfile;
import com.gennext.bizzzwizzz.user.consultancy.BookASlot;
import com.gennext.bizzzwizzz.user.customerDetail.BankDetail;
import com.gennext.bizzzwizzz.user.customerDetail.BusinessDetail;
import com.gennext.bizzzwizzz.user.customerDetail.KYCDetail;
import com.gennext.bizzzwizzz.user.helpdesk.RaiseRequest;
import com.gennext.bizzzwizzz.user.mypackage.UpgradePackage;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.FragmentUtil;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.Rating;
import com.gennext.bizzzwizzz.util.RequestBuilder;


public class MainActivity extends BaseActivity implements ApiCallError.ErrorListener {

    private Dashboard dashboard;
    private Toolbar toolbar;
    private RefreshData refreshTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = setToolBar(getSt(R.string.app_name));
        SetDrawer(MainActivity.this, toolbar);

        initUi();
        setAppRating();

        if (AppUser.getPayThroughMobile(this).equals("success")) {
            executeRefreshTask();
        }
    }


    private void setAppRating() {
        if (Rating.showRating(getApplicationContext())) {
            addFragment(new RatingPopupAlert(), "ratingPopupAlert");
        }
    }

    private void initUi() {
        addDashboard();
    }

    private void addDashboard() {
        dashboard = Dashboard.newInstance();
        addFragmentWithoutBackstack(dashboard, R.id.container_main, "dashboard");
    }

    public void setProfileImageUri(Uri profileImageUri) {
        this.profileImageUri = profileImageUri;
        updateDrawerProfile(MainActivity.this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        dashboard.onDetach();
    }

    @Override
    protected void onResume() {
        super.onResume();
        dashboard.onAttach(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (refreshTask != null) {
            refreshTask.onDetach();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == DocumentMaster.REQ_RAISE_ATTACH) {
            Uri uri = data.getData();
            if (uri != null) {
                RaiseRequest raiseRequest = (RaiseRequest) FragmentUtil.getFragmentByTag(this, "raiseRequest");
                if (raiseRequest != null) {
                    raiseRequest.addThemToView(uri.getPath());
                }
            }
        } else if (resultCode == DocumentMaster.REQ_BOOK_SLOT) {
            Uri uri = data.getData();
            if (uri != null) {
                BookASlot bookASlot = (BookASlot) FragmentUtil.getFragmentByTag(this, "bookASlot");
                if (bookASlot != null) {
                    bookASlot.addThemToView(uri.getPath());
                }
            }
        } else if (resultCode == DocumentMaster.REQ_BANK_ATTACH) {
            Uri uri = data.getData();
            if (uri != null) {
                BankDetail bankDetail = (BankDetail) FragmentUtil.getFragmentByTag(this, "bankDetail");
                if (bankDetail != null) {
                    bankDetail.addThemToView(uri.getPath());
                }
            }
        } else if (resultCode == DocumentMaster.REQ_KYC_PAN) {
            Uri uri = data.getData();
            if (uri != null) {
                KYCDetail kycDetail = (KYCDetail) FragmentUtil.getFragmentByTag(this, "kycDetail");
                if (kycDetail != null) {
                    kycDetail.addPANThemToView(uri.getPath());
                }
            }
        } else if (resultCode == DocumentMaster.REQ_KYC_ID) {
            Uri uri = data.getData();
            if (uri != null) {
                KYCDetail kycDetail1 = (KYCDetail) FragmentUtil.getFragmentByTag(this, "kycDetail");
                if (kycDetail1 != null) {
                    kycDetail1.addIDThemToView(uri.getPath());
                }
            }
        } else if (resultCode == ImageMaster.REQUEST_MYPROFILE) {
            Uri uri = data.getData();
            if (uri != null) {
                MyProfile myProfile = (MyProfile) FragmentUtil.getFragmentByTag(this, "myProfile");
                if (myProfile != null) {
                    myProfile.setImageURI(uri);
                }
            }
        } else if (resultCode == ImageMaster.REQUEST_BUSINESS) {
            Uri uri = data.getData();
            if (uri != null) {
                BusinessDetail businessDetail = (BusinessDetail) FragmentUtil.getFragmentByTag(this, "businessDetail");
                if (businessDetail != null) {
                    businessDetail.setImageURI(uri);
                }
            }
        } else if (resultCode == PackageActivity.REQUEST_PAYMENT) {
            String status = data.getStringExtra(PackageActivity.PAYMENT_STATUS);
            if (status != null) {
                if (status.equals("success")) {
                    updatePaymentRecord();
                } else {
                    Toast.makeText(this, "Payment Failed", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast.makeText(this, "ERROR-103 Invalid Request", Toast.LENGTH_LONG).show();
        }

    }

    private void updatePaymentRecord() {
        Dashboard dashboard = (Dashboard) getSupportFragmentManager().findFragmentByTag("dashboard");
        if (dashboard != null) {
            dashboard.updateUi();
        }
    }

    private void executeRefreshTask() {
        refreshTask = new RefreshData(MainActivity.this);
        refreshTask.execute(AppSettings.LOGIN);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeRefreshTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    private class RefreshData extends AsyncTask<String, Void, Model> {
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public RefreshData(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context, "Reloading, please wait...");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            response = ApiCall.POST(urls[0], RequestBuilder.LoginBody(AppUser.getUserId(context), AppUser.getPassword(context)));
            JsonParser jsonParser = new JsonParser();
            Model model = jsonParser.parseLoginData(context, response);
            AppUser.setAppData(context, response);
            return model;
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        AppUser.isPayThroughMobile(context, "complete");
                    }else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(), MainActivity.this)
                                .show(getSupportFragmentManager(), "apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true, MainActivity.this)
                                .show(getSupportFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }
}
