package com.gennext.bizzzwizzz;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzz.util.L;
import com.gennext.bizzzwizzz.util.filePicker.FileUtils;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;

public class DocumentMaster extends AppCompatActivity {

    private static final int IMAGE_CAMERA = 1,IMAGE_CROPER = 2,IMAGE_PICKER = 3;
    public static final int REQ_RAISE_ATTACH=101,REQ_BANK_ATTACH=102,REQ_KYC_PAN=103,REQ_KYC_ID=104,REQ_BOOK_SLOT=105;
    private ImageView ivPreview;
    private Uri mImageUri;
    private Dialog dialog;
    private TextView tvUri;
    private int fromStatus;
    private PermissionListener permissionlistener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doc_master);
        initUi();
        getFromStatus(getIntent());
        OptionImageAlertWithCheck();
    }

    private void getFromStatus(Intent intent) {
        if(intent!=null);
        int fromStatus=intent.getIntExtra("from",0);
        this.fromStatus=fromStatus;
    }

    private void initUi() {
        ivPreview = (ImageView) findViewById(R.id.iv_preview);
        ImageView ivPlus = (ImageView) findViewById(R.id.iv_plus);
        tvUri = (TextView) findViewById(R.id.tvUri);
        Button btnDone = (Button) findViewById(R.id.btn_done);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mImageUri!=null) {
                    startActivityForResult(mImageUri);
                }else {
                    tvUri.setText("No Image found");
                }
            }
        });
        ivPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OptionImageAlertWithCheck();
            }
        });
        ivPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OptionImageAlertWithCheck();
            }
        });

        permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                OptionImageAlert(DocumentMaster.this);
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getApplicationContext(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }


        };

    }

    private void startActivityForResult(Uri mImageUri) {
        if(fromStatus==REQ_RAISE_ATTACH) {
            Intent backIntent = new Intent();
            backIntent.setData(mImageUri);
            setResult(REQ_RAISE_ATTACH, backIntent);
            finish();
        }else if(fromStatus==REQ_BANK_ATTACH) {
            Intent backIntent = new Intent();
            backIntent.setData(mImageUri);
            setResult(REQ_BANK_ATTACH, backIntent);
            finish();
        }else if(fromStatus==REQ_KYC_PAN) {
            Intent backIntent = new Intent();
            backIntent.setData(mImageUri);
            setResult(REQ_KYC_PAN, backIntent);
            finish();
        }else if(fromStatus==REQ_KYC_ID) {
            Intent backIntent = new Intent();
            backIntent.setData(mImageUri);
            setResult(REQ_KYC_ID, backIntent);
            finish();
        }else if(fromStatus==REQ_BOOK_SLOT) {
            Intent backIntent = new Intent();
            backIntent.setData(mImageUri);
            setResult(REQ_BOOK_SLOT, backIntent);
            finish();
        }else{
            Toast.makeText(this, "Activity not found", Toast.LENGTH_SHORT).show();
        }
    }



    public void OptionImageAlertWithCheck(){
        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setGotoSettingButtonText("setting")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    public void OptionImageAlert(Activity Act) {
        tvUri.setText("");
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);
        LayoutInflater inflater = Act.getLayoutInflater();
        View v = inflater.inflate(R.layout.alert_image_option, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.button1);
        Button button2 = (Button) v.findViewById(R.id.button2);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                mImageUri = FileUtils.getOutputMediaFileUri("DocumentMaster");
                if (mImageUri != null) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                }
                try {
                    intent.putExtra("return-data", true);
                    /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);*/
                    startActivityForResult(intent, IMAGE_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }

            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent target = FileUtils.createGetContentIntent();
                // Create the chooser Intent
                Intent intent = Intent.createChooser(target, "Select Image");
                try {
                    startActivityForResult(intent, IMAGE_PICKER);
                } catch (ActivityNotFoundException e) {
                    // The reason for the existence of aFileChooser
                }

            }
        });
        dialog = dialogBuilder.create();
        dialog.show();

    }




    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                setImageAndStore(result.getUri());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, "Cropping failed: please select a valid Image" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case IMAGE_CAMERA:
                    startCropImageActivity(mImageUri);
                    break;

                case IMAGE_PICKER:
                    if (data != null) {
                        // Get the URI of the selected file
                        final Uri uri = data.getData();
                        try {
                            mImageUri = uri;
                            startCropImageActivity(uri);
                        } catch (Exception e) {
                            L.m("FileSelectorTestActivity" + "File select error" + e.toString());
                        }
                    }
                    break;
            }
        }
    }

    private void setImageAndStore(Uri uri) {
        mImageUri = uri;
        ivPreview.setImageURI(mImageUri);
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setAspectRatio(1,1)
                .setFixAspectRatio(false)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }
}
