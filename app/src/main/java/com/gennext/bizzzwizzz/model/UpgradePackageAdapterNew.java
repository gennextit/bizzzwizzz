package com.gennext.bizzzwizzz.model;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;

import java.util.ArrayList;

public class UpgradePackageAdapterNew extends BaseAdapter {

    private ArrayList<MyPackageModel> list;
    private Context context;

    public UpgradePackageAdapterNew(Context context, ArrayList<MyPackageModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public MyPackageModel getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.slot_upgrade_package, null, false);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        MyPackageModel Appmodel = getItem(position);
//        String pkgName=Appmodel.getPackageName().toLowerCase();

        viewHolder.tvPackage.setText(Appmodel.getPackageName());
        viewHolder.tvPackagePrice.setText(Appmodel.getPackageAmount());
        viewHolder.tvPackagePrice2.setText(Appmodel.getPackageAmount());

        if ((position % 2) == 0) {
            viewHolder.ivPackage.setImageResource(R.drawable.ic_package_gold);
            viewHolder.llPackageTop.setBackgroundResource(R.color.package_gold);
            viewHolder.btnBuy.setBackgroundResource(R.color.package_gold);
            viewHolder.llPackageBase.setBackgroundResource(R.color.package_gold_secondary);
            viewHolder.tvPackage.setTextColor(ContextCompat.getColor(context,R.color.black));
            viewHolder.tvPackagePrice.setTextColor(ContextCompat.getColor(context,R.color.black));
            viewHolder.tvServTex.setTextColor(ContextCompat.getColor(context,R.color.black));
            viewHolder.btnBuy.setTextColor(ContextCompat.getColor(context,R.color.black));
            viewHolder.ivRupee.setColorFilter(ContextCompat.getColor(context,R.color.black_color));
        } else {
            viewHolder.ivPackage.setImageResource(R.drawable.ic_package_silver);
            viewHolder.llPackageTop.setBackgroundResource(R.color.colorPrimaryDark);
            viewHolder.btnBuy.setBackgroundResource(R.color.colorPrimaryDark);
            viewHolder.llPackageBase.setBackgroundResource(R.color.colorPrimary);
            viewHolder.tvPackage.setTextColor(ContextCompat.getColor(context,R.color.white));
            viewHolder.tvPackagePrice.setTextColor(ContextCompat.getColor(context,R.color.white));
            viewHolder.tvServTex.setTextColor(ContextCompat.getColor(context,R.color.white));
            viewHolder.btnBuy.setTextColor(ContextCompat.getColor(context,R.color.white));
            viewHolder.ivRupee.setColorFilter(ContextCompat.getColor(context,R.color.white));
        }

//        if(Appmodel.getColored()){
//
//        }else{
//
//        }

//        switch (pkgName){
//            case "bronze":
//                viewHolder.ivPackage.setImageResource(R.drawable.ic_package_bronze);
//                viewHolder.llPackageTop.setBackgroundResource(R.color.package_bronze);
//                viewHolder.btnBuy.setBackgroundResource(R.color.package_bronze);
//                viewHolder.llPackageBase.setBackgroundResource(R.color.package_bronze_secondary);
//                break;
//            case "silver":
//                viewHolder.ivPackage.setImageResource(R.drawable.ic_package_silver);
//                viewHolder.llPackageTop.setBackgroundResource(R.color.package_silver);
//                viewHolder.btnBuy.setBackgroundResource(R.color.package_silver);
//                viewHolder.llPackageBase.setBackgroundResource(R.color.package_silver_secondary);
//                break;
//            case "gold":
//                viewHolder.ivPackage.setImageResource(R.drawable.ic_package_gold);
//                viewHolder.llPackageTop.setBackgroundResource(R.color.package_gold);
//                viewHolder.btnBuy.setBackgroundResource(R.color.package_gold);
//                viewHolder.llPackageBase.setBackgroundResource(R.color.package_gold_secondary);
//                viewHolder.tvPackage.setTextColor(ContextCompat.getColor(context,R.color.black));
//                viewHolder.tvPackagePrice.setTextColor(ContextCompat.getColor(context,R.color.black));
//                viewHolder.tvServTex.setTextColor(ContextCompat.getColor(context,R.color.black));
//                viewHolder.btnBuy.setTextColor(ContextCompat.getColor(context,R.color.black));
//                viewHolder.ivRupee.setColorFilter(ContextCompat.getColor(context,R.color.black_color));
//                break;
//            case "diamond":
//                viewHolder.ivPackage.setImageResource(R.drawable.ic_package_diamond);
//                viewHolder.llPackageTop.setBackgroundResource(R.color.package_diamond);
//                viewHolder.btnBuy.setBackgroundResource(R.color.package_diamond);
//                viewHolder.llPackageBase.setBackgroundResource(R.color.package_diamond_secondary);
//                viewHolder.tvPackage.setTextColor(ContextCompat.getColor(context,R.color.black));
//                viewHolder.tvPackagePrice.setTextColor(ContextCompat.getColor(context,R.color.black));
//                viewHolder.tvServTex.setTextColor(ContextCompat.getColor(context,R.color.black));
//                viewHolder.btnBuy.setTextColor(ContextCompat.getColor(context,R.color.black));
//                viewHolder.ivRupee.setColorFilter(ContextCompat.getColor(context,R.color.black_color));
//                break;
//            default:
//                if(pkgName.contains("bronze")){
//                    viewHolder.ivPackage.setImageResource(R.drawable.ic_package_bronze);
//                    viewHolder.llPackageTop.setBackgroundResource(R.color.package_bronze);
//                    viewHolder.btnBuy.setBackgroundResource(R.color.package_bronze);
//                    viewHolder.llPackageBase.setBackgroundResource(R.color.package_bronze_secondary);
//                }else if(pkgName.contains("silver")){
//                    viewHolder.ivPackage.setImageResource(R.drawable.ic_package_silver);
//                    viewHolder.llPackageTop.setBackgroundResource(R.color.package_silver);
//                    viewHolder.btnBuy.setBackgroundResource(R.color.package_silver);
//                    viewHolder.llPackageBase.setBackgroundResource(R.color.package_silver_secondary);
//                }else if(pkgName.contains("gold")){
//                    viewHolder.ivPackage.setImageResource(R.drawable.ic_package_gold);
//                    viewHolder.llPackageTop.setBackgroundResource(R.color.package_gold);
//                    viewHolder.btnBuy.setBackgroundResource(R.color.package_gold);
//                    viewHolder.llPackageBase.setBackgroundResource(R.color.package_gold_secondary);
//                }else if(pkgName.contains("diamond")){
//                    viewHolder.ivPackage.setImageResource(R.drawable.ic_package_diamond);
//                    viewHolder.llPackageTop.setBackgroundResource(R.color.package_diamond);
//                    viewHolder.btnBuy.setBackgroundResource(R.color.package_diamond);
//                    viewHolder.llPackageBase.setBackgroundResource(R.color.package_diamond_secondary);
//                }
//                break;
//        }


        return convertView;
    }



    private static class ViewHolder {
        private final LinearLayout llPackageTop,llPackageBase;
        private final ImageView ivPackage,ivRupee;
        private final Button btnBuy;
        private TextView tvPackage,tvPackagePrice,tvPackagePrice2,tvServTex;

        public ViewHolder(View v) {
            ivRupee = (ImageView) v.findViewById(R.id.iv_rupee);
            ivPackage = (ImageView) v.findViewById(R.id.iv_package);
            btnBuy = (Button) v.findViewById(R.id.btn_buy);
            llPackageTop = (LinearLayout) v.findViewById(R.id.ll_package_top);
            llPackageBase = (LinearLayout) v.findViewById(R.id.ll_package_base);
            tvServTex = (TextView) v.findViewById(R.id.tv_package_servicetx);
            tvPackage = (TextView) v.findViewById(R.id.tv_package);
            tvPackagePrice = (TextView) v.findViewById(R.id.tv_package_price);
            tvPackagePrice2 = (TextView) v.findViewById(R.id.tv_package_price2);

        }
    }
}
