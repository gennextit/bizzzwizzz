package com.gennext.bizzzwizzz.model;

/**
 * Created by Abhijit on 23-Dec-16.
 */

public class TreeModel {
    private String parent;
    private String fatherLeft;
    private String fatherRight;
    private String fatherLeftChildLeft;
    private String fatherLeftChildRight;
    private String fatherRightChildLeft;
    private String fatherRightChildRight;

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getFatherLeft() {
        return fatherLeft;
    }

    public void setFatherLeft(String fatherLeft) {
        this.fatherLeft = fatherLeft;
    }

    public String getFatherRight() {
        return fatherRight;
    }

    public void setFatherRight(String fatherRight) {
        this.fatherRight = fatherRight;
    }

    public String getFatherLeftChildLeft() {
        return fatherLeftChildLeft;
    }

    public void setFatherLeftChildLeft(String fatherLeftChildLeft) {
        this.fatherLeftChildLeft = fatherLeftChildLeft;
    }

    public String getFatherLeftChildRight() {
        return fatherLeftChildRight;
    }

    public void setFatherLeftChildRight(String fatherLeftChildRight) {
        this.fatherLeftChildRight = fatherLeftChildRight;
    }

    public String getFatherRightChildLeft() {
        return fatherRightChildLeft;
    }

    public void setFatherRightChildLeft(String fatherRightChildLeft) {
        this.fatherRightChildLeft = fatherRightChildLeft;
    }

    public String getFatherRightChildRight() {
        return fatherRightChildRight;
    }

    public void setFatherRightChildRight(String fatherRightChildRight) {
        this.fatherRightChildRight = fatherRightChildRight;
    }
}
