package com.gennext.bizzzwizzz.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 28-Jan-17.
 */
public class DayAndTimeModel {

    private String dayId;
    private String dayName;
    private String StartTime;
    private String EndTime;
    private String output;
    private Boolean checked;


    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    private ArrayList<DayAndTimeModel>list;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getDayId() {
        return dayId;
    }

    public void setDayId(String dayId) {
        this.dayId = dayId;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public ArrayList<DayAndTimeModel> getList() {
        return list;
    }

    public void setList(ArrayList<DayAndTimeModel> list) {
        this.list = list;
    }
}
