package com.gennext.bizzzwizzz.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 27-Dec-16.
 */

public class StateCityModel {

    private String output;
    private String outputMsg;

    private String stateId;
    private String stateName;
    private String cityId;
    private String CityName;

    private ArrayList<StateCityModel>list;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public ArrayList<StateCityModel> getList() {
        return list;
    }

    public void setList(ArrayList<StateCityModel> list) {
        this.list = list;
    }
}
