package com.gennext.bizzzwizzz.model;


import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TimePicker;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.util.DateTimeUtility;

import java.util.ArrayList;

public class DayAndTimeOfBusinessAdapter extends BaseAdapter {

    private static final int START_TIME=1,END_TIME=2;
    private ArrayList<DayAndTimeModel> list;
    private Activity context;
    private FragmentManager manager;

    public DayAndTimeOfBusinessAdapter(Activity context, ArrayList<DayAndTimeModel> list, FragmentManager manager) {
        this.context = context;
        this.list = list;
        this.manager=manager;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public DayAndTimeModel getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public  ArrayList<DayAndTimeModel> getList() {
        return list;
    }

    public  ArrayList<DayAndTimeModel> getCheckedList() {
        ArrayList<DayAndTimeModel>checkList = new ArrayList<>();
        for(DayAndTimeModel model:list){
            if(model.getChecked()){
                DayAndTimeModel slot=new DayAndTimeModel();
                slot.setDayId(model.getDayId());
                slot.setDayName(model.getDayName());
                slot.setStartTime(model.getStartTime());
                slot.setEndTime(model.getEndTime());
                checkList.add(slot);
            }
        }
        return checkList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.slot_day_time, null, false);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final DayAndTimeModel model=list.get(position);
        viewHolder.tvDayName.setText(model.getDayName());
        viewHolder.tvStartTime.setText(model.getStartTime());
        viewHolder.tvEndTime.setText(model.getEndTime());

        if(model.getChecked()){
            viewHolder.cbCheck.setChecked(true);
            viewHolder.tvStartTime.setBackgroundResource(R.color.white);
            viewHolder.tvEndTime.setBackgroundResource(R.color.white);

        }else {
            viewHolder.cbCheck.setChecked(false);
            viewHolder.tvStartTime.setBackgroundResource(R.color.dialog_secondary);
            viewHolder.tvEndTime.setBackgroundResource(R.color.dialog_secondary);
        }
        viewHolder.cbCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(model.getChecked()){
                    list.get(position).setChecked(false);
                    viewHolder.tvStartTime.setBackgroundResource(R.color.dialog_secondary);
                    viewHolder.tvEndTime.setBackgroundResource(R.color.dialog_secondary);
                }else {
                    list.get(position).setChecked(true);
                    viewHolder.tvStartTime.setBackgroundResource(R.color.white);
                    viewHolder.tvEndTime.setBackgroundResource(R.color.white);
                }
                notifyDataSetChanged();
            }
        });

        viewHolder.tvStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(model.getChecked()) {
                    TimePickerAlert.newInstance(context, START_TIME, position, model, DayAndTimeOfBusinessAdapter.this).
                            show(manager, "datePickerDialog");
                }
            }
        });

        viewHolder.tvEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(model.getChecked()) {
                    TimePickerAlert.newInstance(context, END_TIME, position, model, DayAndTimeOfBusinessAdapter.this).
                            show(manager, "datePickerDialog");
                }
            }
        });

        return convertView;
    }



    private static class ViewHolder {
        private final CheckBox cbCheck;
        private TextView tvDayName,tvStartTime,tvEndTime;

        public ViewHolder(View v) {
            tvDayName = (TextView) v.findViewById(R.id.tv_day);
            tvStartTime = (TextView) v.findViewById(R.id.tv_start_time);
            tvEndTime = (TextView) v.findViewById(R.id.tv_end_time);
            cbCheck = (CheckBox) v.findViewById(R.id.checkBox_header);
        }
    }


    public static class TimePickerAlert extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        private Context context;
        private DayAndTimeModel timeModel;
        private String[] array;
        String time;
        private DayAndTimeOfBusinessAdapter adapter;
        private int position,type;

        public static TimePickerAlert newInstance(Context context, int type, int position, DayAndTimeModel time, DayAndTimeOfBusinessAdapter adapter) {
            TimePickerAlert fragment = new TimePickerAlert();
            fragment.timeModel=time;
            fragment.adapter=adapter;
            fragment.context=context;
            fragment.position=position;
            fragment.type=type;
            return fragment;
        }



        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            TimePickerDialog dpd;
            if(type==START_TIME) {
                time = DateTimeUtility.convertTime12to24Hours(timeModel.getStartTime());
            }else {
                time = DateTimeUtility.convertTime12to24Hours(timeModel.getEndTime());
            }
            array=time.split(":");
            dpd = new TimePickerDialog(context,this, Integer.parseInt(array[0]), Integer.parseInt(array[1]),
                    DateFormat.is24HourFormat(context));
            return dpd;
        }


        @Override
        public void onTimeSet(TimePicker timePicker, int HH, int mm) {
            String sltTime = DateTimeUtility.convertTime24to12Hours(String.valueOf(HH) + ":" + String.valueOf(mm));
            if(type==START_TIME) {
                timeModel.setStartTime(sltTime);
            }else {
                timeModel.setEndTime(sltTime);
            }
            adapter.updateTime(position,timeModel);
        }


    }

    private void updateTime(int position, DayAndTimeModel timeModel) {
        list.set(position,timeModel);
        notifyDataSetChanged();
    }
}
