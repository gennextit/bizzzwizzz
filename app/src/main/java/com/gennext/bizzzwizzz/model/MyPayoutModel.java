package com.gennext.bizzzwizzz.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 08-Dec-16.
 */
public class MyPayoutModel {

    private String payDate;

    private String openingLeft;
    private String openingRight;
    private String leftSale;
    private String rightSale;
    private String totalLeft;
    private String totalRight;
    private String BusinessValueTransfer;
    private String closingLeft;
    private String closingRight;
    private String payout;
    private String AL;
    private String AR;
    private String TDS;
    private String netPay;

    private String criteriaLeft;
    private String criteriaRight;
    private String criteriaBank;
    private String criteriaKyc;

    private String output;
    private String outputMsg;
    private ArrayList<MyPayoutModel> list;


    public String getCriteriaLeft() {
        return criteriaLeft;
    }

    public void setCriteriaLeft(String criteriaLeft) {
        this.criteriaLeft = criteriaLeft;
    }

    public String getCriteriaRight() {
        return criteriaRight;
    }

    public void setCriteriaRight(String criteriaRight) {
        this.criteriaRight = criteriaRight;
    }

    public String getCriteriaBank() {
        return criteriaBank;
    }

    public void setCriteriaBank(String criteriaBank) {
        this.criteriaBank = criteriaBank;
    }

    public String getCriteriaKyc() {
        return criteriaKyc;
    }

    public void setCriteriaKyc(String criteriaKyc) {
        this.criteriaKyc = criteriaKyc;
    }

    public String getAL() {
        return AL;
    }

    public void setAL(String AL) {
        this.AL = AL;
    }

    public String getAR() {
        return AR;
    }

    public void setAR(String AR) {
        this.AR = AR;
    }

    public String getTDS() {
        return TDS;
    }

    public void setTDS(String TDS) {
        this.TDS = TDS;
    }

    public String getNetPay() {
        return netPay;
    }

    public void setNetPay(String netPay) {
        this.netPay = netPay;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getOpeningLeft() {
        return openingLeft;
    }

    public void setOpeningLeft(String openingLeft) {
        this.openingLeft = openingLeft;
    }

    public String getOpeningRight() {
        return openingRight;
    }

    public void setOpeningRight(String openingRight) {
        this.openingRight = openingRight;
    }

    public String getLeftSale() {
        return leftSale;
    }

    public void setLeftSale(String leftSale) {
        this.leftSale = leftSale;
    }

    public String getRightSale() {
        return rightSale;
    }

    public void setRightSale(String rightSale) {
        this.rightSale = rightSale;
    }

    public String getTotalLeft() {
        return totalLeft;
    }

    public void setTotalLeft(String totalLeft) {
        this.totalLeft = totalLeft;
    }

    public String getTotalRight() {
        return totalRight;
    }

    public void setTotalRight(String totalRight) {
        this.totalRight = totalRight;
    }

    public String getBusinessValueTransfer() {
        return BusinessValueTransfer;
    }

    public void setBusinessValueTransfer(String businessValueTransfer) {
        BusinessValueTransfer = businessValueTransfer;
    }

    public String getClosingLeft() {
        return closingLeft;
    }

    public void setClosingLeft(String closingLeft) {
        this.closingLeft = closingLeft;
    }

    public String getClosingRight() {
        return closingRight;
    }

    public void setClosingRight(String closingRight) {
        this.closingRight = closingRight;
    }

    public String getPayout() {
        return payout;
    }

    public void setPayout(String payout) {
        this.payout = payout;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<MyPayoutModel> getList() {
        return list;
    }

    public void setList(ArrayList<MyPayoutModel> list) {
        this.list = list;
    }
}
