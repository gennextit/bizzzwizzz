package com.gennext.bizzzwizzz.model;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.dashboard.Dashboard;
import com.gennext.bizzzwizzz.user.payout.MyPayoutDetail;
import com.gennext.bizzzwizzz.util.DateTimeUtility;

import java.util.ArrayList;

public class DashPayoutsAdapter extends RecyclerView.Adapter<DashPayoutsAdapter.ReyclerViewHolder> {

    private final Dashboard dashboard;
    private final FragmentManager fragmentManager;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<MyPayoutModel> items;

    public DashPayoutsAdapter(Context context, ArrayList<MyPayoutModel> items, Dashboard dashboard, FragmentManager fragmentManager) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.dashboard=dashboard;
        this.fragmentManager=fragmentManager;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_dashboard_payouts, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final MyPayoutModel item = items.get(position);

        holder.tvDate.setText(DateTimeUtility.cDatePayout(item.getPayDate()));
        holder.tvBV.setText(item.getBusinessValueTransfer());
        holder.tvAmt.setText(item.getPayout());
        holder.tvCL.setText(item.getClosingLeft());
        holder.tvCR.setText(item.getClosingRight());
        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyPayoutDetail myPayoutDetail=MyPayoutDetail.newInstance("My Payouts",item.getPayDate(),item);
                dashboard.addFragment(myPayoutDetail,"myPayoutDetail");
//                addSharedFragment(myPayoutDetail,android.R.id.content, holder.tvDate);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout llSlot;
        private TextView tvDate,tvAmt,tvBV,tvCL,tvCR;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvDate = (TextView) v.findViewById(R.id.tv_slot_1);
            tvBV = (TextView) v.findViewById(R.id.tv_slot_2);
            tvAmt = (TextView) v.findViewById(R.id.tv_slot_3);
            tvCL= (TextView) v.findViewById(R.id.tv_slot_4);
            tvCR = (TextView) v.findViewById(R.id.tv_slot_5);
        }
    }


}
