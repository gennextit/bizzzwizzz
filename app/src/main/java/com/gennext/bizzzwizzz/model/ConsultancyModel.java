package com.gennext.bizzzwizzz.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 21-Dec-16.
 */
public class ConsultancyModel {

    private String output;
    private String outputMsg;
    private String availableSlot;

    private String DomainId;
    private String DomainName;
    private String slotTime;
    private String slotDate;
    private String slotDay;
    private String cancel;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private ArrayList<ConsultancyModel>list;
    private ArrayList<ConsultancyModel>timeSlotList;

    public String getAvailableSlot() {
        return availableSlot;
    }

    public void setAvailableSlot(String availableSlot) {
        this.availableSlot = availableSlot;
    }

    public String getSlotDate() {
        return slotDate;
    }

    public void setSlotDate(String slotDate) {
        this.slotDate = slotDate;
    }

    public String getSlotDay() {
        return slotDay;
    }

    public void setSlotDay(String slotDay) {
        this.slotDay = slotDay;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(String slotTime) {
        this.slotTime = slotTime;
    }

    public ArrayList<ConsultancyModel> getTimeSlotList() {
        return timeSlotList;
    }

    public void setTimeSlotList(ArrayList<ConsultancyModel> timeSlotList) {
        this.timeSlotList = timeSlotList;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getDomainId() {
        return DomainId;
    }

    public void setDomainId(String domainId) {
        DomainId = domainId;
    }

    public String getDomainName() {
        return DomainName;
    }

    public void setDomainName(String domainName) {
        DomainName = domainName;
    }

    public ArrayList<ConsultancyModel> getList() {
        return list;
    }

    public void setList(ArrayList<ConsultancyModel> list) {
        this.list = list;
    }
}
