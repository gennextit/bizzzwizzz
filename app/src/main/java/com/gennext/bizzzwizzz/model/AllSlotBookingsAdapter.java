package com.gennext.bizzzwizzz.model;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.databinding.SlotAllConBookingsBinding;
import com.gennext.bizzzwizzz.user.consultancy.AllSlotBookings;
import com.gennext.bizzzwizzz.util.DateTimeUtility;

import java.util.List;


public class AllSlotBookingsAdapter extends RecyclerView.Adapter<AllSlotBookingsAdapter.ViewHolder>  {
    private final AllSlotBookings parentClass;
    private Activity activity;
    private List<ConsultancyModel> list;

    public AllSlotBookingsAdapter(Activity activity, List<ConsultancyModel> list, AllSlotBookings parentClass) {
        this.activity = activity;
        this.parentClass=parentClass;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        SlotAllConBookingsBinding binding = DataBindingUtil.inflate(inflater, R.layout.slot_all_con_bookings, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final ConsultancyModel sample = list.get(holder.getAdapterPosition());
        holder.binding.setModel(sample);
        String date=sample.getSlotDate();
        String time=sample.getSlotTime();
        if(!TextUtils.isEmpty(date)){
            holder.tvDate.setText(DateTimeUtility.convertDateYMD3(date));
        }
        if(!TextUtils.isEmpty(time)){
            holder.tvTime.setText(DateTimeUtility.convertTime24to12Hours(time));
        }
//        if(!TextUtils.isEmpty(pos)){
//            holder.tvPos.setText(pos.substring(0, 1).toUpperCase());
//        }
        holder.binding.layoutSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(holder,sample);
            }
        });
        holder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(holder,sample);
            }
        });
    }

    private void showPopup(ViewHolder holder, final ConsultancyModel sample) {
        final PopupMenu popup = new PopupMenu(activity, holder.btnMore);
        popup.getMenuInflater().inflate(R.menu.context_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int i = item.getItemId();
                if (i == R.id.cancel_menu) {
                    //do something
                    parentClass.cancelBooking(sample);

                    return true;
                }
//                        else if (i == R.id.item2){
//                            //do something
//                            return true;
//                        }
//                        else if (i == R.id.item3) {
//                            //do something
//                            return true;
//                        }
                else {
                    return onMenuItemClick(item);
                }
            }
        });

        popup.show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        final SlotAllConBookingsBinding binding;
        private final ImageButton btnMore;
        private TextView tvDate,tvTime;

        public ViewHolder(View view) {
            super(view);
            binding= DataBindingUtil.bind(view);
            btnMore=(ImageButton)view.findViewById(R.id.btnMore);
            tvDate=(TextView)view.findViewById(R.id.tv_slot_1);
            tvTime=(TextView)view.findViewById(R.id.tv_slot_2);
        }

    }

//    public void setColor(LinearLayout tv) {
//        Random rnd = new Random();
//        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
//        tv.setBackgroundColor(color);
//
//    }
}