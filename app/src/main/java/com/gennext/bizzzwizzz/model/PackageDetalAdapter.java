package com.gennext.bizzzwizzz.model;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;

import java.util.ArrayList;

public class PackageDetalAdapter extends BaseAdapter {

    private ArrayList<MyPackageModel> list;
    private Activity context;

    public PackageDetalAdapter(Activity context, ArrayList<MyPackageModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public MyPackageModel getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public  ArrayList<MyPackageModel> getList() {
        return list;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater viewInflator;
        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.slot_package_detail, null, false);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final MyPackageModel model=list.get(position);
        viewHolder.tvContent.setText(model.getContent());

//        if((position%2)==0){
//            viewHolder.llPackage.setBackgroundResource(R.drawable.bg_package_text2);
//        }else{
//            viewHolder.llPackage.setBackgroundResource(R.drawable.bg_package_text);
//        }

        if(model.getSubContentList()!=null){
            for (MyPackageModel slot:model.getSubContentList()){
                viewInflator = context.getLayoutInflater();
                View custView = viewInflator.inflate(R.layout.package_detail_subcontent, null, false);
                TextView tvSubContent = (TextView) custView.findViewById(R.id.tv_package_subcontent);
                tvSubContent.setText(slot.getSubContent());
                viewHolder.container.addView(custView);
                viewHolder.container.setVisibility(View.VISIBLE);
            }
        }else{
            viewHolder.container.setVisibility(View.GONE);
        }

        return convertView;
    }



    private static class ViewHolder {
        private final LinearLayout llPackage,container;
        private TextView tvContent;

        public ViewHolder(View v) {
            llPackage = (LinearLayout) v.findViewById(R.id.ll_package_content);
            container = (LinearLayout) v.findViewById(R.id.ll_package_container);
            tvContent = (TextView) v.findViewById(R.id.tv_package_content);
        }
    }

}