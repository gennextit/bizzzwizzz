package com.gennext.bizzzwizzz.model;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;

import java.util.ArrayList;

public class ModeOfPayAdapter extends BaseAdapter {

    private ArrayList<BankModel> list;
    private Context context;

    public ModeOfPayAdapter(Context context, ArrayList<BankModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public BankModel getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public  ArrayList<BankModel> getList() {
        return list;
    }

    public  ArrayList<BankModel> getCheckedList() {
        ArrayList<BankModel>checkList = new ArrayList<>();
        for(BankModel model:list){
            if(model.getChecked()){
                BankModel slot=new BankModel();
                slot.setId(model.getId());
                slot.setName(model.getName());
                checkList.add(slot);
            }
        }
        return checkList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.slot_with_checkboxes, null, false);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final BankModel model=list.get(position);
        viewHolder.tvName.setText(model.getName());

        if(model.getChecked()){
            viewHolder.cbCheck.setChecked(true);
        }else {
            viewHolder.cbCheck.setChecked(false);
        }
        viewHolder.cbCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(model.getChecked()){
                    list.get(position).setChecked(false);
                }else {
                    list.get(position).setChecked(true);
                }
                notifyDataSetChanged();
            }
        });

        return convertView;
    }


    private static class ViewHolder {
        private final LinearLayout llSlot;
        private final CheckBox cbCheck;
        private TextView tvName;

        public ViewHolder(View v) {
            tvName = (TextView) v.findViewById(R.id.tv_message);
            cbCheck = (CheckBox) v.findViewById(R.id.checkBox_header);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);

        }
    }
}
