package com.gennext.bizzzwizzz.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;

import java.util.ArrayList;

public class BankAdapter extends BaseAdapter {
    private ArrayList<BankModel> list;
    private Context context;
    private LayoutInflater inflter;

    public BankAdapter(Context applicationContext, ArrayList<BankModel> list) {
        this.context = applicationContext;
        this.list = list;
        inflter = (LayoutInflater.from(applicationContext));
    }

//    @Override
//    public int getCount() {
//        int count = list.size();
//        return count > 0 ? count - 1 : count;
//    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public BankModel getItem(int i) {
        return list.get(i);
    }

    public String getBankId(int i) {
        return list.get(i).getId();
    }

    public String getBankName(int i) {
        return list.get(i).getName();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = inflter.inflate(R.layout.slot_domain, parent, false);
        TextView names = (TextView) view.findViewById(R.id.tv_message);
        BankModel model=getItem(position);
        if (position == 0) {
            names.setText("");
            names.setHint(model.getName()); //"Hint to be displayed"
        } else {
            names.setText(model.getName());

        }
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View view;

        if(position == 0){
            view = inflter.inflate(R.layout.slot_spinner_hint, parent, false); // Hide first row
        } else {
            view = inflter.inflate(R.layout.slot_domain, parent, false);
            BankModel model=getItem(position);
            TextView names = (TextView) view.findViewById(R.id.tv_message);
            names.setText(model.getName());
        }

        return view;
    }
}