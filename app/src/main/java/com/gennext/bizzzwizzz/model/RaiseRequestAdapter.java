package com.gennext.bizzzwizzz.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;

import java.util.ArrayList;

public class RaiseRequestAdapter extends BaseAdapter {
    private ArrayList<HelpdeskModel> list;
    private Context context;
    private LayoutInflater inflter;

    public RaiseRequestAdapter(Context applicationContext, ArrayList<HelpdeskModel> list) {
        this.context = applicationContext;
        this.list = list;
        inflter = (LayoutInflater.from(applicationContext));
    }

//    @Override
//    public int getCount() {
//        int count = list.size();
//        return count > 0 ? count - 1 : count;
//    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public HelpdeskModel getItem(int i) {
        return list.get(i);
    }

    public String getRequestId(int i) {
        return list.get(i).getRequestTypeId();
    }

    public String getRequestName(int i) {
        return list.get(i).getRequestName();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = inflter.inflate(R.layout.slot_domain, parent, false);
        TextView names = (TextView) view.findViewById(R.id.tv_message);

        if (position == 0) {
            names.setText("");
            names.setHint(list.get(position).getRequestName()); //"Hint to be displayed"
        } else {
            names.setText(list.get(position).getRequestName());

        }
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View view;

        if(position == 0){
            view = inflter.inflate(R.layout.slot_spinner_hint, parent, false); // Hide first row
        } else {
            view = inflter.inflate(R.layout.slot_domain, parent, false);
            TextView names = (TextView) view.findViewById(R.id.tv_message);
            names.setText(list.get(position).getRequestName());
        }

        return view;
    }
}