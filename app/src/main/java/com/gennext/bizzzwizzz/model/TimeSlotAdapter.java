package com.gennext.bizzzwizzz.model;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.TimeSlotBinding;
import com.gennext.bizzzwizzz.user.consultancy.ViewTimeSlot;

import java.util.List;


public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.ViewHolder>  {
    private final ViewTimeSlot parentRef;
    private Activity activity;
    private List<ConsultancyModel> list;

    public TimeSlotAdapter(Activity activity, List<ConsultancyModel> list, ViewTimeSlot parentRef) {
        this.activity = activity;
        this.list = list;
        this.parentRef=parentRef;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        TimeSlotBinding binding = DataBindingUtil.inflate(inflater, R.layout.slot_time, parent, false);
        return new ViewHolder(binding.getRoot());
//        View view = inflater.inflate(R.layout.slot_time, parent, false);
//        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

//        if (list.get(position).getSlotTime() != null) {
//            holder.slot1.setText(list.get(position).getSlotTime());
//        }
//
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                DirectReferralIncomeDetail directReferralIncomeDetail=new DirectReferralIncomeDetail();
////                directReferralIncomeDetail.setDetail("Direct Referral Income",list.get(position).getIncomeDate(),list.get(position).getChildList());
////                parentRef.addFragment(directReferralIncomeDetail,"directReferralIncomeDetail");
//            }
//        });
        final ConsultancyModel sample = list.get(viewHolder.getAdapterPosition());
        viewHolder.binding.setConsultancyModel(sample);
        viewHolder.binding.slotLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentRef.setTimeSlot(list.get(position).getSlotTime());
//                 ConsultancyModel data=viewHolder.getAdapterPosition();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
//        private View itemView;
//        private TextView slot1;
        final TimeSlotBinding binding;

        public ViewHolder(View view) {
            super(view);
            binding= DataBindingUtil.bind(view);
//            this.itemView=view;
//            slot1 = (TextView)view.findViewById(R.id.tv_time);
        }

    }
}