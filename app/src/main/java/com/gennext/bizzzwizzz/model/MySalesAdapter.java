package com.gennext.bizzzwizzz.model;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.databinding.SlotSalesBinding;
import com.gennext.bizzzwizzz.user.sales.MySales;
import com.gennext.bizzzwizzz.user.sales.MySalesDetail;

import java.util.List;


public class MySalesAdapter extends RecyclerView.Adapter<MySalesAdapter.ViewHolder>  {
    private final MySales parentClass;
    private Activity activity;
    private List<MySalesModel> list;

    public MySalesAdapter(Activity activity, List<MySalesModel> list, MySales mySales) {
        this.activity = activity;
        this.parentClass=mySales;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        SlotSalesBinding binding = DataBindingUtil.inflate(inflater, R.layout.slot_sales, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final MySalesModel sample = list.get(holder.getAdapterPosition());
        holder.binding.setModel(sample);
        String pos=list.get(position).getPosition();
        if(!TextUtils.isEmpty(pos)){
            holder.tvPos.setText(pos.substring(0, 1).toUpperCase());
        }
        holder.binding.layoutSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MySalesDetail mySalesDetail= MySalesDetail.newInstance(list.get(position).getName(),list.get(position).getDate(),list.get(position));
                parentClass.addFragment(mySalesDetail,"mySalesDetail");
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        final SlotSalesBinding binding;
        private TextView tvPos;

        public ViewHolder(View view) {
            super(view);
            binding= DataBindingUtil.bind(view);
            tvPos=(TextView)view.findViewById(R.id.tv_slot_4);
        }

    }

//    public void setColor(LinearLayout tv) {
//        Random rnd = new Random();
//        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
//        tv.setBackgroundColor(color);
//
//    }
}