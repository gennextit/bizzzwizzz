package com.gennext.bizzzwizzz.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 08-Dec-16.
 */
public class DirectReferralModel {

    private String UserId;
    private String Name;
    private String Status;
    private String Date;
    private String BusinessValue;
    private String PackageName;
    private String Commission;
    private String TDS;
    private String netPay;
    private String Position;


    private String output;
    private String outputMsg;
    private ArrayList<DirectReferralModel> list;


    public String getTDS() {
        return TDS;
    }

    public void setTDS(String TDS) {
        this.TDS = TDS;
    }

    public String getNetPay() {
        return netPay;
    }

    public void setNetPay(String netPay) {
        this.netPay = netPay;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getBusinessValue() {
        return BusinessValue;
    }

    public void setBusinessValue(String businessValue) {
        BusinessValue = businessValue;
    }

    public String getPackageName() {
        return PackageName;
    }

    public void setPackageName(String packageName) {
        PackageName = packageName;
    }

    public String getCommission() {
        return Commission;
    }

    public void setCommission(String commission) {
        Commission = commission;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String position) {
        Position = position;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<DirectReferralModel> getList() {
        return list;
    }

    public void setList(ArrayList<DirectReferralModel> list) {
        this.list = list;
    }
}
