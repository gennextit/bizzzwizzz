package com.gennext.bizzzwizzz.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 10-Dec-16.
 */
public class MySalesModel {

    private String UserId;
    private String Name;
    private String Date;
    private String BusinessValue;
    private String Position;


    private String output;
    private String outputMsg;

    private ArrayList<MySalesModel>list;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getBusinessValue() {
        return BusinessValue;
    }

    public void setBusinessValue(String businessValue) {
        BusinessValue = businessValue;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String position) {
        Position = position;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<MySalesModel> getList() {
        return list;
    }

    public void setList(ArrayList<MySalesModel> list) {
        this.list = list;
    }
}
