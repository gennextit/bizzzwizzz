package com.gennext.bizzzwizzz.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;

import java.util.ArrayList;

public class SpinnerHintAdapter extends BaseAdapter {
    private ArrayList<ConsultancyModel> list;
    private Context context;
    private LayoutInflater inflter;

    public SpinnerHintAdapter(Context applicationContext, ArrayList<ConsultancyModel> list) {
        this.context = applicationContext;
        this.list = list;
        inflter = (LayoutInflater.from(applicationContext));
    }

//    @Override
//    public int getCount() {
//        int count = list.size();
//        return count > 0 ? count - 1 : count;
//    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public ConsultancyModel getItem(int i) {
        return list.get(i);
    }

    public String getDomainId(int i) {
        return list.get(i).getDomainId();
    }

    public String getDomainName(int i) {
        return list.get(i).getDomainName();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = inflter.inflate(R.layout.slot_domain, parent, false);
        TextView names = (TextView) view.findViewById(R.id.tv_message);
        ConsultancyModel model=getItem(position);
        if (position == 0) {
            names.setText("");
            names.setHint(model.getDomainName()); //"Hint to be displayed"
        } else {
            names.setText(model.getDomainName());

        }
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View view;

        if(position == 0){
            view = inflter.inflate(R.layout.slot_spinner_hint, parent, false); // Hide first row
        } else {
            view = inflter.inflate(R.layout.slot_domain, parent, false);
            ConsultancyModel model=getItem(position);
            TextView names = (TextView) view.findViewById(R.id.tv_message);
            names.setText(model.getDomainName());
        }

        return view;
    }
}