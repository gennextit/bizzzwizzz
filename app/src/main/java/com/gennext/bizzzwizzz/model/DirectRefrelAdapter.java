package com.gennext.bizzzwizzz.model;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.databinding.SlotDirectRefrelBinding;
import com.gennext.bizzzwizzz.user.directRefrel.DirectReferralDetail;
import com.gennext.bizzzwizzz.user.directRefrel.DirectRefrel;

import java.util.List;


public class DirectRefrelAdapter extends RecyclerView.Adapter<DirectRefrelAdapter.ViewHolder>  {
    private final DirectRefrel parentClass;
    private Activity activity;
    private List<DirectReferralModel> list;

    public DirectRefrelAdapter(Activity activity, List<DirectReferralModel> list, DirectRefrel mySales) {
        this.activity = activity;
        this.parentClass=mySales;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        SlotDirectRefrelBinding binding = DataBindingUtil.inflate(inflater, R.layout.slot_direct_refrel, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final DirectReferralModel sample = list.get(holder.getAdapterPosition());
        holder.binding.setModel(sample);
        String pos=sample.getPosition();
        if(!TextUtils.isEmpty(pos)){
            holder.tvPos.setText(pos.substring(0, 1).toUpperCase());
        }
        holder.binding.layoutSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DirectReferralDetail directReferralDetail= DirectReferralDetail.newInstance(sample.getName(),sample.getDate(),sample);
                parentClass.addFragment(directReferralDetail,"directReferralDetail");
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        final SlotDirectRefrelBinding binding;
        private TextView tvPos;

        public ViewHolder(View view) {
            super(view);
            binding= DataBindingUtil.bind(view);
            tvPos=(TextView)view.findViewById(R.id.tv_slot_4);
        }

    }

//    public void setColor(LinearLayout tv) {
//        Random rnd = new Random();
//        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
//        tv.setBackgroundColor(color);
//
//    }
}