package com.gennext.bizzzwizzz.model;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.databinding.SlotHelpdesk2Binding;
import com.gennext.bizzzwizzz.user.helpdesk.Helpdesk;
import com.gennext.bizzzwizzz.user.helpdesk.HelpdeskDetail;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.DateTimeUtility;

import java.util.List;


public class HelpdeskAdapter extends RecyclerView.Adapter<HelpdeskAdapter.ViewHolder>  {
    private final Helpdesk parentClass;
    private Activity activity;
    private List<HelpdeskModel> list;

    public HelpdeskAdapter(Activity activity, List<HelpdeskModel> list, Helpdesk helpdesk) {
        this.activity = activity;
        this.parentClass=helpdesk;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        SlotHelpdesk2Binding binding = DataBindingUtil.inflate(inflater, R.layout.slot_helpdesk2, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final HelpdeskModel sample = list.get(holder.getAdapterPosition());
        holder.binding.setModel(sample);
        String status=list.get(position).getStatus();
        setColor(holder.llSlot,holder.tvName,status);
        String date=list.get(position).getRequestDate();

        if(!TextUtils.isEmpty(date)){
            holder.tvDate.setText(DateTimeUtility.convertDateYMD3(date));
        }

        holder.binding.layoutSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HelpdeskDetail helpdeskDetail= HelpdeskDetail.newInstance(list.get(position).getRequestName(),list.get(position));
                AppAnimation.setDialogAnimation(activity,helpdeskDetail);
                parentClass.addFragment(helpdeskDetail,"helpdeskDetail");
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        final SlotHelpdesk2Binding binding;
        private LinearLayout llSlot;
        private TextView tvDate,tvName;

        public ViewHolder(View view) {
            super(view);
            binding= DataBindingUtil.bind(view);
            llSlot=(LinearLayout)view.findViewById(R.id.layoutSlot);
            tvName=(TextView)view.findViewById(R.id.tv_slot_3);
            tvDate=(TextView)view.findViewById(R.id.tv_slot_1);
        }

    }

    public void setColor(LinearLayout tv, TextView tvName, String status) {
        int color;
        switch (status.toLowerCase()){
            case "pending":
                color =activity.getResources().getColor(R.color.status_pending);
                tvName.setTextColor(color);
                tv.setBackgroundColor(color);
                break;
            case "in progress":
                color =activity.getResources().getColor(R.color.status_inprogress);
                tvName.setTextColor(color);
                tv.setBackgroundColor(color);
                break;
            case "complete":
                color =activity.getResources().getColor(R.color.status_complete);
                tvName.setTextColor(color);
                tv.setBackgroundColor(color);
                break;
            case "cancelled":
                color =activity.getResources().getColor(R.color.status_cancelled);
                tvName.setTextColor(color);
                tv.setBackgroundColor(color);
                break;
        }
    }
}