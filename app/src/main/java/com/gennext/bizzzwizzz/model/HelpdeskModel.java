package com.gennext.bizzzwizzz.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 26-Dec-16.
 */
public class HelpdeskModel {

    private String RequestTypeId;
    private String RequestDetail;
    private String RequestFile;
    private String Status;
    private String RequestDate;
    private String RequestName;

    private String output;
    private String outputMsg;

    private ArrayList<HelpdeskModel>list;

    public String getRequestTypeId() {
        return RequestTypeId;
    }

    public void setRequestTypeId(String requestTypeId) {
        RequestTypeId = requestTypeId;
    }

    public String getRequestDetail() {
        return RequestDetail;
    }

    public void setRequestDetail(String requestDetail) {
        RequestDetail = requestDetail;
    }

    public String getRequestFile() {
        return RequestFile;
    }

    public void setRequestFile(String requestFile) {
        RequestFile = requestFile;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getRequestDate() {
        return RequestDate;
    }

    public void setRequestDate(String requestDate) {
        RequestDate = requestDate;
    }

    public String getRequestName() {
        return RequestName;
    }

    public void setRequestName(String requestName) {
        RequestName = requestName;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<HelpdeskModel> getList() {
        return list;
    }

    public void setList(ArrayList<HelpdeskModel> list) {
        this.list = list;
    }
}
