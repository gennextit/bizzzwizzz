package com.gennext.bizzzwizzz.model;

/**
 * Created by Abhijit on 22-Dec-16.
 */
public class ExpModel {

    private String output;
    private String outputMsg;

    private ExpModel rootNode;
    private ExpModel rootLeftNode;
    private ExpModel rootRightNode;

    private RootDetails rootDetails;
    private EntityDetails entityDetails;
    private LeftChildDetails leftChildDetails;
    private RightChildDetails rightChildDetails;

    public void setRootDetails(String entity, String right, String left) {
        RootDetails rootDetails = new RootDetails();
        rootDetails.setRootDetail(entity, right, left);
        this.rootDetails = rootDetails;
    }

    public void setEntityDetails(String bv, String referenceNumber, String name, String activationDate, String packageType) {
        EntityDetails entityDetails = new EntityDetails();
        entityDetails.setEntryDetail(bv, referenceNumber,name, activationDate, packageType);
        this.entityDetails = entityDetails;
    }

    public void setLeftChildDetails(String bv, String referenceNumber, String name, String activationDate, String packageType) {
        LeftChildDetails leftChildDetails = new LeftChildDetails();
        leftChildDetails.setleftChildDetail(bv, referenceNumber,name, activationDate, packageType);
        this.leftChildDetails = leftChildDetails;
    }

    public void setRightChildDetails(String bv, String referenceNumber,String name, String activationDate, String packageType) {
        RightChildDetails rightChildDetails = new RightChildDetails();
        rightChildDetails.setRightChildDetail(bv, referenceNumber,name, activationDate, packageType);
        this.rightChildDetails = rightChildDetails;
    }

    public class RootDetails {
        private String rootEntity;
        private String rightOfRoot;
        private String leftOfRoot;

        public void setRootDetail(String entity, String right, String left) {
            this.rootEntity = entity;
            this.rightOfRoot = right;
            this.leftOfRoot = left;
        }

        public String getRootEntity() {
            return rootEntity;
        }

        public String getRightOfRoot() {
            return rightOfRoot;
        }

        public String getLeftOfRoot() {
            return leftOfRoot;
        }
    }

    public class EntityDetails {
        private String bv;
        private String referenceNumber;
        private String name;
        private String activationDate;
        private String packageType;


        public void setEntryDetail(String bv, String referenceNumber,String name, String activationDate, String packageType) {
            this.bv = bv;
            this.referenceNumber = referenceNumber;
            this.activationDate = activationDate;
            this.packageType = packageType;
            this.name=name;
        }

        public String getBv() {
            return bv;
        }

        public String getReferenceNumber() {
            return referenceNumber;
        }

        public String getName() {
            return name;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public String getPackageType() {
            return packageType;
        }
    }

    public class LeftChildDetails {
        private String bv;
        private String referenceNumber;
        private String activationDate;
        private String packageType;
        private String name;


        public void setleftChildDetail(String bv, String referenceNumber,String name, String activationDate, String packageType) {
            this.bv = bv;
            this.referenceNumber = referenceNumber;
            this.activationDate = activationDate;
            this.name=name;
            this.packageType = packageType;
        }

        public String getBv() {
            return bv;
        }
        public String getName() {
            return name;
        }

        public String getReferenceNumber() {
            return referenceNumber;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public String getPackageType() {
            return packageType;
        }
    }

    public class RightChildDetails {
        private String bv;
        private String referenceNumber;
        private String activationDate;
        private String packageType;
        private String name;

        public void setRightChildDetail(String bv, String referenceNumber,String name, String activationDate, String packageType) {
            this.bv = bv;
            this.referenceNumber = referenceNumber;
            this.activationDate = activationDate;
            this.packageType = packageType;
            this.name=name;
        }

        public String getBv() {
            return bv;
        }
        public String getName() {
            return name;
        }

        public String getReferenceNumber() {
            return referenceNumber;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public String getPackageType() {
            return packageType;
        }
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ExpModel getRootNode() {
        return rootNode;
    }

    public void setRootNode(ExpModel rootNode) {
        this.rootNode = rootNode;
    }

    public ExpModel getRootLeftNode() {
        return rootLeftNode;
    }

    public void setRootLeftNode(ExpModel rootLeftNode) {
        this.rootLeftNode = rootLeftNode;
    }

    public ExpModel getRootRightNode() {
        return rootRightNode;
    }

    public void setRootRightNode(ExpModel rootRightNode) {
        this.rootRightNode = rootRightNode;
    }

    public void setRootDetails(RootDetails rootDetails) {
        this.rootDetails = rootDetails;
    }

    public void setEntityDetails(EntityDetails entityDetails) {
        this.entityDetails = entityDetails;
    }

    public void setLeftChildDetails(LeftChildDetails leftChildDetails) {
        this.leftChildDetails = leftChildDetails;
    }

    public void setRightChildDetails(RightChildDetails rightChildDetails) {
        this.rightChildDetails = rightChildDetails;
    }

    public RootDetails getRootDetails() {
        return rootDetails;
    }

    public EntityDetails getEntityDetails() {
        return entityDetails;
    }

    public LeftChildDetails getLeftChildDetails() {
        return leftChildDetails;
    }

    public RightChildDetails getRightChildDetails() {
        return rightChildDetails;
    }
}
