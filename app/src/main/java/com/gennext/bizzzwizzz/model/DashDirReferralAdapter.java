package com.gennext.bizzzwizzz.model;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.dashboard.Dashboard;
import com.gennext.bizzzwizzz.user.directRefrel.DirectReferralDetail;

import java.util.ArrayList;

public class DashDirReferralAdapter extends RecyclerView.Adapter<DashDirReferralAdapter.ReyclerViewHolder> {

    private final Dashboard dashboard;
    private final FragmentManager fragmentManager;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<DirectReferralModel> items;

    public DashDirReferralAdapter(Context context, ArrayList<DirectReferralModel> items, Dashboard dashboard, FragmentManager fragmentManager) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.dashboard=dashboard;
        this.fragmentManager=fragmentManager;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_dashboard_dirreferral, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final DirectReferralModel item = items.get(position);

        holder.tvName.setText(item.getName());
        holder.tvDate.setText(item.getDate());
        holder.tvUserId.setText(item.getUserId());
        holder.tvCommission.setText(item.getCommission());
        String pos=item.getPosition();
        if(!TextUtils.isEmpty(pos)) {
            holder.tvPlacement.setText(pos.substring(0,1).toUpperCase());
        }
        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DirectReferralDetail directReferralDetail= DirectReferralDetail.newInstance(item.getName(),item.getDate(),item);
                dashboard.addFragment(directReferralDetail,"directReferralDetail");
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout llSlot;
        private TextView tvName,tvDate,tvUserId,tvCommission,tvPlacement;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvName = (TextView) v.findViewById(R.id.tv_slot_1);
            tvDate = (TextView) v.findViewById(R.id.tv_slot_2);
            tvCommission = (TextView) v.findViewById(R.id.tv_slot_3);
            tvUserId= (TextView) v.findViewById(R.id.tv_slot_4);
            tvPlacement = (TextView) v.findViewById(R.id.tv_slot_5);
        }
    }


}
