package com.gennext.bizzzwizzz.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 14-Jan-17.
 */

public class BusinessCategoryModel {

    private String output;
    private String outputMsg;

    private String categoryId;
    private String category;
    private String subCategoryId;
    private String subCategory;
    private Boolean isChecked;

    private ArrayList<BusinessCategoryModel>list;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public ArrayList<BusinessCategoryModel> getList() {
        return list;
    }

    public void setList(ArrayList<BusinessCategoryModel> list) {
        this.list = list;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }
}
