package com.gennext.bizzzwizzz.model;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.PayoutBinding;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.user.payout.MyPayout;
import com.gennext.bizzzwizzz.user.payout.MyPayoutDetail;

import java.util.List;
import java.util.Random;


public class MyPayoutAdapter extends RecyclerView.Adapter<MyPayoutAdapter.ViewHolder> {
    private final MyPayout myPayout;
    private final FragmentManager fragmentManager;
    private Activity activity;
    private List<MyPayoutModel> list;

    public MyPayoutAdapter(Activity activity, List<MyPayoutModel> list, MyPayout myPayout, FragmentManager fragmentManager) {
        this.activity = activity;
        this.fragmentManager=fragmentManager;
        this.list = list;
        this.myPayout=myPayout;
    }




    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        PayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.slot_mypayout, parent, false);
        return new ViewHolder(binding.getRoot());
    }


    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        setColor(viewHolder.slot);
        final MyPayoutModel sample = list.get(viewHolder.getAdapterPosition());
        viewHolder.binding.setModel(sample);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            viewHolder.tvDate.setTransitionName("date"+position);
        }
        viewHolder.binding.layoutSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyPayoutDetail myPayoutDetail=MyPayoutDetail.newInstance("My Payouts",list.get(position).getPayDate(),list.get(position));
                myPayout.addFragment(myPayoutDetail,"myPayoutDetail");
//                addSharedFragment(myPayoutDetail,android.R.id.content, viewHolder.tvDate);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDate;
        LinearLayout slot;
        final PayoutBinding binding;

        public ViewHolder(View view) {
            super(view);
            binding= DataBindingUtil.bind(view);
            slot = (LinearLayout) view.findViewById(R.id.ll_slot);
            tvDate = (TextView) view.findViewById(R.id.tv_slot_1);

        }
    }

    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        tv.setBackgroundColor(color);

    }

    private void addSharedFragment(MyPayoutDetail myPayoutDetail,int tree_container, TextView tvText) {
        String textTransitionName = "";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            myPayout.setSharedElementReturnTransition(TransitionInflater.from(
                    activity).inflateTransition(R.transition.change_image_trans));
            myPayout.setExitTransition(TransitionInflater.from(
                    activity).inflateTransition(R.transition.change_image_trans));

            myPayoutDetail.setSharedElementEnterTransition(TransitionInflater.from(
                    activity).inflateTransition(R.transition.change_image_trans));
            myPayoutDetail.setExitTransition(TransitionInflater.from(
                    activity).inflateTransition(R.transition.change_image_trans));
            myPayoutDetail.setEnterTransition(TransitionInflater.from(
                    activity).inflateTransition(android.R.transition.fade));


            textTransitionName = tvText.getTransitionName();
            Bundle bundle = new Bundle();
            bundle.putString("ACTION", tvText.getText().toString());
            bundle.putString("TRANS_TEXT", textTransitionName);
            myPayoutDetail.setArguments(bundle);
            fragmentManager.beginTransaction()
                    .replace(tree_container, myPayoutDetail)
                    .addToBackStack("treeView")
                    .addSharedElement(tvText, textTransitionName)
                    .commit();
        }


    }
}