package com.gennext.bizzzwizzz.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.dashboard.Dashboard;
import com.gennext.bizzzwizzz.user.sales.MySalesDetail;
import com.gennext.bizzzwizzz.util.DateTimeUtility;

import java.util.ArrayList;

public class DashSalesAdapter extends RecyclerView.Adapter<DashSalesAdapter.ReyclerViewHolder> {

    private final Dashboard parantClass;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<MySalesModel> items;

    public DashSalesAdapter(Context context, ArrayList<MySalesModel> items, Dashboard dashboard) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parantClass=dashboard;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_dashboard_sales, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final MySalesModel item = items.get(position);

        holder.tvName.setText(item.getName());
        holder.tvDate.setText(DateTimeUtility.cDatePayout(item.getDate()));
        holder.tvUserId.setText(item.getUserId());
        holder.tvBV.setText(item.getBusinessValue());
        String pos=item.getPosition();
        if(!TextUtils.isEmpty(pos)) {
            holder.tvPos.setText(pos.substring(0,1).toUpperCase());
        }
        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MySalesDetail mySalesDetail= MySalesDetail.newInstance(item.getName(),item.getDate(),item);
                parantClass.addFragment(mySalesDetail,"mySalesDetail");
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout llSlot;
        private TextView tvName,tvDate,tvUserId,tvBV,tvPos;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvName = (TextView) v.findViewById(R.id.tv_slot_1);
            tvDate = (TextView) v.findViewById(R.id.tv_slot_2);
            tvBV = (TextView) v.findViewById(R.id.tv_slot_3);
            tvUserId = (TextView) v.findViewById(R.id.tv_slot_4);
            tvPos = (TextView) v.findViewById(R.id.tv_slot_5);
        }
    }
}
