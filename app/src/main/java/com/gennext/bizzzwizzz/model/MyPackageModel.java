package com.gennext.bizzzwizzz.model;

import java.util.ArrayList;

/**
 * Created by Abhijit on 08-Dec-16.
 */
public class MyPackageModel {

    //    private String pName;
//    private String pAmount;
    private String Validity;
//    private String pCapping;

    private String PackageId;
    private String PackageType;
    private String PackageName;
    private String PackageAmount;
    private String Capping;
    private String Text;
    private String content;
    private String subContent;
    private Boolean isColored;

    private String output;
    private String outputMsg;

    private ArrayList<MyPackageModel> newList;
    private ArrayList<MyPackageModel> oldList;
    private ArrayList<MyPackageModel> packageList;
    private ArrayList<MyPackageModel> contentList;
    private ArrayList<MyPackageModel> subContentList;

    public String getPackageId() {
        return PackageId;
    }

    public void setPackageId(String packageId) {
        PackageId = packageId;
    }

    public Boolean getColored() {
        return isColored;
    }

    public void setColored(Boolean colored) {
        isColored = colored;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubContent() {
        return subContent;
    }

    public void setSubContent(String subContent) {
        this.subContent = subContent;
    }

    public ArrayList<MyPackageModel> getContentList() {
        return contentList;
    }

    public void setContentList(ArrayList<MyPackageModel> contentList) {
        this.contentList = contentList;
    }

    public ArrayList<MyPackageModel> getSubContentList() {
        return subContentList;
    }

    public void setSubContentList(ArrayList<MyPackageModel> subContentList) {
        this.subContentList = subContentList;
    }

    public String getPackageType() {
        return PackageType;
    }

    public void setPackageType(String packageType) {
        PackageType = packageType;
    }

    public String getPackageName() {
        return PackageName;
    }

    public void setPackageName(String packageName) {
        PackageName = packageName;
    }

    public String getPackageAmount() {
        return PackageAmount;
    }

    public void setPackageAmount(String packageAmount) {
        PackageAmount = packageAmount;
    }

    public String getCapping() {
        return Capping;
    }

    public void setCapping(String capping) {
        Capping = capping;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public ArrayList<MyPackageModel> getPackageList() {
        return packageList;
    }

    public void setPackageList(ArrayList<MyPackageModel> packageList) {
        this.packageList = packageList;
    }

    public String getValidity() {
        return Validity;
    }

    public void setValidity(String validity) {
        Validity = validity;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<MyPackageModel> getNewList() {
        return newList;
    }

    public void setNewList(ArrayList<MyPackageModel> newList) {
        this.newList = newList;
    }

    public ArrayList<MyPackageModel> getOldList() {
        return oldList;
    }

    public void setOldList(ArrayList<MyPackageModel> oldList) {
        this.oldList = oldList;
    }
}
