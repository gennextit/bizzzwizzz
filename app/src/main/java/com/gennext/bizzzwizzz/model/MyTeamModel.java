package com.gennext.bizzzwizzz.model;

/**
 * Created by Abhijit on 22-Dec-16.
 */

public class MyTeamModel {

    private String userId;
    private String bv;
    private String referenceNumber;
    private String activationDate;
    private String packageType;
    private String name;

    private String output;
    private String outputMsg;

    private MyTeamModel parent;
    private MyTeamModel fatherLeft;
    private MyTeamModel fatherRight;
    private MyTeamModel fatherLeftChildLeft;
    private MyTeamModel fatherLeftChildRight;
    private MyTeamModel fatherRightChildLeft;
    private MyTeamModel fatherRightChildRight;

    public void setParent(String userId,String bv, String referenceNumber,String name, String activationDate, String packageType) {
        MyTeamModel model=new MyTeamModel();
        model.setUserId(userId);
        model.setBv(bv);
        model.setReferenceNumber(referenceNumber);
        model.setActivationDate(activationDate);
        model.setName(name);
        model.setPackageType(packageType);
        this.parent=model;
    }
    public void setFatherLeft(String userId,String bv, String referenceNumber,String name, String activationDate, String packageType) {
        MyTeamModel model=new MyTeamModel();
        model.setUserId(userId);
        model.setBv(bv);
        model.setReferenceNumber(referenceNumber);
        model.setActivationDate(activationDate);
        model.setName(name);
        model.setPackageType(packageType);
        this.fatherLeft=model;
    }
    public void setFatherRight(String userId,String bv, String referenceNumber,String name, String activationDate, String packageType) {
        MyTeamModel model=new MyTeamModel();
        model.setUserId(userId);
        model.setBv(bv);
        model.setReferenceNumber(referenceNumber);
        model.setActivationDate(activationDate);
        model.setName(name);
        model.setPackageType(packageType);
        this.fatherRight=model;
    }
    public void setFatherLeftChildLeft(String userId,String bv, String referenceNumber,String name, String activationDate, String packageType) {
        MyTeamModel model=new MyTeamModel();
        model.setUserId(userId);
        model.setBv(bv);
        model.setReferenceNumber(referenceNumber);
        model.setActivationDate(activationDate);
        model.setName(name);
        model.setPackageType(packageType);
        this.fatherLeftChildLeft=model;
    }
    public void setFatherLeftChildRight(String userId,String bv, String referenceNumber,String name, String activationDate, String packageType) {
        MyTeamModel model=new MyTeamModel();
        model.setUserId(userId);
        model.setBv(bv);
        model.setReferenceNumber(referenceNumber);
        model.setActivationDate(activationDate);
        model.setName(name);
        model.setPackageType(packageType);
        this.fatherLeftChildRight=model;
    }
    public void setFatherRightChildLeft(String userId,String bv, String referenceNumber,String name, String activationDate, String packageType) {
        MyTeamModel model=new MyTeamModel();
        model.setUserId(userId);
        model.setBv(bv);
        model.setReferenceNumber(referenceNumber);
        model.setActivationDate(activationDate);
        model.setName(name);
        model.setPackageType(packageType);
        this.fatherRightChildLeft=model;
    }
    public void setFatherRightChildRight(String userId,String bv, String referenceNumber,String name, String activationDate, String packageType) {
        MyTeamModel model=new MyTeamModel();
        model.setUserId(userId);
        model.setBv(bv);
        model.setReferenceNumber(referenceNumber);
        model.setActivationDate(activationDate);
        model.setName(name);
        model.setPackageType(packageType);
        this.fatherRightChildRight=model;
    }

    public MyTeamModel getParent() {
        return parent;
    }

    public MyTeamModel getFatherLeft() {
        return fatherLeft;
    }

    public MyTeamModel getFatherRight() {
        return fatherRight;
    }

    public MyTeamModel getFatherLeftChildLeft() {
        return fatherLeftChildLeft;
    }

    public MyTeamModel getFatherLeftChildRight() {
        return fatherLeftChildRight;
    }

    public MyTeamModel getFatherRightChildLeft() {
        return fatherRightChildLeft;
    }

    public MyTeamModel getFatherRightChildRight() {
        return fatherRightChildRight;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBv() {
        return bv;
    }

    public void setBv(String bv) {
        this.bv = bv;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }
}
