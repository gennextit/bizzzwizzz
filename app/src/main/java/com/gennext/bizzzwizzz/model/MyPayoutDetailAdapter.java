package com.gennext.bizzzwizzz.model;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;

import java.util.List;


public class MyPayoutDetailAdapter extends RecyclerView.Adapter<MyPayoutDetailAdapter.ViewHolder>  {
    private Activity activity;
    private List<MyPayoutModel> list;

    public MyPayoutDetailAdapter(Activity activity, List<MyPayoutModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.slot_my_payout_detail, parent, false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        if (list.get(position).getPayDate() != null) {
            holder.slot1.setText(list.get(position).getPayDate());
        }
        if (list.get(position).getPayout() != null) {
            holder.slot2.setText(list.get(position).getPayout());
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private View itemView;
        private TextView slot1,slot2;

        public ViewHolder(View view) {
            super(view);
            this.itemView=view;
            slot1 = (TextView)view.findViewById(R.id.tv_slot_1);
            slot2 = (TextView)view.findViewById(R.id.tv_slot_2);
        }

    }

}