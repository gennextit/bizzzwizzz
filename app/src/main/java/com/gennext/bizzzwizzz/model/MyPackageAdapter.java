package com.gennext.bizzzwizzz.model;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;

import java.util.List;
import java.util.Random;


public class MyPackageAdapter extends RecyclerView.Adapter<MyPackageAdapter.ViewHolder> {
    private Activity activity;
    private List<MyPackageModel> list;

    public MyPackageAdapter(Activity activity, List<MyPackageModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.slot_mypackage, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        setColor(holder.slot);

        if (list.get(position).getPackageName() != null) {
            holder.slot1.setText(list.get(position).getPackageName());
        }
        if (list.get(position).getPackageAmount() != null) {
            holder.slot2.setText(list.get(position).getPackageAmount());
        }
        if (list.get(position).getValidity() != null) {
            holder.slot3.setText(list.get(position).getValidity());
        }
        if (list.get(position).getCapping() != null) {
            holder.slot4.setText(list.get(position).getCapping());
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private TextView slot1,slot2,slot3,slot4;
        LinearLayout slot;

        public ViewHolder(View view) {
            super(view);
            slot = (LinearLayout) view.findViewById(R.id.ll_slot);
            slot1 = (TextView)view.findViewById(R.id.tv_slot_1);
            slot2 = (TextView)view.findViewById(R.id.tv_slot_2);
            slot3 = (TextView)view.findViewById(R.id.tv_slot_3);
            slot4 = (TextView)view.findViewById(R.id.tv_slot_4);

        }

    }

    public void setColor(LinearLayout tv) {
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        tv.setBackgroundColor(color);

    }
}