package com.gennext.bizzzwizzz.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.gennext.bizzzwizzz.SignupActivity;
import com.gennext.bizzzwizzz.model.ProfileModel;

import org.json.JSONObject;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class AppUser {
    public static final String BUSINESS = "Business", PERSONAL = "personal";
    public static final String COMMON = "bizzzwizzz";
    public static final String APP_DATA = "appData" + COMMON;
    public static final String BUSINESS_DETAILS = "businessDetails" + COMMON;
    public static final String PACKAGE_DETAILS = "packageDetails" + COMMON;
    public static final String OLD_PACKAGE_DETAILS = "oldPackageDetails" + COMMON;
    public static final String DOMAINS = "domains" + COMMON;
    public static final String KYC_DETAILS = "kycDetails" + COMMON;
    public static final String BANK_DETAILS = "bankDetails" + COMMON;
    public static final String HELP_DESK = "helpDesk" + COMMON;
    public static final String REQUEST_TYPE = "requestType" + COMMON;
    public static final String PACKAGES_JSON = "packagesJson" + COMMON;
    public static final String STATES_JSON = "statesjson" + COMMON;
    public static final String CITIES_JSON = "citiesJson" + COMMON;
    public static final String IDENTITY_PROOF_JSON = "identityProof" + COMMON;
    public static final String BANK_LIST_JSON = "bankList" + COMMON;
    public static final String PAYMENT_MODE_JSON = "paymentMode" + COMMON;
    public static final String CATEGORIES_JSON = "bcategories" + COMMON;
    public static final String SUB_CATEGORIES_JSON = "subCategories" + COMMON;
    public static final String ENTITY_DATA_JSON = "entityData" + COMMON;

    private static final String REMEMBER_USER = "remUser" + COMMON;
    private static final String REMEMBER_PASS = "remPass" + COMMON;
    private static final String NAME = "name" + COMMON;
    public static final String MOBILE = "mobile" + COMMON;
    public static final String GENDER = "gender" + COMMON;
    public static final String LANDLINE = "landline" + COMMON;
    public static final String EMAIL_ADDRESS = "emailAddress" + COMMON;
    public static final String DOB = "dob" + COMMON;
    public static final String STATE_ID = "stateId" + COMMON;
    public static final String CITY_ID = "cityId" + COMMON;
    public static final String STATE = "state" + COMMON;
    public static final String CITY = "city" + COMMON;
    public static final String PIN_CODE = "pinCode" + COMMON;
    public static final String ADDRESS = "address" + COMMON;
    public static final String IMAGE = "image" + COMMON;
    public static final String PACKAGE_TYPE = "PackageType" + COMMON;
    public static final String USER_ID = "userId" + COMMON;
    public static final String APP_PASSWORD = "password" + COMMON;

    public static final String CUST_BUSINESS_DETAIL = "bDetail" + COMMON;
    public static final String CUST_KYC_DETAIL = "kDetail" + COMMON;
    public static final String CUST_BANK_DETAIL = "bankDetail" + COMMON;
    private static final String IS_PACKAGE_AVAIL = "isPackAvail" + COMMON;
    private static final String IS_PAY_MOBILE = "isPayMob" + COMMON;


    public static void setUserProfile(Context context, JSONObject userProfile) {
        if (context != null && userProfile != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (userProfile.optString("name") != null)
                editor.putString(NAME, userProfile.optString("name"));
            if (userProfile.optString("mobile") != null)
                editor.putString(MOBILE, userProfile.optString("mobile"));
            if (userProfile.optString("gender") != null)
                editor.putString(GENDER, userProfile.optString("gender"));
            if (userProfile.optString("userId") != null)
                editor.putString(USER_ID, userProfile.optString("userId"));
            if (userProfile.optString("email") != null)
                editor.putString(EMAIL_ADDRESS, userProfile.optString("email"));
            if (userProfile.optString("landline") != null)
                editor.putString(LANDLINE, userProfile.optString("landline"));
            if (userProfile.optString("dob") != null)
                editor.putString(DOB, userProfile.optString("dob"));
            if (userProfile.optString("stateId") != null)
                editor.putString(STATE_ID, userProfile.optString("stateId"));
            if (userProfile.optString("cityId") != null)
                editor.putString(CITY_ID, userProfile.optString("cityId"));
            if (userProfile.optString("state") != null)
                editor.putString(STATE, userProfile.optString("state"));
            if (userProfile.optString("city") != null)
                editor.putString(CITY, userProfile.optString("city"));
            if (userProfile.optString("address") != null)
                editor.putString(ADDRESS, userProfile.optString("address"));
            if (userProfile.optString("image") != null)
                editor.putString(IMAGE, userProfile.optString("image"));
            if (userProfile.optString("PackageType") != null)
                editor.putString(PACKAGE_TYPE, userProfile.optString("PackageType"));

            editor.apply();
        }
    }

    public static ProfileModel getUserProfile(Context context) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            ProfileModel model = new ProfileModel();
            model.setName(sharedPreferences.getString(NAME, ""));
            model.setMobile(sharedPreferences.getString(MOBILE, ""));
            model.setGender(sharedPreferences.getString(GENDER, ""));
            model.setLandline(sharedPreferences.getString(LANDLINE, ""));
            model.setEmail(sharedPreferences.getString(EMAIL_ADDRESS, ""));
            model.setDob(sharedPreferences.getString(DOB, ""));
            model.setStateId(sharedPreferences.getString(STATE_ID, ""));
            model.setCityId(sharedPreferences.getString(CITY_ID, ""));
            model.setState(sharedPreferences.getString(STATE, ""));
            model.setCity(sharedPreferences.getString(CITY, ""));
            model.setPinCode(sharedPreferences.getString(PIN_CODE, ""));
            model.setAddress(sharedPreferences.getString(ADDRESS, ""));
            model.setImage(sharedPreferences.getString(IMAGE, ""));
            model.setPackageType(sharedPreferences.getString(PACKAGE_TYPE, ""));
            return model;
        }
        return null;
    }


    //    public static void setLoginDetail(Context context, JSONObject msgObj) {
//        if (context != null && msgObj != null) {
//            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            if (msgObj.optJSONObject("businessDetails") != null)
//                editor.putString(BUSINESS_DETAILS, msgObj.optJSONObject("businessDetails").toString());
//            if (msgObj.optJSONObject("packageDetails") != null)
//                editor.putString(PACKAGE_DETAILS, msgObj.optJSONObject("packageDetails").toString());
//            if (msgObj.optJSONArray("oldPackageDetails") != null)
//                editor.putString(OLD_PACKAGE_DETAILS, msgObj.optJSONArray("oldPackageDetails").toString());
//            if (msgObj.optJSONArray("domains") != null)
//                editor.putString(DOMAINS, msgObj.optJSONArray("domains").toString());
//            if (msgObj.optJSONObject("kycDetails") != null)
//                editor.putString(KYC_DETAILS, msgObj.optJSONObject("kycDetails").toString());
//            if (msgObj.optJSONObject("bankDetails") != null)
//                editor.putString(BANK_DETAILS, msgObj.optJSONObject("bankDetails").toString());
//            if (msgObj.optJSONArray("helpDesk") != null)
//                editor.putString(HELP_DESK, msgObj.optJSONArray("helpDesk").toString());
//            if (msgObj.optJSONArray("requestType") != null)
//                editor.putString(REQUEST_TYPE, msgObj.optJSONArray("requestType").toString());
//            if (msgObj.optJSONArray("packages") != null)
//                editor.putString(PACKAGES_JSON, msgObj.optJSONArray("packages").toString());
//            if (msgObj.optJSONArray("states") != null)
//                editor.putString(STATES_JSON, msgObj.optJSONArray("states").toString());
//            if (msgObj.optJSONArray("cities") != null)
//                editor.putString(CITIES_JSON, msgObj.optJSONArray("cities").toString());
//            if (msgObj.optJSONArray("identityProof") != null)
//                editor.putString(IDENTITY_PROOF_JSON, msgObj.optJSONArray("identityProof").toString());
//            if (msgObj.optJSONArray("bankList") != null)
//                editor.putString(BANK_LIST_JSON, msgObj.optJSONArray("bankList").toString());
//            if (msgObj.optJSONArray("paymentMode") != null)
//                editor.putString(PAYMENT_MODE_JSON, msgObj.optJSONArray("paymentMode").toString());
//            if (msgObj.optJSONArray("categories") != null)
//                editor.putString(CATEGORIES_JSON, msgObj.optJSONArray("categories").toString());
//            if (msgObj.optJSONArray("subCategories") != null)
//                editor.putString(SUB_CATEGORIES_JSON, msgObj.optJSONArray("subCategories").toString());
//            if (msgObj.optJSONArray("entityData") != null)
//                editor.putString(ENTITY_DATA_JSON, msgObj.optJSONArray("entityData").toString());
//
//            editor.apply();
//        }
//    }

    public static void setLoginDetail2(Context context, JSONObject msgObj) {
        if (context != null && msgObj != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (msgObj.optJSONObject("businessDetails") != null) {
                editor.putString(BUSINESS_DETAILS, msgObj.optJSONObject("businessDetails").toString());
            } else {
                editor.putString(BUSINESS_DETAILS, "");
            }
            if (msgObj.optJSONObject("packageDetails") != null) {
                editor.putString(PACKAGE_DETAILS, msgObj.optJSONObject("packageDetails").toString());
            } else {
                editor.putString(PACKAGE_DETAILS, "");
            }
            if (msgObj.optJSONArray("oldPackageDetails") != null) {
                editor.putString(OLD_PACKAGE_DETAILS, msgObj.optJSONArray("oldPackageDetails").toString());
            } else {
                editor.putString(OLD_PACKAGE_DETAILS, "");
            }
            if (msgObj.optJSONArray("domains") != null) {
                editor.putString(DOMAINS, msgObj.optJSONArray("domains").toString());
            } else {
                editor.putString(DOMAINS, "");
            }
            if (msgObj.optJSONObject("kycDetails") != null) {
                editor.putString(KYC_DETAILS, msgObj.optJSONObject("kycDetails").toString());
            } else {
                editor.putString(KYC_DETAILS, "");
            }
            if (msgObj.optJSONObject("bankDetails") != null) {
                editor.putString(BANK_DETAILS, msgObj.optJSONObject("bankDetails").toString());
            } else {
                editor.putString(BANK_DETAILS, "");
            }
            if (msgObj.optJSONArray("helpDesk") != null) {
                editor.putString(HELP_DESK, msgObj.optJSONArray("helpDesk").toString());
            } else {
                editor.putString(HELP_DESK, "");
            }
            if (msgObj.optJSONArray("requestType") != null) {
                editor.putString(REQUEST_TYPE, msgObj.optJSONArray("requestType").toString());
            } else {
                editor.putString(REQUEST_TYPE, "");
            }
            if (msgObj.optJSONArray("packages") != null) {
                editor.putString(PACKAGES_JSON, msgObj.optJSONArray("packages").toString());
            } else {
                editor.putString(PACKAGES_JSON, "");
            }
            if (msgObj.optJSONArray("states") != null) {
                editor.putString(STATES_JSON, msgObj.optJSONArray("states").toString());
            } else {
                editor.putString(STATES_JSON, "");
            }
            if (msgObj.optJSONArray("cities") != null) {
                editor.putString(CITIES_JSON, msgObj.optJSONArray("cities").toString());
            } else {
                editor.putString(CITIES_JSON, "");
            }
            if (msgObj.optJSONArray("identityProof") != null) {
                editor.putString(IDENTITY_PROOF_JSON, msgObj.optJSONArray("identityProof").toString());
            } else {
                editor.putString(IDENTITY_PROOF_JSON, "");
            }
            if (msgObj.optJSONArray("bankList") != null) {
                editor.putString(BANK_LIST_JSON, msgObj.optJSONArray("bankList").toString());
            } else {
                editor.putString(BANK_LIST_JSON, "");
            }
            if (msgObj.optJSONArray("paymentMode") != null) {
                editor.putString(PAYMENT_MODE_JSON, msgObj.optJSONArray("paymentMode").toString());
            } else {
                editor.putString(PAYMENT_MODE_JSON, "");
            }
            if (msgObj.optJSONArray("categories") != null) {
                editor.putString(CATEGORIES_JSON, msgObj.optJSONArray("categories").toString());
            } else {
                editor.putString(CATEGORIES_JSON, "");
            }
            if (msgObj.optJSONArray("subCategories") != null) {
                editor.putString(SUB_CATEGORIES_JSON, msgObj.optJSONArray("subCategories").toString());
            } else {
                editor.putString(SUB_CATEGORIES_JSON, "");
            }
            if (msgObj.optJSONArray("entityData") != null) {
                editor.putString(ENTITY_DATA_JSON, msgObj.optJSONArray("entityData").toString());
            } else {
                editor.putString(ENTITY_DATA_JSON, "");
            }
            editor.apply();
        }
    }

    public static String LoadPref(Context context, String key) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String data = sharedPreferences.getString(key, "");
            return data;
        }
        return "";
    }

    public static void setAppData(Context act, String response) {
        Utility.SavePref(act, APP_DATA, response);
    }

    public static void setCustomerDetail(Context act) {
        Utility.SavePref(act, CUST_BUSINESS_DETAIL, "success");
    }

    public static String getCustomerDetail(Context act) {
        return LoadPref(act, CUST_BUSINESS_DETAIL);
    }

    public static void setRememberUser(Context act, String username) {
        Utility.SavePref(act, REMEMBER_USER, username);
    }

    public static String getRememberUser(Context act) {
        return LoadPref(act, REMEMBER_USER);
    }

    public static void setRememberPass(Context act, String password) {
        Utility.SavePref(act, REMEMBER_PASS, password);
    }

    public static String getRememberPass(Context act) {
        return LoadPref(act, REMEMBER_PASS);
    }

    public static void setCustKycDetail(Context act, String response) {
        Utility.SavePref(act, CUST_KYC_DETAIL, response);
    }

    public static String getCustKycDetail(Context act) {
        return LoadPref(act, CUST_KYC_DETAIL);
    }

    public static void setCustBankDetail(Context act, String response) {
        Utility.SavePref(act, CUST_BANK_DETAIL, response);
    }

    public static String getIdentityProofJson(Context act) {
        return LoadPref(act, IDENTITY_PROOF_JSON);
    }

    public static String getBankListJson(Context act) {
        return LoadPref(act, BANK_LIST_JSON);
    }

    public static String getPaymentModeJson(Context act) {
        return LoadPref(act, PAYMENT_MODE_JSON);
    }

    public static String getCategoriesJson(Context act) {
        return LoadPref(act, CATEGORIES_JSON);
    }

    public static String getSubCategoriesJson(Context act) {
        return LoadPref(act, SUB_CATEGORIES_JSON);
    }

    public static String getEntityDataJson(Context act) {
        return LoadPref(act, ENTITY_DATA_JSON);
    }

    public static String getCustBankDetail(Context act) {
        return LoadPref(act, CUST_BANK_DETAIL);
    }

    public static String getAppData(Context act) {
        return LoadPref(act, APP_DATA);
    }

    public static String getName(Context act) {
        return LoadPref(act, NAME);
    }

    public static String getImage(Context context) {
        return LoadPref(context, IMAGE);
    }

    public static String getBusinessDetails(Context context) {
        return LoadPref(context, BUSINESS_DETAILS);
    }

    public static String getPackagesJson(Context context) {
        return LoadPref(context, PACKAGES_JSON);
    }

    public static String getStatesJson(Context context) {
        return LoadPref(context, STATES_JSON);
    }

    public static String getCitiesJson(Context context) {
        return LoadPref(context, CITIES_JSON);
    }

    public static String getPackageDetails(Context context) {
        return LoadPref(context, PACKAGE_DETAILS);
    }

    public static String getOldPackageDetails(Context context) {
        return LoadPref(context, OLD_PACKAGE_DETAILS);
    }

    public static String getDOMAINS(Context context) {
        return LoadPref(context, DOMAINS);
    }

    public static String getKycDetails(Context context) {
        return LoadPref(context, KYC_DETAILS);
    }

    public static String getBankDetails(Context context) {
        return LoadPref(context, BANK_DETAILS);
    }

    public static String getHelpDesk(Context context) {
        return LoadPref(context, HELP_DESK);
    }

    public static String getRequestType(Context context) {
        return LoadPref(context, REQUEST_TYPE);
    }

    public static String getNAME(Context context) {
        return LoadPref(context, NAME);
    }

    public static String getMOBILE(Context context) {
        return LoadPref(context, MOBILE);
    }

    public static String getEmailAddress(Context context) {
        return LoadPref(context, EMAIL_ADDRESS);
    }

    public static String getDOB(Context context) {
        return LoadPref(context, DOB);
    }

    public static String getSTATE(Context context) {
        return LoadPref(context, STATE);
    }

    public static String getCITY(Context context) {
        return LoadPref(context, CITY);
    }

    public static String getADDRESS(Context context) {
        return LoadPref(context, ADDRESS);
    }

    public static String getIMAGE(Context context) {
        return LoadPref(context, IMAGE);
    }


    public static String getUserId(Context context) {
        return LoadPref(context, USER_ID);
    }

    public static void setUserProfile(Context context, String name, String mobile, String gender, String email, String dob,
                                      String state, String city, String pinCode, String address, String landline) {
         if (context != null) {
             SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
             SharedPreferences.Editor editor = sharedPreferences.edit();
             if (name != null)
                editor.putString(NAME, name);
            if (mobile != null)
                editor.putString(MOBILE, mobile);
            if (gender != null)
                editor.putString(GENDER, gender);
            if (email != null)
                editor.putString(EMAIL_ADDRESS, email);
            if (landline != null)
                editor.putString(LANDLINE, landline);
            if (dob != null)
                editor.putString(DOB, dob);
            if (state != null)
                editor.putString(STATE, state);
            if (city != null)
                editor.putString(CITY, city);
            if (address != null)
                editor.putString(PIN_CODE, pinCode);
            if (address != null)
                editor.putString(ADDRESS, address);
            editor.apply();
        }
    }

    public static void setPassword(Context activity, String password) {
        Utility.SavePref(activity, APP_PASSWORD, password);
    }

    public static String getPassword(Context activity) {
        return LoadPref(activity, APP_PASSWORD);
    }

    public static String getPackageType(Context context) {
        return LoadPref(context, PACKAGE_TYPE);
    }

    public static boolean isPackageAvailable(Context context) {
        String temp=LoadPref(context,IS_PACKAGE_AVAIL);
        if(temp.equals("")){
            return false;
        }else {
            return true;
        }
    }

    public static void setPackageAvailability(Context context,String output) {
        Utility.SavePref(context, IS_PACKAGE_AVAIL, output);
    }

    public static void isPayThroughMobile(Context context, String output) {
        Utility.SavePref(context, IS_PAY_MOBILE, output);
    }
    public static String getPayThroughMobile(Context context) {
        return LoadPref(context, IS_PAY_MOBILE);
    }

}
