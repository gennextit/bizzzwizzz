package com.gennext.bizzzwizzz.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.gennext.bizzzwizzz.model.BankModel;
import com.gennext.bizzzwizzz.model.BusinessCategoryModel;
import com.gennext.bizzzwizzz.model.ConsultancyModel;
import com.gennext.bizzzwizzz.model.DayAndTimeModel;
import com.gennext.bizzzwizzz.model.DirectReferralModel;
import com.gennext.bizzzwizzz.model.HelpdeskModel;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.model.MyPackageModel;
import com.gennext.bizzzwizzz.model.MyPayoutModel;
import com.gennext.bizzzwizzz.model.MySalesModel;
import com.gennext.bizzzwizzz.model.MyTeamModel;
import com.gennext.bizzzwizzz.model.PaymentModel;
import com.gennext.bizzzwizzz.model.StateCityModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Abhijit on 08-Dec-16.
 */
public class JsonParser {
    public static String ERRORMESSAGE = "Not Available";


    public static String toDayAndTimingIdJsonArray(ArrayList<DayAndTimeModel> list) {
        try {
            // In this case we need a json array to hold the java list
            JSONArray jsonArr = new JSONArray();
            if (list != null) {
                for (DayAndTimeModel model : list) {
                    JSONObject pnObj = new JSONObject();
                    pnObj.put("Id", model.getDayId());
                    pnObj.put("StartTime", model.getStartTime());
                    pnObj.put("EndTime", model.getEndTime());
                    jsonArr.put(pnObj);
                }
            } else {
                return null;
            }
            return jsonArr.toString();


        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Model parseLoginDataTest() {
        Model model = new Model();
        model.setOutput("success");
        return model;
    }

    public static Model failureResponse() {
        Model model = new Model();
        model.setOutput("failure");
        model.setOutputMsg("test failure message");
        return model;
    }

    public Model defaultParser(String response) {
        Model jsonModel = new Model();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("IOException")) {
            jsonModel.setOutput("internet");
            jsonModel.setOutputMsg(response);
            return jsonModel;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        jsonModel.setOutput("success");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    } else if (mainObject.getString("status").equals("failure")) {
                        jsonModel.setOutput("failure");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput("server");
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput("server");
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static MyPackageModel parseMyPackageDetail(String response) {
        MyPackageModel jsonModel = new MyPackageModel();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("IOException")) {
            jsonModel.setOutput("internet");
            jsonModel.setOutputMsg(response);
            return jsonModel;
        }else {
            if (response.contains("{")) {
                try {
                    JSONObject mainObj = new JSONObject(response);
                    jsonModel.setOutput("success");
                    jsonModel.setPackageType(mainObj.optString("PackageType"));
                    jsonModel.setPackageName(mainObj.optString("PackageName"));
                    jsonModel.setPackageAmount(mainObj.optString("PackageAmount"));
                    jsonModel.setValidity(mainObj.optString("PackageValidity"));
                    jsonModel.setCapping(mainObj.optString("Capping"));

                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput("server");
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput("server");
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public DirectReferralModel parseDirectReferralDetail(String response) {
        DirectReferralModel model = new DirectReferralModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<DirectReferralModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        JSONObject msgObj = mainObject.optJSONObject("message");
                        JSONArray pArray = msgObj.optJSONArray("directReferrals");
                        if (pArray != null) {
                            model.setOutput("success");
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject obj = pArray.optJSONObject(i);
                                DirectReferralModel slot = new DirectReferralModel();
                                slot.setUserId(obj.optString("UserId"));
                                slot.setName(obj.optString("Name"));
                                slot.setBusinessValue(obj.optString("BusinessValue"));
                                slot.setCommission(obj.optString("Commission"));
                                slot.setTDS(obj.optString("TDS"));
                                slot.setNetPay(obj.optString("netPay"));
                                String pos = obj.optString("Position");
                                slot.setPosition(pos);
                                String date = obj.optString("Date");
                                if (!TextUtils.isEmpty(date)) {
                                    slot.setDate(DateTimeUtility.convertDateYMD2(date.substring(0, 10)));
                                } else {
                                    slot.setDate("");
                                }
                                model.getList().add(slot);
                            }
                        }

                    } else if (mainObject.getString("status").equals("failure")) {
                        model.setOutput("failure");
                        model.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }


    public Model parseLoginData(Context activity, String response) {
        Model model = new Model();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        JSONObject msgObj = mainObject.optJSONObject("message");
                        if (msgObj != null) {
                            AppUser.setLoginDetail2(activity, msgObj);
                            AppUser.setUserProfile(activity, msgObj.optJSONObject("userProfile"));
                            MyPackageModel temp = JsonParser.parseMyPackageDetail(AppUser.getPackageDetails(activity));
                            if (temp != null && temp.getOutput().equalsIgnoreCase("success")) {
                                AppUser.setPackageAvailability(activity, "success");
                            } else {
                                AppUser.setPackageAvailability(activity, "");
                            }
                        }
                        model.setOutput("success");
                    } else if (mainObject.getString("status").equals("failure")) {
                        model.setOutput("failure");
                        model.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public ConsultancyModel parseDomain(String response) {
        ConsultancyModel model = new ConsultancyModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<ConsultancyModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    if (mainArray != null) {
                        ConsultancyModel sp = new ConsultancyModel();
                        sp.setDomainId("0");
                        sp.setDomainName("Select Domain");
                        model.getList().add(sp);
                        for (int i = 0; i < mainArray.length(); i++) {
                            JSONObject domainObj = mainArray.getJSONObject(i);
                            model.setOutput("success");
                            ConsultancyModel slot = new ConsultancyModel();
                            slot.setDomainId(domainObj.optString("id"));
                            slot.setDomainName(domainObj.optString("DomainName"));
                            model.getList().add(slot);
                        }
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public ConsultancyModel parseGetSlot(String response) {
        ConsultancyModel model = new ConsultancyModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setTimeSlotList(new ArrayList<ConsultancyModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        JSONArray msgArray = mainObject.optJSONArray("message");
                        if (msgArray != null) {
                            model.setOutput("success");
                            for (int i = 0; i < msgArray.length(); i++) {
                                JSONObject obj = msgArray.optJSONObject(i);
                                ConsultancyModel slot = new ConsultancyModel();
                                String time = obj.optString("timeSlot");
                                if (!TextUtils.isEmpty(time)) {
                                    slot.setSlotTime(time.substring(0, 5));
                                } else {
                                    slot.setSlotTime(time);
                                }
                                model.getTimeSlotList().add(slot);
                            }
                        }

                    } else if (mainObject.getString("status").equals("failure")) {
                        model.setOutput("failure");
                        model.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }


    private ConsultancyModel setSlot(String time) {
        ConsultancyModel slot = new ConsultancyModel();
        slot.setSlotTime(time);
        return slot;
    }

    public ConsultancyModel parseBookingRequest(String response) {
        ConsultancyModel jsonModel = new ConsultancyModel();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("IOException")) {
            jsonModel.setOutput("internet");
            jsonModel.setOutputMsg(response);
            return jsonModel;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        jsonModel.setOutput("success");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    } else if (mainObject.getString("status").equals("failure")) {
                        jsonModel.setOutput("failure");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput("server");
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput("server");
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }


    public MyTeamModel parseTreeView2(String response) {
        MyTeamModel model = new MyTeamModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        JSONObject msgObj = mainObject.optJSONObject("message");
                        if (msgObj != null) {
                            JSONObject rootObj = msgObj.optJSONObject("root");
                            if (rootObj != null) {
                                model.setOutput("success");
                                String parent = rootObj.optString("entity");
                                String fatherLeft = rootObj.optString("left");
                                String fatherRight = rootObj.optString("right");

                                JSONObject eDetails = rootObj.optJSONObject("entityDetails");
                                if (parent != null && eDetails != null) {
                                    model.setParent(parent, eDetails.optString("bv"), eDetails.optString("referenceNumber"), eDetails.optString("name"),
                                            eDetails.optString("activationDate"), eDetails.optString("packageType"));
                                }
                                JSONObject lcDetails = rootObj.optJSONObject("leftChildDetails");
                                if (fatherLeft != null && lcDetails != null) {
                                    model.setFatherLeft(fatherLeft, lcDetails.optString("bv"), lcDetails.optString("referenceNumber"), lcDetails.optString("name"),
                                            lcDetails.optString("activationDate"), lcDetails.optString("packageType"));
                                }
                                JSONObject rcDetails = rootObj.optJSONObject("rightChildDetails");
                                if (fatherRight != null && rcDetails != null) {
                                    model.setFatherRight(fatherRight, rcDetails.optString("bv"), rcDetails.optString("referenceNumber"), rcDetails.optString("name"),
                                            rcDetails.optString("activationDate"), rcDetails.optString("packageType"));
                                }

                            }
                            JSONObject rootLeftObj = msgObj.optJSONObject("rootLeft");
                            if (rootLeftObj != null) {
                                String childLeft = rootLeftObj.optString("left");
                                String childRight = rootLeftObj.optString("right");

                                JSONObject lcDetails = rootLeftObj.optJSONObject("leftChildDetails");
                                if (childLeft != null && lcDetails != null) {
                                    model.setFatherLeftChildLeft(childLeft, lcDetails.optString("bv"), lcDetails.optString("referenceNumber"), lcDetails.optString("name"),
                                            lcDetails.optString("activationDate"), lcDetails.optString("packageType"));
                                }

                                JSONObject rcDetails = rootLeftObj.optJSONObject("rightChildDetails");
                                if (childRight != null && rcDetails != null) {
                                    model.setFatherLeftChildRight(childRight, rcDetails.optString("bv"), rcDetails.optString("referenceNumber"), rcDetails.optString("name"),
                                            rcDetails.optString("activationDate"), rcDetails.optString("packageType"));
                                }


                            }
                            JSONObject rootRightObj = msgObj.optJSONObject("rootRight");
                            if (rootRightObj != null) {

                                String childLeft = rootRightObj.optString("left");
                                String childRight = rootRightObj.optString("right");

                                JSONObject lcDetails = rootRightObj.optJSONObject("leftChildDetails");
                                if (childLeft != null && lcDetails != null) {
                                    model.setFatherRightChildLeft(childLeft, lcDetails.optString("bv"), lcDetails.optString("referenceNumber"), lcDetails.optString("name"),
                                            lcDetails.optString("activationDate"), lcDetails.optString("packageType"));
                                }

                                JSONObject rcDetails = rootRightObj.optJSONObject("rightChildDetails");
                                if (childRight != null && rcDetails != null) {
                                    model.setFatherRightChildRight(childRight, rcDetails.optString("bv"), rcDetails.optString("referenceNumber"), rcDetails.optString("name"),
                                            rcDetails.optString("activationDate"), rcDetails.optString("packageType"));
                                }

                            }
                        }

                    } else if (mainObject.getString("status").equals("failure")) {
                        model.setOutput("failure");
                        model.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;

    }


    public MyPayoutModel parseMyPayoutDetail(String response) {
        MyPayoutModel model = new MyPayoutModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<MyPayoutModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        JSONObject msgObj = mainObject.optJSONObject("message");
                        JSONArray pArray = msgObj.optJSONArray("payoutDetails");
                        JSONObject cObj = msgObj.optJSONObject("criteriaDetails");
                        if (cObj != null) {
                            model.setCriteriaLeft(cObj.optString("left"));
                            model.setCriteriaRight(cObj.optString("right"));
                            model.setCriteriaBank(cObj.optString("bank"));
                            model.setCriteriaKyc(cObj.optString("kyc"));
                        }
                        if (pArray != null) {
                            model.setOutput("success");
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject obj = pArray.optJSONObject(i);
                                MyPayoutModel slot = new MyPayoutModel();
                                slot.setOpeningLeft(applyFormula(obj.optString("OpeningLeft")));
                                slot.setOpeningRight(applyFormula(obj.optString("OpeningRight")));
                                slot.setLeftSale(applyFormula(obj.optString("LeftSale")));
                                slot.setRightSale(applyFormula(obj.optString("RightSale")));
                                slot.setTotalLeft(applyFormula(obj.optString("TotalLeft")));
                                slot.setTotalRight(applyFormula(obj.optString("TotalRight")));
                                String bvt = obj.optString("BusinessValueTransfer");
                                slot.setBusinessValueTransfer(applyFormula(bvt));
                                slot.setClosingLeft(applyFormula(obj.optString("ClosingLeft")));
                                slot.setClosingRight(applyFormula(obj.optString("ClosingRight")));
                                slot.setAL(obj.optString("AL"));
                                slot.setAR(obj.optString("AR"));
                                slot.setTDS(obj.optString("TDS"));
                                slot.setNetPay(obj.optString("netPay"));
                                slot.setPayDate(DateTimeUtility.convertDateYMD2(obj.optString("Date").substring(0, 10)));
                                try {
                                    int BVT = Integer.parseInt(bvt);
                                    BVT = ((BVT * 50) / 1000);
                                    slot.setPayout(String.valueOf(BVT));
                                } catch (NumberFormatException e) {
                                    slot.setPayout("0");
                                }
                                model.getList().add(slot);
                            }
                        }

                    } else if (mainObject.getString("status").equals("failure")) {
                        model.setOutput("failure");
                        model.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    @NonNull
    private String applyFormula(String value) {
        if (!TextUtils.isEmpty(value)) {
            try {
                int output = Integer.parseInt(value);
                return String.valueOf(output / 1000);
            } catch (NumberFormatException e) {
                return "";
            }
        } else {
            return "";
        }
    }

    public MySalesModel parseMySalesDetail(String response) {
        MySalesModel model = new MySalesModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<MySalesModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        JSONObject msgObj = mainObject.optJSONObject("message");
                        JSONArray pArray = msgObj.optJSONArray("otherIncoms");
                        if (pArray != null) {
                            model.setOutput("success");
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject obj = pArray.optJSONObject(i);
                                MySalesModel slot = new MySalesModel();
                                slot.setUserId(obj.optString("UserId"));
                                slot.setName(obj.optString("Name"));
                                slot.setBusinessValue(obj.optString("BusinessValue"));
                                String pos = obj.optString("Position");
                                slot.setPosition(pos);
                                String date = obj.optString("Date");
                                if (!TextUtils.isEmpty(date)) {
                                    slot.setDate(DateTimeUtility.convertDateYMD2(date.substring(0, 10)));
                                } else {
                                    slot.setDate("");
                                }


                                model.getList().add(slot);
                            }
                        }

                    } else if (mainObject.getString("status").equals("failure")) {
                        model.setOutput("failure");
                        model.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public HelpdeskModel parseHelpdeskDetail(String response) {
        HelpdeskModel model = new HelpdeskModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<HelpdeskModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        JSONArray pArray = mainObject.optJSONArray("message");
                        if (pArray != null) {
                            model.setOutput("success");
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject obj = pArray.optJSONObject(i);
                                HelpdeskModel slot = new HelpdeskModel();
                                slot.setRequestTypeId(obj.optString("RequestTypeId"));
                                slot.setRequestDetail(obj.optString("RequestDetail"));
                                slot.setRequestFile(obj.optString("RequestFile"));
                                slot.setStatus(obj.optString("Status"));
                                slot.setRequestName(obj.optString("RequestName"));
                                String date = obj.optString("RequestDate");
                                if (!TextUtils.isEmpty(date)) {
                                    slot.setRequestDate(date.substring(0, 10));
                                } else {
                                    slot.setRequestDate("");
                                }


                                model.getList().add(slot);
                            }
                        }

                    } else if (mainObject.getString("status").equals("failure")) {
                        model.setOutput("failure");
                        model.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public HelpdeskModel parseRequest(String response) {
        HelpdeskModel model = new HelpdeskModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<HelpdeskModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    if (mainArray != null) {
                        HelpdeskModel sp = new HelpdeskModel();
                        sp.setRequestFile("0");
                        sp.setRequestName("Select Type");
                        model.getList().add(sp);
                        for (int i = 0; i < mainArray.length(); i++) {
                            JSONObject domainObj = mainArray.getJSONObject(i);
                            model.setOutput("success");
                            HelpdeskModel slot = new HelpdeskModel();
                            slot.setRequestTypeId(domainObj.optString("id"));
                            slot.setRequestName(domainObj.optString("RequestName"));
                            model.getList().add(slot);
                        }
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public HelpdeskModel parseRaiseReqDetail(String response) {
        HelpdeskModel jsonModel = new HelpdeskModel();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("IOException")) {
            jsonModel.setOutput("internet");
            jsonModel.setOutputMsg(response);
            return jsonModel;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        jsonModel.setOutput("success");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    } else if (mainObject.getString("status").equals("failure")) {
                        jsonModel.setOutput("failure");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput("server");
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput("server");
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static StateCityModel parseStateJson(String response) {
        StateCityModel model = new StateCityModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<StateCityModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    if (mainArray != null) {
                        for (int i = 0; i < mainArray.length(); i++) {
                            JSONObject domainObj = mainArray.getJSONObject(i);
                            model.setOutput("success");
                            StateCityModel slot = new StateCityModel();
                            slot.setStateId(domainObj.optString("id"));
                            slot.setStateName(domainObj.optString("StateName"));
                            model.getList().add(slot);
                        }
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public static StateCityModel parseCityJson(String response, String stateId) {
        StateCityModel model = new StateCityModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<StateCityModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    if (mainArray != null) {
                        for (int i = 0; i < mainArray.length(); i++) {
                            JSONObject domainObj = mainArray.getJSONObject(i);
                            if (domainObj.optString("StateId").equalsIgnoreCase(stateId)) {
                                model.setOutput("success");
                                StateCityModel slot = new StateCityModel();
                                slot.setCityId(domainObj.optString("id"));
                                slot.setCityName(domainObj.optString("CityName"));
                                model.getList().add(slot);
                            }
                        }
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public static MyPackageModel parsePackageDetail(String response, String packageType) {
        MyPackageModel model = new MyPackageModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setPackageList(new ArrayList<MyPackageModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    if (mainArray != null) {
                        for (int i = 0; i < mainArray.length(); i++) {
                            JSONObject domainObj = mainArray.getJSONObject(i);
                            String pkgTyp = domainObj.optString("PackageType");
                            if (pkgTyp.equalsIgnoreCase(packageType)) {
                                model.setOutput("success");
                                MyPackageModel slot = new MyPackageModel();
                                if ((i % 2) == 0) {
                                    slot.setColored(true);
                                } else {
                                    slot.setColored(false);
                                }
                                slot.setPackageId(domainObj.optString("id"));
                                slot.setPackageType(domainObj.optString("PackageType"));
                                slot.setPackageName(domainObj.optString("PackageName"));
                                slot.setPackageAmount(domainObj.optString("PackageAmount"));
                                slot.setCapping(domainObj.optString("Capping"));
                                String text = domainObj.optString("Text");
                                JSONArray textArr = new JSONArray(text);
                                if (textArr != null) {
                                    slot.setContentList(new ArrayList<MyPackageModel>());
                                    for (int j = 0; j < textArr.length(); j++) {
                                        JSONObject jsonObject = textArr.getJSONObject(j);
                                        MyPackageModel content = new MyPackageModel();
                                        content.setContent(jsonObject.optString("content"));
                                        JSONArray detailsArr = jsonObject.optJSONArray("details");
                                        if (detailsArr != null) {
                                            content.setSubContentList(new ArrayList<MyPackageModel>());
                                            for (int k = 0; k < detailsArr.length(); k++) {
                                                JSONObject sbuConObj = detailsArr.getJSONObject(k);
                                                MyPackageModel subCon = new MyPackageModel();
                                                subCon.setSubContent(sbuConObj.optString("subContent"));
                                                content.getSubContentList().add(subCon);
                                            }
                                        }
                                        slot.getContentList().add(content);
                                    }
                                }
                                model.getPackageList().add(slot);
                            }
                        }
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public ConsultancyModel parseAllSlotsBookingDetail(String response) {
        ConsultancyModel jsonModel = new ConsultancyModel();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        jsonModel.setList(new ArrayList<ConsultancyModel>());
        if (response.contains("IOException")) {
            jsonModel.setOutput("internet");
            jsonModel.setOutputMsg(response);
            return jsonModel;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        JSONObject msgObj = mainObject.optJSONObject("message");
                        if (msgObj != null) {
                            String avalSlots = msgObj.optString("availableSlot");
                            if (avalSlots != null) {
                                jsonModel.setOutput("success");
                                jsonModel.setAvailableSlot(avalSlots);
                                JSONArray detailArray = msgObj.optJSONArray("details");
                                for (int i = 0; i < detailArray.length(); i++) {
                                    JSONObject domainObj = detailArray.getJSONObject(i);
                                    ConsultancyModel slot = new ConsultancyModel();
                                    slot.setDomainId(domainObj.optString("id"));
                                    slot.setDomainName(domainObj.optString("DomainName"));
                                    slot.setCancel(domainObj.optString("cancel"));
                                    slot.setSlotDate(domainObj.optString("Date"));
                                    slot.setSlotDay(domainObj.optString("Day"));
                                    slot.setSlotTime(domainObj.optString("Time"));
                                    slot.setStatus(domainObj.optString("status"));
                                    jsonModel.getList().add(slot);
                                }
                            }
                        } else {
                            jsonModel.setOutput("failure");
                            jsonModel.setOutputMsg("Empty Booking list");
                        }
                    } else if (mainObject.getString("status").equals("failure")) {
                        jsonModel.setOutput("failure");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput("server");
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput("server");
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

//    public ConsultancyModel parseAllSlotsBookingDetail2(String response) {
//        ConsultancyModel jsonModel = new ConsultancyModel();
//        jsonModel.setOutput("success");
//        jsonModel.setList(new ArrayList<ConsultancyModel>());
//        for (int i = 0; i < 5; i++) {
//            ConsultancyModel slot = new ConsultancyModel();
//            slot.setDomainName("DomainName " + i);
//            slot.setCancel("cancel " + i);
//            slot.setSlotDate("2016-12-1" + i);
//            slot.setSlotDay("Day " + i);
//            slot.setSlotTime("1" + i + ":00");
//            jsonModel.getList().add(slot);
//        }
//        return jsonModel;
//    }

    public ConsultancyModel parseCancelBooking(String response) {
        ConsultancyModel jsonModel = new ConsultancyModel();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("IOException")) {
            jsonModel.setOutput("internet");
            jsonModel.setOutputMsg(response);
            return jsonModel;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        jsonModel.setOutput("success");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    } else if (mainObject.getString("status").equals("failure")) {
                        jsonModel.setOutput("failure");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput("server");
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput("server");
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

//    public MyPackageModel parseAllPackageDetail(String packagesJson) {
//
//        return null;
//    }

    public BankModel parseBankData(String response) {
        BankModel model = new BankModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<BankModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    if (mainArray != null) {
                        BankModel sp = new BankModel();
                        sp.setId("0");
                        sp.setName("Select Bank");
                        model.getList().add(sp);
                        for (int i = 0; i < mainArray.length(); i++) {
                            JSONObject domainObj = mainArray.getJSONObject(i);
                            model.setOutput("success");
                            BankModel slot = new BankModel();
                            slot.setId(domainObj.optString("id"));
                            slot.setName(domainObj.optString("name"));
                            model.getList().add(slot);
                        }
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public BankModel parseIdentityProofData(String response) {
        BankModel model = new BankModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<BankModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    if (mainArray != null) {
                        BankModel sp = new BankModel();
                        sp.setId(null);
                        sp.setName("Select Identity proof");
                        model.getList().add(sp);
                        for (int i = 0; i < mainArray.length(); i++) {
                            JSONObject domainObj = mainArray.getJSONObject(i);
                            model.setOutput("success");
                            BankModel slot = new BankModel();
                            slot.setId(domainObj.optString("id"));
                            slot.setName(domainObj.optString("name"));
                            model.getList().add(slot);
                        }
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public BankModel parseTypeOfEntityData(String response) {
        BankModel model = new BankModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<BankModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    if (mainArray != null) {
                        BankModel sp = new BankModel();
                        sp.setId(null);
                        sp.setName("Select Entity type");
                        model.getList().add(sp);
                        for (int i = 0; i < mainArray.length(); i++) {
                            JSONObject domainObj = mainArray.getJSONObject(i);
                            model.setOutput("success");
                            BankModel slot = new BankModel();
                            slot.setId(domainObj.optString("id"));
                            slot.setName(domainObj.optString("Name"));
                            model.getList().add(slot);
                        }
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }


    public static BusinessCategoryModel parseCategoryData(String response) {
        BusinessCategoryModel model = new BusinessCategoryModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<BusinessCategoryModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    for (int i = 0; i < mainArray.length(); i++) {
                        JSONObject domainObj = mainArray.getJSONObject(i);
                        model.setOutput("success");
                        BusinessCategoryModel slot = new BusinessCategoryModel();
                        slot.setCategoryId(domainObj.optString("id"));
                        slot.setCategory(domainObj.optString("category"));
                        model.getList().add(slot);
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public static BankModel parsePaymentMode(String response) {
        BankModel model = new BankModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<BankModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    for (int i = 0; i < mainArray.length(); i++) {
                        JSONObject domainObj = mainArray.getJSONObject(i);
                        model.setOutput("success");
                        BankModel slot = new BankModel();
                        slot.setId(domainObj.optString("id"));
                        slot.setName(domainObj.optString("name"));
                        slot.setChecked(false);
                        model.getList().add(slot);
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public static BusinessCategoryModel parseSubCategoryMode(String response, String sltCategoryId) {
        BusinessCategoryModel model = new BusinessCategoryModel();
        model.setOutput("ERROR-101");
        model.setOutputMsg("ERROR-101 No msg available");
        model.setList(new ArrayList<BusinessCategoryModel>());
        if (response.contains("IOException")) {
            model.setOutput("internet");
            model.setOutputMsg(response);
            return model;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    for (int i = 0; i < mainArray.length(); i++) {
                        JSONObject domainObj = mainArray.getJSONObject(i);
                        if (domainObj.optString("CategoryId").equalsIgnoreCase(sltCategoryId)) {
                            model.setOutput("success");
                            BusinessCategoryModel slot = new BusinessCategoryModel();
                            slot.setSubCategoryId(domainObj.optString("id"));
                            slot.setSubCategory(domainObj.optString("SubCategory"));
                            slot.setChecked(false);
                            model.getList().add(slot);
                        }
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    model.setOutput("server");
                    model.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                model.setOutput("server");
                model.setOutputMsg(response);
            }
        }
        return model;
    }

    public static ArrayList<String> parseBankDetail(String response) {
        ArrayList<String> detailArray;
        if (response.contains("{")) {
            try {
                JSONObject mainObj = new JSONObject(response);
                detailArray = new ArrayList<>();
                detailArray.add(mainObj.optString("PersonName"));
                detailArray.add(mainObj.optString("BankName"));
                detailArray.add(mainObj.optString("Branch"));
                detailArray.add(mainObj.optString("AccountNumber"));
                detailArray.add(mainObj.optString("IfscCode"));
                detailArray.add(mainObj.optString("BankDocument"));

                detailArray.add(mainObj.optString("BankId"));//6
                detailArray.add(mainObj.optString("Verified"));//7
                detailArray.add(mainObj.optString("Reason"));//8
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + "\n" + response;
                return null;
            }
        } else {
            return null;
        }
        return detailArray;
    }

    public static ArrayList<String> parseKYCDetail(String response) {
        ArrayList<String> detailArray;
        if (response.contains("{")) {
            try {
                JSONObject mainObj = new JSONObject(response);
                detailArray = new ArrayList<>();
                detailArray.add(mainObj.optString("PanNumber"));
                detailArray.add(mainObj.optString("ProofName"));
                detailArray.add(mainObj.optString("DocumentNumber"));
                detailArray.add(mainObj.optString("PersonName"));
                detailArray.add(mainObj.optString("NomineeName"));
                detailArray.add(mainObj.optString("NomineeMobile"));
                detailArray.add(mainObj.optString("NomineeAddress"));
                detailArray.add(mainObj.optString("PanDocument"));
                detailArray.add(mainObj.optString("ProofDocument"));//8

                detailArray.add(mainObj.optString("ProofId"));
                detailArray.add(mainObj.optString("Verified"));//10
                detailArray.add(mainObj.optString("Reason"));//11
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + "\n" + response;
                return null;
            }
        } else {
            return null;
        }
        return detailArray;
    }

    public static ArrayList<String> parseBusinessDetail(String response) {
        ArrayList<String> detailArray;
        if (response.contains("{")) {
            try {
                JSONObject mainObj = new JSONObject(response);
                detailArray = new ArrayList<>();
                detailArray.add(mainObj.optString("CompanyName"));
                detailArray.add(mainObj.optString("EntityName"));
                detailArray.add(mainObj.optString("ContactPerson"));
                detailArray.add(mainObj.optString("DesContactPerson"));
                detailArray.add(mainObj.optString("MobileNumber"));
                detailArray.add(mainObj.optString("Email"));
                detailArray.add(mainObj.optString("Landline1"));
                detailArray.add(mainObj.optString("Landline2"));
                detailArray.add(mainObj.optString("Website"));
                detailArray.add(mainObj.optString("Address") + ", " + mainObj.optString("CityName") + ", " + mainObj.optString("StateName") + ", " + mainObj.optString("Pincode"));

                detailArray.add(mainObj.optString("EntityId"));//10
                detailArray.add(mainObj.optString("Address"));
                detailArray.add(mainObj.optString("StateId"));//12
                detailArray.add(mainObj.optString("CityId"));
                detailArray.add(mainObj.optString("Pincode"));
                detailArray.add(mainObj.optString("CategoryId"));//15
                detailArray.add(mainObj.optString("SubCategoryIds"));
                detailArray.add(mainObj.optString("PaymentModeId"));
                detailArray.add(mainObj.optString("DayAndTimingId"));//18
                detailArray.add(mainObj.optString("CategoryName"));//19
                detailArray.add(mainObj.optString("StateName"));//20
                detailArray.add(mainObj.optString("CityName"));//21
                detailArray.add(mainObj.optString("CompanyLogo"));//22
                detailArray.add(mainObj.optString("DealingItem"));//23

            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + "\n" + response;
                return null;
            }
        } else {
            return null;
        }
        return detailArray;
    }

    public static DayAndTimeModel parseDayAndTimingIdJson(String response) {
        DayAndTimeModel model = new DayAndTimeModel();
        model.setOutput("ERROR-101");
        model.setList(new ArrayList<DayAndTimeModel>());
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                for (int i = 0; i < mainArray.length(); i++) {
                    JSONObject domainObj = mainArray.getJSONObject(i);
                    model.setOutput("success");
                    DayAndTimeModel slot = new DayAndTimeModel();
                    slot.setDayId(domainObj.optString("Id"));
                    switch (domainObj.optString("Id")) {
                        case "1":
                            slot.setDayName("Monday");
                            break;
                        case "2":
                            slot.setDayName("Tuesday");
                            break;
                        case "3":
                            slot.setDayName("Wednesday");
                            break;
                        case "4":
                            slot.setDayName("Thursday");
                            break;
                        case "5":
                            slot.setDayName("Friday");
                            break;
                        case "6":
                            slot.setDayName("Saturday");
                            break;
                        case "7":
                            slot.setDayName("Sunday");
                            break;
                    }
                    slot.setStartTime(domainObj.optString("StartTime"));
                    slot.setEndTime(domainObj.optString("EndTime"));
                    slot.setChecked(true);
                    model.getList().add(slot);
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + "\n" + response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return model;
    }

    public static String parseDayAndTimingIds(String response) {
        String res = "";
        if (response.contains("[")) {
            try {
                JSONArray mainArray = new JSONArray(response);
                if (mainArray != null) {
                    for (int i = 0; i < mainArray.length(); i++) {
                        JSONObject domainObj = mainArray.getJSONObject(i);
                        switch (domainObj.optString("Id")) {
                            case "1":
                                res += "Monday" + ",";
                                break;
                            case "2":
                                res += "Tuesday" + ",";
                                break;
                            case "3":
                                res += "Wednesday" + ",";
                                break;
                            case "4":
                                res += "Thursday" + ",";
                                break;
                            case "5":
                                res += "Friday" + ",";
                                break;
                            case "6":
                                res += "Saturday" + ",";
                                break;
                            case "7":
                                res += "Sunday" + ",";
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + "\n" + response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return res;
    }

    public static DayAndTimeModel parseDayAndTimingIdJson() {
        DayAndTimeModel model = new DayAndTimeModel();
        model.setOutput("success");
        model.setList(new ArrayList<DayAndTimeModel>());

        model.getList().add(setSlotDayAndTimeModel("1", "Monday", "10:00 AM", "6:00 PM"));
        model.getList().add(setSlotDayAndTimeModel("2", "Tuesday", "10:00 AM", "6:00 PM"));
        model.getList().add(setSlotDayAndTimeModel("3", "Wednesday", "10:00 AM", "6:00 PM"));
        model.getList().add(setSlotDayAndTimeModel("4", "Thursday", "10:00 AM", "6:00 PM"));
        model.getList().add(setSlotDayAndTimeModel("5", "Friday", "10:00 AM", "6:00 PM"));
        model.getList().add(setSlotDayAndTimeModel("6", "Saturday", "10:00 AM", "6:00 PM"));
        model.getList().add(setSlotDayAndTimeModel("7", "Sunday", "10:00 AM", "6:00 PM"));

        return model;
    }

    private static DayAndTimeModel setSlotDayAndTimeModel(String id, String day, String startTime, String endTime) {
        DayAndTimeModel slot = new DayAndTimeModel();
        slot.setDayId(id);
        slot.setDayName(day);
        slot.setStartTime(startTime);
        slot.setEndTime(endTime);
        slot.setChecked(false);
        return slot;
    }

    public static BankModel parsePaymoentModeIdJson(String response) {
        BankModel model = new BankModel();
        model.setOutput("ERROR-101");
        model.setList(new ArrayList<BankModel>());
        if (response.contains("[")) {
            try {
                JSONArray main = new JSONArray(response);
                for (int i = 0; i < main.length(); i++) {
                    model.setOutput("success");
                    BankModel slot = new BankModel();
                    slot.setId(main.optString(i));
                    model.getList().add(slot);
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString() + "\n" + response;
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return model;
    }


    public Model parseMobileVerify(Context context, String response) {
        Model jsonModel = new Model();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("IOException")) {
            jsonModel.setOutput("internet");
            jsonModel.setOutputMsg(response);
            return jsonModel;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        jsonModel.setOutput("success");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                        Signup.setMobileOtp(context, mainObject.getString("otp"));
                    } else if (mainObject.getString("status").equals("failure")) {
                        jsonModel.setOutput("failure");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput("server");
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput("server");
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public Model parseMobileOtpVerify(Context context, String response) {
        Model jsonModel = new Model();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("IOException")) {
            jsonModel.setOutput("internet");
            jsonModel.setOutputMsg(response);
            return jsonModel;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        jsonModel.setOutput("success");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                        Signup.setMobileOtp(context, mainObject.getString("otp"));
                    } else if (mainObject.getString("status").equals("failure")) {
                        jsonModel.setOutput("failure");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput("server");
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput("server");
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public Model parseEmailVerificationDate(Context context, String response) {
        Model jsonModel = new Model();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("IOException")) {
            jsonModel.setOutput("internet");
            jsonModel.setOutputMsg(response);
            return jsonModel;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        jsonModel.setOutput("success");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                        Signup.setEmailOtp(context, mainObject.getString("otp"));
                    } else if (mainObject.getString("status").equals("failure")) {
                        jsonModel.setOutput("failure");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput("server");
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput("server");
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static PaymentModel parseBayerDetail(String response) {
        PaymentModel jsonModel = new PaymentModel();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("IOException")) {
            jsonModel.setOutput("internet");
            jsonModel.setOutputMsg(response);
            return jsonModel;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        JSONObject msgObj = mainObject.optJSONObject("message");
                        if (msgObj != null) {
                            jsonModel.setOutput("success");
                            jsonModel.setOfferKey(msgObj.optString("offer_key"));
                            jsonModel.setTxnId(msgObj.optString("transactionId"));
                            jsonModel.setProductInfo(msgObj.optString("package"));
                            jsonModel.setCapping(msgObj.optString("capping"));
                            jsonModel.setAmount(msgObj.optString("amount"));
                            jsonModel.setTax(msgObj.optString("tax"));
                            jsonModel.setName(msgObj.optString("name"));
                            jsonModel.setReferenceName(msgObj.optString("referrerName"));
                            jsonModel.setEmail(msgObj.optString("email"));
                        }
                    } else if (mainObject.getString("status").equals("failure")) {
                        jsonModel.setOutput("failure");
                        jsonModel.setOutputMsg(mainObject.optString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput("server");
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput("server");
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    public static PaymentModel parsePaymentSuccess(String response) {
        PaymentModel jsonModel = new PaymentModel();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("IOException")) {
            jsonModel.setOutput("internet");
            jsonModel.setOutputMsg(response);
            return jsonModel;
        }else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString("status").equals("success")) {
                        jsonModel.setOutput("success");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    } else if (mainObject.getString("status").equals("failure")) {
                        jsonModel.setOutput("failure");
                        jsonModel.setOutputMsg(mainObject.getString("message"));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput("server");
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput("server");
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }


    public static PaymentModel parseResponseFromPayuMoney(String responseData,PaymentConstant paymentConstant) {
        PaymentModel model;
        try {
            JSONObject resObj = new JSONObject(responseData);
            model = new PaymentModel();
            model.setId(resObj.optString("id"));
            model.setStatus(resObj.optString("status"));
            model.setTxnId(resObj.optString("txnid"));
            model.setTransaction_fee(resObj.optString("transaction_fee"));
            String amt=resObj.optString("amount");
            String disc=resObj.optString("discount");
            model.setAmount(amt);
            model.setDiscount(disc);
            float amount = Float.parseFloat(amt);
            float discount = Float.parseFloat(disc);
            model.setNetAmount(String.valueOf(amount-discount));
            model.setAdditional_charges(resObj.optString("additional_charges"));
            model.setProductInfo(resObj.optString("productinfo"));
            model.setEmail(resObj.optString("email"));
            model.setPackageAmt(paymentConstant.getPkgAMT());
            model.setPlacement(paymentConstant.getPlacement());
            model.setPurchaseType(paymentConstant.getPurchaseType());
            model.setPackageId(paymentConstant.getPkgId());
            model.setRefrenceNo(paymentConstant.getRefNo());
            model.setCapping(paymentConstant.getCapping());
            model.setError_code(resObj.optString("error_code"));
            model.setError_Message(resObj.optString("Error_Message"));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;

        }
        return model;
    }


}
