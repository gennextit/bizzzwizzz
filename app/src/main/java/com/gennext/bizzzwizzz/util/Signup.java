package com.gennext.bizzzwizzz.util;

import android.content.Context;
import android.text.TextUtils;

/**
 * Created by Abhijit-PC on 04-Mar-17.
 */

public class Signup {

    public static final String COMMON = "bizzzwizzz";
    private static final String MOBILE_TEMP = "mtempVeri" + COMMON;
    private static final String EMAIL_TEMP = "emailtempVeri" + COMMON;
    private static final String MOBILE_VERIFICATION = "mVeri" + COMMON;
    private static final String EMAIL_VERIFICATION = "emailVeri" + COMMON;
    private static final String PERSONAL_VERIFICATION = "personalVeri" + COMMON;
    private static final String MOBILE_OTP ="mOtp" + COMMON;
    private static final String EMAIL_OTP = "eOtp" + COMMON;

    public static void setMobileTemp(Context context, String mobile) {
        Utility.SavePref(context, MOBILE_TEMP, mobile);
    }

    public static void setEmailTemp(Context context, String email) {
        Utility.SavePref(context, EMAIL_TEMP, email);
    }
    public static String getMobileTemp(Context context) {
        return Utility.LoadPref(context, MOBILE_TEMP);
    }

    public static String getEmailTemp(Context context) {
        return Utility.LoadPref(context, EMAIL_TEMP);
    }


    public static boolean isMobileVerified(Context context) {
        if (TextUtils.isEmpty(Utility.LoadPref(context, MOBILE_VERIFICATION))) {
            return false;
        } else {
            return true;
        }
    }

    public static void setMobileVerification(Context context,String output) {
        Utility.SavePref(context, MOBILE_VERIFICATION, output);
    }

    public static boolean isEmailVerified(Context context) {
        if (TextUtils.isEmpty(Utility.LoadPref(context, EMAIL_VERIFICATION))) {
            return false;
        } else {
            return true;
        }
    }

    public static void setEmailVerification(Context context,String output) {
        Utility.SavePref(context, EMAIL_VERIFICATION, output);
    }

    public static boolean isPersonalVerified(Context context) {
        if (TextUtils.isEmpty(Utility.LoadPref(context, PERSONAL_VERIFICATION))) {
            return false;
        } else {
            return true;
        }
    }

    public static void setPersonalVerification(Context context) {
        Utility.SavePref(context, PERSONAL_VERIFICATION, "success");
    }

    public static void setMobileOtp(Context context, String otp) {
        Utility.SavePref(context, MOBILE_OTP, otp);
    }
    public static String getMobileOtp(Context context) {
        return Utility.LoadPref(context, MOBILE_OTP);
    }
    public static void setEmailOtp(Context context, String otp) {
        Utility.SavePref(context, EMAIL_OTP, otp);
    }
    public static String getEmailOtp(Context context) {
        return Utility.LoadPref(context, EMAIL_OTP);
    }
}
