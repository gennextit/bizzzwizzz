package com.gennext.bizzzwizzz.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.PopupAlert;

public class CompactFragment extends Fragment {
    ProgressDialog progressDialog;
    boolean conn = false;
    public AlertDialog dialog = null;
    public static int OPEN_TO_ALL = 1, INVITE_ONLY = 2, PAID_ENTRY = 3;
    LinearLayout llProgress, llButton;
    private AlertDialog alertDialog;
    public Uri profileImageUri;

    /*DrawerLayout dLayout;
    ListView dList;
    List<SideMenu> sideMenuList;
    SideMenuAdapter slideMenuAdapter;
*/
    public CompactFragment() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }

    public boolean APICheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return true;
        } else {
            return false;
        }
    }




    public void initToolBar(final Activity act, View v, String title) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        AppCompatActivity activity = (AppCompatActivity) act;
        activity.setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.mipmap.ic_back);
        toolbar.setNavigationOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeybord(act);
                        getFragmentManager().popBackStack();
                    }
                }

        );
    }

    public void initToolBarForActivity(final Activity act, View v, String title) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        AppCompatActivity activity = (AppCompatActivity) act;
        activity.setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.mipmap.ic_back);
        toolbar.setNavigationOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideKeybord(act);
                        act.finish();
                    }
                }

        );
    }


    public void closeFragmentDialog(String dialogName) {
        FragmentManager fragmentManager = getFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    protected void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public void showPopupAlert(String title, String description, int finishType) {
        showPopupAlert(title, description, finishType,0);
    }

    public void showPopupAlert(String title, String description, int finishType,int popupTimeOutInSec) {
        PopupAlert popupAlert = PopupAlert.newInstance(title, description, finishType, popupTimeOutInSec);
        AppAnimation.setDialogAnimation(getContext(),popupAlert);
        addFragment(popupAlert,"popupAlert");
    }


    public static void hideKeybord(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = activity.getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    public void showProgressDialog(Context context, String msg) {
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progressDialog.setMessage(msg);
//        progressDialog.setIndeterminate(false);
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        PDialog cDialog=PDialog.newInstance(context,msg);
        alertDialog=cDialog.show();
    }

    public void hideProgressDialog() {
//        if (progressDialog != null)
//            progressDialog.dismiss();
        if (alertDialog != null)
            alertDialog.dismiss();
    }

    public void setTypsFace(EditText et, EditText et1, EditText et2, EditText et3, EditText et4) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
        et1.setTypeface(externalFont);
        et2.setTypeface(externalFont);
        et3.setTypeface(externalFont);
        et4.setTypeface(externalFont);
    }

    public void setTypsFace(AutoCompleteTextView et1, AutoCompleteTextView et2, AutoCompleteTextView et3, AutoCompleteTextView et4) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et1.setTypeface(externalFont);
        et2.setTypeface(externalFont);
        et3.setTypeface(externalFont);
        et4.setTypeface(externalFont);
    }

    public void setTypsFace(EditText et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public void setTypsFace(CheckBox et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public void setTypsFace(TextView et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public void setTypsFace(AutoCompleteTextView et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public void setTypsFace(Button et) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
        et.setTypeface(externalFont);
    }

    public String LoadPref(String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public void SavePref(String key, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }


    public String getSt(int id) {
        return getResources().getString(id);
    }

    public boolean isOnline() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getActivity()
                .getSystemService(getActivity().CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        if (haveConnectedWifi == true || haveConnectedMobile == true) {
            L.m("Log-Wifi" + String.valueOf(haveConnectedWifi));
            L.m("Log-Mobile" + String.valueOf(haveConnectedMobile));
            conn = true;
        } else {
            L.m("Log-Wifi" + String.valueOf(haveConnectedWifi));
            L.m("Log-Mobile" + String.valueOf(haveConnectedMobile));
            conn = false;
        }

        return conn;
    }

//    public String getMACAddress(Activity activity) {
//        // TODO Auto-generated method stub
//        String address;
//        WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
//
//        if (wifiManager.isWifiEnabled()) {
//            // WIFI ALREADY ENABLED. GRAB THE MAC ADDRESS HERE
//            WifiInfo info = wifiManager.getConnectionInfo();
//            address = info.getMacAddress();
//            // Toast.makeText(getBaseContext(),address,
//            // Toast.LENGTH_SHORT).show();
//
//        } else {
//            // ENABLE THE WIFI FIRST
//            wifiManager.setWifiEnabled(true);
//
//            // WIFI IS NOW ENABLED. GRAB THE MAC ADDRESS HERE
//            WifiInfo info = wifiManager.getConnectionInfo();
//            address = info.getMacAddress();
//            // Toast.makeText(getBaseContext(),address,
//            // Toast.LENGTH_SHORT).show();
//
//            // DISABLE THE WIFI
//            wifiManager.setWifiEnabled(false);
//
//        }
//        return address;
//    }


    public void showInternetAlertBox(Activity activity) {
        showDefaultAlertBox(activity, getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg));
    }

    public void showDefaultAlertBox(Activity activity, String title, String Description) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_dialog, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);

        tvTitle.setText(title);
        tvDescription.setText(Description);

        button1.setText("Setting");
        button1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                EnableMobileIntent();
            }
        });
        button2.setText("Cancel");
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

    }

    public void EnableMobileIntent() {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.provider.Settings.ACTION_SETTINGS);
        startActivity(intent);

    }

    public void hideBaseServerErrorAlertBox() {
        if (dialog != null)
            dialog.dismiss();
    }

//    public Button showBaseServerErrorAlertBox(String errorDetail) {
//        return showBaseAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), errorDetail);
//    }
//
//    public Button showBaseServerErrorAlertBox() {
//        return showBaseAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), null);
//    }
//
//    public Button showBaseAlertBox(String title, String Description, final String errorMessage) {
//
//        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getActivity().getLayoutInflater();
//
//        View v = inflater.inflate(R.layout.alert_internet, null);
//        dialogBuilder.setView(v);
//        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
//        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
//        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
//        //TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
//        final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
//        ivAbout.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                if (errorMessage != null) {
//                    tvDescription.setText(errorMessage);
//                }
//            }
//        });
//
//        tvDescription.setText(Description);
//
//        button1.setText("Retry");
//
//        button2.setText("Cancel");
//        button2.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Close dialog
//                dialog.dismiss();
//            }
//        });
//
//        dialog = dialogBuilder.create();
//        dialog.show();
//        return button1;
//    }

    protected void replaceFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void setFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    public void addFragment(Fragment fragment, String tag) {
        addFragment(fragment, android.R.id.content, tag);
    }

    public void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }
    public void addFragmentWithoutBackStack(Fragment fragment, String tag) {
        addFragmentWithoutBackStack(fragment, android.R.id.content, tag);
    }
    public void addFragmentWithoutBackStack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.commitAllowingStateLoss();
    }

    public void setUnSetFragment(FragmentManager manager, int containerId, Fragment addFragment, String fragTag
            , Fragment rFrag1) {
        FragmentTransaction ft = manager.beginTransaction();
        if (addFragment != null) {
            if (addFragment.isAdded()) { // if the fragment is already in container
                ft.show(addFragment);
            } else { // fragment needs to be added to frame container
                ft.add(containerId, addFragment, fragTag);
            }
        }

        // Hide other fragments
        if (rFrag1 != null && rFrag1.isAdded()) {
            ft.hide(rFrag1);
        }

        ft.commit();
    }
}
