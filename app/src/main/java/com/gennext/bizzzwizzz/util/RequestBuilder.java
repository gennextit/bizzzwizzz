package com.gennext.bizzzwizzz.util;

import android.net.Uri;

import java.io.File;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class RequestBuilder {
    public static RequestBody Default(String userId) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("userId", userId == null ? "" : userId)
                .build();
        return formBody;
    }

    public static RequestBody LoginBody(String username, String password) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("userId", username == null ? "" : username)
                .addEncoded("password", password == null ? "" : password)
                .build();
        return formBody;
    }

    public static RequestBody UpdateProfile(String userId, String name, String mobile, String gender, String email
            , String dob, String state, String city, String pinCode, String address, String landline, Uri mImageUri) {
        if (mImageUri == null) {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("userId", userId == null ? "" : userId)
                    .addFormDataPart("name", name == null ? "" : name)
                    .addFormDataPart("mobile", mobile == null ? "" : mobile)
                    .addFormDataPart("gender", gender == null ? "" : gender)
                    .addFormDataPart("email", email == null ? "" : email)
                    .addFormDataPart("dob", dob == null ? "" : dob)
                    .addFormDataPart("state", state == null ? "" : state)
                    .addFormDataPart("city", city == null ? "" : city)
                    .addFormDataPart("pinCode", pinCode == null ? "" : pinCode)
                    .addFormDataPart("address", address == null ? "" : address)
                    .addFormDataPart("landline", landline == null ? "" : landline)
                    .build();
            return formBody;
        } else {
            File mFile = new File(mImageUri.getPath());
//            File mFile = new File(mImageUri.toString());
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("profileImage", mFile.getName(),
                            RequestBody.create(MediaType.parse("text/plain"), mFile))
                    .addFormDataPart("userId", userId == null ? "" : userId)
                    .addFormDataPart("name", name == null ? "" : name)
                    .addFormDataPart("mobile", mobile == null ? "" : mobile)
                    .addFormDataPart("gender", gender == null ? "" : gender)
                    .addFormDataPart("email", email == null ? "" : email)
                    .addFormDataPart("dob", dob == null ? "" : dob)
                    .addFormDataPart("state", state == null ? "" : state)
                    .addFormDataPart("city", city == null ? "" : city)
                    .addFormDataPart("pinCode", pinCode == null ? "" : pinCode)
                    .addFormDataPart("address", address == null ? "" : address)
                    .addFormDataPart("landline", landline == null ? "" : landline)
                    .build();
            return formBody;
        }
    }


    public static RequestBody BSlotRequest(String domainId, String date) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("domain", domainId == null ? "" : domainId)
                .addEncoded("date", date == null ? "" : date)
                .build();
        return formBody;
    }

    public static RequestBody BookingRequest(String domainId, String date, String time, String description, String userId, File sltFile) {

        if (sltFile == null) {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("userId", userId == null ? "" : userId)
                    .addFormDataPart("domain", domainId == null ? "" : domainId)
                    .addFormDataPart("date", date == null ? "" : date)
                    .addFormDataPart("slotTime", time == null ? "" : time + ":00")
                    .addFormDataPart("description", description == null ? "" : description)
                    .build();
            return formBody;
        } else {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("image", sltFile.getName(),
                            RequestBody.create(MediaType.parse("text/plain"), sltFile))
                    .addFormDataPart("userId", userId == null ? "" : userId)
                    .addFormDataPart("domain", domainId == null ? "" : domainId)
                    .addFormDataPart("date", date == null ? "" : date)
                    .addFormDataPart("slotTime", time == null ? "" : time + ":00")
                    .addFormDataPart("description", description == null ? "" : description)
                    .build();
            return formBody;
        }
    }

    public static RequestBody RaiseReq(String userId, String sltReqType, String detail, String requestDocument) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("userId", userId == null ? "" : userId)
                .addEncoded("requestType", sltReqType == null ? "" : sltReqType)
                .addEncoded("requestDetails", detail == null ? "" : detail)
                .addEncoded("requestDocument", requestDocument == null ? "" : requestDocument)
                .build();
        return formBody;
    }

    public static RequestBody CancelBooking(String userId, String slotDate, String slotTime) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("userId", userId == null ? "" : userId)
                .addEncoded("date", slotDate == null ? "" : slotDate)
                .addEncoded("time", slotTime == null ? "" : slotTime)
                .build();
        return formBody;
    }

    //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook
    public static RequestBody UpdateBankDetail(String userId, String personName, String sltBankName, String branch,
                                               String accountNumber, String ifscCode, File sltBankFile, String sltBankFilePath) {
        if (sltBankFilePath != null) {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("checkbookPassbook", sltBankFile.getName(),
                            RequestBody.create(MediaType.parse("text/plain"), sltBankFile))
                    .addFormDataPart("userId", userId == null ? "" : userId)
                    .addFormDataPart("personName", personName == null ? "" : personName)
                    .addFormDataPart("bankId", sltBankName == null ? "" : sltBankName)
                    .addFormDataPart("branch", branch == null ? "" : branch)
                    .addFormDataPart("accountNumber", accountNumber == null ? "" : accountNumber)
                    .addFormDataPart("ifscCode", ifscCode == null ? "" : ifscCode)
                    .build();
            return formBody;
        } else {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("userId", userId == null ? "" : userId)
                    .addFormDataPart("personName", personName == null ? "" : personName)
                    .addFormDataPart("bankId", sltBankName == null ? "" : sltBankName)
                    .addFormDataPart("branch", branch == null ? "" : branch)
                    .addFormDataPart("accountNumber", accountNumber == null ? "" : accountNumber)
                    .addFormDataPart("ifscCode", ifscCode == null ? "" : ifscCode)
                    .build();
            return formBody;
        }
    }

    //panNumber,identityProofId,idNumber,personName,nomineeName,nomineeMobile,nomineeAddress,userId,panDocument,identityDocument
    public static RequestBody UpdateKYCDetail(String userId, String panNumber, String sltIdentityProof,
                                              String idNum, String personName, String nameOfNominee,
                                              String nomineeMobile, String nomineeAddress, File sltPANFile, File sltIdFile,
                                              String sltIDFilePath, String sltPANFilePath) {
        if (sltIDFilePath != null && sltPANFilePath != null) {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("panDocument", sltPANFile.getName(),
                            RequestBody.create(MediaType.parse("text/plain"), sltPANFile))
                    .addFormDataPart("identityDocument", sltIdFile.getName(),
                            RequestBody.create(MediaType.parse("text/plain"), sltIdFile))
                    .addFormDataPart("userId", userId == null ? "" : userId)
                    .addFormDataPart("panNumber", panNumber == null ? "" : panNumber)
                    .addFormDataPart("identityProofId", sltIdentityProof == null ? "" : sltIdentityProof)
                    .addFormDataPart("idNumber", idNum == null ? "" : idNum)
                    .addFormDataPart("personName", personName == null ? "" : personName)
                    .addFormDataPart("nomineeName", nameOfNominee == null ? "" : nameOfNominee)
                    .addFormDataPart("nomineeMobile", nomineeMobile == null ? "" : nomineeMobile)
                    .addFormDataPart("nomineeAddress", nomineeAddress == null ? "" : nomineeAddress)
                    .build();
            return formBody;
        } else if (sltIDFilePath != null) {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("identityDocument", sltIdFile.getName(),
                            RequestBody.create(MediaType.parse("text/plain"), sltIdFile))
                    .addFormDataPart("userId", userId == null ? "" : userId)
                    .addFormDataPart("panNumber", panNumber == null ? "" : panNumber)
                    .addFormDataPart("identityProofId", sltIdentityProof == null ? "" : sltIdentityProof)
                    .addFormDataPart("idNumber", idNum == null ? "" : idNum)
                    .addFormDataPart("personName", personName == null ? "" : personName)
                    .addFormDataPart("nomineeName", nameOfNominee == null ? "" : nameOfNominee)
                    .addFormDataPart("nomineeMobile", nomineeMobile == null ? "" : nomineeMobile)
                    .addFormDataPart("nomineeAddress", nomineeAddress == null ? "" : nomineeAddress)
                    .build();
            return formBody;
        } else if (sltPANFilePath != null) {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("panDocument", sltPANFile.getName(),
                            RequestBody.create(MediaType.parse("text/plain"), sltPANFile))
                    .addFormDataPart("userId", userId == null ? "" : userId)
                    .addFormDataPart("panNumber", panNumber == null ? "" : panNumber)
                    .addFormDataPart("identityProofId", sltIdentityProof == null ? "" : sltIdentityProof)
                    .addFormDataPart("idNumber", idNum == null ? "" : idNum)
                    .addFormDataPart("personName", personName == null ? "" : personName)
                    .addFormDataPart("nomineeName", nameOfNominee == null ? "" : nameOfNominee)
                    .addFormDataPart("nomineeMobile", nomineeMobile == null ? "" : nomineeMobile)
                    .addFormDataPart("nomineeAddress", nomineeAddress == null ? "" : nomineeAddress)
                    .build();
            return formBody;
        } else {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("userId", userId == null ? "" : userId)
                    .addFormDataPart("panNumber", panNumber == null ? "" : panNumber)
                    .addFormDataPart("identityProofId", sltIdentityProof == null ? "" : sltIdentityProof)
                    .addFormDataPart("idNumber", idNum == null ? "" : idNum)
                    .addFormDataPart("personName", personName == null ? "" : personName)
                    .addFormDataPart("nomineeName", nameOfNominee == null ? "" : nameOfNominee)
                    .addFormDataPart("nomineeMobile", nomineeMobile == null ? "" : nomineeMobile)
                    .addFormDataPart("nomineeAddress", nomineeAddress == null ? "" : nomineeAddress)
                    .build();
            return formBody;
        }
    }

    public static RequestBody UpdateBusinessDetail(String userId, String compName, String sltEntityType
            , String contactPerson, String destContactPerson, String mobile, String email, String landline1
            , String landline2, String website, String address, String stateId, String cityId, String pinCode
            , String sltDayAndTimeIds, String sltModeOfPayment, String sltCategoryId, String sltSubCategoryIds
            , String dealInIteams, Uri mImageUri) {
        if (mImageUri != null) {
            File mFile = new File(mImageUri.getPath());
//            File mFile = new File(mImageUri.toString());
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("CompanyLogo", mFile.getName(),
                            RequestBody.create(MediaType.parse("text/plain"), mFile))
                    .addFormDataPart("UserId", userId == null ? "" : userId)
                    .addFormDataPart("CompanyName", compName == null ? "" : compName)
                    .addFormDataPart("EntityId", sltEntityType == null ? "" : sltEntityType)
                    .addFormDataPart("ContactPerson", contactPerson == null ? "" : contactPerson)
                    .addFormDataPart("DesContactPerson", destContactPerson == null ? "" : destContactPerson)
                    .addFormDataPart("MobileNumber", mobile == null ? "" : mobile)
                    .addFormDataPart("Email", email == null ? "" : email)
                    .addFormDataPart("Landline1", landline1 == null ? "" : landline1)
                    .addFormDataPart("Landline2", landline2 == null ? "" : landline2)
                    .addFormDataPart("Website", website == null ? "" : website)
                    .addFormDataPart("Address", address == null ? "" : address)
                    .addFormDataPart("StateId", stateId == null ? "" : stateId)
                    .addFormDataPart("Country", "India")
                    .addFormDataPart("CityId", cityId == null ? "" : cityId)
                    .addFormDataPart("Pincode", pinCode == null ? "" : pinCode)
                    .addFormDataPart("CategoryId", sltCategoryId == null ? "" : sltCategoryId)
                    .addFormDataPart("SubCategoryIds", sltSubCategoryIds == null ? "" : sltSubCategoryIds)
                    .addFormDataPart("DayAndTimingId", sltDayAndTimeIds == null ? "" : sltDayAndTimeIds)
                    .addFormDataPart("DealingItem", dealInIteams == null ? "" : dealInIteams)
                    .addFormDataPart("PaymentModeId", sltModeOfPayment == null ? "" : sltModeOfPayment)
                    .build();
            return formBody;
        } else {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("UserId", userId == null ? "" : userId)
                    .addFormDataPart("CompanyName", compName == null ? "" : compName)
                    .addFormDataPart("EntityId", sltEntityType == null ? "" : sltEntityType)
                    .addFormDataPart("ContactPerson", contactPerson == null ? "" : contactPerson)
                    .addFormDataPart("DesContactPerson", destContactPerson == null ? "" : destContactPerson)
                    .addFormDataPart("MobileNumber", mobile == null ? "" : mobile)
                    .addFormDataPart("Email", email == null ? "" : email)
                    .addFormDataPart("Landline1", landline1 == null ? "" : landline1)
                    .addFormDataPart("Landline2", landline2 == null ? "" : landline2)
                    .addFormDataPart("Website", website == null ? "" : website)
                    .addFormDataPart("Address", address == null ? "" : address)
                    .addFormDataPart("StateId", stateId == null ? "" : stateId)
                    .addFormDataPart("Country", "India")
                    .addFormDataPart("CityId", cityId == null ? "" : cityId)
                    .addFormDataPart("Pincode", pinCode == null ? "" : pinCode)
                    .addFormDataPart("CategoryId", sltCategoryId == null ? "" : sltCategoryId)
                    .addFormDataPart("SubCategoryIds", sltSubCategoryIds == null ? "" : sltSubCategoryIds)
                    .addFormDataPart("DayAndTimingId", sltDayAndTimeIds == null ? "" : sltDayAndTimeIds)
                    .addFormDataPart("DealingItem", dealInIteams == null ? "" : dealInIteams)
                    .addFormDataPart("PaymentModeId", sltModeOfPayment == null ? "" : sltModeOfPayment)
                    .build();
            return formBody;
        }
    }

    public static RequestBody FeedbackDetail(String userId, String feedback) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("userId", userId == null ? "" : userId)
                .addEncoded("feedback", feedback == null ? "" : feedback)
                .build();
        return formBody;
    }

    public static RequestBody raiseRequest(String userId, String sltReqType, String description, File file) {
        if (file != null) {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("requestDocument", file.getName(),
                            RequestBody.create(MediaType.parse("text/plain"), file))
                    .addFormDataPart("userId", userId == null ? "" : userId)
                    .addFormDataPart("requestDetails", description == null ? "" : description)
                    .addFormDataPart("requestType", sltReqType == null ? "" : sltReqType)
                    .build();
            return formBody;
        } else {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("userId", userId == null ? "" : userId)
                    .addFormDataPart("requestDetails", description == null ? "" : description)
                    .addFormDataPart("requestType", sltReqType == null ? "" : sltReqType)
                    .build();
            return formBody;
        }
    }

    public static RequestBody Signup(String name, String mobile, String email, String panNo, String pass,
                                     String profileType) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("name", name == null ? "" : name)
                .addEncoded("mobile", mobile == null ? "" : mobile)
                .addEncoded("email", email == null ? "" : email)
                .addEncoded("panNo", panNo == null ? "" : panNo)
                .addEncoded("pass", pass == null ? "" : pass)
                .addEncoded("profileType", profileType == null ? "" : profileType)
                .build();
        return formBody;
    }

    public static RequestBody VerifyMobile(String mobile) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("mobile", mobile == null ? "" : mobile)
                .build();
        return formBody;
    }

    public static RequestBody VerifyOtp(String mobile, String otp) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("mobile", mobile == null ? "" : mobile)
                .addEncoded("otp", otp == null ? "" : otp)
                .build();
        return formBody;
    }

    public static RequestBody EmailVerification(String email) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("email", email == null ? "" : email)
                .build();
        return formBody;
    }

    public static RequestBody ConsultantFeedback(String userId, String domainId, String feedback, String rating) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("userId", userId == null ? "" : userId)
                .addEncoded("domainId", domainId == null ? "" : domainId)
                .addEncoded("feedback", feedback == null ? "" : feedback)
                .addEncoded("rating", rating == null ? "" : rating)
                .build();
        return formBody;
    }

    public static RequestBody getbayersDetail(String userId, String pkgId, String refNo) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("userId", userId == null ? "" : userId)
                .addEncoded("packageId", pkgId == null ? "" : pkgId)
                .addEncoded("referenceNumber", refNo == null ? "" : refNo)
                .build();
        return formBody;
    }

    public static RequestBody getbayersDetail(String userId, String pkgId) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("userId", userId == null ? "" : userId)
                .addEncoded("packageId", pkgId == null ? "" : pkgId)
                .build();
        return formBody;
    }

    public static RequestBody successfillBuyPackage(String userId, String packageId, String txID, String bankRefID
            , String packageAmount, String netAmount, String capping, String referenceNumber, String position
            , String customerType) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("userId", userId == null ? "" : userId)
                .addEncoded("packageId", packageId == null ? "" : packageId)
                .addEncoded("txID", txID == null ? "" : txID)
                .addEncoded("bankRefID", bankRefID == null ? "" : bankRefID)
                .addEncoded("packageAmount", packageAmount == null ? "" : packageAmount)
                .addEncoded("netAmount", netAmount == null ? "" : netAmount)
                .addEncoded("capping", capping == null ? "" : capping)
                .addEncoded("referenceNumber", referenceNumber == null ? "" : referenceNumber)
                .addEncoded("position", position == null ? "" : position)
                .addEncoded("customerType", customerType == null ? "" : customerType)
                .build();
        return formBody;
    }

    public static RequestBody ErrorReport(String report) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("reportLog", report)
                .build();
        return formBody;
    }
}
