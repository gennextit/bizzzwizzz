package com.gennext.bizzzwizzz.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Abhijit on 23-Dec-16.
 */
public class Utility {

    private static AlertDialog aDialog;

    private static int arrayIndexFindingByMatchString(List<String>list, String userSTring){
        return Arrays.asList(list).indexOf(userSTring);
    }

    public static void SavePref(Context context,String key, String value) {
       if (context != null && value != null) {
           SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
           SharedPreferences.Editor editor = sharedPreferences.edit();
           editor.putString(key, value);
            editor.apply();
        }
    }
    public static String LoadPref(Context context, String key) {
        if(context!=null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String data = sharedPreferences.getString(key, "");
            return data;
        }
        return "";
    }

    public static void showProgressDialog(Activity context, String msg) {
        PDialog cDialog = PDialog.newInstance(context, msg);
        aDialog = cDialog.show();
    }

    public static void hideProgressDialog() {
        if (aDialog != null)
            aDialog.dismiss();
    }

    public static String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public static void getWhatsAppMsg(Activity activity) {
        Intent waIntent = new Intent(Intent.ACTION_SEND);
        waIntent.setType("text/plain");
        String text = "Sorry For Interruption,I'm Just Trying Something";
        waIntent.setPackage("com.whatsapp");
        if (waIntent != null) {
            waIntent.putExtra(Intent.EXTRA_TEXT, text);//
            activity.startActivity(Intent.createChooser(waIntent, "Share with"));
        }
    }
    public static void openWhatsAppMsgToSpecificPerson(Activity activity,String number) {
//        Intent intent = new Intent(Intent.ACTION_SENDTO,Uri.parse("smsto:<whatsappnumber>");
//        intent.setpackage("com.whatsapp");
//        startActivity(intent);
        Intent sendIntent = new Intent("android.intent.action.MAIN");
        sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
        sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(number)+"@s.whatsapp.net");

        activity.startActivity(sendIntent);
    }
    public static void sendWhatsAppMsgToSpecificPerson(Activity activity,String number,String message) {
        Intent sendIntent = new Intent("android.intent.action.MAIN");
        sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
        sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(number)+"@s.whatsapp.net");
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);

        activity.startActivity(sendIntent);
    }


}
