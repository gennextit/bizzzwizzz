package com.gennext.bizzzwizzz.util;

/**
 * Created by Abhijit on 13-Dec-16.
 */

/**
 * ERROR-101 (Unformetted Json Response)
 * ERROR-102 ()
 */
public class AppSettings {

    public static final String SERVER = "http://bizzzwizzz.com";
    public static final String COMMON = SERVER+ "/index.php/Rest/";
    public static final String LOGIN = COMMON + "login";

    public static final String TERMS_OF_USE  = SERVER + "/files/Terms%20of%20Use.pdf";
    public static final String TERMS_OF_USE_DIRECT  = SERVER + "/files/Terms%20of%20Use%20for%20Direct%20Seller.pdf";
    public static final String PRIVACY_POLICY  = SERVER + "/files/Privacy%20Policy.pdf";

    public static final String GET_SLOTS = COMMON + "getSlots";//domain date
    public static final String BOOK_SLOT = COMMON + "bookSlot";//domain date slotTime description userId
    public static final String GENERATE_TREE_VIEW = COMMON + "generateTreeView"; //userId

    public static final String MY_PAYOUTS = COMMON + "myPayouts"; //userId
    public static final String DIRECT_REFERRAL_DETAILS = COMMON + "directReferralDetails"; //userId
    public static final String MY_SALES = COMMON + "otherIncomeDetails"; //userId
    public static final String ALL_REQUESTS = COMMON + "allrequests"; //userId
    public static final String RAISE_REQUEST = COMMON + "raiseRequest"; //requestType,requestDetails,userId,requestDocument

    public static final String UPDATE_PROFILE = COMMON + "updateProfile"; //name,mobile,gender,landline,dob,email,state,city,address,userId
    public static final String UPGRADE_PACKAGE = COMMON + "upgradePackage"; //userId,packageId

    public static final String AVAILABLE_SLOTS = COMMON + "availableSlots"; //userId
    public static final String CANCEL_SLOT = COMMON + "cancelSlot"; //userId,date,time

    public static final String PUT_BUSINESS_DETAILS  = COMMON + "putBusinessDetails ";
    public static final String PUT_KYC_DETAILS  = COMMON + "putKycDetails "; //panNumber,identityProofId,idNumber,personName,nomineeName,nomineeMobile,nomineeAddress,userId,panDocument,identityDocument
    public static final String PUT_BANK_DETAILS  = COMMON + "putBankDetails "; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook
    public static final String SEND_FEEDBACK  = COMMON + "sendFeedback "; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook

    public static final String SEND_CONSULTANT_FEEDBACK  = COMMON + "sendConsultantFeedback "; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook


    //required APIs  name,mobile,email,pass,profileType,panNo
    public static final String SIGNUP = COMMON + "signup";//name,mobile,email,panNo,pass,profileType

    public static final String EMAIL_VERIFY = COMMON + "verifyEmail";//email
    public static final String MOBILE_VERIFY = COMMON + "verifyMobile";//mobile
    public static final String GET_PAYMENT_INFO = COMMON + "getPaymentInfo";//packageId,userId,referenceNumber
    public static final String PAYMENT_SUCCESS = COMMON + "paymentSuccess";// packageId,txID,bankRefID,packageAmount,netAmount,capping,referenceNumber,position,customerType

    public static final String REPORT_SERVER_ERROR = COMMON + "serverErrorReporting";;
    //PAyment error
    public static final String ERROR105 = "Error:105 ";

}

/* New API's required for BizzzWizzz App

API 1:   http://bizzzwizzz.com/index.php/Rest/signup

Params: name,mobile,email,panNo,pass,profileType

Response: success/failure.(return success and basic info similar to login api)

*/
/*
    1 1xx Informational.
    2 2xx Success.
    3 3xx Redirection.
    4 4xx Client Error.
    5 5xx Server Error.
*/
