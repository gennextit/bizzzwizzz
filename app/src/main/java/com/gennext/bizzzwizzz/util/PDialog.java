package com.gennext.bizzzwizzz.util;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;

/**
 * Created by Abhijit on 19-Dec-16.
 */

public class PDialog {

    private Context context;
    private String message;

    public static PDialog newInstance(Context context, String message) {
        PDialog pDialog=new PDialog();
        pDialog.context=context;
        pDialog.message=message;
        return pDialog;
    }

    public AlertDialog show() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        final View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        TextView tvMessage = (TextView) dialogView.findViewById(R.id.tv_message1);
        tvMessage.setText(message);

        ImageView iv = (ImageView)dialogView.findViewById(R.id.iv_dialog);
        iv.setBackgroundResource(R.drawable.dialog_animation);
        AnimationDrawable aniFrame = (AnimationDrawable) iv.getBackground();
        aniFrame = (AnimationDrawable) iv.getBackground();
        aniFrame.start();


//        mainLayout.setBackgroundResource(R.drawable.my_progress_one);
//        AnimationDrawable frameAnimation = (AnimationDrawable)mainLayout.getBackground();
//        frameAnimation.start();
        AlertDialog b = dialogBuilder.create();
        b.show();
        return b;
    }


}

