package com.gennext.bizzzwizzz.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.gennext.bizzzwizzz.model.ProfileModel;

/**
 * Created by Abhijit-PC on 05-Apr-17.
 */

public class PaymentConstant {

    private static final String COMMON = "pc";
    private static final String PKGAMT = "pkgAmt" + COMMON;
    private static final String PLACEMENT = "placement" + COMMON;
    private static final String PURCHASETYPE = "purchasetype" + COMMON;
    private static final String PKGID = "pkgId" + COMMON;
    private static final String REFNO = "refNo" + COMMON;
    private static final String CAPPING = "capping" + COMMON;

    private String pkgAMT;
    private String placement;
    private String purchaseType;
    private String pkgId;
    private String refNo;
    private String capping;

    public static void set(Context context, String pkgAmt, String placement, String purchaseType, String pkgId,
                           String refNo, String capping) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (pkgAmt != null)
                editor.putString(PKGAMT, pkgAmt);
            if (placement != null)
                editor.putString(PLACEMENT, placement);
            if (purchaseType != null)
                editor.putString(PURCHASETYPE, purchaseType);
            if (pkgId != null)
                editor.putString(PKGID, pkgId);
            if (refNo != null)
                editor.putString(REFNO, refNo);
            if (capping != null)
                editor.putString(CAPPING, capping);
            editor.apply();
        }
    }

    public static PaymentConstant get(Context context) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            PaymentConstant model = new PaymentConstant();
            model.setPkgAMT(sharedPreferences.getString(PKGAMT, ""));
            model.setPlacement(sharedPreferences.getString(PLACEMENT, ""));
            model.setPurchaseType(sharedPreferences.getString(PURCHASETYPE, ""));
            model.setPkgId(sharedPreferences.getString(PKGID, ""));
            model.setRefNo(sharedPreferences.getString(REFNO, ""));
            model.setCapping(sharedPreferences.getString(CAPPING, ""));
            return model;
        }
        return null;
    }

    public String getPkgAMT() {
        return pkgAMT;
    }

    public void setPkgAMT(String pkgAMT) {
        this.pkgAMT = pkgAMT;
    }

    public String getPlacement() {
        return placement;
    }

    public void setPlacement(String placement) {
        this.placement = placement;
    }

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType;
    }

    public String getPkgId() {
        return pkgId;
    }

    public void setPkgId(String pkgId) {
        this.pkgId = pkgId;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getCapping() {
        return capping;
    }

    public void setCapping(String capping) {
        this.capping = capping;
    }
}
