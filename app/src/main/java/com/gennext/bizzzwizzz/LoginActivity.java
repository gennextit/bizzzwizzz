package com.gennext.bizzzwizzz;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.common.AppWebView;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.user.SignupUser;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.L;
import com.gennext.bizzzwizzz.util.RequestBuilder;

/**
 * Created by Abhijit on 06-Dec-16.
 */

public class LoginActivity extends BaseActivity implements ApiCallError.ErrorListener {

    private AssignTask assignTask;
    private EditText etUser,etPass;
    private CheckBox cbRememberMe;

    @Override
    protected void onStart() {
        super.onStart();
        if (assignTask != null) {
            assignTask.onAttach(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        AppAnimation.setupWindowExplodeAnimations(LoginActivity.this);
        initUi();
        setCredentials();
    }

    private void setCredentials() {
        String userName = AppUser.getRememberUser(getApplicationContext());
        String pass = AppUser.getRememberPass(getApplicationContext());
        if(!TextUtils.isEmpty(userName)){
            cbRememberMe.setChecked(true);
            etUser.setText(userName);
            etUser.setSelection(userName.length());
            etPass.setText(pass);
            etPass.setSelection(pass.length());
        }else{
            cbRememberMe.setChecked(false);
        }
    }

    private void initUi() {
        etUser = (EditText) findViewById(R.id.et_signup_username);
        etPass = (EditText) findViewById(R.id.et_signup_password);
        LinearLayout llTermsOfUse = (LinearLayout) findViewById(R.id.ll_terms_use);
        TextView tvRemember = (TextView) findViewById(R.id.tv_rememberme);
        cbRememberMe = (CheckBox) findViewById(R.id.cb_login);
        Button btnLogin = (Button) findViewById(R.id.btn_mobile_signin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(LoginActivity.this);
                attemptLogin();
            }
        });
        tvRemember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cbRememberMe.isChecked()){
                    cbRememberMe.setChecked(false);
                }else {
                    cbRememberMe.setChecked(true);
                }
            }
        });

        findViewById(R.id.ll_mobile_signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,SignupActivity.class);
                startActivity(intent);
            }
        });
        llTermsOfUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(AppWebView.newInstance("Terms Of Use",AppSettings.TERMS_OF_USE,AppWebView.PDF),"appWebView");
            }
        });
    }

    private void attemptLogin() {
        assignTask = new AssignTask(this,etUser.getText().toString(),etPass.getText().toString());
        assignTask.execute(AppSettings.LOGIN);
    }



    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        attemptLogin();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private String username,password;
        private Activity activity;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, String username, String password) {
            this.username=username;
            this.password=password;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Authentication, please wait...");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            response = ApiCall.POST(urls[0],RequestBuilder.LoginBody(username, password));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.parseLoginData(activity, response);
//            if(model!=null && model.getOutput().equals("success")) {
////                AppUser.setAppData(activity,response);
//                return model;
//            }else{
//                return model;
////                response= AppUser.getAppData(activity);
////                jsonParser = new JsonParser();
////                return jsonParser.parseLoginData(activity, response);
//            }
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if(cbRememberMe.isChecked()){
                            AppUser.setRememberUser(activity,username);
                            AppUser.setRememberPass(activity,password);
                        }else{
                            AppUser.setRememberUser(activity,"");
                            AppUser.setRememberPass(activity,"");
                        }
                        AppUser.setPassword(activity,password);
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),LoginActivity.this)
                                .show(getSupportFragmentManager(),"apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,LoginActivity.this)
                                .show(getSupportFragmentManager(),"apiCallError");
                    }
                }
            }
        }
    }
}
