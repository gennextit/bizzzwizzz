package com.gennext.bizzzwizzz.intro;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.skyfishjy.library.RippleBackground;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class IntroSales extends CompactFragment {
    private FragmentManager manager;
    private Context context;
    private int introCount;
    private RippleBackground rippleBackground1,rippleBackground2,rippleBackground3;
    private RelativeLayout whitespace;
    private RelativeLayout introPatout1,introPatout2,introPatout3;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    public static IntroSales newInstance() {
        IntroSales popupAlert = new IntroSales();
        return popupAlert;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.intro_sales, container, false);
        manager = getFragmentManager();
        InitUI(v);
        startIntro(1);
        return v;
    }

    private void startIntro(int introCount) {
        switch (introCount){
            case 1:
                showFirstIntro();
                break;
            case 2:
                hideFirstIntro();
                showSecendIntro();
                break;
            case 3:
                hideSecendIntro();
                showThirdIntro();
                break;
            case 4:
                hideThirdIntro();
                IntroTokens.salesIntroClose(context);
//                    manager.popBackStack("introSales", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                manager.popBackStack();
                break;
        }
        this.introCount=introCount+1;
    }


    private void showThirdIntro() {
        introPatout3.setVisibility(View.VISIBLE);
        rippleBackground3.startRippleAnimation();
    }
    private void hideThirdIntro() {
        introPatout3.setVisibility(View.GONE);
        rippleBackground3.stopRippleAnimation();
    }
    private void showSecendIntro() {
        introPatout2.setVisibility(View.VISIBLE);
        rippleBackground2.startRippleAnimation();
    }
    private void hideSecendIntro() {
        introPatout2.setVisibility(View.GONE);
        rippleBackground2.stopRippleAnimation();
    }
    private void showFirstIntro() {
        introPatout1.setVisibility(View.VISIBLE);
        rippleBackground1.startRippleAnimation();
    }
    private void hideFirstIntro() {
        introPatout1.setVisibility(View.GONE);
        rippleBackground1.stopRippleAnimation();
    }

    private void InitUI(View v) {
        introPatout1=(RelativeLayout)v.findViewById(R.id.intro_patout_1);
        introPatout2=(RelativeLayout)v.findViewById(R.id.intro_patout_2);
        introPatout3=(RelativeLayout)v.findViewById(R.id.intro_patout_3);
        rippleBackground1=(RippleBackground)v.findViewById(R.id.content1);
        rippleBackground2=(RippleBackground)v.findViewById(R.id.content2);
        rippleBackground3=(RippleBackground)v.findViewById(R.id.content3);
        whitespace = (RelativeLayout) v.findViewById(R.id.intro_whitespace);

        whitespace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startIntro(introCount);
            }
        });

    }
}