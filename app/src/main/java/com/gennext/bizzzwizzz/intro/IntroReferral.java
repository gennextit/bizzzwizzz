package com.gennext.bizzzwizzz.intro;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.skyfishjy.library.RippleBackground;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class IntroReferral extends CompactFragment {
    private FragmentManager manager;
    private Context context;
    private int introCount=1;
    private RippleBackground rippleBackground1,rippleBackground2;
    private RelativeLayout whitespace;
    private RelativeLayout introPatout1,introPatout2;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    public static IntroReferral newInstance() {
        IntroReferral popupAlert = new IntroReferral();
        return popupAlert;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.intro_sales, container, false);
        manager = getFragmentManager();
        InitUI(v);
        setIntro();
        showFirstIntro();
        return v;
    }

    private void setIntro() {

    }

    private void showSecendIntro() {
        rippleBackground1.stopRippleAnimation();
        introPatout1.setVisibility(View.GONE);
        introPatout2.setVisibility(View.VISIBLE);
        rippleBackground2.startRippleAnimation();
    }
    private void showFirstIntro() {
        introPatout1.setVisibility(View.VISIBLE);
        rippleBackground1.startRippleAnimation();
        introPatout2.setVisibility(View.GONE);
        rippleBackground2.stopRippleAnimation();
    }

    private void InitUI(View v) {
        introPatout1=(RelativeLayout)v.findViewById(R.id.intro_patout_1);
        introPatout2=(RelativeLayout)v.findViewById(R.id.intro_patout_2);
        rippleBackground1=(RippleBackground)v.findViewById(R.id.content1);
        rippleBackground2=(RippleBackground)v.findViewById(R.id.content2);
        whitespace = (RelativeLayout) v.findViewById(R.id.intro_whitespace);

        whitespace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(introCount==1){
                    introCount++;
                    showSecendIntro();
                }else{
                    rippleBackground2.stopRippleAnimation();
                    IntroTokens.referralIntroClose(context);
                    manager.popBackStack();
                }
            }
        });

    }
}