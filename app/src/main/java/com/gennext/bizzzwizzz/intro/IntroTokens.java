package com.gennext.bizzzwizzz.intro;

import android.content.Context;

import com.gennext.bizzzwizzz.util.Utility;

/**
 * Created by Abhijit on 12-Jan-17.
 */

public class IntroTokens {
    public static final String COMMON = "bizzzwizzz";

    public static final String INTRO_PAYOUT = "introPay"+COMMON;
    public static final String INTRO_SALES = "iSale"+COMMON;
    public static final String INTRO_REFERRAL = "iReferral"+COMMON;
    private static final String INTRO_PACKAGE = "pkgIntro" + COMMON;

    public static void payoutIntroClose(Context context) {
        Utility.SavePref(context,INTRO_PAYOUT,COMMON);
    }
    public static Boolean payoutIntroOpen(Context context) {
        if(Utility.LoadPref(context,INTRO_PAYOUT).equals("")) {
            return true;
        }else{
            return false;
        }
    }

    public static void salesIntroClose(Context context) {
        Utility.SavePref(context,INTRO_SALES,COMMON);
    }
    public static Boolean salesIntroOpen(Context context) {
        if(Utility.LoadPref(context,INTRO_SALES).equals("")) {
            return true;
        }else{
            return false;
        }
    }
    public static void referralIntroClose(Context context) {
        Utility.SavePref(context,INTRO_REFERRAL,COMMON);
    }
    public static Boolean referralIntroOpen(Context context) {
        if(Utility.LoadPref(context,INTRO_REFERRAL).equals("")) {
            return true;
        }else{
            return false;
        }
    }

    public static void packageIntroClose(Context context) {
        Utility.SavePref(context,INTRO_PACKAGE,COMMON);
    }

    public static Boolean packageIntroOpen(Context context) {
        if(Utility.LoadPref(context,INTRO_PACKAGE).equals("")) {
            return true;
        }else{
            return false;
        }
    }

}
