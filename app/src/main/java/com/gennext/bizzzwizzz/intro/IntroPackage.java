package com.gennext.bizzzwizzz.intro;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.skyfishjy.library.RippleBackground;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class IntroPackage extends CompactFragment {
    private FragmentManager manager;
    private Context context;
    private RippleBackground rippleBackground2;
    private RelativeLayout whitespace;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    public static IntroPackage newInstance() {
        IntroPackage popupAlert = new IntroPackage();
        return popupAlert;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.intro_package, container, false);
        manager = getFragmentManager();
        InitUI(v);
        showFirstIntro();
        return v;
    }

    private void showFirstIntro() {
        rippleBackground2.startRippleAnimation();
    }

    private void InitUI(View v) {
        rippleBackground2=(RippleBackground)v.findViewById(R.id.content2);
        whitespace = (RelativeLayout) v.findViewById(R.id.intro_whitespace);

        whitespace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rippleBackground2.stopRippleAnimation();
                IntroTokens.packageIntroClose(context);
                manager.popBackStack();
            }
        });
    }
}