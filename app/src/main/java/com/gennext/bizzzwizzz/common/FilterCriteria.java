package com.gennext.bizzzwizzz.common;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.FilterSpinnerAdapter;
import com.gennext.bizzzwizzz.util.DateTimeUtility;

import java.util.Calendar;


public class FilterCriteria extends DialogFragment implements View.OnClickListener, DatePickerDialog.DateSelectFlagListener {
    private AlertDialog dialog = null;
    private Spinner spFilter;
    private String sltSpinnerItem;
    private int sday, smonth, syear;
    private int eday, emonth, eyear;
    private FilterCriteria fragment;
    private String sDate,eDate;
    private Button btnStartDate,btnEndDate;
    private int startDateTimeStamp,endDateTimeStamp;

    public void getDateTime() {
        final Calendar cal = Calendar.getInstance();
        if(sDate==null) {
            syear = cal.get(Calendar.YEAR);
            smonth = cal.get(Calendar.MONTH);
            sday = cal.get(Calendar.DAY_OF_MONTH);
            startDateTimeStamp=DateTimeUtility.generateTimeStamp(syear,smonth,sday);
        }else {
            String date=DateTimeUtility.convertDateSplit(sDate);
            String[] temp = date.split("-");
            sday=Integer.parseInt(temp[0]);
            int tempMonth = Integer.parseInt(temp[1]);
            smonth=tempMonth-1;
            syear=Integer.parseInt(temp[2]);
            btnStartDate.setText(sDate);
            startDateTimeStamp=DateTimeUtility.generateTimeStamp(syear,smonth,sday);
        }
        if(eDate==null) {
            eyear = cal.get(Calendar.YEAR);
            emonth = cal.get(Calendar.MONTH);
            eday = cal.get(Calendar.DAY_OF_MONTH);
            endDateTimeStamp=DateTimeUtility.generateTimeStamp(eyear,emonth,eday);
        }else {
            String date=DateTimeUtility.convertDateSplit(eDate);
            String[] temp = date.split("-");
            eday=Integer.parseInt(temp[0]);
            int tempMonth = Integer.parseInt(temp[1]);
            emonth=tempMonth-1;
            eyear=Integer.parseInt(temp[2]);
            btnEndDate.setText(eDate);
            endDateTimeStamp=DateTimeUtility.generateTimeStamp(eyear,emonth,eday);
        }
    }


    @Override
    public void onSelectDateFlagClick(DialogFragment dialog, int dateType, int date, int month, int year) {
        sltSpinnerItem=null;
        if (dateType == DatePickerDialog.START_DATE) {
            sday = date;
            smonth = month;
            syear = year;
            sDate=DateTimeUtility.cDateDDMMMYY(date,month,year);
            startDateTimeStamp=DateTimeUtility.generateTimeStamp(syear,smonth,sday);
            btnStartDate.setText(sDate);
        } else {
            eday = date;
            emonth = month;
            eyear = year;
            eDate=DateTimeUtility.cDateDDMMMYY(date,month,year);
            btnEndDate.setText(eDate);
            endDateTimeStamp=DateTimeUtility.generateTimeStamp(eyear,emonth,eday);
        }
    }

    public interface FilterDialogListener {
        void onFilterOkClick(DialogFragment dialog,String sltFilterItem,String startDate,String endDate);

        void onFilterCancelClick(DialogFragment dialog);
    }

    private FilterDialogListener mListener;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static FilterCriteria newInstance(FilterDialogListener listener, String sltFilterItem, String startDate, String endDate) {
        FilterCriteria fragment = new FilterCriteria();
        fragment.mListener = listener;
        fragment.sltSpinnerItem=sltFilterItem;
        fragment.sDate=startDate;
        fragment.eDate=endDate;
        fragment.fragment=fragment;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_filter_criteria, null);
        dialogBuilder.setView(v);
        spFilter = (Spinner) v.findViewById(R.id.sp_filter);
        btnStartDate = (Button) v.findViewById(R.id.btn_startdate);
        btnEndDate = (Button) v.findViewById(R.id.btn_enddate);
        Button btnApply = (Button) v.findViewById(R.id.btn_submit);
        Button btnCancel = (Button) v.findViewById(R.id.btn_cancel);
        btnStartDate.setOnClickListener(this);
        btnEndDate.setOnClickListener(this);
        btnApply.setOnClickListener(this);
        btnCancel.setOnClickListener(this);


        spFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                // TODO Auto-generated method stub
                String gender = spFilter.getSelectedItem().toString();
                sltSpinnerItem = gender;
//                sDate=null;
//                eDate=null;
//                btnStartDate.setText("Start Date");
//                btnEndDate.setText("End Date");
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        getDateTime();
        setSpinnerData();

//        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
//                if (adapter != null) {
//                    StateCityModel model = adapter.getItem(pos);
//                    if (model != null) {
//                        dialog.dismiss();
//                        if (mListener != null) {
//                            mListener.onStateClick(FilterCriteria.this,model.getStateId(),model.getStateName());
//                        }
//                        if (fragment != null) {
//                            fragment.dismiss();
//                        }
//                    }
//                }
//            }
//        });


        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_startdate:
                DatePickerDialog.newInstance(FilterCriteria.this,false, DatePickerDialog.START_DATE, sday, smonth, syear)
                        .show(getFragmentManager(),"datePickerDialog");
                break;
            case R.id.btn_enddate:
                DatePickerDialog.newInstance(FilterCriteria.this,false, DatePickerDialog.END_DATE, eday, emonth, eyear)
                        .show(getFragmentManager(),"datePickerDialog");;
                break;
            case R.id.btn_submit:
                if (mListener != null) {
                    if(sltSpinnerItem==null){
                        if(sDate!=null&&eDate!=null){
                            if(startDateTimeStamp>endDateTimeStamp){
                                Toast.makeText(getActivity(), "Start date must be less than or equals to end date", Toast.LENGTH_SHORT).show();
                            }else {
                                dialog.dismiss();
                                mListener.onFilterOkClick(FilterCriteria.this, sltSpinnerItem, sDate, eDate);
                                if (fragment != null) {
                                    fragment.dismiss();
                                }
                            }
                        }else {
                            dialog.dismiss();
                            mListener.onFilterOkClick(FilterCriteria.this, sltSpinnerItem, sDate, eDate);
                            if (fragment != null) {
                                fragment.dismiss();
                            }
                        }
                    }else {
                        dialog.dismiss();
                        mListener.onFilterOkClick(FilterCriteria.this, sltSpinnerItem, sDate, eDate);
                        if (fragment != null) {
                            fragment.dismiss();
                        }
                    }
                }

                break;
            case R.id.btn_cancel:
                if (mListener != null) {
                    mListener.onFilterCancelClick(FilterCriteria.this);
                }
                dialog.dismiss();
                break;
        }
    }


    private void setSpinnerData() {
        String[] gender = getResources().getStringArray(R.array.filter_by_row);
        FilterSpinnerAdapter adapter = new FilterSpinnerAdapter(getActivity(), R.layout.slot_spinner);
        adapter.addAll(gender);
        spFilter.setAdapter(adapter);
        if(sltSpinnerItem!=null){
            for(int i=0;i<gender.length;i++){
                if(gender[i].equals(sltSpinnerItem)){
                    spFilter.setSelection(i);
                }
            }
        }
//        spGender.setSelection(adapter.getCount());
    }


}
