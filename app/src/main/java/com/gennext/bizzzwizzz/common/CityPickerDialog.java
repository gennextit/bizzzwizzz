package com.gennext.bizzzwizzz.common;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.CityAdapter;
import com.gennext.bizzzwizzz.model.StateCityModel;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.JsonParser;

import java.util.ArrayList;


public class CityPickerDialog extends DialogFragment {
    private AlertDialog dialog = null;
    private CityPickerDialog fragment;
    private CityAdapter adapter;
    private String stateId;
    private String stateName;


    public interface CityDialogListener {
        void onCityClick(DialogFragment dialog, String cityId, String cityName);
    }

    private CityDialogListener mListener;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static CityPickerDialog newInstance(CityDialogListener listener, String stateId, String stateName) {
        CityPickerDialog fragment = new CityPickerDialog();
        fragment.mListener = listener;
        fragment.fragment = fragment;
        fragment.stateId=stateId;
        fragment.stateName=stateName;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_state_picker, null);
        dialogBuilder.setView(v);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        EditText etSearch = (EditText) v.findViewById(R.id.et_search);
        ListView lvMain = (ListView) v.findViewById(R.id.lv_main);
        tvTitle.setText("Select City");
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter != null)
                    adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    StateCityModel model = adapter.getItem(pos);
                    if (model != null) {
                        dialog.dismiss();
                        if (mListener != null) {
                            mListener.onCityClick(CityPickerDialog.this,model.getCityId(),model.getCityName());
                        }
                        if (fragment != null) {
                            fragment.dismiss();
                        }
                    }
                }
            }
        });

        StateCityModel listModel = getCityList(stateId);
        if (listModel.getOutput().equals("success")) {
            adapter = new CityAdapter(getActivity(), listModel.getList());
            lvMain.setAdapter(adapter);
        }else{
            ArrayList<String> errorList = new ArrayList<>();
            errorList.add("Not exist city under "+stateName+" state");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.slot_domain,
                    R.id.tv_message, errorList);
            lvMain.setAdapter(adapter);
        }

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }

    private StateCityModel getCityList(String stateId) {
        return JsonParser.parseCityJson(AppUser.getCitiesJson(getActivity()),stateId);
    }

//    private StateCityModel getStateList() {
//        return JsonParser.parseStateJson(AppUser.getStatesJson(getActivity()));
//    }
}
