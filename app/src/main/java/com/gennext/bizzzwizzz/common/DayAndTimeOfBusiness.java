package com.gennext.bizzzwizzz.common;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.DayAndTimeModel;
import com.gennext.bizzzwizzz.model.DayAndTimeOfBusinessAdapter;
import com.gennext.bizzzwizzz.util.JsonParser;

import java.util.ArrayList;


public class DayAndTimeOfBusiness extends DialogFragment {
    private AlertDialog dialog = null;
    private DayAndTimeOfBusiness fragment;
    private DayAndTimeOfBusinessAdapter adapter;
    private ArrayList<DayAndTimeModel> paymontList;
    private ArrayList<DayAndTimeModel> originalPaymentList;
    private String DayAndTimingIdJson;

    public interface DayAndTimeOfBussListener {
        void onDayAndTimeOfBussClick(DayAndTimeOfBusiness modeOfPaymentDialog, ArrayList<DayAndTimeModel> checkedList, ArrayList<DayAndTimeModel> originalList);
    }

    private DayAndTimeOfBussListener mListener;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static DayAndTimeOfBusiness newInstance(DayAndTimeOfBussListener listener, String DayAndTimingIdJson, ArrayList<DayAndTimeModel> originalPaymentList) {
        DayAndTimeOfBusiness fragment = new DayAndTimeOfBusiness();
        fragment.mListener = listener;
        fragment.fragment = fragment;
        fragment.DayAndTimingIdJson = DayAndTimingIdJson;
        fragment.originalPaymentList = originalPaymentList;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_day_time_bussiness, null);
        dialogBuilder.setView(v);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        ListView lvMain = (ListView) v.findViewById(R.id.lv_main);
        Button btnSubmit = (Button) v.findViewById(R.id.btn_submit);

        tvTitle.setText("Select Days");


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (adapter != null) {
                    if (mListener != null) {
                        mListener.onDayAndTimeOfBussClick(DayAndTimeOfBusiness.this, adapter.getCheckedList(), adapter.getList());
                    }
                    if (fragment != null) {
                        fragment.dismiss();
                    }
                }
            }
        });
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    DayAndTimeModel model = adapter.getItem(pos);
                    if (model != null)
                        if (model.getChecked()) {
                            model.setChecked(false);
                            paymontList.set(pos, model);
                            adapter.notifyDataSetChanged();
                        } else {
                            model.setChecked(true);
                            paymontList.set(pos, model);
                            adapter.notifyDataSetChanged();
                        }
                }
            }
        });

        if (originalPaymentList == null) {
            ArrayList<DayAndTimeModel> listM = getAllSelectedDayAndBusinessList();
            if (listM!=null) {
                paymontList=listM;
                adapter = new DayAndTimeOfBusinessAdapter(getActivity(), paymontList,getFragmentManager());
                lvMain.setAdapter(adapter);
            }
        } else {
            paymontList = originalPaymentList;
            adapter = new DayAndTimeOfBusinessAdapter(getActivity(), paymontList,getFragmentManager());
            lvMain.setAdapter(adapter);
        }

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }

    private ArrayList<DayAndTimeModel> getAllSelectedDayAndBusinessList() {
        DayAndTimeModel allModel = getAllDayAndBusinessList();
        DayAndTimeModel sltModel = getSelectDayAndBusinessList();
        ArrayList<DayAndTimeModel> allSelectedList;

        ArrayList<DayAndTimeModel> allList = allModel.getList();
        allSelectedList=new ArrayList<>();
        allSelectedList.addAll(allList);
        if(sltModel!=null&&sltModel.getOutput().equals("success")){
            ArrayList<DayAndTimeModel> sltList = sltModel.getList();
            for(DayAndTimeModel model:sltList){
                for(int pos=0;pos<allList.size();pos++){
                    if(allList.get(pos).getDayId().equals(model.getDayId())){
                        DayAndTimeModel m=new DayAndTimeModel();
                        m.setDayId(model.getDayId());
                        m.setDayName(model.getDayName());
                        m.setStartTime(model.getStartTime());
                        m.setEndTime(model.getEndTime());
                        m.setChecked(true);
                        allSelectedList.set(pos,m);
                    }
                }
            }
        }
        return allSelectedList;
    }

    private DayAndTimeModel getAllDayAndBusinessList() {
        return JsonParser.parseDayAndTimingIdJson();
    }

    private DayAndTimeModel getSelectDayAndBusinessList() {
        return JsonParser.parseDayAndTimingIdJson(DayAndTimingIdJson);
    }


}
