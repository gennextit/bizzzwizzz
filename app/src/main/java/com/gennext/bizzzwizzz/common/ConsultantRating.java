package com.gennext.bizzzwizzz.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.PDialog;
import com.gennext.bizzzwizzz.util.RequestBuilder;


public class ConsultantRating extends DialogFragment {
    private static AlertDialog aDialog;
    private Dialog dialog;
    private TextView tvTitle,tvDescription;
    private Button btnOK;
    private EditText etFeedback;
    private RatingBar rbRating;
    private String domainId,domainName;

    private AssignTask assignTask;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static ConsultantRating newInstance(String domainId,String domainName) {
        ConsultantRating fragment = new ConsultantRating();
        fragment.domainId = domainId;
        fragment.domainName = domainName;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_consultant, null);
        dialogBuilder.setView(v);
        tvTitle = (TextView) v.findViewById(R.id.tv_popup_title);
        tvDescription = (TextView) v.findViewById(R.id.custom_dialog_baseSubTitle);
        btnOK = (Button) v.findViewById(R.id.btn_popup);
        etFeedback = (EditText) v.findViewById(R.id.et_alert_feedback);
        rbRating = (RatingBar) v.findViewById(R.id.ratingBar);

        tvDescription.setText(domainName);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeTask(domainId,etFeedback.getText().toString(),rbRating.getRating());
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }

    private void executeTask(String domainId, String feedback, float rating) {
        assignTask = new AssignTask(getActivity(),domainId,feedback,String.valueOf(rating));
        assignTask.execute(AppSettings.SEND_CONSULTANT_FEEDBACK);
    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String domainId,feedback,rating;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String domainId, String feedback, String rating) {
            this.context = context;
            this.domainId = domainId;
            this.feedback = feedback;
            this.rating = rating;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context,"Processing please wait");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            String userId= AppUser.getUserId(context);
            response = ApiCall.POST(urls[0], RequestBuilder.ConsultantFeedback(userId,domainId,feedback,rating));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.defaultParser(response);
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        Toast.makeText(getActivity(),"Thanks for your feedback",Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    } else if (result.getOutput().equals("failure")) {
                        Toast.makeText(getActivity(),result.getOutputMsg(),Toast.LENGTH_LONG).show();
                    } else if (result.getOutput().equals("internet")) {
                        Toast.makeText(getActivity(),result.getOutputMsg(),Toast.LENGTH_LONG).show();
                    } else if (result.getOutput().equals("server")) {
                        Toast.makeText(getActivity(),result.getOutputMsg(),Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    public static void showProgressDialog(Context context, String msg) {
        PDialog cDialog = PDialog.newInstance(context, msg);
        aDialog = cDialog.show();
    }

    public static void hideProgressDialog() {
        if (aDialog != null)
            aDialog.dismiss();
    }

}
