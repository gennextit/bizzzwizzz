package com.gennext.bizzzwizzz.common;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Abhijit on 21-Dec-16.
 */


public class DatePickerDialog extends DialogFragment implements android.app.DatePickerDialog.OnDateSetListener {

    public static final int START_DATE = 1,END_DATE=2;
    private DateSelectListener mListener;
    private DateSelectFlagListener mFlagListener;
    private int sday, smonth, syear;
    private int dateType;
    private Boolean minDate;

    public interface DateSelectListener {
        void onSelectDateClick(DialogFragment dialog, int date, int month, int year);
    }

    public interface DateSelectFlagListener {
        void onSelectDateFlagClick(DialogFragment dialog,int dateType, int date, int month, int year);
    }

    public static DatePickerDialog newInstance(DateSelectListener listener, int sday, int smonth, int syear) {
        DatePickerDialog fragment = new DatePickerDialog();
        fragment.sday=sday;
        fragment.smonth=smonth;
        fragment.syear=syear;
        fragment.mListener = listener;
        fragment.minDate=true;
        return fragment;
    }

    public static DatePickerDialog newInstance(DateSelectFlagListener listener,Boolean minDate,int dateType, int sday, int smonth, int syear) {
        DatePickerDialog fragment = new DatePickerDialog();
        fragment.sday=sday;
        fragment.smonth=smonth;
        fragment.syear=syear;
        fragment.mFlagListener = listener;
        fragment.minDate = minDate;
        fragment.dateType=dateType;
        return fragment;
    }



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        android.app.DatePickerDialog dpd;
            dpd = new android.app.DatePickerDialog(getActivity(), this, syear, smonth, sday);


        if(minDate) {
            // Set the DatePicker minimum date selection to current date
            dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());// get the current day
        }
        return dpd;
    }



    @Override
    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        if(mListener!=null) {
            mListener.onSelectDateClick(DatePickerDialog.this, dd, mm, yy);
        }
        if(mFlagListener!=null){
            mFlagListener.onSelectDateFlagClick(DatePickerDialog.this,dateType, dd, mm, yy);
        }
//        populateSetDate(yy, mm, dd);
//			storeTimeStamp(yy,mm,dd);
    }

//    public void populateSetDate(int year, int month, int day) {
//        if (status == START_DATE) {
//            startDay = DateTimeUtility.convertDate(day, month, year);
//            btnStartDate.setText(startDay);
//            startDateStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(year,month,day));
//            syear = year;
//            smonth = month;
//            sday = day;
//
//        } else {
//            endDay = DateTimeUtility.convertDate(day, month, year);
//            btnEndDate.setText(endDay);
//            endDateStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(year,month,day));
//            eyear = year;
//            emonth = month;
//            eday = day;
//        }
//    }



}