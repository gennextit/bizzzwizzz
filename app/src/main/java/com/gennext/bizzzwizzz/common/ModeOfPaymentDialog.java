package com.gennext.bizzzwizzz.common;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.BankModel;
import com.gennext.bizzzwizzz.model.ModeOfPayAdapter;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.JsonParser;

import java.util.ArrayList;


public class ModeOfPaymentDialog extends DialogFragment {
    private AlertDialog dialog = null;
    private ModeOfPaymentDialog fragment;
    private ModeOfPayAdapter adapter;
    private ArrayList<BankModel> paymontList;
    private CheckBox cmSelectAll;
    private ArrayList<BankModel> originalPaymentList;
    private String paymoentModeJson;

    public interface ModeOfPayListener {
        void onModeOfPayClick(ModeOfPaymentDialog modeOfPaymentDialog, ArrayList<BankModel> checkedList, ArrayList<BankModel> originalList);
    }

    private ModeOfPayListener mListener;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static ModeOfPaymentDialog newInstance(ModeOfPayListener listener, String paymoentModeJson, ArrayList<BankModel> originalPaymentList) {
        ModeOfPaymentDialog fragment = new ModeOfPaymentDialog();
        fragment.mListener = listener;
        fragment.fragment = fragment;
        fragment.paymoentModeJson=paymoentModeJson;
        fragment.originalPaymentList=originalPaymentList;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_payment_mode, null);
        dialogBuilder.setView(v);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        ListView lvMain = (ListView) v.findViewById(R.id.lv_main);
        cmSelectAll = (CheckBox) v.findViewById(R.id.checkBox_header);
        Button btnSubmit = (Button) v.findViewById(R.id.btn_submit);

        tvTitle.setText("Select Mode of payment");
        cmSelectAll.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                for(BankModel model:paymontList){
                    if(cmSelectAll.isChecked()){
                        model.setChecked(true);
                    }else {
                        model.setChecked(false);
                    }
                }
                adapter.notifyDataSetChanged();

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if(adapter!=null) {
                    if (mListener != null) {
                        mListener.onModeOfPayClick(ModeOfPaymentDialog.this, adapter.getCheckedList(),adapter.getList());
                    }
                    if (fragment != null) {
                        fragment.dismiss();
                    }
                }
            }
        });
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    BankModel model = adapter.getItem(pos);
                    if(model != null)
                    if (model.getChecked()) {
                        model.setChecked(false);
                        paymontList.set(pos,model);
                        adapter.notifyDataSetChanged();
                    }else{
                        model.setChecked(true);
                        paymontList.set(pos,model);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });

        if(originalPaymentList==null) {
            ArrayList<BankModel> listM = getAllSelectedPaymoentModeList();
            if (listM!=null) {
                paymontList = listM;
                adapter = new ModeOfPayAdapter(getActivity(), paymontList);
                lvMain.setAdapter(adapter);
            }
        }else{
            paymontList = originalPaymentList;
            adapter = new ModeOfPayAdapter(getActivity(), paymontList);
            lvMain.setAdapter(adapter);
        }

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }

    private ArrayList<BankModel> getAllSelectedPaymoentModeList() {
        BankModel allModel = getAllPaymoentModeList();
        BankModel sltModel = getSelectPaymoentModeList();
        ArrayList<BankModel> allSelectedList;

        ArrayList<BankModel> allList = allModel.getList();
        allSelectedList=new ArrayList<>();
        allSelectedList.addAll(allList);
        if(sltModel!=null&&sltModel.getOutput().equals("success")){
            ArrayList<BankModel> sltList = sltModel.getList();
            for(BankModel model:sltList){
                BankModel m;
                for(int pos=0;pos<allList.size();pos++){
                    m=allList.get(pos);
                    if(m.getId().equals(model.getId())){
                        m.setChecked(true);
                        allSelectedList.set(pos,m);
                    }
                }
            }
        }
        return allSelectedList;
    }

    private BankModel getAllPaymoentModeList() {
        return JsonParser.parsePaymentMode(AppUser.getPaymentModeJson(getActivity()));
    }
    private BankModel getSelectPaymoentModeList() {
        return JsonParser.parsePaymoentModeIdJson(paymoentModeJson);
    }

}
