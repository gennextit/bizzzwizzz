package com.gennext.bizzzwizzz.common;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.StateCityModel;
import com.gennext.bizzzwizzz.model.StatesAdapter;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.JsonParser;


public class StatePickerDialog extends DialogFragment {
    private AlertDialog dialog = null;
    private StatePickerDialog fragment;
    private StatesAdapter adapter;

    public interface StateDialogListener {
        void onStateClick(DialogFragment dialog, String stateId, String stateName);
    }

    private StateDialogListener mListener;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static StatePickerDialog newInstance(StateDialogListener listener) {
        StatePickerDialog fragment = new StatePickerDialog();
        fragment.mListener = listener;
        fragment.fragment = fragment;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_state_picker, null);
        dialogBuilder.setView(v);
        EditText etSearch = (EditText) v.findViewById(R.id.et_search);
        ListView lvMain = (ListView) v.findViewById(R.id.lv_main);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter != null)
                    adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    StateCityModel model = adapter.getItem(pos);
                    if (model != null) {
                        dialog.dismiss();
                        if (mListener != null) {
                            mListener.onStateClick(StatePickerDialog.this,model.getStateId(),model.getStateName());
                        }
                        if (fragment != null) {
                            fragment.dismiss();
                        }
                    }
                }
            }
        });

        StateCityModel listModel = getStateList();
        if (listModel.getOutput().equals("success")) {
            adapter = new StatesAdapter(getActivity(), listModel.getList());
            lvMain.setAdapter(adapter);
        }
        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }

    private StateCityModel getStateList() {
        return JsonParser.parseStateJson(AppUser.getStatesJson(getActivity()));
    }
}
