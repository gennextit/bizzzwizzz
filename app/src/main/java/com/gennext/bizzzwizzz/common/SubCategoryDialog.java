package com.gennext.bizzzwizzz.common;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.BusinessCategoryModel;
import com.gennext.bizzzwizzz.model.SubCategoryAdapter;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.JsonParser;

import java.util.ArrayList;


public class SubCategoryDialog extends DialogFragment {
    private AlertDialog dialog = null;
    private SubCategoryDialog fragment;
    private SubCategoryAdapter adapter;
    private ArrayList<BusinessCategoryModel> paymontList;
    private CheckBox cmSelectAll;
    private ArrayList<BusinessCategoryModel> originalPaymentList;
    private String sltCategoryId;

    public interface SubCatListener {
        void onSubCatClick(SubCategoryDialog SubCategoryDialog, ArrayList<BusinessCategoryModel> checkedList, ArrayList<BusinessCategoryModel> originalList);
    }

    private SubCatListener mListener;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static SubCategoryDialog newInstance(SubCatListener listener, String sltCategoryId, ArrayList<BusinessCategoryModel> originalPaymentList) {
        SubCategoryDialog fragment = new SubCategoryDialog();
        fragment.mListener = listener;
        fragment.fragment = fragment;
        fragment.sltCategoryId=sltCategoryId;
        fragment.originalPaymentList=originalPaymentList;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_payment_mode, null);
        dialogBuilder.setView(v);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        ListView lvMain = (ListView) v.findViewById(R.id.lv_main);
        cmSelectAll = (CheckBox) v.findViewById(R.id.checkBox_header);
        Button btnSubmit = (Button) v.findViewById(R.id.btn_submit);

        tvTitle.setText("Sub Category");
        cmSelectAll.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                for(BusinessCategoryModel model:paymontList){
                    if(cmSelectAll.isChecked()){
                        model.setChecked(true);
                    }else {
                        model.setChecked(false);
                    }
                }
                adapter.notifyDataSetChanged();

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if(adapter!=null) {
                    if (mListener != null) {
                        mListener.onSubCatClick(SubCategoryDialog.this, adapter.getCheckedList(),adapter.getList());
                    }
                    if (fragment != null) {
                        fragment.dismiss();
                    }
                }
            }
        });
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    BusinessCategoryModel model = adapter.getItem(pos);
                    if(model != null)
                        if (model.getChecked()) {
                            model.setChecked(false);
                            paymontList.set(pos,model);
                            adapter.notifyDataSetChanged();
                        }else{
                            model.setChecked(true);
                            paymontList.set(pos,model);
                            adapter.notifyDataSetChanged();
                        }
                }
            }
        });

        if(originalPaymentList==null) {
            BusinessCategoryModel listModel = getList();
            if (listModel.getOutput().equals("success")) {
                paymontList = listModel.getList();
                adapter = new SubCategoryAdapter(getActivity(), paymontList);
                lvMain.setAdapter(adapter);
            }
        }else{
            paymontList = originalPaymentList;
            adapter = new SubCategoryAdapter(getActivity(), paymontList);
            lvMain.setAdapter(adapter);
        }

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }

    private BusinessCategoryModel getList() {
        return JsonParser.parseSubCategoryMode(AppUser.getSubCategoriesJson(getActivity()),sltCategoryId);
    }
}
