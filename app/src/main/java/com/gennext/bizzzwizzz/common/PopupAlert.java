package com.gennext.bizzzwizzz.common;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.util.CompactFragment;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class PopupAlert extends CompactFragment {
    public static final int ACTIVITY = 1, FRAGMENT = 2, POPUP_DIALOG = 3;
    private TextView tvTitle, tvDesc;
    private Button btnOK;
    private String title, desc;
    private FragmentManager manager;
    private int finishType;
    private LinearLayout llWhitespace;

    private int popupTimeOutInSec;
    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context=null;
    }
    public static PopupAlert newInstance(String title, String description, int finishType, int popupTimeOutInSec) {
        PopupAlert popupAlert=new PopupAlert();
        popupAlert.title = title;
        popupAlert.desc = description;
        popupAlert.finishType = finishType;
        popupAlert.popupTimeOutInSec=popupTimeOutInSec;
        return popupAlert;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_popup_alert, container, false);
        manager = getFragmentManager();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        tvTitle = (TextView) v.findViewById(R.id.tv_popup_title);
        tvDesc = (TextView) v.findViewById(R.id.tv_popup_description);
        llWhitespace = (LinearLayout) v.findViewById(R.id.ll_whitespace);
        btnOK = (Button) v.findViewById(R.id.btn_popup);

        tvTitle.setText(title);
        tvDesc.setText(desc);

        llWhitespace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (finishType == ACTIVITY) {
                    getActivity().finish();
                } else if (finishType == POPUP_DIALOG) {
                    manager.popBackStack("popupAlert", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } else {
                    manager.popBackStack("popupAlert", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    manager.popBackStack();
                }
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (finishType == ACTIVITY) {
                    getActivity().finish();
                } else if (finishType == POPUP_DIALOG) {
                    manager.popBackStack("popupAlert", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } else {
                    manager.popBackStack("popupAlert", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    manager.popBackStack();
                }
            }
        });
        if(popupTimeOutInSec!=0) {
            startTimerForMainActivity();
        }
    }


    private void startTimerForMainActivity() {
        // TODO Auto-generated method stub
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(context!=null) {
                    PopupAlert popupAlert = (PopupAlert) getFragmentManager().findFragmentByTag("popupAlert");
                    if (popupAlert != null) {
                        if (finishType == ACTIVITY) {
                            getActivity().finish();
                        } else if (finishType == POPUP_DIALOG) {
                            manager.popBackStack("popupAlert", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        } else {
                            manager.popBackStack("popupAlert", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            manager.popBackStack();
                        }
                    }
                }
            }
        }, (popupTimeOutInSec*1000));
    }



}