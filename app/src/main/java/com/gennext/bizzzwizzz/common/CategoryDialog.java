package com.gennext.bizzzwizzz.common;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.BusinessCategoryModel;
import com.gennext.bizzzwizzz.model.CategoriesAdapter;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.JsonParser;

import java.util.ArrayList;


public class CategoryDialog extends DialogFragment {
    private AlertDialog dialog = null;
    private CategoryDialog fragment;
    private CategoriesAdapter adapter;


    public interface CatListener {
        void onCategoryClick(DialogFragment dialog, String categoryId, String categoryName);
    }

    private CatListener mListener;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static CategoryDialog newInstance(CatListener listener) {
        CategoryDialog fragment = new CategoryDialog();
        fragment.mListener = listener;
        fragment.fragment = fragment;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_state_picker, null);
        dialogBuilder.setView(v);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        EditText etSearch = (EditText) v.findViewById(R.id.et_search);
        ListView lvMain = (ListView) v.findViewById(R.id.lv_main);
        tvTitle.setText("Select Category");
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter != null)
                    adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (adapter != null) {
                    BusinessCategoryModel model = adapter.getItem(pos);
                    if (model != null) {
                        dialog.dismiss();
                        if (mListener != null) {
                            mListener.onCategoryClick(CategoryDialog.this,model.getCategoryId(),model.getCategory());
                        }
                        if (fragment != null) {
                            fragment.dismiss();
                        }
                    }
                }
            }
        });
        BusinessCategoryModel cModel = getCategoriesList();
        if (cModel!=null&& cModel.getOutput().equals("success")) {
            adapter = new CategoriesAdapter(getActivity(), cModel.getList());
            lvMain.setAdapter(adapter);
        }else{
            ArrayList<String> errorList = new ArrayList<>();
            errorList.add("Not category found");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.slot_domain,
                    R.id.tv_message, errorList);
            lvMain.setAdapter(adapter);
        }

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
    private BusinessCategoryModel getCategoriesList() {
        return JsonParser.parseCategoryData(AppUser.getCategoriesJson(getActivity()));
    }


}
