package com.gennext.bizzzwizzz.signup;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.SignupActivity;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.FieldValidation;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;
import com.gennext.bizzzwizzz.util.Signup;

import java.util.concurrent.TimeUnit;


/**
 * Created by Abhijit-PC on 28-Feb-17.
 */

public class MobileVerifyTab extends CompactFragment implements ApiCallError.ErrorFlagListener {
    private static final int TASK_OTP = 1,TASK_RESEND_OTP=2;
    private EditText etMobileOtp;

    private AssignTask assignTask;
    private long timeInMSec=60000;
    private TextView tvTimerCount;
    private Button btnResendOtp;
    private ProgressBar progressBarResentOtp;
    private String mobile;
    private LinearLayout llResendOtp;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public static MobileVerifyTab newInstance(Context context) {
        MobileVerifyTab fragment = new MobileVerifyTab();
        AppAnimation.setSlideAnimation(context,fragment, Gravity.RIGHT);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_mobile_verify_tab, container, false);
        InitUI(v);
        startCountdownTimer();
        return v;
    }
    private void startCountdownTimer() {
        CounterClass timer = new CounterClass(timeInMSec,1000);
        timer.start();
    }


    private void InitUI(View v) {
        etMobileOtp = (EditText) v.findViewById(R.id.et_profile_otp);
        tvTimerCount = (TextView) v.findViewById(R.id.tv_timer_count);
        btnResendOtp = (Button) v.findViewById(R.id.btn_mobile_retry);
        llResendOtp = (LinearLayout) v.findViewById(R.id.ll_resent_otp);
        progressBarResentOtp = (ProgressBar) v.findViewById(R.id.progressBar2);


        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeTask(TASK_RESEND_OTP);
            }
        });
        v.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeTask(TASK_OTP);
            }
        });
        v.findViewById(R.id.btn_mobile_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Signup.setMobileTemp(getActivity(),"");
                ((SignupActivity)getActivity()).setMobileTab();
            }
        });
    }

    private void executeTask(int task) {
        if(task==TASK_OTP) {
            if (checkValidation()) {
                hideKeybord(getActivity());
                String enteredOtp=etMobileOtp.getText().toString();
                if(enteredOtp.equalsIgnoreCase(Signup.getMobileOtp(getActivity()))) {
                    Signup.setMobileTemp(getActivity(), mobile);
                    Signup.setMobileVerification(getActivity(), "success");
                    ((SignupActivity) getActivity()).setEmailTab();
                }else {
                    Toast.makeText(getActivity(), "Invalid Otp", Toast.LENGTH_SHORT).show();
                }
            }
        }else if(task==TASK_RESEND_OTP) {
            hideKeybord(getActivity());
            assignTask = new AssignTask(getActivity(),task, etMobileOtp.getText().toString());
            assignTask.execute(AppSettings.MOBILE_VERIFY);
        }
    }

    private boolean checkValidation() {
        if (!FieldValidation.validate(getActivity(), etMobileOtp, FieldValidation.OTP, true)) {
            return false;
        }
        return true;
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, int flag) {
        executeTask(flag);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, int flag) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final int task;
        private String otp;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context,int task,String otp) {
            this.context = context;
            this.otp = otp;
            this.task = task;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context, "Processing please wait");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            String tempMobile = Signup.getMobileTemp(context);
            response = ApiCall.POST(urls[0], RequestBuilder.VerifyOtp(tempMobile,otp));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.parseMobileOtpVerify(context, response);
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if (task==TASK_OTP) {
//                            Signup.setMobileTemp(context,mobile);
//                            Signup.setMobileVerification(context,"success");
//                            ((SignupActivity)getActivity()).setEmailTab();
                        } else if (task==TASK_RESEND_OTP) {
                            tvTimerCount.setVisibility(View.VISIBLE);
                            llResendOtp.setVisibility(View.GONE);
                            startCountdownTimer();
                        }
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),task, MobileVerifyTab.this)
                                .show(getFragmentManager(), "apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,task, MobileVerifyTab.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }

    public class CounterClass extends CountDownTimer {
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            tvTimerCount.setVisibility(View.GONE);
            llResendOtp.setVisibility(View.VISIBLE);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
//            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
//                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
//                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            String hms = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));


//            long res=(millis *100)/timeInMSec;

            tvTimerCount.setText("Wait "+hms.substring(3,5)+" sec");
//            arcProgress.setProgress((int)res);
//            arcProgress.setBottomText(hms);

        }

    }
}
