package com.gennext.bizzzwizzz.signup;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.SignupActivity;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.FieldValidation;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;
import com.gennext.bizzzwizzz.util.Signup;

import java.util.concurrent.TimeUnit;


/**
 * Created by Abhijit-PC on 28-Feb-17.
 */

public class EmailVerifyTab extends CompactFragment implements ApiCallError.ErrorFlagListener {
    private static final int TASK_OTP = 1,TASK_RESEND_OTP=2;
    private EditText etEmailOtp;

    private AssignTask assignTask;
    private long timeInMSec=60000;
    private TextView tvTimerCount;
    private Button btnResendOtp;
    private String email;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public static EmailVerifyTab newInstance(Context context) {
        EmailVerifyTab fragment = new EmailVerifyTab();
        AppAnimation.setSlideAnimation(context,fragment, Gravity.RIGHT);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_email_verify_tab, container, false);
        InitUI(v);
        startCountdownTimer();
        return v;
    }
    private void startCountdownTimer() {
        CounterClass timer = new CounterClass(timeInMSec,1000);
        timer.start();
    }


    private void InitUI(View v) {
        etEmailOtp = (EditText) v.findViewById(R.id.et_profile_otp);
        tvTimerCount = (TextView) v.findViewById(R.id.tv_timer_count);
        btnResendOtp = (Button) v.findViewById(R.id.btn_mobile_retry);


        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeTask(TASK_RESEND_OTP);
            }
        });
        v.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeTask(TASK_OTP);
            }
        });
    }

    private void executeTask(int task) {
        if(task==TASK_OTP) {
            if (checkValidation()) {
                hideKeybord(getActivity());
                String enteredOtp=etEmailOtp.getText().toString();
                if (enteredOtp.equalsIgnoreCase(Signup.getEmailOtp(getActivity()))) {
                    Signup.setEmailTemp(getActivity(),email);
                    Signup.setEmailVerification(getActivity(),"success");
                    ((SignupActivity)getActivity()).setPersonalTab();
                } else {
                    Toast.makeText(getActivity(), "Invalid Otp", Toast.LENGTH_SHORT).show();
                }
            }
        }else if(task==TASK_RESEND_OTP) {
            hideKeybord(getActivity());
            assignTask = new AssignTask(getActivity(),task, etEmailOtp.getText().toString());
            assignTask.execute(AppSettings.EMAIL_VERIFY);
        }
    }

    private boolean checkValidation() {
        if (!FieldValidation.validate(getActivity(), etEmailOtp, FieldValidation.OTP, true)) {
            return false;
        }
        return true;
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, int flag) {
        executeTask(flag);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, int flag) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final int task;
        private String otp;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context,int task,String otp) {
            this.context = context;
            this.otp = otp;
            this.task = task;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context, "Processing please wait");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            String tempEmail=Signup.getEmailTemp(context);
            response = ApiCall.POST(urls[0], RequestBuilder.VerifyOtp(tempEmail,otp));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.parseLoginData(context, response);
//            return jsonParser.parseLoginDataTest();
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if (task==TASK_OTP) {

                        } else if (task==TASK_RESEND_OTP) {
                            tvTimerCount.setVisibility(View.VISIBLE);
                            btnResendOtp.setVisibility(View.GONE);
                            startCountdownTimer();
                        }
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    }
                } else {
                    ApiCallError.newInstance(JsonParser.ERRORMESSAGE,task, EmailVerifyTab.this)
                            .show(getFragmentManager(), "apiCallError");
                }
            }
        }
    }

    public class CounterClass extends CountDownTimer {
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            tvTimerCount.setVisibility(View.GONE);
            btnResendOtp.setVisibility(View.VISIBLE);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
//            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
//                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
//                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            String hms = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));


//            long res=(millis *100)/timeInMSec;

            tvTimerCount.setText("Wait "+hms.substring(3,5)+" sec");
//            arcProgress.setProgress((int)res);
//            arcProgress.setBottomText(hms);

        }

    }
}
