package com.gennext.bizzzwizzz.signup;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.SignupActivity;
import com.gennext.bizzzwizzz.common.AppWebView;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.FieldValidation;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;
import com.gennext.bizzzwizzz.util.Signup;


/**
 * Created by Abhijit-PC on 28-Feb-17.
 */

public class MobileTab extends CompactFragment implements ApiCallError.ErrorListener {
    private EditText etMobile;
    private CheckBox cbAccept;

    private AssignTask assignTask;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    public static MobileTab newInstance() {
        MobileTab fragment = new MobileTab();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_mobile_tab, container, false);
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        etMobile = (EditText) v.findViewById(R.id.et_profile_mobile);
        cbAccept = (CheckBox) v.findViewById(R.id.cb_accept);

        String mobile= Signup.getMobileTemp(getActivity());
        if (!TextUtils.isEmpty(mobile)) {
            etMobile.setText(mobile);
        }


        v.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeTask();
            }
        });
        v.findViewById(R.id.btn_privacy_policy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(AppWebView.newInstance("Privacy Policy", AppSettings.PRIVACY_POLICY, AppWebView.PDF), "appWebView");
            }
        });
        v.findViewById(R.id.btn_terms_of_use).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(AppWebView.newInstance("Terms Of Use", AppSettings.TERMS_OF_USE, AppWebView.PDF), "appWebView");
            }
        });


    }

    private void executeTask() {
        if (checkValidation()) {
            hideKeybord(getActivity());
            assignTask = new AssignTask(getActivity(),etMobile.getText().toString());
            assignTask.execute(AppSettings.MOBILE_VERIFY);
        }
    }

    private boolean checkValidation() {
        if (!FieldValidation.validate(getActivity(), etMobile, FieldValidation.MOBILE, true)) {
            return false;
        } else if (!cbAccept.isChecked()) {
            showToast(getActivity(), "Please Accept our Privacy Policy and Terms of Use");
            return false;
        }
        return true;
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }



    private class AssignTask extends AsyncTask<String, Void, Model> {
        private String  mobile;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String mobile) {
            this.context = context;
            this.mobile = mobile;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context, "Processing please wait");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            response = ApiCall.POST(urls[0], RequestBuilder.VerifyMobile(mobile));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.parseMobileVerify(context,response);
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        Signup.setMobileTemp(context,mobile);
                        ((SignupActivity)getActivity()).setMobileVerifyTab(mobile);
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    }else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(), MobileTab.this)
                                .show(getFragmentManager(), "apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true, MobileTab.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }
}
