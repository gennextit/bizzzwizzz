package com.gennext.bizzzwizzz.signup;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.IntentCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.gennext.bizzzwizzz.MainActivity;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.AppWebView;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.FieldValidation;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;
import com.gennext.bizzzwizzz.util.Signup;


/**
 * Created by Abhijit-PC on 28-Feb-17.
 */

public class PersonalTab extends CompactFragment implements ApiCallError.ErrorListener {
    private EditText etName, etPanNo, etPassword;
    private String profileType = "";

    private AssignTask assignTask;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    public static PersonalTab newInstance(Context context) {
        PersonalTab fragment = new PersonalTab();
        AppAnimation.setSlideAnimation(context,fragment, Gravity.RIGHT);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_personal_tab, container, false);
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        etName = (EditText) v.findViewById(R.id.et_profile_name);
        etPanNo = (EditText) v.findViewById(R.id.et_profile_pan);
        etPassword = (EditText) v.findViewById(R.id.et_profile_pass);


        RadioGroup rgProfileType = (RadioGroup) v.findViewById(R.id.rg_profile_type);


        v.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeTask();
            }
        });
        v.findViewById(R.id.btn_privacy_policy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(AppWebView.newInstance("Privacy Policy", AppSettings.PRIVACY_POLICY, AppWebView.PDF), "appWebView");
            }
        });
        v.findViewById(R.id.btn_terms_of_use).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(AppWebView.newInstance("Terms Of Use", AppSettings.TERMS_OF_USE, AppWebView.PDF), "appWebView");
            }
        });

        profileType = "personal";
        rgProfileType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId == R.id.personal) {
                    profileType = "personal";
                } else if (checkedId == R.id.business) {
                    profileType = "business";
                }
            }
        });

    }

    private void executeTask() {
        if (checkValidation()) {
            hideKeybord(getActivity());
            assignTask = new AssignTask(getActivity(), etName.getText().toString(), etPanNo.getText().toString(), etPassword.getText().toString(),
                    profileType);
            assignTask.execute(AppSettings.SIGNUP);
        }
    }

    private boolean checkValidation() {
        if (!FieldValidation.validate(getActivity(), etName, FieldValidation.NAME, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etPanNo, FieldValidation.STRING, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etPassword, FieldValidation.STRING, true)) {
            return false;
        } else if (TextUtils.isEmpty(profileType)) {
            showToast(getActivity(), "Please Select profile type");
            return false;
        }
        return true;
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String name, panNo, pass, profileType;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String name, String panNo, String pass
                , String profileType) {
            this.context = context;
            this.name = name;
            this.panNo = panNo;
            this.pass = pass;
            this.profileType = profileType;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context, "Processing please wait");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            String mobile= Signup.getMobileTemp(context);
            String email=Signup.getEmailTemp(context);
            response = ApiCall.POST(urls[0], RequestBuilder.Signup(name, mobile, email, panNo, pass, profileType));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.parseLoginData(context, response);
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        Signup.setMobileTemp(context,"");
                        Signup.setEmailTemp(context,"");
                        Signup.setMobileVerification(context,"");
                        Signup.setEmailVerification(context,"");
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
//                        getActivity().finish();
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(), PersonalTab.this)
                                .show(getFragmentManager(), "apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true, PersonalTab.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }
}
