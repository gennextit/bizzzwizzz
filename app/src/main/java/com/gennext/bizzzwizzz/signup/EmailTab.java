package com.gennext.bizzzwizzz.signup;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.SignupActivity;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.FieldValidation;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;
import com.gennext.bizzzwizzz.util.Signup;


/**
 * Created by Abhijit-PC on 28-Feb-17.
 */

public class EmailTab extends CompactFragment implements ApiCallError.ErrorListener {
    private EditText etEmail;

    private AssignTask assignTask;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    public static EmailTab newInstance(Context context) {
        EmailTab fragment = new EmailTab();
        AppAnimation.setSlideAnimation(context,fragment, Gravity.RIGHT);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_email_tab, container, false);
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        etEmail = (EditText) v.findViewById(R.id.et_profile_email);

        String emailAddress= Signup.getEmailTemp(getActivity());
        if (!TextUtils.isEmpty(emailAddress)) {
            etEmail.setText(emailAddress);
        }

        v.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeTask();
            }
        });

    }

    private void executeTask() {
        if (checkValidation()) {
            hideKeybord(getActivity());
            assignTask = new AssignTask(getActivity(),etEmail.getText().toString() );
            assignTask.execute(AppSettings.EMAIL_VERIFY);
        }
    }

    private boolean checkValidation() {
        if (!FieldValidation.validate(getActivity(), etEmail, FieldValidation.EMAIL, true)) {
            return false;
        }
        return true;
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String  email ;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context , String email ) {
            this.context = context;
            this.email = email;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context, "Processing please wait");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            response = ApiCall.POST(urls[0], RequestBuilder.EmailVerification(email));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.parseEmailVerificationDate(context, response);
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        Signup.setEmailTemp(context,email);
                        ((SignupActivity)getActivity()).setEmailVerifyTab(email);
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    }else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(), EmailTab.this)
                                .show(getFragmentManager(), "apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true, EmailTab.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }
}
