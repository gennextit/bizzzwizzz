package com.gennext.bizzzwizzz;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.gennext.bizzzwizzz.signup.EmailTab;
import com.gennext.bizzzwizzz.signup.EmailVerifyTab;
import com.gennext.bizzzwizzz.signup.MobileTab;
import com.gennext.bizzzwizzz.signup.MobileVerifyTab;
import com.gennext.bizzzwizzz.signup.PersonalTab;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.Signup;

/**
 * Created by Abhijit-PC on 01-Mar-17.
 */

public class SignupActivity extends BaseActivity implements View.OnClickListener{

    public static final int TAB_MOBILE = 1,TAB_MOBILE_VERIFY=2,TAB_EMAIL=3,TAB_EMAIL_VERIFY=4,TAB_PERSONAL=5;
    private MobileTab mobileTab;
    private MobileVerifyTab mobileVerifyTab;
    private EmailTab emailTab;
    private EmailVerifyTab emailVerifyTab;
    private PersonalTab personalTab;
    private Button tabMobile,tabPersonal,tabEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initUi();
        initTabs();
        setTab();
    }

    private void initTabs() {
        mobileTab=mobileTab.newInstance();
        mobileVerifyTab=MobileVerifyTab.newInstance(SignupActivity.this);
        emailTab=EmailTab.newInstance(SignupActivity.this);
        emailVerifyTab=EmailVerifyTab.newInstance(SignupActivity.this);
        personalTab=PersonalTab.newInstance(SignupActivity.this);
    }

    private void initUi() {
        tabMobile=(Button)findViewById(R.id.btn_verify_mobile);
        tabEmail=(Button)findViewById(R.id.btn_verify_email);
        tabPersonal=(Button)findViewById(R.id.btn_verify_personal);
        tabMobile.setOnClickListener(this);
        tabEmail.setOnClickListener(this);
        tabPersonal.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_verify_mobile:
                if(Signup.isMobileVerified(SignupActivity.this)){
                    setTab(TAB_MOBILE);
                }
                break;
            case R.id.btn_verify_email:
                if(Signup.isEmailVerified(SignupActivity.this)){
                    setTab(TAB_EMAIL);
                }
                break;
            case R.id.btn_verify_personal:
                if(Signup.isEmailVerified(SignupActivity.this)){
                    setTab(TAB_PERSONAL);
                }
                break;
        }
    }

    public void setTab() {
        if(!Signup.isMobileVerified(SignupActivity.this)){
            if(TextUtils.isEmpty(Signup.getMobileTemp(getApplicationContext()))){
                setTab(TAB_MOBILE);
            }else {
                setTab(TAB_MOBILE_VERIFY);
            }
        }else if(!Signup.isEmailVerified(SignupActivity.this)){
            if(TextUtils.isEmpty(Signup.getEmailTemp(getApplicationContext()))){
                setTab(TAB_EMAIL);
            }else {
                setTab(TAB_EMAIL_VERIFY);
            }
        }else if(!Signup.isPersonalVerified(SignupActivity.this)){
            setTab(TAB_PERSONAL);
        }
    }
    public void setTab(int tab) {
        switch (tab){
            case TAB_MOBILE:
                tabMobile.setTextColor(ContextCompat.getColor(SignupActivity.this,R.color.black));
                tabEmail.setTextColor(ContextCompat.getColor(SignupActivity.this,R.color.text_lite));
                tabPersonal.setTextColor(ContextCompat.getColor(SignupActivity.this,R.color.text_lite));
                tabMobile.setTypeface(Typeface.DEFAULT_BOLD);
                tabEmail.setTypeface(Typeface.DEFAULT);
                tabPersonal.setTypeface(Typeface.DEFAULT);
                setTabFragment(R.id.signup_container,mobileTab,"mobileTab",mobileVerifyTab,emailTab,emailVerifyTab,personalTab);
                break;
            case TAB_MOBILE_VERIFY:
                setTabFragment(R.id.signup_container,mobileVerifyTab,"mobileVerifyTab",mobileTab,emailTab,emailVerifyTab,personalTab);
                break;
            case TAB_EMAIL:
                tabMobile.setTextColor(ContextCompat.getColor(SignupActivity.this,R.color.green));
                tabEmail.setTextColor(ContextCompat.getColor(SignupActivity.this,R.color.black));
                tabPersonal.setTextColor(ContextCompat.getColor(SignupActivity.this,R.color.text_lite));
                tabMobile.setTypeface(Typeface.DEFAULT);
                tabEmail.setTypeface(Typeface.DEFAULT_BOLD);
                tabPersonal.setTypeface(Typeface.DEFAULT);
                setTabFragment(R.id.signup_container,emailTab,"emailTab",mobileVerifyTab,mobileTab,emailVerifyTab,personalTab);
                break;
            case TAB_EMAIL_VERIFY:
                setTabFragment(R.id.signup_container,emailVerifyTab,"emailVerifyTab",mobileVerifyTab,emailTab,mobileTab,personalTab);
                break;
            case TAB_PERSONAL:
                tabMobile.setTextColor(ContextCompat.getColor(SignupActivity.this,R.color.green));
                tabEmail.setTextColor(ContextCompat.getColor(SignupActivity.this,R.color.green));
                tabPersonal.setTextColor(ContextCompat.getColor(SignupActivity.this,R.color.black));
                tabMobile.setTypeface(Typeface.DEFAULT);
                tabEmail.setTypeface(Typeface.DEFAULT);
                tabPersonal.setTypeface(Typeface.DEFAULT_BOLD);
                setTabFragment(R.id.signup_container,personalTab,"personalTab",mobileVerifyTab,emailTab,emailVerifyTab,mobileTab);
                break;
        }
    }

    public void setMobileVerifyTab(String mobile) {
        mobileVerifyTab.setMobile(mobile);
        setTab(TAB_MOBILE_VERIFY);
    }

    public void setEmailTab() {
        setTab(TAB_EMAIL);
    }

    public void setEmailVerifyTab(String email) {
        emailVerifyTab.setEmail(email);
        setTab(TAB_EMAIL_VERIFY);
    }

    public void setPersonalTab() {
        setTab(TAB_PERSONAL);
    }

    public void setMobileTab() {
        setTab(TAB_MOBILE);
    }
}
