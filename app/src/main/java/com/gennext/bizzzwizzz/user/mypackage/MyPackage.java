package com.gennext.bizzzwizzz.user.mypackage;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.MyPackageModel;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.DateTimeUtility;
import com.gennext.bizzzwizzz.util.JsonParser;

/**
 * Created by Abhijit on 08-Dec-16.
 */

public class MyPackage extends CompactFragment {
//    private static final int LOAD_LIST_TASK = 1, UPGRADE_TASK = 2;
//    private Button btnSend;
////    private AssignTask assignTask;
//    private ArrayList<MyPackageModel> cList;
//    private LinearLayout llProgress;
//    private RecyclerView lvMain;
//    private ProgressBar pBar;
//    private SwipeRefreshLayout mSwipeRefreshLayout;
//    private MyPackageAdapter adapter;

//    @Override
//    public void onAttach(Activity activity) {
//        // TODO Auto-generated method stub
//        super.onAttach(activity);
//        if (assignTask != null) {
//            assignTask.onAttach(activity);
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        if (assignTask != null) {
//            assignTask.onDetach();
//        }
//    }


    private TextView tvPackageName, tvPackageAmt, tvPackageVal, tvPackageCapping, tvPackageType;

    public static MyPackage newInstance() {
        MyPackage myPackage = new MyPackage();
        return myPackage;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_my_package, container, false);
//        initToolBar(getActivity(), v, "My Package");
        InitUI(v);
        loadPackageDetail();
        return v;
    }

    private void loadPackageDetail() {
        MyPackageModel modelList = JsonParser.parseMyPackageDetail(AppUser.getPackageDetails(getActivity()));
        if (modelList.getOutput().equalsIgnoreCase("success")) {
            tvPackageName.setText(modelList.getPackageName());
            tvPackageAmt.setText(modelList.getPackageAmount());
            tvPackageVal.setText(DateTimeUtility.convertDateYMD(modelList.getValidity()));
            tvPackageCapping.setText(modelList.getCapping());
            tvPackageType.setText(modelList.getPackageType());
        }
    }

    private void InitUI(View v) {
        tvPackageName = (TextView) v.findViewById(R.id.tv_package_slot_1);
        tvPackageAmt = (TextView) v.findViewById(R.id.tv_package_slot_2);
        tvPackageVal = (TextView) v.findViewById(R.id.tv_package_slot_3);
        tvPackageCapping = (TextView) v.findViewById(R.id.tv_package_slot_4);
        tvPackageType = (TextView) v.findViewById(R.id.tv_package_slot_5);
    }


}

