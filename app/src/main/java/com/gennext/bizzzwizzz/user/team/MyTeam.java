package com.gennext.bizzzwizzz.user.team;

/**
 * Created by Abhijit on 08-Dec-16.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.MyTeamModel;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;


public class MyTeam extends CompactFragment implements ApiCallError.ErrorListener{

    private AssignTask assignTask;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String userId;
    private TextView tvTreeText;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static MyTeam newInstance() {
        MyTeam myTeam = new MyTeam();
        return myTeam;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_myteam, container, false);


        initToolBar(getActivity(), v, "My Team");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        ImageView ivHome = (ImageView) v.findViewById(R.id.iv_home);
        ivHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack("myTeam",FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               // executeTask(null,null);
                hideProgressBar();
            }
        });
        executeTask(null,null);
    }

    private void executeTask(String userId,TextView tvTreeText) {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        assignTask = new AssignTask(getActivity(),userId,tvTreeText);
        assignTask.execute(AppSettings.GENERATE_TREE_VIEW);
    }
    private void hideProgressBar() {
        mSwipeRefreshLayout.setRefreshing(false);

    }

    public void addNewTree(String userId, TextView tvTreeText) {
        this.userId=userId;
        this.tvTreeText=tvTreeText;
        executeTask(userId,tvTreeText);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask(userId,tvTreeText);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }


    private class AssignTask extends AsyncTask<String, Void, MyTeamModel> {
        private Context context;
        private String userId;
        private TextView tvTreeText;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String userId,TextView tvTreeText) {
            this.context = context;
            this.userId=userId;
            this.tvTreeText=tvTreeText;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showProgressDialog(context, "Processing, please wait");
        }

        @Override
        protected MyTeamModel doInBackground(String... urls) {
            String response;
            if(userId==null) {
                response = ApiCall.POST(urls[0], RequestBuilder.Default(AppUser.getUserId(context)));
            }else{
                response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
            }
            JsonParser jsonParser = new JsonParser();

            return jsonParser.parseTreeView2(response);
        }


        @Override
        protected void onPostExecute(MyTeamModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        TreeView treeView= TreeView.newInstance(result,MyTeam.this);
                        if(userId!=null&&tvTreeText!=null) {
                            addFragment(treeView, R.id.tree_container, "treeView");
//                            addSharedFragment(treeView, R.id.tree_container,tvTreeText);
                        }else{
                            addFragmentWithoutBackStack(treeView, R.id.tree_container, "treeView");
                        }
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),MyTeam.this)
                                .show(getFragmentManager(),"apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,MyTeam.this)
                                .show(getFragmentManager(),"apiCallError");
                    }
                }
            }
        }
    }

    private void addSharedFragment(TreeView treeView, int tree_container, TextView tvText) {
        String textTransitionName = "";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementReturnTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(R.transition.change_image_trans));
            setExitTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(android.R.transition.fade));

            treeView.setSharedElementEnterTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(R.transition.change_image_trans));
            treeView.setEnterTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(android.R.transition.fade));

            textTransitionName = tvText.getTransitionName();
        }

        Bundle bundle = new Bundle();
        bundle.putString("ACTION", tvText.getText().toString());
        bundle.putString("TRANS_TEXT", textTransitionName);
        treeView.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .add(tree_container, treeView)
                .addToBackStack("treeView")
                .addSharedElement(tvText, textTransitionName)
                .commit();
    }


}
