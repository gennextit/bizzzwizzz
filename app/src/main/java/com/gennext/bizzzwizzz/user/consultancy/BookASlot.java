package com.gennext.bizzzwizzz.user.consultancy;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzz.DocumentMaster;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.DatePickerDialog;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.ConsultancyModel;
import com.gennext.bizzzwizzz.model.SpinnerHintAdapter;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.DateTimeUtility;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;
import com.gennext.bizzzwizzz.util.Utility;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Abhijit on 08-Dec-16.
 */

public class BookASlot extends CompactFragment implements DatePickerDialog.DateSelectListener ,ApiCallError.ErrorFlagListener{
    private static final int SPINNER_TASK = 1, GET_SLOT_REQUEST = 2, BOOKING_REQUEST = 3;
    private Button btnSend,btnDoc;
    private AssignTask assignTask;
    private Spinner spDomain;
    private String sltDomain;
    private SpinnerHintAdapter adapter;
    private String sltDate;
    private int sday, smonth, syear;
    private Button btnSelectDate;
    private EditText etDescription;
    private String timeSlot;
    private AllSlotBookings allSlotBookings;
    private TextView tvDocStatus;
    private PermissionListener onPermissionListener;
    private String sltFilePath;
    private File sltFile;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static BookASlot newInstance(AllSlotBookings allSlotBookings) {
        BookASlot bookASlot = new BookASlot();
        bookASlot.allSlotBookings=allSlotBookings;
        return bookASlot;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_bookaslot, container, false);
        initToolBar(getActivity(), v, "Book A Slot for Consultancy");
        InitUI(v);
        setPermissionListener();
        return v;
    }

    private void setPermissionListener() {
        onPermissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                onPickDoc();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getActivity(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }


        };
    }

    public void onPickDoc() {
        Intent intent = new Intent(getActivity(), DocumentMaster.class);
        intent.putExtra("from", DocumentMaster.REQ_BOOK_SLOT);
        startActivityForResult(intent, DocumentMaster.REQ_BOOK_SLOT);
    }

    public void addThemToView(String filePath) {
        sltFilePath=filePath;
        sltFile =new File(filePath);
        String string1 = Utility.getColoredSpanned(sltFile.getName(),"#3ab54a");
        String string2 = Utility.getColoredSpanned("\nis selected","#666666");
        tvDocStatus.setText(Html.fromHtml(string1+" "+string2));
        Toast.makeText(getActivity(), sltFile.getName(), Toast.LENGTH_LONG).show();
    }


    private void InitUI(View v) {
        btnSend = (Button) v.findViewById(R.id.btn_send);
        spDomain = (Spinner) v.findViewById(R.id.sp_domain);
        etDescription = (EditText) v.findViewById(R.id.et_bookslot_description);
        btnSelectDate = (Button) v.findViewById(R.id.btn_select_date);
        btnSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePicker();
            }
        });
        btnDoc = (Button) v.findViewById(R.id.btn_browse_bank);
        tvDocStatus = (TextView) v.findViewById(R.id.tv_browse_bank);
        btnDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TedPermission(getActivity())
                        .setPermissionListener(onPermissionListener)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setGotoSettingButtonText("setting")
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .check();
            }
        });
        spDomain.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> ad, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
//                    ConsultancyModel model=adapter.getItem(position);
                if(adapter!=null)
                sltDomain = adapter.getDomainId(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                if (!sltDomain.equals("0")) {
                    if (sltDate != null) {
                        executeTask(GET_SLOT_REQUEST, sltDomain, sltDate, null);
                    } else {
                        openDatePicker();
                    }
                } else {
                    showToast(getActivity(), "Please select function");
                }
            }
        });
        executeTask(SPINNER_TASK, null, null, null);
    }


    private void openDatePicker() {
        DatePickerDialog.newInstance(BookASlot.this, sday, smonth, syear).
                show(getFragmentManager(), "datePickerDialog");
    }

    @Override
    public void onSelectDateClick(DialogFragment dialog, int date, int month, int year) {
        dialog.dismiss();
        sltDate = DateTimeUtility.cDateMDY(date, month, year);
        btnSelectDate.setText(DateTimeUtility.cViewDateMDY(date, month, year));
        syear = year;
        smonth = month;
        sday = date;
    }


    public void executeTask(int task, String sltDomain, String sltDate, String slotTime) {
        String description =etDescription.getText().toString();
        if (task != SPINNER_TASK) {
            if(!TextUtils.isEmpty(description)) {
                assignTask = new AssignTask(getActivity(), task, sltDomain, sltDate, slotTime, description,sltFile);
                assignTask.execute();
            }else{
                showToast(getActivity(), "Please enter description.");
            }
        }else{
            assignTask = new AssignTask(getActivity(), task, sltDomain, sltDate, slotTime, description,sltFile);
            assignTask.execute();
        }

    }

    public void requestBooking(String slotTime) {
        this.timeSlot=slotTime;
        executeTask(BOOKING_REQUEST, sltDomain, sltDate, slotTime);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, int task) {
        if (task == SPINNER_TASK) {
            executeTask(SPINNER_TASK, null, null, null);
        } else if (task == GET_SLOT_REQUEST) {
            executeTask(GET_SLOT_REQUEST, sltDomain, sltDate, null);
        } else if (task == BOOKING_REQUEST) {
            executeTask(BOOKING_REQUEST, sltDomain, sltDate, timeSlot);
        }
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, int flag) {

    }



    private class AssignTask extends AsyncTask<Void, Void, ConsultancyModel> {
        private final String domainId;
        private final String date;
        private final String time;
        private final String description;
        private final File sltFile;
        private int task;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, int task, String domainId, String date, String time,
                          String description, File sltFile) {
            this.context = context;
            this.task = task;
            this.domainId = domainId;
            this.date = date;
            this.time = time;
            this.description = description;
            this.sltFile=sltFile;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (task != SPINNER_TASK) {
                showProgressDialog(context, "Processing, please wait");
            }
        }

        @Override
        protected ConsultancyModel doInBackground(Void... urls) {
            String response = "";
            if (task == SPINNER_TASK) {
                response = AppUser.getDOMAINS(context);
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseDomain(response);
            } else if (task == GET_SLOT_REQUEST) {
                response = ApiCall.POST(AppSettings.GET_SLOTS, RequestBuilder.BSlotRequest(domainId, date));
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseGetSlot(response);
            } else if (task == BOOKING_REQUEST) {
                String userId = AppUser.getUserId(context);
                response = ApiCall.upload(AppSettings.BOOK_SLOT, RequestBuilder.BookingRequest(domainId, date, time, description, userId,sltFile));
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseBookingRequest(response);
            }
            return null;
        }


        @Override
        protected void onPostExecute(ConsultancyModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (task != SPINNER_TASK) {
                    hideProgressDialog();
                }
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if (task == SPINNER_TASK) {
                            adapter = new SpinnerHintAdapter(getActivity(), result.getList());
                            spDomain.setAdapter(adapter);
                        } else if (task == GET_SLOT_REQUEST) {
                            ViewTimeSlot bookTimeSlot = ViewTimeSlot.newInstance(BookASlot.this, result.getTimeSlotList());
                            addFragment(bookTimeSlot, "bookTimeSlot");
                        } else if (task == BOOKING_REQUEST) {
//                            reInitilizeAllField();
                            getFragmentManager().popBackStack();
                            allSlotBookings.bookingConfirmed(getSt(R.string.Booking_Confirmed_tag)
                                    ,result.getOutputMsg());
                        }
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),task,BookASlot.this)
                                .show(getFragmentManager(),"apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,task,BookASlot.this)
                                .show(getFragmentManager(),"apiCallError");
                    }
                }
            }
        }
    }




    private void reInitilizeAllField() {
        allSlotBookings.hideBookingSlotOption();
        spDomain.setSelection(0);
        etDescription.setText("");
        btnSelectDate.setText("Select Date");
        sltDate = null;
    }

}
