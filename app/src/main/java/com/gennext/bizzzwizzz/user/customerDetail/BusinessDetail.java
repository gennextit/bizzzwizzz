package com.gennext.bizzzwizzz.user.customerDetail;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gennext.bizzzwizzz.ImageMaster;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.UpdateCustomerDetail;
import com.gennext.bizzzwizzz.common.CategoryDialog;
import com.gennext.bizzzwizzz.common.CityPickerDialog;
import com.gennext.bizzzwizzz.common.DayAndTimeOfBusiness;
import com.gennext.bizzzwizzz.common.ModeOfPaymentDialog;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.common.StatePickerDialog;
import com.gennext.bizzzwizzz.common.SubCategoryDialog;
import com.gennext.bizzzwizzz.model.BankAdapter;
import com.gennext.bizzzwizzz.model.BankModel;
import com.gennext.bizzzwizzz.model.BusinessCategoryModel;
import com.gennext.bizzzwizzz.model.DayAndTimeModel;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.FieldValidation;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.L;
import com.gennext.bizzzwizzz.util.RequestBuilder;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Abhijit on 12-Jan-17.
 */

public class BusinessDetail extends CompactFragment implements CategoryDialog.CatListener,DayAndTimeOfBusiness.DayAndTimeOfBussListener, SubCategoryDialog.SubCatListener, ModeOfPaymentDialog.ModeOfPayListener, CityPickerDialog.CityDialogListener, StatePickerDialog.StateDialogListener, View.OnClickListener, ApiCallError.ErrorListener ,ApiCallError.ErrorFlagListener{


    private AssignTask assignTask;
    private Button btnSubmit;
    private EditText etCompName, etContactPerson, etDestContactPerson, etMobile, etEmail, etLandline1, etLandline2, etWebsite, etAddress, etPinCode, etDealInIteams;
    private String sltEntityType, sltCategoryId;
    private BankAdapter entityAdapter;
    private Spinner spTypeOfEntity;
//    private CategoryAdapter categoryAdapter;
    private LinearLayout llState, llCity, llDayAndTymeOfBusiness, llModeOfPayment, llCat, llSubCat;
    private TextView tvState, tvCity, tvModeOfPayment, tvCat, tvSubCat, tvDayAndTymeOfBusiness;
    private String sltState, sltStateId, sltCity, sltCityId;
    private ArrayList<BankModel> originalPaymentList, sltPaymentList;
    private ArrayList<BusinessCategoryModel> originalSubCatList, sltSubCatList;
    private String sltDayAndTimeIds , sltModeOfPaymentIDs, sltSubCategoryIds;
    private CustomerDetail customerDetail;
    private ArrayList<String> businessArray;
    private ArrayList<BankModel> typeOfEntityArray;
//    private ArrayList<BusinessCategoryModel> categoryArray;
    private ArrayList<DayAndTimeModel> originalDayAndTimeList, sltDayAndTimeList;
    private String sltDayAndTime;
    private Uri mImageUri;
    private ImageView ivProfile;
    private RefreshData refreshData;
//    private String sltDayAndTimeString;


    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
        if (refreshData != null) {
            refreshData.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
        if (refreshData != null) {
            refreshData.onDetach();
        }
    }

    public static BusinessDetail newInstance(CustomerDetail customerDetail, ArrayList<String> businessArray) {
        BusinessDetail fragment = new BusinessDetail();
        fragment.customerDetail = customerDetail;
        fragment.businessArray = businessArray;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_business_detail, container, false);
        Toolbar toolBar = (Toolbar) v.findViewById(R.id.toolbar);
        if (customerDetail != null) {
            toolBar.setVisibility(View.VISIBLE);
            initToolBar(getActivity(), v, "Business");
        } else {
            toolBar.setVisibility(View.VISIBLE);
            initToolBar(getActivity(), v, "Business");
        }
        InitUI(v);
        setTypeOfEntity();
//        setCategory();
        if(customerDetail==null){
            businessArray=loadBusinessDetail();
        }
        if (businessArray != null) {
            setData(businessArray);
        }
        return v;
    }


    private void setData(ArrayList<String> model) {
        if (model != null) {
            btnSubmit.setText("Update");
            etCompName.setText(model.get(0));
//            tvTypOfEntity.setText(model.get(1));
            etContactPerson.setText(model.get(2));
            etDestContactPerson.setText(model.get(3));
            etMobile.setText(model.get(4));
            etEmail.setText(model.get(5));
            etLandline1.setText(model.get(6));
            etLandline2.setText(model.get(7));
            etWebsite.setText(model.get(8));
            etAddress.setText(model.get(11));
            etPinCode.setText(model.get(14));
            etDealInIteams.setText(model.get(23));
            sltCategoryId = model.get(15);
            for (int i = 0; i < typeOfEntityArray.size(); i++) {
                if (typeOfEntityArray.get(i).getId() != null && typeOfEntityArray.get(i).getId().equalsIgnoreCase(model.get(10))) {
                    spTypeOfEntity.setSelection(i);
                }
            }
            setCityState(model.get(12),model.get(20),model.get(13),model.get(21));
            setCategoryIds(model.get(15),model.get(19));

            this.sltSubCategoryIds =model.get(16);
            setSubCategoryIds(model.get(16));
            this.sltModeOfPaymentIDs=model.get(17);
            setPaymentModeId(model.get(17));
            this.sltDayAndTimeIds=model.get(18);
            setDayAndTimeIds(model.get(18));

            if(!TextUtils.isEmpty(model.get(22))) {
                Glide.with(getActivity())
                        .load(model.get(22))
                        .bitmapTransform(new CropCircleTransformation(getActivity()))
                        .into(ivProfile);
            }else {
                Glide.with(getActivity())
                        .load(R.drawable.profile)
                        .bitmapTransform(new CropCircleTransformation(getActivity()))
                        .into(ivProfile);
            }
        }
    }

    private void setDayAndTimeIds(String sArray) {
        String ids = JsonParser.parseDayAndTimingIds(sArray);
        if(!TextUtils.isEmpty(ids)){
            tvDayAndTymeOfBusiness.setText(ids.substring(0,(ids.length()-1)));
        }
    }

    private void setCityState(String stateId, String stateName, String cityId, String cityName) {
        this.sltStateId=stateId;
        this.sltCityId=cityId;
        this.sltState = stateName;
        this.sltCity = cityName;
        tvState.setText(stateName);
        tvCity.setText(cityName);
    }

    private void setCategoryIds(String catId,String catName) {
        sltCategoryId=catId;
        tvCat.setText(catName);
    }

    private void setPaymentModeId(String sArray) {
        if (sArray != null) {
            BankModel jModel = JsonParser.parsePaymentMode(AppUser.getPaymentModeJson(getActivity()));
            ArrayList<BankModel> oList = jModel.getList();
            ArrayList<BankModel> sltList = new ArrayList<>();
            String id, name = "";
            if (oList != null && sArray.contains("[")) {
                try {
                    JSONArray main = new JSONArray(sArray);

                    for (int i = 0; i < main.length(); i++) {
                        id = main.optString(i);
                        BankModel m;
                        for (int j = 0; j < oList.size(); j++) {
                            m = oList.get(j);
                            if (m.getId().equals(id)) {
                                m.setChecked(true);
                                sltList.add(m);
                                name += m.getName() + ",";
                                oList.set(j, m);
                            }
                        }
                    }
                } catch (JSONException e) {
                    L.m(e.toString());
                }
            }
            if (!TextUtils.isEmpty(name)) {
                tvModeOfPayment.setText(name);
            }
            originalPaymentList = new ArrayList<>();
            originalPaymentList.addAll(oList);
            this.sltPaymentList = sltList;
        }
    }

    private void setSubCategoryIds(String sArray) {
        if (sArray != null) {
            BusinessCategoryModel jModel = JsonParser.parseSubCategoryMode(AppUser.getSubCategoriesJson(getActivity()), sltCategoryId);
            ArrayList<BusinessCategoryModel> oList = jModel.getList();
            ArrayList<BusinessCategoryModel> sltList = new ArrayList<>();
            String id, name = "";
            if (oList != null && sArray.contains("[")) {
                try {
                    JSONArray main = new JSONArray(sArray);

                    for (int i = 0; i < main.length(); i++) {
                        id = main.optString(i);
                        BusinessCategoryModel m;
                        for (int j = 0; j < oList.size(); j++) {
                            m = oList.get(j);
                            if (oList.get(j).getSubCategoryId().equals(id)) {
                                m.setChecked(true);
                                sltList.add(m);
                                name += m.getSubCategory() + ",";
                                oList.set(j, m);
                            }
                        }
                    }
                } catch (JSONException e) {
                    L.m(e.toString());
                }
            }
            if (!TextUtils.isEmpty(name)) {
                tvSubCat.setText(name);
            }
            originalSubCatList = new ArrayList<>();
            originalSubCatList.addAll(oList);
            this.sltSubCatList = sltList;
        }
    }

    private void InitUI(View v) {
        btnSubmit = (Button) v.findViewById(R.id.btn_submit);
        etCompName = (EditText) v.findViewById(R.id.et_business_1);
        etContactPerson = (EditText) v.findViewById(R.id.et_business_2);
        etDestContactPerson = (EditText) v.findViewById(R.id.et_business_3);
        etMobile = (EditText) v.findViewById(R.id.et_business_4);
        etEmail = (EditText) v.findViewById(R.id.et_business_5);
        etLandline1 = (EditText) v.findViewById(R.id.et_business_6);
        etLandline2 = (EditText) v.findViewById(R.id.et_business_7);
        etWebsite = (EditText) v.findViewById(R.id.et_business_8);
        etAddress = (EditText) v.findViewById(R.id.et_business_9);
        etPinCode = (EditText) v.findViewById(R.id.et_business_10);
        etDealInIteams = (EditText) v.findViewById(R.id.et_business_11);

        spTypeOfEntity = (Spinner) v.findViewById(R.id.sp_type_of_entity);

        spTypeOfEntity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (entityAdapter != null)
                    sltEntityType = entityAdapter.getBankId(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
//        spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
//                if (categoryAdapter != null) {
//                    if (!categoryAdapter.getCategoryId(position).equals("")) {
//                        if (originalSubCatList != null) {
//                            sltCategoryId = categoryAdapter.getCategoryId(position);
//                            sltSubCatList = null;
//                            originalSubCatList = null;
//                            tvSubCat.setText("Select Sub category");
//                        }
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        llState = (LinearLayout) v.findViewById(R.id.ll_select_state);
        llCity = (LinearLayout) v.findViewById(R.id.ll_select_city);
        llModeOfPayment = (LinearLayout) v.findViewById(R.id.ll_select_modeofpayment);
        llDayAndTymeOfBusiness = (LinearLayout) v.findViewById(R.id.ll_select_d_and_t_business);
        llSubCat = (LinearLayout) v.findViewById(R.id.ll_select_subcat);
        llCat = (LinearLayout) v.findViewById(R.id.ll_select_cat);

        llState.setOnClickListener(this);
        llCity.setOnClickListener(this);
        llModeOfPayment.setOnClickListener(this);
        llDayAndTymeOfBusiness.setOnClickListener(this);
        llSubCat.setOnClickListener(this);
        llCat.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);


        tvState = (TextView) v.findViewById(R.id.tv_select_state);
        tvCity = (TextView) v.findViewById(R.id.tv_select_city);
        tvModeOfPayment = (TextView) v.findViewById(R.id.tv_select_modeofpayment);
        tvSubCat = (TextView) v.findViewById(R.id.tv_select_subcat);
        tvCat = (TextView) v.findViewById(R.id.tv_select_cat);
        tvDayAndTymeOfBusiness = (TextView) v.findViewById(R.id.tv_select_d_and_t_business);
        ivProfile = (ImageView) v.findViewById(R.id.iv_profile);
        Glide.with(getActivity())
                .load(R.drawable.profile)
                .bitmapTransform(new CropCircleTransformation(getActivity()))
                .into(ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ImageMaster.class);
                intent.putExtra("from", ImageMaster.REQUEST_BUSINESS);
                startActivityForResult(intent, ImageMaster.REQUEST_BUSINESS);
            }
        });

    }

    public void setImageURI(Uri uri) {
        mImageUri=uri;
//        ivProfile.setImageURI(uri);
        Glide.with(getActivity())
                .load(uri)
                .bitmapTransform(new CropCircleTransformation(getActivity()))
                .into(ivProfile);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                executeTask();
                break;
            case R.id.ll_select_state:
                showStateDialog();
                break;
            case R.id.ll_select_city:
                if (!TextUtils.isEmpty(sltStateId)) {
                    showCityDialog(sltStateId, sltState);
                } else {
                    showToast(getActivity(), "Please select state");
                    showStateDialog();
                }
                break;
            case R.id.ll_select_d_and_t_business:
                showDayAndTimeOfBusinessDialog();
                break;
            case R.id.ll_select_modeofpayment:
                showPaymoentModeDialog();
                break;
            case R.id.ll_select_cat:
                showCategoryDialog();
                break;
            case R.id.ll_select_subcat:
                if (!TextUtils.isEmpty(sltCategoryId)) {
                    showSubCategoryDialog(sltCategoryId);
                } else {
                    showToast(getActivity(), "Please select category");
                }
                break;
        }
    }

    private void showCategoryDialog() {
        CategoryDialog dialog = CategoryDialog.newInstance(BusinessDetail.this);
        AppAnimation.setDialogAnimation(getActivity(), dialog);
        dialog.show(getFragmentManager(), "categoryDialog");
    }


    private void showDayAndTimeOfBusinessDialog() {
        String dayAndTimeJson = "";
        if (businessArray != null) {
            dayAndTimeJson = businessArray.get(18);
        }
        DayAndTimeOfBusiness dialog = DayAndTimeOfBusiness.newInstance(BusinessDetail.this, dayAndTimeJson, originalDayAndTimeList);
        AppAnimation.setDialogAnimation(getActivity(), dialog);
        dialog.show(getFragmentManager(), "dayAndTimeOfBusiness");
    }

    @Override
    public void onDayAndTimeOfBussClick(DayAndTimeOfBusiness modeOfPaymentDialog, ArrayList<DayAndTimeModel> checkedList, ArrayList<DayAndTimeModel> originalList) {
        L.m("");
        this.sltDayAndTimeList = checkedList;
        this.originalDayAndTimeList = originalList;
        String sltDay = "";
        if (sltDayAndTimeList != null && sltDayAndTimeList.size() != 0) {
            for (DayAndTimeModel model : sltDayAndTimeList) {
                sltDay += model.getDayName() + ",";
            }
            sltDayAndTime = sltDay.substring(0, sltDay.length() - 1);
            tvDayAndTymeOfBusiness.setText(sltDayAndTime);
        }
    }


    private void showStateDialog() {
        StatePickerDialog dialog = StatePickerDialog.newInstance(BusinessDetail.this);
        AppAnimation.setDialogAnimation(getActivity(), dialog);
        dialog.show(getFragmentManager(), "statePickerDialog");
    }

    @Override
    public void onStateClick(DialogFragment dialog, String stateId, String stateName) {
        tvState.setText(stateName);
        this.sltState = stateName;
        this.sltStateId = stateId;
        this.sltCity = null;
        this.sltCityId = null;
        tvCity.setText("Select City");
    }

    private void showCityDialog(String stateId, String stateName) {
        CityPickerDialog dialog = CityPickerDialog.newInstance(BusinessDetail.this, stateId, stateName);
        AppAnimation.setDialogAnimation(getActivity(), dialog);
        dialog.show(getFragmentManager(), "cityPickerDialog");
    }

    private void showPaymoentModeDialog() {
        String paymoentModeJson = "";
        if (businessArray != null) {
            paymoentModeJson = businessArray.get(17);
        }
        ModeOfPaymentDialog dialog = ModeOfPaymentDialog.newInstance(BusinessDetail.this,paymoentModeJson, originalPaymentList);
        AppAnimation.setDialogAnimation(getActivity(), dialog);
        dialog.show(getFragmentManager(), "modeOfPaymentDialog");
    }

    @Override
    public void onModeOfPayClick(ModeOfPaymentDialog modeOfPaymentDialog, ArrayList<BankModel> checkedList, ArrayList<BankModel> originalList) {
        this.sltPaymentList = checkedList;
        this.originalPaymentList = originalList;
        String sltId = "";
        if (sltPaymentList != null && sltPaymentList.size() != 0) {
            String name = "", id = "";
            for (BankModel model : sltPaymentList) {
                name += model.getName() + ",";
                id += model.getId() + ",";
            }
            sltId = id.substring(0, id.length() - 1);
            tvModeOfPayment.setText(name.substring(0, name.length() - 1));
        }
        this.sltModeOfPaymentIDs = "[" + sltId + "]";
    }

    private void showSubCategoryDialog(String sltCategoryId) {
        SubCategoryDialog dialog = SubCategoryDialog.newInstance(BusinessDetail.this, sltCategoryId, originalSubCatList);
        AppAnimation.setDialogAnimation(getActivity(), dialog);
        dialog.show(getFragmentManager(), "subCategoryDialog");
    }

    @Override
    public void onSubCatClick(SubCategoryDialog SubCategoryDialog, ArrayList<BusinessCategoryModel> checkedList, ArrayList<BusinessCategoryModel> originalList) {
        this.sltSubCatList = checkedList;
        this.originalSubCatList = originalList;
        String sltId = "";
        if (sltSubCatList != null && sltSubCatList.size() != 0) {
            String name = "", id = "";
            for (BusinessCategoryModel model : sltSubCatList) {
                name += model.getSubCategory() + ",";
                id += model.getSubCategoryId() + ",";
            }
            sltId = id.substring(0, id.length() - 1);
            tvSubCat.setText(name.substring(0, name.length() - 1));
        }
        this.sltSubCategoryIds = "[" + sltId + "]";
    }


    @Override
    public void onCityClick(DialogFragment dialog, String cityId, String cityName) {
        tvCity.setText(cityName);
        this.sltCity = cityName;
        this.sltCityId = cityId;
    }


    private void setTypeOfEntity() {
        JsonParser jsonParser = new JsonParser();
        BankModel bankModel = jsonParser.parseTypeOfEntityData(AppUser.getEntityDataJson(getActivity()));
        if(bankModel.getOutput().equalsIgnoreCase("success")) {
            this.typeOfEntityArray = bankModel.getList();
            entityAdapter = new BankAdapter(getActivity(), typeOfEntityArray);
            spTypeOfEntity.setAdapter(entityAdapter);
        }else{
            ApiCallError.newInstance(bankModel.getOutputMsg(),true, BusinessDetail.this)
                    .show(getFragmentManager(), "apiCallError");
        }
    }

    private void setCategory() {
        if(customerDetail==null){

        }
//        JsonParser jsonParser = new JsonParser();
//        BusinessCategoryModel bankModel = jsonParser.parseCategoryData(AppUser.getCategoriesJson(getActivity()));
//        categoryArray=bankModel.getList();
//        categoryAdapter = new CategoryAdapter(getActivity(),categoryArray );
//        spCategory.setAdapter(categoryAdapter);
    }


    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    private void executeTask() {
        if (sltDayAndTimeList != null) {
            sltDayAndTimeIds = JsonParser.toDayAndTimingIdJsonArray(sltDayAndTimeList);
        }
        if (checkValidation()) {
            hideKeybord(getActivity());
            assignTask = new AssignTask(getActivity(), etCompName.getText().toString(), sltEntityType,
                    etContactPerson.getText().toString(), etDestContactPerson.getText().toString(), etMobile.getText().toString(),
                    etEmail.getText().toString(), etLandline1.getText().toString(), etLandline2.getText().toString(), etWebsite.getText().toString(),
                    etAddress.getText().toString(), sltStateId, sltCityId, etPinCode.getText().toString(), sltDayAndTimeIds, sltModeOfPaymentIDs, sltCategoryId, sltSubCategoryIds,
                    etDealInIteams.getText().toString(),mImageUri);
            assignTask.execute(AppSettings.PUT_BUSINESS_DETAILS);
        }
    }

    private boolean checkValidation() {
        if (!FieldValidation.validate(getActivity(), etCompName, FieldValidation.STRING, true)) {
            return false;
        } else if (TextUtils.isEmpty(sltEntityType)) {
            showToast(getActivity(), "Please Select Type of Entity");
            return false;
        } else if (!FieldValidation.validate(getActivity(), etContactPerson, FieldValidation.NAME, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etDestContactPerson, FieldValidation.STRING, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etMobile, FieldValidation.MOBILE, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etEmail, FieldValidation.EMAIL, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etAddress, FieldValidation.STRING, true)) {
            return false;
        } else if (TextUtils.isEmpty(sltStateId)) {
            showToast(getActivity(), "Please select state");
            return false;
        } else if (TextUtils.isEmpty(sltCityId)) {
            showToast(getActivity(), "Please select city");
            return false;
        } else if (!FieldValidation.validate(getActivity(), etPinCode, FieldValidation.PIN_CODE, true)) {
            return false;
        } else if (TextUtils.isEmpty(sltDayAndTimeIds)) {
            showToast(getActivity(), "Please select days and timings of business");
            return false;
        }  else if (TextUtils.isEmpty(sltModeOfPaymentIDs)) {
            showToast(getActivity(), "Please select Mode of payment");
            return false;
        } else if (TextUtils.isEmpty(sltCategoryId)) {
            showToast(getActivity(), "Please select Category");
            return false;
        } else if (TextUtils.isEmpty(sltSubCategoryIds)) {
            showToast(getActivity(), "Please select sub category");
            return false;
        } else if (!FieldValidation.validate(getActivity(), etDealInIteams, FieldValidation.STRING, true)) {
            return false;
        }
        return true;
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    @Override
    public void onCategoryClick(DialogFragment dialog, String categoryId, String categoryName) {
        sltCategoryId = categoryId;
        tvCat.setText(categoryName);
        sltSubCatList = null;
        originalSubCatList = null;
        tvSubCat.setText("Select Sub category");
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, int flag) {
        executeRefreshTask();
    }

    private void executeRefreshTask() {
        refreshData=new RefreshData(getActivity());
        refreshData.execute(AppSettings.LOGIN);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, int flag) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final Uri mImageUri;
        private String CompName, sltEntityType, ContactPerson,
                DestContactPerson, Mobile, Email, Landline1, Landline2, Website, Address, stateId, cityId, PinCode, PaymentModeId, categoryId, subCategoryIds, dayAndTimeIds, DealInIteams;
        private Context context;

        public AssignTask(Context context, String CompName, String sltEntityType, String ContactPerson,
                          String DestContactPerson, String Mobile, String Email, String Landline1
                , String Landline2, String Website, String Address, String stateId, String cityId
                , String PinCode, String dayAndTimeIds, String PaymentModeId, String categoryId
                , String subCategoryIds, String DealInIteams, Uri mImageUri) {
            this.context = context;
            this.CompName = CompName;
            this.sltEntityType = sltEntityType;
            this.ContactPerson = ContactPerson;
            this.DestContactPerson = DestContactPerson;
            this.Mobile = Mobile;
            this.Email = Email;
            this.Landline1 = Landline1;
            this.Landline2 = Landline2;
            this.Website = Website;
            this.Address = Address;
            this.stateId = stateId;
            this.cityId = cityId;
            this.PinCode = PinCode;
            this.dayAndTimeIds = dayAndTimeIds;
            this.PaymentModeId = PaymentModeId;
            this.categoryId = categoryId;
            this.subCategoryIds = subCategoryIds;
            this.DealInIteams = DealInIteams;
            this.mImageUri=mImageUri;
        }

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context, "Processing, please wait");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            String userId = AppUser.getUserId(context);
            response = ApiCall.upload(urls[0], RequestBuilder.UpdateBusinessDetail(userId, CompName, sltEntityType, ContactPerson,
                    DestContactPerson, Mobile, Email, Landline1, Landline2, Website, Address, stateId, cityId, PinCode, dayAndTimeIds
                    , PaymentModeId, categoryId, subCategoryIds, DealInIteams,mImageUri));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.defaultParser(response);
        }

        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressDialog();
            if (context != null) {
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if (customerDetail != null) {
                            showToast(context, result.getOutputMsg());
                            getFragmentManager().popBackStack();
                            customerDetail.attemptLogin();
                        } else {
                            executeRefreshTask();
//                            AppUser.setCustomerDetail(context);
//                            showToast(context, result.getOutputMsg());
//                            ((UpdateCustomerDetail) getActivity()).setTab(UpdateCustomerDetail.TAB_KYC);
                        }
//                        CustomerTokens.setBankDetails(context,personName, sltBankName, branch, accountNumber, IFSCCode, sltBankFile);
//                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(), BusinessDetail.this)
                                .show(getFragmentManager(), "apiCallError");

                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true, BusinessDetail.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }

    private ArrayList<String> loadBusinessDetail() {
        return JsonParser.parseBusinessDetail(AppUser.getBusinessDetails(getActivity()));
    }

    private class RefreshData extends AsyncTask<String, Void, Model> {
        private Context activity;

        public void onAttach(Context activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public RefreshData(Context activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Reloading, please wait...");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            response = ApiCall.POST(urls[0], RequestBuilder.LoginBody(AppUser.getUserId(activity), AppUser.getPassword(activity)));
            JsonParser jsonParser = new JsonParser();
            Model model = jsonParser.parseLoginData(activity, response);
            AppUser.setAppData(activity,response);
            return model;
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressDialog();
            if (activity != null) {
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        showToast(activity,"Business detail updated successful");
                        getFragmentManager().popBackStack();
                    } else if (result.getOutput().equals("failure")) {
                        Toast.makeText(activity, result.getOutputMsg(), Toast.LENGTH_LONG).show();
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),0,BusinessDetail.this)
                                .show(getFragmentManager(),"apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,0,BusinessDetail.this)
                                .show(getFragmentManager(),"apiCallError");
                    }
                }
            }
        }
    }
}