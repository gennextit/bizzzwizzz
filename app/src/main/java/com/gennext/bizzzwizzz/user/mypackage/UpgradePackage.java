package com.gennext.bizzzwizzz.user.mypackage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.gennext.bizzzwizzz.PackageActivity;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.UpgradePackageActivity;
import com.gennext.bizzzwizzz.dialog.PackageDetail;
import com.gennext.bizzzwizzz.intro.IntroPackage;
import com.gennext.bizzzwizzz.intro.IntroTokens;
import com.gennext.bizzzwizzz.model.MyPackageModel;
import com.gennext.bizzzwizzz.model.UpgradePackageAdapterNew;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.DateTimeUtility;
import com.gennext.bizzzwizzz.util.JsonParser;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;


/**
 * Created by Abhijit on 27-Dec-16.
 */

public class UpgradePackage extends CompactFragment implements PackageDetail.PackageDetailListener{

    public static final int PACKAGE_BUY = 1,PACKAGE_UPGRADE=2;
    private FeatureCoverFlow coverFlow;
    private UpgradePackageAdapterNew adapter;
    private int packageType;
    private MyPackageModel currentPackageModel;

    public static UpgradePackage newInstance() {
        UpgradePackage fragment=new UpgradePackage();
        fragment.packageType=PACKAGE_BUY;
        return fragment;
    }
    public static UpgradePackage newInstance(int packageType) {
        UpgradePackage fragment=new UpgradePackage();
        fragment.packageType=packageType;
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_upgrade_package, container, false);
        if(packageType==PACKAGE_UPGRADE){
            initToolBarForActivity(getActivity(), v, "Buy Package");
        }else {
            initToolBar(getActivity(), v, "Buy Package");
        }

        InitUI(v);
        if(packageType==PACKAGE_UPGRADE) {
            getCurrentPackageDerail();
        }
        return v;
    }

    private void getCurrentPackageDerail() {
        MyPackageModel myPackageModel = JsonParser.parseMyPackageDetail(AppUser.getPackageDetails(getActivity()));
        if (myPackageModel.getOutput().equalsIgnoreCase("success")) {
            currentPackageModel=myPackageModel;
        }
    }

    private void InitUI(View v) {
        coverFlow = (FeatureCoverFlow) v.findViewById(R.id.coverflow);

        MyPackageModel myPackageModel=getPackageDetail(AppUser.getPackagesJson(getActivity()));
        if(myPackageModel!=null && myPackageModel.getPackageList()!=null) {
            adapter = new UpgradePackageAdapterNew(getActivity(), myPackageModel.getPackageList());
            coverFlow.setAdapter(adapter);
        }else{
            showToast(getActivity(),"Package list not available");
        }
        coverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                PackageDetail.newInstance(adapter.getItem(position),UpgradePackage.this)
                        .show(getFragmentManager(),"packageDetail");
            }
        });
        if(IntroTokens.packageIntroOpen(getActivity())){
            addFragment(IntroPackage.newInstance(),"introPackage");
        }
    }

    private MyPackageModel getPackageDetail(String packagesJson) {
        return JsonParser.parsePackageDetail(packagesJson,AppUser.getPackageType(getActivity()));
    }

    @Override
    public void onPackageDetailClick(DialogFragment dialog,MyPackageModel packageDetail) {
        if(packageType==PACKAGE_BUY) {
            Intent intent = new Intent(getActivity(), PackageActivity.class);
            intent.putExtra(PackageActivity.PKG_ID, packageDetail.getPackageId());
            intent.putExtra(PackageActivity.PKG_NAME, packageDetail.getPackageName());
            intent.putExtra(PackageActivity.PKG_AMT, packageDetail.getPackageAmount());
            startActivityForResult(intent, PackageActivity.REQUEST_PAYMENT);
        }else if(packageType==PACKAGE_UPGRADE) {
            if(currentPackageModel!=null){
                String currTempPkgAmt = currentPackageModel.getPackageAmount();
                String sltTempPkgAmt = packageDetail.getPackageAmount();

                Float currPkgAmt=convertAmt(currTempPkgAmt);
                Float sltPkgAmt=convertAmt(sltTempPkgAmt);
                if(sltPkgAmt.equals(currPkgAmt)){
                    showToast(getActivity(),"You have already bought this package");
                } else if(sltPkgAmt>currPkgAmt){
                    ((UpgradePackageActivity)getActivity()).setData(packageDetail.getPackageId(),packageDetail.getPackageAmount());
                } else {
                    showToast(getActivity(),"Please choose higher package");
                }
            }
        }
    }

    private Float convertAmt(String amt) {
        try {
            return Float.valueOf(amt);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0.0f;
        }
    }


}