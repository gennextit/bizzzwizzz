package com.gennext.bizzzwizzz.user.customerDetail;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzz.DocumentMaster;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.BankAdapter;
import com.gennext.bizzzwizzz.model.BankModel;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.FieldValidation;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;
import com.gennext.bizzzwizzz.util.Utility;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.File;
import java.util.ArrayList;



/**
 * Created by Abhijit on 12-Jan-17.
 */

public class BankDetail extends CompactFragment implements ApiCallError.ErrorListener{

    public static final int ATTACHMENT_BANK = 1;
    private Button btnDoc,btnSubmit;
    private TextView tvDocStatus;
    private File sltBankFile;
    private EditText etPersonName;
    private EditText etBranch,etAccount,etIFSCCode;
    private AssignTask assignTask;
    private String sltBankName;
    private Spinner spBank;
    private BankAdapter adapter;
    private String sltBankFilePath;
    private CustomerDetail customerDetail;
    private ArrayList<String> bankArray;
    private ArrayList<BankModel> bankListArray;
    private PermissionListener onPermissionListener;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static BankDetail newInstance(CustomerDetail customerDetail, ArrayList<String> bankArray) {
        BankDetail fragment = new BankDetail();
        fragment.customerDetail=customerDetail;
        fragment.bankArray=bankArray;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_bank_detail, container, false);
        if(customerDetail!=null){
            ((Toolbar)v.findViewById(R.id.toolbar)).setVisibility(View.VISIBLE);
            initToolBar(getActivity(), v, "Bank");
        }else {
            ((Toolbar)v.findViewById(R.id.toolbar)).setVisibility(View.GONE);
        }
        InitUI(v);
        setPermissionListener();
        setBankDetail();
        if(bankArray!=null){
            setBankData(bankArray);
        }
        return v;
    }


    private void setBankData(ArrayList<String> model) {
        btnSubmit.setText("Update");
        etPersonName.setText(model.get(0));
//        tvBankName.setText(model.get(1));
        etBranch.setText(model.get(2));
        etAccount.setText(model.get(3));
        etIFSCCode.setText(model.get(4));
        for(int i=0;i<bankListArray.size();i++){
            if(bankListArray.get(i).getId()!=null&&bankListArray.get(i).getId().equalsIgnoreCase(model.get(6))){
                spBank.setSelection(i);
            }
        }
    }

    private void setBankDetail() {
        JsonParser jsonParser = new JsonParser();
        BankModel bankModel = jsonParser.parseBankData(AppUser.getBankListJson(getActivity()));
        if(bankModel.getOutput().equalsIgnoreCase("success")) {
            bankListArray = bankModel.getList();
            adapter = new BankAdapter(getActivity(), bankListArray);
            spBank.setAdapter(adapter);
        }else{
            ApiCallError.newInstance(bankModel.getOutputMsg(),true, BankDetail.this)
                    .show(getFragmentManager(), "apiCallError");
        }
    }

    private void InitUI(View v) {

        btnSubmit = (Button) v.findViewById(R.id.btn_submit);
        btnDoc = (Button) v.findViewById(R.id.btn_browse_bank);
        tvDocStatus = (TextView) v.findViewById(R.id.tv_browse_bank);
        etPersonName = (EditText) v.findViewById(R.id.et_bank_1);
        etBranch = (EditText) v.findViewById(R.id.et_bank_2);
        etAccount = (EditText) v.findViewById(R.id.et_bank_3);
        etIFSCCode = (EditText) v.findViewById(R.id.et_bank_4);
        btnDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TedPermission(getActivity())
                        .setPermissionListener(onPermissionListener)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setGotoSettingButtonText("setting")
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .check();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeTask();
            }
        });

        spBank = (Spinner) v.findViewById(R.id.sp_bank);
        spBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> ad, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
//                    ConsultancyModel model=adapter.getItem(position);
                if(adapter!=null)
                    sltBankName = adapter.getBankId(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        spBank.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeybord(getActivity());
                return false;
            }
        });
    }

    private void executeTask() {
        if (checkValidation()) {
            hideKeybord(getActivity());
            assignTask = new AssignTask(getActivity(), etPersonName.getText().toString(),sltBankName,
                    etBranch.getText().toString(),  etAccount.getText().toString(), etIFSCCode.getText().toString(),sltBankFile);
            assignTask.execute(AppSettings.PUT_BANK_DETAILS);

        }
    }

    private boolean checkValidation() {
        if (!FieldValidation.validate(getActivity(), etPersonName, FieldValidation.NAME, true)) {
            return false;
        } else if (TextUtils.isEmpty(sltBankName)) {
            showToast(getActivity(), "Please Select Bank");
            return false;
        }  else if (!FieldValidation.validate(getActivity(), etBranch, FieldValidation.STRING, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etAccount, FieldValidation.STRING, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etIFSCCode, FieldValidation.STRING, true)) {
            return false;
        } else if (sltBankFile==null) {
            showToast(getActivity(), "Please Select Checkbook or passbook copy");
            return false;
        }
        return true;
    }

    public void addThemToView(String filePath) {
        sltBankFilePath=filePath;
        sltBankFile =new File(filePath);
        String string1 = Utility.getColoredSpanned(sltBankFile.getName(),"#3ab54a");
        String string2 = Utility.getColoredSpanned("\nis selected","#666666");
        tvDocStatus.setText(Html.fromHtml(string1+" "+string2));
        Toast.makeText(getActivity(), sltBankFile.getName(), Toast.LENGTH_LONG).show();
    }

    private void setPermissionListener() {
        onPermissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                onPickDoc();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getActivity(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }


        };
    }

    public void onPickDoc() {
//        new MaterialFilePicker()
//                .withActivity(getActivity())
//                .withRequestCode(ATTACHMENT_BANK)
////                .withFilter(Pattern.compile(".*\\.txt$")) // Filtering files and directories by file name using regexp
//                .withFilterDirectories(false) // Set directories filterable (false by default)
//                .withHiddenFiles(true) // Show hidden files and folders
//                .start();

        Intent intent = new Intent(getActivity(), DocumentMaster.class);
        intent.putExtra("from", DocumentMaster.REQ_BANK_ATTACH);
        startActivityForResult(intent, DocumentMaster.REQ_BANK_ATTACH);
    }



    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final File sltBankFile;
        private String personName, sltBankName, branch, accountNumber, IFSCCode;
        private Context context;

        public AssignTask(Context context, String personName, String sltBankName, String branch,
                          String accountNumber, String IFSCCode, File sltBankFile) {
            this.context = context;
            this.personName=personName;
            this.sltBankName=sltBankName;
            this.branch=branch;
            this.accountNumber=accountNumber;
            this.IFSCCode=IFSCCode;
            this.sltBankFile=sltBankFile;

        }
        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context, "Processing, please wait");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            String userId = AppUser.getUserId(context);
            response = ApiCall.upload(urls[0], RequestBuilder.UpdateBankDetail(userId, personName, sltBankName, branch, accountNumber,IFSCCode,sltBankFile,sltBankFilePath));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.defaultParser(response);
        }

        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressDialog();
            if (context != null) {
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if(customerDetail!=null){
                            showToast(context,result.getOutputMsg());
                            getFragmentManager().popBackStack();
                            customerDetail.attemptLogin();
                        }else{
                            showToast(context,result.getOutputMsg());
                            AppUser.setCustomerDetail(context);
                            getActivity().finish();
                            addFragment(CustomerDetail.newInstance(),"customerDetail");
                        }
//                        CustomerTokens.setBankDetails(context,personName, sltBankName, branch, accountNumber, IFSCCode, sltBankFile);
//                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    }else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),BankDetail.this)
                                .show(getFragmentManager(),"apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,BankDetail.this)
                                .show(getFragmentManager(),"apiCallError");
                    }
                }
            }
        }
    }
}