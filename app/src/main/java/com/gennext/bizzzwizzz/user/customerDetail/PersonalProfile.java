package com.gennext.bizzzwizzz.user.customerDetail;

/**
 * Created by Abhijit on 27-Jan-17.
 */

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.ProfileModel;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;

import jp.wasabeef.glide.transformations.CropCircleTransformation;


public class PersonalProfile extends CompactFragment {

    private TextView tvName,tvEmail,tvMobile,tvLandline,tvDOB,tvGender,tvAddress;
    private ImageView ivProflie;

    public static PersonalProfile newInstance(Context context) {
        PersonalProfile fragment = new PersonalProfile();
        AppAnimation.setDialogAnimation(context,fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.detail_profile, container, false);
        InitUI(v);
        loadBankDetail();
        return v;
    }


    private void InitUI(View v) {
        ivProflie=(ImageView)v.findViewById(R.id.iv_profile_logo);
        tvName=(TextView)v.findViewById(R.id.tv_profile_slot_1);
        tvEmail=(TextView)v.findViewById(R.id.tv_profile_slot_2);
        tvMobile=(TextView)v.findViewById(R.id.tv_profile_slot_3);
        tvLandline=(TextView)v.findViewById(R.id.tv_profile_slot_4);
        tvDOB=(TextView)v.findViewById(R.id.tv_profile_slot_5);
        tvGender=(TextView)v.findViewById(R.id.tv_profile_slot_6);
        tvAddress=(TextView)v.findViewById(R.id.tv_profile_slot_7);
    }

    private void loadBankDetail() {
        ProfileModel model1 = AppUser.getUserProfile(getActivity());
        if(model1!=null){
            tvName.setText(model1.getName());
            tvEmail.setText(model1.getEmail());
            tvMobile.setText(model1.getMobile());
            tvLandline.setText(model1.getLandline());
            tvDOB.setText(model1.getDob());
            tvGender.setText(model1.getGender());
            String address="";
            if(!TextUtils.isEmpty(model1.getAddress())) {
                address = model1.getAddress();
            }
            if(!TextUtils.isEmpty(model1.getCity())){
                address +=", "+model1.getCity();
            }
            if(!TextUtils.isEmpty(model1.getState())){
                address +=", "+model1.getState();
            }
            if(!TextUtils.isEmpty(model1.getPinCode())){
                address +=" ("+model1.getPinCode()+")";
            }

            tvAddress.setText(address);

            if(!TextUtils.isEmpty(model1.getImage())) {
                Glide.with(getActivity())
                        .load(model1.getImage())
                        .bitmapTransform(new CropCircleTransformation(getActivity()))
                        .into(ivProflie);
            }else {
                Glide.with(getActivity())
                        .load(R.drawable.profile)
                        .bitmapTransform(new CropCircleTransformation(getActivity()))
                        .into(ivProflie);
            }
        }
    }

}
