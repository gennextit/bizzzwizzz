package com.gennext.bizzzwizzz.user.customerDetail;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzz.DocumentMaster;
import com.gennext.bizzzwizzz.ImageMaster;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.UpdateCustomerDetail;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.BankAdapter;
import com.gennext.bizzzwizzz.model.BankModel;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.FieldValidation;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;
import com.gennext.bizzzwizzz.util.Utility;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.nbsp.materialfilepicker.MaterialFilePicker;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by Abhijit on 12-Jan-17.
 */

public class KYCDetail extends CompactFragment implements ApiCallError.ErrorListener {
    public static final int ATTACHMENT_PAN = 2, ATTACHMENT_ID_PROOF = 3;

    private Button btnSubmit, btnPAN, btnIDProof;
    private TextView tvAttachPan, tvAttachId;
    private File sltPANFile, sltIDFile;
    private String sltPANFilePath, sltIDFilePath;
    private EditText etPanNumber, etIdNumber, etPersonName, etNameOfNominee, etNomineeMobile, etNomineeAddress;

    private AssignTask assignTask;
    private String sltIdentityProof;
    private Spinner spIdentityProof;
    private BankAdapter adapter;
    private CustomerDetail customerDetail;
    private ArrayList<String> kycArray;
    private ArrayList<BankModel> idFroopArray;
    private PermissionListener onPicPanListener,onPicProofListener;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static KYCDetail newInstance(CustomerDetail customerDetail, ArrayList<String> model) {
        KYCDetail fragment = new KYCDetail();
        fragment.customerDetail = customerDetail;
        fragment.kycArray = model;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_kyc_detail, container, false);
        if (customerDetail != null) {
            ((Toolbar) v.findViewById(R.id.toolbar)).setVisibility(View.VISIBLE);
            initToolBar(getActivity(), v, "KYC");
        } else {
            ((Toolbar) v.findViewById(R.id.toolbar)).setVisibility(View.GONE);
        }
        InitUI(v);
        setIdentityProof();
        setPermissionListener();
        if (kycArray != null) {
            setKycData(kycArray);
        }
        return v;
    }



    private void setKycData(ArrayList<String> model) {
        btnSubmit.setText("Update");
        etPanNumber.setText(model.get(0));
//        tvIDProof.setText(model.get(1));
        etIdNumber.setText(model.get(2));
        etPersonName.setText(model.get(3));
        etNameOfNominee.setText(model.get(4));
        etNomineeMobile.setText(model.get(5));
        etNomineeAddress.setText(model.get(6));

        for (int i = 0; i < idFroopArray.size(); i++) {
            if (idFroopArray.get(i).getId() != null && idFroopArray.get(i).getId().equalsIgnoreCase(model.get(9))) {
                spIdentityProof.setSelection(i);
            }
        }
    }

    private void InitUI(View v) {

        btnSubmit = (Button) v.findViewById(R.id.btn_submit);
        btnPAN = (Button) v.findViewById(R.id.btn_attach_pan);
        btnIDProof = (Button) v.findViewById(R.id.btn_attach_idproof);
        tvAttachPan = (TextView) v.findViewById(R.id.tv_attach_pan);
        tvAttachId = (TextView) v.findViewById(R.id.tv_attach_id);
        etPanNumber = (EditText) v.findViewById(R.id.et_kyc_1);
        etIdNumber = (EditText) v.findViewById(R.id.et_kyc_2);
        etPersonName = (EditText) v.findViewById(R.id.et_kyc_3);
        etNameOfNominee = (EditText) v.findViewById(R.id.et_kyc_4);
        etNomineeMobile = (EditText) v.findViewById(R.id.et_kyc_5);
        etNomineeAddress = (EditText) v.findViewById(R.id.et_kyc_6);

        btnPAN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TedPermission(getActivity())
                        .setPermissionListener(onPicPanListener)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setGotoSettingButtonText("setting")
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .check();
            }
        });
        btnIDProof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TedPermission(getActivity())
                        .setPermissionListener(onPicProofListener)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setGotoSettingButtonText("setting")
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .check();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeTask();
            }
        });
        spIdentityProof = (Spinner) v.findViewById(R.id.sp_identity_proof);
        spIdentityProof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> ad, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
//                    ConsultancyModel model=adapter.getItem(position);
                if (adapter != null)
                    sltIdentityProof = adapter.getBankId(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        spIdentityProof.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeybord(getActivity());
                return false;
            }
        });
    }

    private void setIdentityProof() {
        JsonParser jsonParser = new JsonParser();
        BankModel bankModel = jsonParser.parseIdentityProofData(AppUser.getIdentityProofJson(getActivity()));
        if(bankModel.getOutput().equalsIgnoreCase("success")) {
            idFroopArray = bankModel.getList();
            adapter = new BankAdapter(getActivity(), idFroopArray);
            spIdentityProof.setAdapter(adapter);
        }else{
            ApiCallError.newInstance(bankModel.getOutputMsg(),true, KYCDetail.this)
                    .show(getFragmentManager(), "apiCallError");
        }
    }


    private void executeTask() {
        if (checkValidation()) {
            hideKeybord(getActivity());
            assignTask = new AssignTask(getActivity(), etPanNumber.getText().toString(), sltIdentityProof,
                    etIdNumber.getText().toString(), etPersonName.getText().toString(), etNameOfNominee.getText().toString()
                    , etNomineeMobile.getText().toString(), etNomineeAddress.getText().toString(), sltPANFile, sltIDFile);
            assignTask.execute(AppSettings.PUT_KYC_DETAILS);
        }
    }

    private boolean checkValidation() {
        if (!FieldValidation.validate(getActivity(), etPanNumber, FieldValidation.PAN_NUMBER, true)) {
            return false;
        } else if (TextUtils.isEmpty(sltIdentityProof)) {
            showToast(getActivity(), "Please Attach PAN file");
            return false;
        } else if (sltPANFile==null) {
            showToast(getActivity(), "Please Select Identity proof");
            return false;
        } else if (TextUtils.isEmpty(sltIdentityProof)) {
            showToast(getActivity(), "Please Attach ID Proof file");
            return false;
        } else if (!FieldValidation.validate(getActivity(), etIdNumber, FieldValidation.ALP_NUM, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etPersonName, FieldValidation.NAME, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etNameOfNominee, FieldValidation.NAME, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etNomineeMobile, FieldValidation.MOBILE, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etNomineeAddress, FieldValidation.STRING, true)) {
            return false;
        }
        return true;
    }

    public void addPANThemToView(String filePath) {
        sltPANFilePath = filePath;
        sltPANFile = new File(filePath);
        String string1 = Utility.getColoredSpanned(sltPANFile.getName(), "#3ab54a");
        String string2 = Utility.getColoredSpanned("\nis selected", "#666666");
        tvAttachPan.setText(Html.fromHtml(string1 + " " + string2));
        Toast.makeText(getActivity(), sltPANFile.getName(), Toast.LENGTH_LONG).show();
    }

    public void addIDThemToView(String filePath) {
        sltIDFilePath = filePath;
        sltIDFile = new File(filePath);
        String string1 = Utility.getColoredSpanned(sltIDFile.getName(), "#3ab54a");
        String string2 = Utility.getColoredSpanned("\nis selected", "#666666");
        tvAttachId.setText(Html.fromHtml(string1 + " " + string2));
        Toast.makeText(getActivity(), sltIDFile.getName(), Toast.LENGTH_LONG).show();
    }

    private void setPermissionListener() {
        onPicPanListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                onPickPAN();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getActivity(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        onPicProofListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                onPickIDProof();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getActivity(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void onPickPAN() {
//        new MaterialFilePicker()
//                .withActivity(getActivity())
//                .withRequestCode(ATTACHMENT_PAN)
////                .withFilter(Pattern.compile(".*\\.txt$")) // Filtering files and directories by file name using regexp
//                .withFilterDirectories(false) // Set directories filterable (false by default)
//                .withHiddenFiles(true) // Show hidden files and folders
//                .start();
        Intent intent = new Intent(getActivity(), DocumentMaster.class);
        intent.putExtra("from", DocumentMaster.REQ_KYC_PAN);
        startActivityForResult(intent, DocumentMaster.REQ_KYC_PAN);
    }

    public void onPickIDProof() {
//        new MaterialFilePicker()
//                .withActivity(getActivity())
//                .withRequestCode(ATTACHMENT_ID_PROOF)
////                .withFilter(Pattern.compile(".*\\.txt$")) // Filtering files and directories by file name using regexp
//                .withFilterDirectories(false) // Set directories filterable (false by default)
//                .withHiddenFiles(true) // Show hidden files and folders
//                .start();
        Intent intent = new Intent(getActivity(), DocumentMaster.class);
        intent.putExtra("from", DocumentMaster.REQ_KYC_ID);
        startActivityForResult(intent, DocumentMaster.REQ_KYC_ID);
    }



    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final File sltPANFile, sltIdFile;
        private String panNumber, sltIdentityProof, idNum, personName, nameOfNominee, nomineeMobile, nomineeAddress;
        private Context context;

        public AssignTask(Context context, String panNumber, String sltIdentityProof, String idNum,
                          String personName, String nameOfNominee, String nomineeMobile, String nomineeAddress,
                          File sltPANFile, File sltIdFile) {
            this.context = context;
            this.panNumber = panNumber;
            this.sltIdentityProof = sltIdentityProof;
            this.idNum = idNum;
            this.personName = personName;
            this.nameOfNominee = nameOfNominee;
            this.nomineeMobile = nomineeMobile;
            this.nomineeAddress = nomineeAddress;
            this.sltPANFile = sltPANFile;
            this.sltIdFile = sltIdFile;

        }

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context, "Processing, please wait");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            String userId = AppUser.getUserId(context);
            response = ApiCall.upload(urls[0], RequestBuilder.UpdateKYCDetail(userId, panNumber, sltIdentityProof, idNum,
                    personName, nameOfNominee, nomineeMobile, nomineeAddress, sltPANFile, sltIdFile,sltIDFilePath,sltPANFilePath));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.defaultParser(response);
        }

        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressDialog();
            if (context != null) {
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if (customerDetail != null) {
                            showToast(context, result.getOutputMsg());
                            getFragmentManager().popBackStack();
                            customerDetail.attemptLogin();
                        } else {
                            AppUser.setCustomerDetail(context);
                            showToast(context, result.getOutputMsg());
                            ((UpdateCustomerDetail) getActivity()).setTab(UpdateCustomerDetail.TAB_BANK);
                        }
////                        CustomerTokens.setBankDetails(context,personName, sltBankName, branch, accountNumber, IFSCCode, sltBankFile);
//                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    }else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutput(), KYCDetail.this)
                                .show(getFragmentManager(), "apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutput(),true, KYCDetail.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }
}