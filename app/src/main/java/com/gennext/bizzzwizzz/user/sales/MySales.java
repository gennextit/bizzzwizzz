package com.gennext.bizzzwizzz.user.sales;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.FilterCriteria;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.intro.IntroSales;
import com.gennext.bizzzwizzz.intro.IntroTokens;
import com.gennext.bizzzwizzz.model.MySalesAdapter;
import com.gennext.bizzzwizzz.model.MySalesModel;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.DateTimeUtility;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.L;
import com.gennext.bizzzwizzz.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Abhijit on 08-Dec-16.
 */

public class MySales extends CompactFragment implements ApiCallError.ErrorListener, FilterCriteria.FilterDialogListener {
    private ArrayList<MySalesModel> cList, originalList;
    private RecyclerView lvMain;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private MySalesAdapter adapter;

    private AssignTask assignTask;
    private ImageView ivFilter;
    private String sltFilterItem, startDate, endDate;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static MySales newInstance() {
        MySales mySales = new MySales();
        return mySales;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_mysales, container, false);
        initToolBar(getActivity(), v, "My Sales");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        ivFilter = (ImageView) v.findViewById(R.id.iv_filter);
        ivFilter.setVisibility(View.GONE);
        ivFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterCriteria.newInstance(MySales.this, sltFilterItem, startDate, endDate).show(getFragmentManager(), "filterCriteria");
            }
        });
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask();
            }
        });
        cList = new ArrayList<>();
        adapter = new MySalesAdapter(getActivity(), cList, MySales.this);
        lvMain.setAdapter(adapter);


        executeTask();
    }

    private void executeTask() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.MY_SALES);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    private void hideProgressBar() {
        mSwipeRefreshLayout.setRefreshing(false);

    }


    @Override
    public void onFilterOkClick(DialogFragment dialog, String sltFilterItem, String startDate, String endDate) {
        this.sltFilterItem = sltFilterItem;
        this.startDate = startDate;
        this.endDate = endDate;
        if (sltFilterItem != null) {
            setMinContentInList(sltFilterItem);
        } else if (startDate != null && endDate != null) {
            filterListByDate(startDate, endDate);
        } else if (startDate != null) {
            filterListByDate(startDate);
        } else if (endDate != null) {
            filterListByDate(endDate);
        }
    }

    private void filterListByDate(String startDate, String endDate) {
        ArrayList<MySalesModel> cTempList;
        int startDateTStamp = DateTimeUtility.generateTimeStamp(startDate);
        int endDateTStamp = DateTimeUtility.generateTimeStamp(endDate);
        if (cList != null && originalList != null) {
            cTempList = new ArrayList<>();
            if(originalList.size()!=0){
                for (MySalesModel model:originalList) {
                    int sltDateTStamp = DateTimeUtility.generateTimeStamp(model.getDate());
                    if (startDateTStamp<=sltDateTStamp && sltDateTStamp<=endDateTStamp) {
                        cTempList.add(model);
                    }
                }
            }
            cList.clear();
            cList.addAll(cTempList);
            adapter.notifyDataSetChanged();
        }
    }


    private void filterListByDate(String date) {
        ArrayList<MySalesModel> cTempList;
        if (cList != null && originalList != null) {

            cTempList = new ArrayList<>();
            for (int i = 0; i < originalList.size(); i++) {
                if (originalList.get(i).getDate().equals(date)) {
                    cTempList.add(originalList.get(i));
                }
            }
            cList.clear();
            cList.addAll(cTempList);
            adapter.notifyDataSetChanged();
        }
    }

    private void setMinContentInList(String sltFilterItem) {
        if (!TextUtils.isEmpty(sltFilterItem)) {
            switch (sltFilterItem.toLowerCase()) {
                case "all":
                    if (originalList != null) {
                        maxContentInList(originalList.size());
                    }
                    break;
                case "10":
                    maxContentInList(10);
                    break;
                case "20":
                    maxContentInList(20);
                    break;
                case "30":
                    maxContentInList(30);
                    break;
                case "40":
                    maxContentInList(40);
                    break;
            }
        }
    }

    private void maxContentInList(int maxLength) {
        ArrayList<MySalesModel> cTempList;
        if (cList != null && originalList != null) {
            int listSize = originalList.size();
            if (listSize >= maxLength) {
                cTempList = new ArrayList<>();
                for (int i = 0; i < maxLength; i++) {
                    cTempList.add(originalList.get(i));
                }
                cList.clear();
                cList.addAll(cTempList);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onFilterCancelClick(DialogFragment dialog) {

    }

    private class AssignTask extends AsyncTask<String, Void, MySalesModel> {
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected MySalesModel doInBackground(String... urls) {
            String response;
            String userId = AppUser.getUserId(context);
            response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.parseMySalesDetail(response);
        }


        @Override
        protected void onPostExecute(MySalesModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        cList.clear();
                        cList.addAll(result.getList());
                        originalList = result.getList();
                        adapter.notifyDataSetChanged();
                        ivFilter.setVisibility(View.VISIBLE);
                        if (IntroTokens.salesIntroOpen(context)) {
                            addFragment(IntroSales.newInstance(), "introSales");
                        }
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(), MySales.this)
                                .show(getFragmentManager(), "apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true, MySales.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }

}


