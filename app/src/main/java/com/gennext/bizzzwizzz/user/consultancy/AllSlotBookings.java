package com.gennext.bizzzwizzz.user.consultancy;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.common.PopupDialog;
import com.gennext.bizzzwizzz.model.AllSlotBookingsAdapter;
import com.gennext.bizzzwizzz.model.ConsultancyModel;
import com.gennext.bizzzwizzz.user.mypackage.UpgradePackage;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Abhijit on 08-Dec-16.
 */

public class AllSlotBookings extends CompactFragment implements ApiCallError.ErrorFlagListener ,PopupDialog.DialogTaskListener{
    public static final int LOAD_LIST = 1,CANCEL_BOOKING=2;
    private static final int DIALOG_CANCEL_SLOT=1,DIALOG_BUY_PKG = 2;
    private ArrayList<ConsultancyModel> cList;
    private RecyclerView lvMain;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private AllSlotBookingsAdapter adapter;

    private AssignTask assignTask;
    private ConsultancyModel cancelBookingModel;
    private TextView tvAvailabeSlots;
    private int avalSlots;
    private Button btnSlotBooking;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static AllSlotBookings newInstance() {
        AllSlotBookings allSlotBookings = new AllSlotBookings();
        return allSlotBookings;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_all_bookings, container, false);
        initToolBar(getActivity(), v, "Booking");
        InitUI(v);
        executeTask(LOAD_LIST);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void InitUI(View v) {
        btnSlotBooking = (Button) v.findViewById(R.id.btn_slot_booking);
        tvAvailabeSlots = (TextView) v.findViewById(R.id.tv_available_slots);
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask(LOAD_LIST);
            }
        });
        cList = new ArrayList<>();
        adapter = new AllSlotBookingsAdapter(getActivity(), cList, AllSlotBookings.this);
        lvMain.setAdapter(adapter);

        btnSlotBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BookASlot bookASlot = BookASlot.newInstance(AllSlotBookings.this);
                AppAnimation.setDialogAnimation(getActivity(),bookASlot);
                addFragment(bookASlot,"bookASlot");
            }
        });
    }

    public void executeTask(int task) {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        if(task==LOAD_LIST) {
            assignTask = new AssignTask(getActivity(), task);
            assignTask.execute(AppSettings.AVAILABLE_SLOTS);
        }else if(task==CANCEL_BOOKING) {
            assignTask = new AssignTask(getActivity(), task);
            assignTask.execute(AppSettings.CANCEL_SLOT);
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, int task) {
        executeTask(task);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, int flag) {

    }

    private void hideProgressBar() {
        mSwipeRefreshLayout.setRefreshing(false);

    }

    public void cancelBooking(ConsultancyModel sample) {
        this.cancelBookingModel=sample;
        PopupDialog.newInstance("Alert","Do you realy want to cancel booking on "+sample.getSlotDay()+" "+sample.getSlotDate(),
                AllSlotBookings.DIALOG_CANCEL_SLOT,AllSlotBookings.this)
                .show(getFragmentManager(),"popupDialog");
    }

    @Override
    public void onDialogOkClick(DialogFragment dialog, int task) {
        if(task==DIALOG_CANCEL_SLOT){
            executeTask(CANCEL_BOOKING);
        }else  if(task==DIALOG_BUY_PKG){
            UpgradePackage upgradePackage = UpgradePackage.newInstance();
            AppAnimation.setDialogAnimation(getActivity(),upgradePackage);
            addFragment(upgradePackage,"upgradePackage");
        }
    }

    @Override
    public void onDialogCancelClick(DialogFragment dialog, int task) {

    }



    public void hideBookingSlotOption() {
        btnSlotBooking.setVisibility(View.GONE);
    }

    public void bookingConfirmed(String title, String outputMsg) {
        showPopupAlert(title,outputMsg,PopupAlert.POPUP_DIALOG);
        executeTask(LOAD_LIST);
    }



    private class AssignTask extends AsyncTask<String, Void, ConsultancyModel> {
        private final int task;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, int task) {
            this.context = context;
            this.task=task;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ConsultancyModel doInBackground(String... urls) {
            String response;
            if(task==LOAD_LIST) {
                String userId = AppUser.getUserId(context);
                response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseAllSlotsBookingDetail(response);
            }else if(task==CANCEL_BOOKING) {
                String userId = AppUser.getUserId(context);
                response = ApiCall.POST(urls[0], RequestBuilder.CancelBooking(userId,cancelBookingModel.getSlotDate(),cancelBookingModel.getSlotTime()));
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseCancelBooking(response);
            }
            return null;
        }

        @Override
        protected void onPostExecute(ConsultancyModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (result != null) {
                    String availableSlot=result.getAvailableSlot();
                    setSlotAvailability(availableSlot);
                    if (result.getOutput().equals("success")) {
                        if(task==LOAD_LIST) {
                            cList.clear();
                            cList.addAll(result.getList());
                            adapter.notifyDataSetChanged();
                        }else if(task==CANCEL_BOOKING){
                            executeTask(LOAD_LIST);
                        }
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),task, AllSlotBookings.this)
                                .show(getFragmentManager(), "apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,task, AllSlotBookings.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }
    }

    private void setSlotAvailability(String availableSlot) {
        btnSlotBooking.setVisibility(View.GONE);
        if(!TextUtils.isEmpty(availableSlot)){
            tvAvailabeSlots.setText("Available Slots : "+availableSlot);
            try {
                avalSlots=Integer.parseInt(availableSlot);
                if(avalSlots==0){
                    tvAvailabeSlots.setText(R.string.slot_booking_full);
                }else {
                    btnSlotBooking.setVisibility(View.VISIBLE);
                }
            }catch (NumberFormatException e){
                tvAvailabeSlots.setText("Plz contact to admin");
            }
        }else {
            tvAvailabeSlots.setText("Plz retry after sometime");
        }
        tvAvailabeSlots.setVisibility(View.VISIBLE);
    }

}


