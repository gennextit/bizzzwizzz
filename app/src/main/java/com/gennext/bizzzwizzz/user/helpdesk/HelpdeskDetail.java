package com.gennext.bizzzwizzz.user.helpdesk;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.databinding.AlertHelpdesk2Binding;
import com.gennext.bizzzwizzz.model.HelpdeskModel;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.DateTimeUtility;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class HelpdeskDetail extends CompactFragment {
    private static String requestName;
    private static HelpdeskModel helpdeskModel;
    private TextView tvTitle, tvDate;
    private Button btnOK;
    private FragmentManager manager;
    private LinearLayout llWhitespace;


    public static HelpdeskDetail newInstance(String requestName, HelpdeskModel helpdeskModel) {
        HelpdeskDetail helpdeskDetail = new HelpdeskDetail();
        helpdeskDetail.requestName = requestName;
        helpdeskDetail.helpdeskModel = helpdeskModel;
        return helpdeskDetail;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AlertHelpdesk2Binding binding = DataBindingUtil.inflate(inflater, R.layout.alert_helpdesk2, container, false);
        binding.setModel(helpdeskModel);
        View v = binding.getRoot();
        manager = getFragmentManager();
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        tvTitle = (TextView) v.findViewById(R.id.tv_popup_1);
        tvDate = (TextView) v.findViewById(R.id.tv_slot_4);
        ImageView ivAttachment = (ImageView) v.findViewById(R.id.iv_attachment);
        btnOK = (Button) v.findViewById(R.id.btn_popup);
        llWhitespace = (LinearLayout) v.findViewById(R.id.ll_whitespace);

        tvTitle.setText(requestName);
        tvDate.setText(DateTimeUtility.convertDateYMD2(helpdeskModel.getRequestDate()));

        Glide.with(getActivity())
                .load(helpdeskModel.getRequestFile())
                .placeholder(R.drawable.ic_doc2)
                .error(R.drawable.ic_doc2)
                .into(ivAttachment);

        llWhitespace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.popBackStack();

            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.popBackStack();

            }
        });
    }

}
