package com.gennext.bizzzwizzz.user.team;

/**
 * Created by Abhijit on 08-Dec-16.
 */

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.MyTeamModel;
import com.gennext.bizzzwizzz.model.TreeModel;
import com.gennext.bizzzwizzz.util.CompactFragment;


public class TreeView extends CompactFragment implements View.OnClickListener,View.OnLongClickListener{

//    private TextView tvRoot, tvRootLeft, tvRootRight, tvRootLeftChildLeft, tvRootLeftChildRight, tvRootRightChildLeft, tvRootRightChildRight;
//    private LinearLayout parent, childLeft, childRight, llRoot,llRootLeft, llRootRight, llRootLeftChildLeft, llRootLeftChildRight, llRootRightChildLeft, llRootRightChildRight;

    private TextView tvParent, tvFatherLeft, tvFatherRight, tvFatherLeftChildLeft, tvFatherLeftChildRight, tvFatherRightChildLeft, tvFatherRightChildRight;
    private LinearLayout parentLayout, fatherLeftLayout, fatherRightLayout, llParent, llFatherLeft, llFatherRight, llFatherLeftChildLeft, llFatherLeftChildRight, llFatherRightChildLeft, llFatherRightChildRight;

    private MyTeamModel treeModel;
    private TreeModel model;
    private MyTeam myTeam;

    public static TreeView newInstance(MyTeamModel treeModel, MyTeam myTeam) {
        TreeView treeView = new TreeView();
        treeView.myTeam=myTeam;
        treeView.treeModel=treeModel;
        return treeView;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        Bundle bundle = getArguments();
//        String actionTitle = "DUMMY ACTION";
//        String transText = "";
//
//        if (bundle != null) {
//            actionTitle = bundle.getString("ACTION");
//            transText = bundle.getString("TRANS_TEXT");
//        }

        View v = inflater.inflate(R.layout.frag_tree_view, container, false);

        InitUI(v);
        if(treeModel!=null) {
            populateTree2(treeModel);
        }
        return v;
    }

    private void InitUI(View v) {

        tvParent = (TextView) v.findViewById(R.id.tv_tree_root);
        tvFatherLeft = (TextView) v.findViewById(R.id.tv_tree_root_left);
        tvFatherRight = (TextView) v.findViewById(R.id.tv_tree_root_right);
        tvFatherLeftChildLeft = (TextView) v.findViewById(R.id.tv_tree_root_left_child_left);
        tvFatherLeftChildRight = (TextView) v.findViewById(R.id.tv_tree_root_left_child_right);
        tvFatherRightChildLeft = (TextView) v.findViewById(R.id.tv_tree_root_right_child_left);
        tvFatherRightChildRight = (TextView) v.findViewById(R.id.tv_tree_root_right_child_right);
        llParent = (LinearLayout) v.findViewById(R.id.ll_root);


        parentLayout = (LinearLayout) v.findViewById(R.id.parent);
        fatherLeftLayout = (LinearLayout) v.findViewById(R.id.childLeft);
        fatherRightLayout = (LinearLayout) v.findViewById(R.id.childRight);
        llFatherLeft = (LinearLayout) v.findViewById(R.id.ll_tree_root_left);
        llFatherRight = (LinearLayout) v.findViewById(R.id.ll_tree_root_right);
        llFatherLeftChildLeft = (LinearLayout) v.findViewById(R.id.ll_tree_root_left_child_left);
        llFatherLeftChildRight = (LinearLayout) v.findViewById(R.id.ll_tree_root_left_child_right);
        llFatherRightChildLeft = (LinearLayout) v.findViewById(R.id.ll_tree_root_right_child_left);
        llFatherRightChildRight = (LinearLayout) v.findViewById(R.id.ll_tree_root_right_child_right);

//        tvRootLeft = (TextView) v.findViewById(R.id.tv_tree_root_left);
//        tvRootRight = (TextView) v.findViewById(R.id.tv_tree_root_right);
//        tvRootLeftChildLeft = (TextView) v.findViewById(R.id.tv_tree_root_left_child_left);
//        tvRootLeftChildRight = (TextView) v.findViewById(R.id.tv_tree_root_left_child_right);
//        tvRootRightChildLeft = (TextView) v.findViewById(R.id.tv_tree_root_right_child_left);
//        tvRootRightChildRight = (TextView) v.findViewById(R.id.tv_tree_root_right_child_right);
//        llRoot = (LinearLayout) v.findViewById(R.id.ll_root);
//
//        tvRoot = (TextView) v.findViewById(R.id.tv_tree_root);
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            tvRoot.setTransitionName(transText);
//            tvRoot.setText(actionTitle);
//        }

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            tvRoot.setTransitionName("parent");
//            tvRootLeft.setTransitionName("fatherLeft");
//            tvRootRight.setTransitionName("fatherRight");
//            tvRootLeftChildLeft.setTransitionName("fatherLeftChildLeft");
//            tvRootLeftChildRight.setTransitionName("fatherLeftChildRight");
//            tvRootRightChildLeft.setTransitionName("fatherRightChildLeft");
//            tvRootRightChildRight.setTransitionName("fatherRightChildRight");
//        }


        tvParent.setOnClickListener(this);
        tvFatherLeft.setOnClickListener(this);
        tvFatherRight.setOnClickListener(this);
        tvFatherLeftChildLeft.setOnClickListener(this);
        tvFatherLeftChildRight.setOnClickListener(this);
        tvFatherRightChildLeft.setOnClickListener(this);
        tvFatherRightChildRight.setOnClickListener(this);

//        tvRoot.setOnLongClickListener(this);
//        tvRootLeft.setOnLongClickListener(this);
//        tvRootRight.setOnLongClickListener(this);
//        tvRootLeftChildLeft.setOnLongClickListener(this);
//        tvRootLeftChildRight.setOnLongClickListener(this);
//        tvRootRightChildLeft.setOnLongClickListener(this);
//        tvRootRightChildRight.setOnLongClickListener(this);


//        parent = (LinearLayout) v.findViewById(R.id.parent);
//        childLeft = (LinearLayout) v.findViewById(R.id.childLeft);
//        childRight = (LinearLayout) v.findViewById(R.id.childRight);
//        llRootLeft = (LinearLayout) v.findViewById(R.id.ll_tree_root_left);
//        llRootRight = (LinearLayout) v.findViewById(R.id.ll_tree_root_right);
//        llRootLeftChildLeft = (LinearLayout) v.findViewById(R.id.ll_tree_root_left_child_left);
//        llRootLeftChildRight = (LinearLayout) v.findViewById(R.id.ll_tree_root_left_child_right);
//        llRootRightChildLeft = (LinearLayout) v.findViewById(R.id.ll_tree_root_right_child_left);
//        llRootRightChildRight = (LinearLayout) v.findViewById(R.id.ll_tree_root_right_child_right);

        model=new TreeModel();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_tree_root:
                break;
            case R.id.tv_tree_root_left:
                myTeam.addNewTree(model.getFatherLeft(),tvFatherLeft);
                break;
            case R.id.tv_tree_root_right:
                myTeam.addNewTree(model.getFatherRight(),tvFatherRight);
                break;
            case R.id.tv_tree_root_left_child_left:
                myTeam.addNewTree(model.getFatherLeftChildLeft(),tvFatherLeftChildLeft);
                break;
            case R.id.tv_tree_root_left_child_right:
                myTeam.addNewTree(model.getFatherLeftChildRight(),tvFatherLeftChildRight);
                break;
            case R.id.tv_tree_root_right_child_left:
                myTeam.addNewTree(model.getFatherRightChildLeft(),tvFatherRightChildLeft);
                break;
            case R.id.tv_tree_root_right_child_right:
                myTeam.addNewTree(model.getFatherRightChildRight(),tvFatherRightChildRight);
                break;

        }
    }

    @Override
    public boolean onLongClick(View view) {
//        switch (view.getId()){
//            case R.id.tv_tree_root:
//                break;
//            case R.id.tv_tree_root_left:
//                myTeam.addNewTree(model.getFatherLeft(),tvRootLeft);
//                break;
//            case R.id.tv_tree_root_right:
//                myTeam.addNewTree(model.getFatherRight(),tvRootRight);
//                break;
//            case R.id.tv_tree_root_left_child_left:
//                myTeam.addNewTree(model.getFatherLeftChildLeft(),tvRootLeftChildLeft);
//                break;
//            case R.id.tv_tree_root_left_child_right:
//                myTeam.addNewTree(model.getFatherLeftChildRight(),tvRootLeftChildRight);
//                break;
//            case R.id.tv_tree_root_right_child_left:
//                myTeam.addNewTree(model.getFatherRightChildLeft(),tvRootRightChildLeft);
//                break;
//            case R.id.tv_tree_root_right_child_right:
//                myTeam.addNewTree(model.getFatherRightChildRight(),tvRootRightChildRight);
//                break;
//        }
        return false;
    }

    private void populateTree2(MyTeamModel result) {
        MyTeamModel parent = result.getParent();
        MyTeamModel fatherLeft = result.getFatherLeft();
        MyTeamModel fatherRight = result.getFatherRight();

        if (parent != null) {
            parent.getName();
            parent.getReferenceNumber();
            tvParent.setText(parent.getName() + "\n" +parent.getUserId() + "\nBV=" + parent.getBv() + "\n" + parent.getActivationDate());
            model.setParent(parent.getUserId());
            if (parent.getPackageType().equalsIgnoreCase("Business")) {
                tvParent.setBackgroundResource(R.color.tree_business);
            } else {
                tvParent.setBackgroundResource(R.color.tree_personal);
            }
            showParent();
        } else {
            hideParent();
        }
        if (fatherLeft != null || fatherRight != null) {
            showBothFather();
            if (fatherLeft != null) {
                tvFatherLeft.setText(fatherLeft.getName() + "\n" +fatherLeft.getUserId() + "\nBV=" + fatherLeft.getBv() + "\n" + fatherLeft.getActivationDate());
                model.setFatherLeft(fatherLeft.getUserId());
                if (fatherLeft.getPackageType().equalsIgnoreCase("Business")) {
                    tvFatherLeft.setBackgroundResource(R.color.tree_business);
                } else {
                    tvFatherLeft.setBackgroundResource(R.color.tree_personal);
                }
                showFatherLeft();
                setFatherLeftChildrens(result);
            } else {
                hideFatherLeft();
            }
            if (fatherRight != null) {
                tvFatherRight.setText(fatherRight.getName() + "\n" +fatherRight.getUserId() + "\nBV=" + fatherRight.getBv() + "\n" + fatherRight.getActivationDate());
                model.setFatherRight(fatherRight.getUserId());
                if (fatherRight.getPackageType().equalsIgnoreCase("Business")) {
                    tvFatherRight.setBackgroundResource(R.color.tree_business);
                } else {
                    tvFatherRight.setBackgroundResource(R.color.tree_personal);
                }
                showFatherRight();
                setFatherRightChildrens(result);
            } else {
                hideFatherRight();
            }
        } else {
            hideBothFather();
        }

    }

    private void setFatherLeftChildrens(MyTeamModel result) {
        MyTeamModel fatherLeftChildLeft = result.getFatherLeftChildLeft();
        MyTeamModel fatherLeftChildRight = result.getFatherLeftChildRight();

        if (fatherLeftChildLeft != null || fatherLeftChildRight != null) {
            showFatherLeftBothChildrens();
            if (fatherLeftChildLeft != null) {
                tvFatherLeftChildLeft.setText(fatherLeftChildLeft.getUserId());
                model.setFatherLeftChildLeft(fatherLeftChildLeft.getUserId());
                if (fatherLeftChildLeft.getPackageType().equalsIgnoreCase("Business")) {
                    tvFatherLeftChildLeft.setBackgroundResource(R.color.tree_business);
                } else {
                    tvFatherLeftChildLeft.setBackgroundResource(R.color.tree_personal);
                }
                llFatherLeftChildLeft.setVisibility(View.VISIBLE);
            } else {
                llFatherLeftChildLeft.setVisibility(View.INVISIBLE);
            }
            if (fatherLeftChildRight != null) {
                tvFatherLeftChildRight.setText(fatherLeftChildRight.getUserId());
                model.setFatherLeftChildRight(fatherLeftChildRight.getUserId());
                if (fatherLeftChildRight.getPackageType().equalsIgnoreCase("Business")) {
                    tvFatherLeftChildRight.setBackgroundResource(R.color.tree_business);
                } else {
                    tvFatherLeftChildRight.setBackgroundResource(R.color.tree_personal);
                }
                llFatherLeftChildRight.setVisibility(View.VISIBLE);
            } else {
                llFatherLeftChildRight.setVisibility(View.INVISIBLE);
            }
        }else{
            hideFatherLeftBothChildrens();
        }
    }

    private void setFatherRightChildrens(MyTeamModel result) {
        MyTeamModel fatherRightChildLeft = result.getFatherRightChildLeft();
        MyTeamModel fatherRightChildRight = result.getFatherRightChildRight();

        if (fatherRightChildLeft != null || fatherRightChildRight != null) {
            showFatherRightBothChildrens();
            if (fatherRightChildLeft != null) {
                tvFatherRightChildLeft.setText(fatherRightChildLeft.getUserId());
                model.setFatherLeftChildLeft(fatherRightChildLeft.getUserId());
                if (fatherRightChildLeft.getPackageType().equalsIgnoreCase("Business")) {
                    tvFatherRightChildLeft.setBackgroundResource(R.color.tree_business);
                } else {
                    tvFatherRightChildLeft.setBackgroundResource(R.color.tree_personal);
                }
                llFatherRightChildLeft.setVisibility(View.VISIBLE);
            } else {
                llFatherRightChildLeft.setVisibility(View.INVISIBLE);
            }
            if (fatherRightChildRight != null) {
                tvFatherRightChildRight.setText(fatherRightChildRight.getUserId());
                model.setFatherLeftChildRight(fatherRightChildRight.getUserId());
                if (fatherRightChildRight.getPackageType().equalsIgnoreCase("Business")) {
                    tvFatherRightChildRight.setBackgroundResource(R.color.tree_business);
                } else {
                    tvFatherRightChildRight.setBackgroundResource(R.color.tree_personal);
                }
                llFatherRightChildRight.setVisibility(View.VISIBLE);
            } else {
                llFatherRightChildRight.setVisibility(View.INVISIBLE);
            }
        }else{
            hideFatherRightBothChildrens();
        }
    }
    private void showParent() {
        llParent.setVisibility(View.VISIBLE);
        parentLayout.setVisibility(View.VISIBLE);
    }

    private void hideParent() {
        llParent.setVisibility(View.INVISIBLE);
        parentLayout.setVisibility(View.INVISIBLE);
    }

    private void showFatherLeft() {
        fatherLeftLayout.setVisibility(View.VISIBLE);
        llFatherLeft.setVisibility(View.VISIBLE);
    }

    private void hideFatherLeft() {
        fatherLeftLayout.setVisibility(View.INVISIBLE);
        llFatherLeft.setVisibility(View.INVISIBLE);
    }

    private void showFatherRight() {
        fatherRightLayout.setVisibility(View.VISIBLE);
        llFatherRight.setVisibility(View.VISIBLE);
    }

    private void hideFatherRight() {
        fatherRightLayout.setVisibility(View.INVISIBLE);
        llFatherRight.setVisibility(View.INVISIBLE);
    }

    private void hideBothFather() {
        parentLayout.setVisibility(View.INVISIBLE);
        fatherLeftLayout.setVisibility(View.INVISIBLE);
        fatherRightLayout.setVisibility(View.INVISIBLE);
    }

    private void showBothFather() {
        parentLayout.setVisibility(View.VISIBLE);
        fatherLeftLayout.setVisibility(View.VISIBLE);
        fatherRightLayout.setVisibility(View.VISIBLE);
    }

    private void showFatherLeftBothChildrens() {
        fatherLeftLayout.setVisibility(View.VISIBLE);
    }
    private void hideFatherLeftBothChildrens() {
        fatherLeftLayout.setVisibility(View.INVISIBLE);
    }

    private void showFatherRightBothChildrens() {
        fatherRightLayout.setVisibility(View.VISIBLE);
    }
    private void hideFatherRightBothChildrens() {
        fatherRightLayout.setVisibility(View.INVISIBLE);
    }


//    private void populateTree(ExpModel result) {
//        ExpModel root = result.getRootNode();
//        ExpModel rootLeftNode = result.getRootLeftNode();
//        ExpModel rootRightNode = result.getRootRightNode();
//
//        if (root != null) {
//
//            ExpModel.RootDetails rootDetails;
//            ExpModel.EntityDetails entityDetails;
//            rootDetails = root.getRootDetails();
//            entityDetails = root.getEntityDetails();
//
//            if (entityDetails != null) {
//                String entry = rootDetails.getRootEntity();
//                String bv = entityDetails.getBv();
//                String referenceNumber = entityDetails.getReferenceNumber();
//                String activationDate = entityDetails.getActivationDate();
//                String packageType = entityDetails.getPackageType();
//                tvRoot.setText(entry + "\nBV=" + bv + "\n" + activationDate);
//                model.setParent(entry);
//                if(packageType.equalsIgnoreCase("Business")){
//                    tvRoot.setBackgroundColor(getResources().getColor(R.color.tree_business));
//                }else{
//                    tvRoot.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//                }
//            }
//            if (rootLeftNode == null && rootRightNode == null) {
//                parent.setVisibility(View.INVISIBLE);
//                childLeft.setVisibility(View.INVISIBLE);
//                childRight.setVisibility(View.INVISIBLE);
//            }else {
//                parent.setVisibility(View.VISIBLE);
//            }
//            if (rootLeftNode != null) {
//                childLeft.setVisibility(View.VISIBLE);
//                llRootLeft.setVisibility(View.VISIBLE);
//                setLeftChild(rootLeftNode);
//            } else {
//                childLeft.setVisibility(View.INVISIBLE);
//                llRootLeft.setVisibility(View.INVISIBLE);
//            }
//            if (rootRightNode != null) {
//                childRight.setVisibility(View.VISIBLE);
//                llRootRight.setVisibility(View.VISIBLE);
//                setRightChild(rootRightNode);
//            } else {
//                childRight.setVisibility(View.INVISIBLE);
//                llRootRight.setVisibility(View.INVISIBLE);
//            }
//
//            llRoot.setVisibility(View.VISIBLE);
//        } else {
//            llRoot.setVisibility(View.INVISIBLE);
//        }
//    }
//
//    private void setLeftChild(ExpModel rootLeftNode) {
//        ExpModel.RootDetails rootDetails = rootLeftNode.getRootDetails();
//        ExpModel.EntityDetails entityDetails = rootLeftNode.getEntityDetails();
//        ExpModel.LeftChildDetails leftChildDetails = rootLeftNode.getLeftChildDetails();
//        ExpModel.RightChildDetails rightChildDetails = rootLeftNode.getRightChildDetails();
//
//        String entry = rootDetails.getRootEntity();
//        String left = rootDetails.getLeftOfRoot();
//        String right = rootDetails.getRightOfRoot();
//        if (entityDetails != null) {
//            String bv = entityDetails.getBv();
//            String referenceNumber = entityDetails.getReferenceNumber();
//            String activationDate = entityDetails.getActivationDate();
//            String packageType = entityDetails.getPackageType();
//            tvRootLeft.setText(entry + "\nBV=" + bv + "\n" + activationDate);
//            model.setFatherLeft(entry);
//            if(packageType.equalsIgnoreCase("Business")){
//                tvRootLeft.setBackgroundColor(getResources().getColor(R.color.tree_business));
//            }else{
//                tvRootLeft.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//            }
//        }
//        if (leftChildDetails == null && rightChildDetails == null) {
//            childLeft.setVisibility(View.INVISIBLE);
//        }else{
//            childLeft.setVisibility(View.VISIBLE);
//        }
//        if (leftChildDetails != null) {
//            setLeftChildLeft(leftChildDetails, left);
//            llRootLeftChildLeft.setVisibility(View.VISIBLE);
//        } else {
//            llRootLeftChildLeft.setVisibility(View.INVISIBLE);
//        }
//        if (rightChildDetails != null) {
//            setLeftChildRight(rightChildDetails, right);
//            llRootLeftChildRight.setVisibility(View.VISIBLE);
//        } else {
//            llRootLeftChildRight.setVisibility(View.INVISIBLE);
//        }
//    }
//    private void setLeftChildLeft(ExpModel.LeftChildDetails leftChildDetails, String left) {
//        String bv = leftChildDetails.getBv();
//        String referenceNumber = leftChildDetails.getReferenceNumber();
//        String activationDate = leftChildDetails.getActivationDate();
//        String packageType = leftChildDetails.getPackageType();
//        tvRootLeftChildLeft.setText(left);
//        model.setFatherLeftChildLeft(left);
//        if(packageType.equalsIgnoreCase("Business")){
//            tvRootLeftChildLeft.setBackgroundColor(getResources().getColor(R.color.tree_business));
//        }else{
//            tvRootLeftChildLeft.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//        }
//    }
//    private void setLeftChildRight(ExpModel.RightChildDetails rightChildDetails, String right) {
//        String bv = rightChildDetails.getBv();
//        String referenceNumber = rightChildDetails.getReferenceNumber();
//        String activationDate = rightChildDetails.getActivationDate();
//        String packageType = rightChildDetails.getPackageType();
//        tvRootLeftChildRight.setText(right);
//        model.setFatherLeftChildRight(right);
//        if(packageType.equalsIgnoreCase("Business")){
//            tvRootLeftChildRight.setBackgroundColor(getResources().getColor(R.color.tree_business));
//        }else{
//            tvRootLeftChildRight.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//        }
//    }
//    private void setRightChild(ExpModel rootRightNode) {
//        ExpModel.RootDetails rootDetails = rootRightNode.getRootDetails();
//        ExpModel.EntityDetails entityDetails = rootRightNode.getEntityDetails();
//        ExpModel.LeftChildDetails leftChildDetails = rootRightNode.getLeftChildDetails();
//        ExpModel.RightChildDetails rightChildDetails = rootRightNode.getRightChildDetails();
//
//        String entry = rootDetails.getRootEntity();
//        String left = rootDetails.getLeftOfRoot();
//        String right = rootDetails.getRightOfRoot();
//        if (entityDetails != null) {
//            String bv = entityDetails.getBv();
//            String referenceNumber = entityDetails.getReferenceNumber();
//            String activationDate = entityDetails.getActivationDate();
//            String packageType = entityDetails.getPackageType();
//            tvRootRight.setText(entry + "\nBV=" + bv + "\n" + activationDate);
//            model.setFatherRight(entry);
//            if(packageType.equalsIgnoreCase("Business")){
//                tvRootRight.setBackgroundColor(getResources().getColor(R.color.tree_business));
//            }else{
//                tvRootRight.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//            }
//        }
//        if (leftChildDetails == null && rightChildDetails == null) {
//            childRight.setVisibility(View.INVISIBLE);
//        }else{
//            childRight.setVisibility(View.VISIBLE);
//        }
//        if (leftChildDetails != null) {
//            setRightChildLeft(leftChildDetails, left);
//            llRootRightChildLeft.setVisibility(View.VISIBLE);
//        } else {
//            llRootRightChildLeft.setVisibility(View.INVISIBLE);
//        }
//        if (rightChildDetails != null) {
//            setRightChildRight(rightChildDetails, right);
//            llRootRightChildRight.setVisibility(View.VISIBLE);
//        } else {
//            llRootRightChildRight.setVisibility(View.INVISIBLE);
//        }
//    }
//    private void setRightChildLeft(ExpModel.LeftChildDetails leftChildDetails, String left) {
//        String bv = leftChildDetails.getBv();
//        String referenceNumber = leftChildDetails.getReferenceNumber();
//        String activationDate = leftChildDetails.getActivationDate();
//        String packageType = leftChildDetails.getPackageType();
//        tvRootRightChildLeft.setText(left);
//        model.setFatherRightChildLeft(left);
//        if(packageType.equalsIgnoreCase("Business")){
//            tvRootRightChildLeft.setBackgroundColor(getResources().getColor(R.color.tree_business));
//        }else{
//            tvRootRightChildLeft.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//        }
//    }
//    private void setRightChildRight(ExpModel.RightChildDetails rightChildDetails, String right) {
//        String bv = rightChildDetails.getBv();
//        String referenceNumber = rightChildDetails.getReferenceNumber();
//        String activationDate = rightChildDetails.getActivationDate();
//        String packageType = rightChildDetails.getPackageType();
//        tvRootRightChildRight.setText(right);
//        model.setFatherRightChildRight(right);
//        if(packageType.equalsIgnoreCase("Business")){
//            tvRootRightChildRight.setBackgroundColor(getResources().getColor(R.color.tree_business));
//        }else{
//            tvRootRightChildRight.setBackgroundColor(getResources().getColor(R.color.tree_personal));
//        }
//    }


}
