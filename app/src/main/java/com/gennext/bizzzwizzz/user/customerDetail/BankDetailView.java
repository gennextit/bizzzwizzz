package com.gennext.bizzzwizzz.user.customerDetail;

/**
 * Created by Abhijit on 27-Jan-17.
 */

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.JsonParser;

import java.util.ArrayList;


public class BankDetailView extends CompactFragment {

    private TextView tvName,tvBranch,tvBankName,tvAccount,tvIFSC;
    private ImageView ivDoc;
    private CustomerDetail customerDetail;

    public static BankDetailView newInstance(Context context, CustomerDetail customerDetail) {
        BankDetailView fragment = new BankDetailView();
        AppAnimation.setDialogAnimation(context,fragment);
        fragment.customerDetail=customerDetail;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.detail_bank, container, false);
        InitUI(v);
        loadBankDetail();
        return v;
    }


    private void InitUI(View v) {
        ivDoc=(ImageView)v.findViewById(R.id.iv_bank_slot);
        tvName=(TextView)v.findViewById(R.id.tv_bank_slot_1);
        tvBankName=(TextView)v.findViewById(R.id.tv_bank_slot_2);
        tvBranch=(TextView)v.findViewById(R.id.tv_bank_slot_3);
        tvAccount=(TextView)v.findViewById(R.id.tv_bank_slot_4);
        tvIFSC=(TextView)v.findViewById(R.id.tv_bank_slot_5);
    }

    private void loadBankDetail() {
        ArrayList<String> model= JsonParser.parseBankDetail(AppUser.getBankDetails(getActivity()));
        if(model!=null){
            tvName.setText(model.get(0));
            tvBankName.setText(model.get(1));
            tvBranch.setText(model.get(2));
            tvAccount.setText(model.get(3));
            tvIFSC.setText(model.get(4));

            Glide.with(getActivity())
                    .load(model.get(5))
                    .placeholder(R.drawable.ic_doc2)
                    .error(R.drawable.ic_doc2)
                    .into(ivDoc);

            customerDetail.setBankDetail(model);
        }
    }

}
