package com.gennext.bizzzwizzz.user;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.gennext.bizzzwizzz.PackageActivity;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.AppWebView;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.FieldValidation;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;


/**
 * Created by Abhijit-PC on 28-Feb-17.
 */

public class SignupUser extends CompactFragment implements ApiCallError.ErrorListener{
    private EditText etName, etMobile, etEmail, etPanNo, etPassword;
    private String profileType = "";
    private CheckBox cbAccept;

    private AssignTask assignTask;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }


    public static SignupUser newInstance() {
        SignupUser fragment = new SignupUser();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_personal_tab, container, false);
        initToolBar(getActivity(), v, "Sign Up");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        etName = (EditText) v.findViewById(R.id.et_profile_name);
        etMobile = (EditText) v.findViewById(R.id.et_profile_mobile);
        etEmail = (EditText) v.findViewById(R.id.et_profile_email);
        etPanNo = (EditText) v.findViewById(R.id.et_profile_pan);
        etPassword = (EditText) v.findViewById(R.id.et_profile_pass);
        cbAccept = (CheckBox) v.findViewById(R.id.cb_accept);


        RadioGroup rgProfileType = (RadioGroup) v.findViewById(R.id.rg_profile_type);


        v.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                executeTask();
            }
        });
        v.findViewById(R.id.btn_privacy_policy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(AppWebView.newInstance("Privacy Policy", AppSettings.PRIVACY_POLICY,AppWebView.PDF),"appWebView");
            }
        });
        v.findViewById(R.id.btn_terms_of_use).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(AppWebView.newInstance("Terms Of Use",AppSettings.TERMS_OF_USE,AppWebView.PDF),"appWebView");
            }
        });

        rgProfileType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId == R.id.personal) {
                    profileType = "male";
                } else if (checkedId == R.id.business) {
                    profileType = "female";
                }
            }
        });

    }

    private void executeTask() {
        if(checkValidation()) {
            hideKeybord(getActivity());
            assignTask = new AssignTask(getActivity(),etName.getText().toString(),etMobile.getText().toString(),
                    etEmail.getText().toString(),etPanNo.getText().toString(),etPassword.getText().toString(),
                    profileType);
            assignTask.execute(AppSettings.SIGNUP);
        }
    }

    private boolean checkValidation() {
        if (!FieldValidation.validate(getActivity(), etName, FieldValidation.NAME, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etMobile, FieldValidation.MOBILE, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etEmail, FieldValidation.EMAIL, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etPanNo, FieldValidation.STRING, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etPassword, FieldValidation.STRING, true)) {
            return false;
        } else if (TextUtils.isEmpty(profileType)) {
            showToast(getActivity(), "Please Select profile type");
            return false;
        } else if (!cbAccept.isChecked()) {
            showToast(getActivity(), "Please Accept our Privacy Policy and Terms of Use");
            return false;
        }
        return true;
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }


    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String name,mobile,email,panNo,pass,profileType;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String name, String mobile, String email, String panNo, String pass
                , String profileType) {
            this.context = context;
            this.name = name;
            this.mobile = mobile;
            this.email = email;
            this.panNo = panNo;
            this.pass = pass;
            this.profileType = profileType;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context,"Processing please wait");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            response = ApiCall.POST(urls[0], RequestBuilder.Signup(name,mobile,email,panNo,pass,profileType));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.parseLoginData(context, response);
//            return jsonParser.parseLoginDataTest();
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        Intent intent = new Intent(getActivity(), PackageActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert",result.getOutputMsg(),PopupAlert.POPUP_DIALOG);
                    } else {
                        showPopupAlert("Alert",result.getOutputMsg(),PopupAlert.POPUP_DIALOG);
                    }
                }else{
                    ApiCallError.newInstance(JsonParser.ERRORMESSAGE,SignupUser.this)
                            .show(getFragmentManager(),"apiCallError");
                }
            }
        }
    }
}
