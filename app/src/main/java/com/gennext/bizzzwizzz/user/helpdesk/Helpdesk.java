package com.gennext.bizzzwizzz.user.helpdesk;


import android.animation.Animator;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.HelpdeskAdapter;
import com.gennext.bizzzwizzz.model.HelpdeskModel;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Abhijit on 08-Dec-16.
 */

public class Helpdesk extends CompactFragment implements ApiCallError.ErrorFlagListener{
    private static final int LOAD_LIST = 1,RAISE_REQ=2;
    private ArrayList<HelpdeskModel> cList;
    private RecyclerView lvMain;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private HelpdeskAdapter adapter;

    private AssignTask assignTask;
    private LinearLayout layoutBottom;
    private String sltReqType;
    private String sltReqDetail;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Helpdesk newInstance() {
        Helpdesk helpdesk = new Helpdesk();
        return helpdesk;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_helpdesk, container, false);
        initToolBar(getActivity(), v, "Helpdesk");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        layoutBottom = (LinearLayout) v.findViewById(R.id.layoutBottom);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask(LOAD_LIST, null, null);
            }
        });
        cList = new ArrayList<>();
        adapter = new HelpdeskAdapter(getActivity(), cList, Helpdesk.this);
        lvMain.setAdapter(adapter);

        Button fab = (Button) v.findViewById(R.id.btn_send);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                addRaiseRequest();

            }
        });

        executeTask(LOAD_LIST, null, null);
    }

    private void addRaiseRequest() {
        RaiseRequest raiseRequest =RaiseRequest.newInstance(Helpdesk.this);
        AppAnimation.setDialogAnimation(getActivity(),raiseRequest);
        addFragment(raiseRequest, "raiseRequest");
    }

    private void showOrHideButton(int visibility) {
        if (visibility == View.VISIBLE && layoutBottom.getVisibility() != View.VISIBLE) {
            layoutBottom.setVisibility(View.VISIBLE);
            layoutBottom.animate().alpha(1).setStartDelay(0).setDuration(100)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    });
        } else if (visibility == View.GONE && layoutBottom.getVisibility() != View.GONE) {
            layoutBottom.animate().alpha(0).setStartDelay(1000).setDuration(100)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            layoutBottom.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    });
        }
    }

    private void executeTask(int task, String sltReqType, String detail) {
        if(task==LOAD_LIST) {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
            assignTask = new AssignTask(getActivity(), task, sltReqType, detail);
            assignTask.execute(AppSettings.ALL_REQUESTS);
        }else if(task==RAISE_REQ) {
            assignTask = new AssignTask(getActivity(), task, sltReqType, detail);
            assignTask.execute(AppSettings.RAISE_REQUEST);
        }
    }


    private void hideProgressBar() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void raiseRequestMethod(String sltReqType, String sltReqDetail) {
        this.sltReqType=sltReqType;
        this.sltReqDetail=sltReqDetail;
        executeTask(RAISE_REQ,sltReqType,sltReqDetail);
    }
    public void refreshListMethod() {
        executeTask(LOAD_LIST, null, null);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, int task) {
        if(task==LOAD_LIST) {
            executeTask(LOAD_LIST, null, null);
        }else if(task==RAISE_REQ) {
            executeTask(task, sltReqType, sltReqDetail);
        }
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, int flag) {

    }


    private class AssignTask extends AsyncTask<String, Void, HelpdeskModel> {
        private final int task;
        private final String sltReqType,detail;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, int task, String sltReqType, String detail) {
            this.context = context;
            this.task=task;
            this.sltReqType=sltReqType;
            this.detail=detail;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(task==RAISE_REQ){
                showProgressDialog(context,"Processing, Please wait.");
            }
        }

        @Override
        protected HelpdeskModel doInBackground(String... urls) {
            String response;
            String userId = AppUser.getUserId(context);
            if(task==LOAD_LIST) {
                response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseHelpdeskDetail(response);
            }else{
                response = ApiCall.POST(urls[0], RequestBuilder.RaiseReq(userId,sltReqType,detail,""));
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseRaiseReqDetail(response);
            }
        }


        @Override
        protected void onPostExecute(HelpdeskModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if(task==RAISE_REQ){
                    hideProgressDialog();
                }
                hideProgressBar();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if(task==LOAD_LIST) {
                            cList.clear();
                            cList.addAll(result.getList());
                            adapter.notifyDataSetChanged();
                        }else  if(task==RAISE_REQ) {
                            showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                            executeTask(LOAD_LIST,null,null);
                        }
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),task,Helpdesk.this)
                                .show(getFragmentManager(),"apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,task,Helpdesk.this)
                                .show(getFragmentManager(),"apiCallError");
                    }
                }
            }
        }
    }

}

//lvMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
//
//
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//
//                if (newState == RecyclerView.SCROLL_STATE_IDLE)
//                {
//                    showOrHideButton(View.VISIBLE);
//                }
//                super.onScrollStateChanged(recyclerView, newState);
//
//
////                switch (newState) {
////                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING: // SCROLL_STATE_FLING
////                        //hide button here
////                        showOrHideButton(View.GONE);
////                        break;
////
////                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL: // SCROLL_STATE_TOUCH_SCROLL
////                        //hide button here
////                        showOrHideButton(View.GONE);
////                        break;
////
////                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE: // SCROLL_STATE_IDLE
////                        //show button here
////                        showOrHideButton(View.VISIBLE);
////                        break;
////
////                    default:
////                        //show button here
////                        break;
////                }
//            }
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                if (dy > 0 ||dy<0 && layoutBottom.isShown())
//                {
//                    showOrHideButton(View.GONE);
//                }
//            }
//        });
