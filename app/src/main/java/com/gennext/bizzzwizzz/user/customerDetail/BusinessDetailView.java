package com.gennext.bizzzwizzz.user.customerDetail;

/**
 * Created by Abhijit on 27-Jan-17.
 */

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.JsonParser;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.CropCircleTransformation;


public class BusinessDetailView extends CompactFragment {

    private TextView tvCompName,tvTypOfEntity,tvContactPer,tvDOfCP,tvMobNo,tvEmail,tvLand1,tvLand2,tvWeb
            ,tvAddress;
    private CustomerDetail customerDetail;
    private ImageView ivCompLogo;
//    ,tvDayAndTofB,tvModeOfPayment,tvCat,tvSubCat,tvDealsItem;

    public static BusinessDetailView newInstance(Context context, CustomerDetail customerDetail) {
        BusinessDetailView fragment = new BusinessDetailView();
        AppAnimation.setDialogAnimation(context,fragment);
        fragment.customerDetail=customerDetail;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.detail_business, container, false);
        InitUI(v);
        loadBusinessDetail();
        return v;
    }


    private void InitUI(View v) {
        ivCompLogo=(ImageView)v.findViewById(R.id.iv_business_logo);
        tvCompName=(TextView)v.findViewById(R.id.tv_business_slot_1);
        tvTypOfEntity=(TextView)v.findViewById(R.id.tv_business_slot_2);
        tvContactPer=(TextView)v.findViewById(R.id.tv_business_slot_3);
        tvDOfCP=(TextView)v.findViewById(R.id.tv_business_slot_4);
        tvMobNo=(TextView)v.findViewById(R.id.tv_business_slot_5);
        tvEmail=(TextView)v.findViewById(R.id.tv_business_slot_6);
        tvLand1=(TextView)v.findViewById(R.id.tv_business_slot_7);
        tvLand2=(TextView)v.findViewById(R.id.tv_business_slot_8);
        tvWeb=(TextView)v.findViewById(R.id.tv_business_slot_9);
        tvAddress=(TextView)v.findViewById(R.id.tv_business_slot_10);

//        tvDayAndTofB=(TextView)v.findViewById(R.id.tv_business_slot_11);
//        tvModeOfPayment=(TextView)v.findViewById(R.id.tv_business_slot_12);
//        tvCat=(TextView)v.findViewById(R.id.tv_business_slot_13);
//        tvSubCat=(TextView)v.findViewById(R.id.tv_business_slot_14);
//        tvDealsItem=(TextView)v.findViewById(R.id.tv_business_slot_15);
    }

    private void loadBusinessDetail() {
        Glide.with(getActivity())
                .load(R.drawable.profile)
                .bitmapTransform(new CropCircleTransformation(getActivity()))
                .into(ivCompLogo);
        ArrayList<String> model= JsonParser.parseBusinessDetail(AppUser.getBusinessDetails(getActivity()));
        if(model!=null){
            tvCompName.setText(model.get(0));
            tvTypOfEntity.setText(model.get(1));
            tvContactPer.setText(model.get(2));
            tvDOfCP.setText(model.get(3));
            tvMobNo.setText(model.get(4));
            tvEmail.setText(model.get(5));
            tvLand1.setText(model.get(6));
            tvLand2.setText(model.get(7));
            tvWeb.setText(model.get(8));
            tvAddress.setText(model.get(9));
            customerDetail.setBusinessArray(model);

            if(!TextUtils.isEmpty(model.get(22))) {
                Glide.with(getActivity())
                        .load(model.get(22))
                        .bitmapTransform(new CropCircleTransformation(getActivity()))
                        .into(ivCompLogo);
            }else {
                Glide.with(getActivity())
                        .load(R.drawable.profile)
                        .bitmapTransform(new CropCircleTransformation(getActivity()))
                        .into(ivCompLogo);
            }
        }
    }

}
