package com.gennext.bizzzwizzz.user.customerDetail;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.model.ProfileModel;
import com.gennext.bizzzwizzz.user.MyProfile;
import com.gennext.bizzzwizzz.user.mypackage.MyPackage;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;

import java.util.ArrayList;


/**
 * Created by Abhijit on 12-Jan-17.
 */

public class CustomerDetail extends CompactFragment implements View.OnClickListener,ApiCallError.ErrorListener{

    private ArrayList<String> businessArray;
    private ArrayList<String> kycArray;
    private ArrayList<String> bankArray;
    private AssignTask assignTask;
    private TextView tvStatusBank,tvStatusBankStatus,tvStatusKyc,tvStatusKycStatus;
    private Button btnProfile,btnBusiness,btnKyc,btnbank;
    private LinearLayout llBusiness;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static CustomerDetail newInstance() {
        CustomerDetail fragment = new CustomerDetail();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_cusromer_detail, container, false);
        initToolBar(getActivity(), v, "Customer Detail");
        InitUI(v);
        setData();
        return v;
    }

    private void setData() {
        addFragmentWithoutBackStack(MyPackage.newInstance(), R.id.container0,"myPackage");
        addFragmentWithoutBackStack(PersonalProfile.newInstance(getActivity()), R.id.container1,"personalProfile");
        String pkgType=AppUser.getPackageType(getActivity());
        if(pkgType.equalsIgnoreCase("business")){
            llBusiness.setVisibility(View.VISIBLE);
            addFragmentWithoutBackStack(BusinessDetailView.newInstance(getActivity(),CustomerDetail.this), R.id.container2,"businessDetailView");
        }else {
            llBusiness.setVisibility(View.GONE);
        }
        addFragmentWithoutBackStack(KYCDetailView.newInstance(getActivity(),CustomerDetail.this), R.id.container3,"kycDetailView");
        addFragmentWithoutBackStack(BankDetailView.newInstance(getActivity(),CustomerDetail.this), R.id.container4,"bankDetailView");
    }

    public void attemptLogin() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.LOGIN);
    }

    private void InitUI(View v) {
        llBusiness=(LinearLayout)v.findViewById(R.id.ll_business);
        btnProfile=(Button)v.findViewById(R.id.btn_edit_profile);
        btnBusiness=(Button)v.findViewById(R.id.btn_edit_business);
        btnKyc=(Button)v.findViewById(R.id.btn_edit_kyc);
        btnbank=(Button)v.findViewById(R.id.btn_edit_bank);
        tvStatusBank=(TextView)v.findViewById(R.id.tv_status_bank);
        tvStatusBankStatus=(TextView)v.findViewById(R.id.tv_status_bank_status);
        tvStatusKyc=(TextView)v.findViewById(R.id.tv_status_kyc);
        tvStatusKycStatus=(TextView)v.findViewById(R.id.tv_status_kyc_status);

        btnProfile.setOnClickListener(this);
        btnBusiness.setOnClickListener(this);
        btnKyc.setOnClickListener(this);
        btnbank.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_edit_profile:
                ProfileModel model = AppUser.getUserProfile(getActivity());
                addFragment(MyProfile.newInstance(model,CustomerDetail.this),"myProfile");
                break;
            case R.id.btn_edit_business:
                addFragment(BusinessDetail.newInstance(CustomerDetail.this,businessArray),"businessDetail");
                break;
            case R.id.btn_edit_kyc:
                addFragment(KYCDetail.newInstance(CustomerDetail.this,kycArray),"kycDetail");
                break;
            case R.id.btn_edit_bank:
                addFragment(BankDetail.newInstance(CustomerDetail.this,bankArray),"bankDetail");
                break;
        }
    }

    public void setBusinessArray(ArrayList<String> model) {
        this.businessArray=model;
    }

    public void setKYCDetail(ArrayList<String> model) {
        this.kycArray=model;
        setKYCVerifiedStatus(model.get(10),model.get(11));
    }

    private void setKYCVerifiedStatus(String verifyStatus,String reason) {
        switch (verifyStatus.toLowerCase()) {
            case "":
                tvStatusKyc.setText("( Pending )");
                break;
            case "yes":
                tvStatusKyc.setText("( Approved )");
                tvStatusKyc.setTextColor(getResources().getColor(R.color.status_inprogress));
                btnKyc.setVisibility(View.INVISIBLE);
                btnProfile.setVisibility(View.INVISIBLE);
                break;
            case "no":
                tvStatusKyc.setText("( Rejected )");
                if(!TextUtils.isEmpty(reason)){
                    tvStatusKycStatus.setText("Reason : "+reason);
                    tvStatusKycStatus.setVisibility(View.VISIBLE);
                    tvStatusKycStatus.setTextColor(getResources().getColor(R.color.status_cancelled));
                }
                tvStatusKyc.setTextColor(getResources().getColor(R.color.status_cancelled));
                btnKyc.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void setBankDetail(ArrayList<String> model) {
        this.bankArray=model;
        setBankVerifiedStatus(model.get(7),model.get(8));
    }

    private void setBankVerifiedStatus(String verifyStatus,String reason) {
        switch (verifyStatus.toLowerCase()){
            case "":
                tvStatusBank.setText("( Pending )");
                break;
            case "yes":
                tvStatusBank.setText("( Approved )");
                tvStatusBank.setTextColor(getResources().getColor(R.color.status_inprogress));
                btnbank.setVisibility(View.INVISIBLE);
                break;
            case "no":
                tvStatusBank.setText("( Rejected )");
                if(!TextUtils.isEmpty(reason)){
                    tvStatusBankStatus.setText("Reason : "+reason);
                    tvStatusBankStatus.setVisibility(View.VISIBLE);
                    tvStatusBankStatus.setTextColor(getResources().getColor(R.color.status_cancelled));
                }
                tvStatusBank.setTextColor(getResources().getColor(R.color.status_cancelled));
                btnbank.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        attemptLogin();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    private class AssignTask extends AsyncTask<String, Void, Model> {
        private Context activity;

        public void onAttach(Context activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Context activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Reloading, please wait...");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            response = ApiCall.POST(urls[0], RequestBuilder.LoginBody(AppUser.getUserId(activity), AppUser.getPassword(activity)));
            JsonParser jsonParser = new JsonParser();
            Model model = jsonParser.parseLoginData(activity, response);
//            if(model!=null && model.getOutput().equals("success")) {
                AppUser.setAppData(activity,response);
                return model;
//            }else{
//                response= AppUser.getAppData(activity);
//                jsonParser = new JsonParser();
//                return jsonParser.parseLoginData(activity, response);
//            }
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressDialog();
            if (activity != null) {
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        setData();
                    } else if (result.getOutput().equals("failure")) {
                        Toast.makeText(activity, result.getOutputMsg(), Toast.LENGTH_LONG).show();
                    }  else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),CustomerDetail.this)
                                .show(getFragmentManager(),"apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,CustomerDetail.this)
                                .show(getFragmentManager(),"apiCallError");
                    }
                }
            }
        }
    }
}