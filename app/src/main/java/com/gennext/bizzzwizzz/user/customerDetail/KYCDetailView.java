package com.gennext.bizzzwizzz.user.customerDetail;

/**
 * Created by Abhijit on 27-Jan-17.
 */

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.JsonParser;

import java.util.ArrayList;


public class KYCDetailView extends CompactFragment {

    private TextView tvPanNo,tvIDProof,tvIDNo,tvName,tvNameOfN,tvNMobile,tvNAddress;
    private ImageView ivDocID,ivDocPAN;
    private CustomerDetail customerDetail;

    public static KYCDetailView newInstance(Context context, CustomerDetail customerDetail) {
        KYCDetailView fragment = new KYCDetailView();
        AppAnimation.setDialogAnimation(context,fragment);
        fragment.customerDetail=customerDetail;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.detail_kyc, container, false);
        InitUI(v);
        loadKYCDetail();
        return v;
    }


    private void InitUI(View v) {
        ivDocPAN=(ImageView)v.findViewById(R.id.iv_kyc_slot_1);
        ivDocID=(ImageView)v.findViewById(R.id.iv_kyc_slot_2);
        tvPanNo=(TextView)v.findViewById(R.id.tv_kyc_slot_1);
        tvIDProof=(TextView)v.findViewById(R.id.tv_kyc_slot_2);
        tvIDNo=(TextView)v.findViewById(R.id.tv_kyc_slot_3);
        tvName=(TextView)v.findViewById(R.id.tv_kyc_slot_4);
        tvNameOfN=(TextView)v.findViewById(R.id.tv_kyc_slot_5);
        tvNMobile=(TextView)v.findViewById(R.id.tv_kyc_slot_6);
        tvNAddress=(TextView)v.findViewById(R.id.tv_kyc_slot_7);
    }

    private void loadKYCDetail() {
        ArrayList<String> model= JsonParser.parseKYCDetail(AppUser.getKycDetails(getActivity()));
        if(model!=null){
            tvPanNo.setText(model.get(0));
            tvIDProof.setText(model.get(1));
            tvIDNo.setText(model.get(2));
            tvName.setText(model.get(3));
            tvNameOfN.setText(model.get(4));
            tvNMobile.setText(model.get(5));
            tvNAddress.setText(model.get(6));

            Glide.with(getActivity())
                    .load(model.get(7))
                    .placeholder(R.drawable.ic_doc2)
                    .error(R.drawable.ic_doc2)
                    .into(ivDocPAN);
            Glide.with(getActivity())
                    .load(model.get(8))
                    .placeholder(R.drawable.ic_doc2)
                    .error(R.drawable.ic_doc2)
                    .into(ivDocID);

            customerDetail.setKYCDetail(model);
        }
    }

}
