package com.gennext.bizzzwizzz.user.directRefrel;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.intro.IntroReferral;
import com.gennext.bizzzwizzz.intro.IntroTokens;
import com.gennext.bizzzwizzz.model.DirectReferralModel;
import com.gennext.bizzzwizzz.model.DirectRefrelAdapter;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Abhijit on 08-Dec-16.
 */

public class DirectRefrel extends CompactFragment implements ApiCallError.ErrorListener{
    private ArrayList<DirectReferralModel> cList;
    private RecyclerView lvMain;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private DirectRefrelAdapter adapter;

    private AssignTask assignTask;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    public static DirectRefrel newInstance() {
        DirectRefrel directRefrel=new DirectRefrel();
        return directRefrel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_directreferal, container, false);
        initToolBar(getActivity(), v, "Direct Referral Detail");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask();
            }
        });
        cList = new ArrayList<>();
        adapter = new DirectRefrelAdapter(getActivity(), cList,DirectRefrel.this);
        lvMain.setAdapter(adapter);


        executeTask();
    }

    private void executeTask() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.DIRECT_REFERRAL_DETAILS);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    private void hideProgressBar() {
        mSwipeRefreshLayout.setRefreshing(false);

    }

    private class AssignTask extends AsyncTask<String, Void, DirectReferralModel> {
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected DirectReferralModel doInBackground(String... urls) {
            String response;
            String userId= AppUser.getUserId(context);
            response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.parseDirectReferralDetail(response);
        }


        @Override
        protected void onPostExecute(DirectReferralModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        cList.clear();
                        cList.addAll(result.getList());
                        adapter.notifyDataSetChanged();
                        if(IntroTokens.referralIntroOpen(context)){
                            addFragment(IntroReferral.newInstance(),"introReferral");
                        }
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert",result.getOutputMsg(), PopupAlert.FRAGMENT);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),DirectRefrel.this)
                                .show(getFragmentManager(),"apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,DirectRefrel.this)
                                .show(getFragmentManager(),"apiCallError");
                    }
                }
            }
        }
    }

}


