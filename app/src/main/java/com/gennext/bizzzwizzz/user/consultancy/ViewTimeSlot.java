package com.gennext.bizzzwizzz.user.consultancy;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.model.ConsultancyModel;
import com.gennext.bizzzwizzz.model.TimeSlotAdapter;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.CompactFragment;

import java.util.ArrayList;


/**
 * Created by Abhijit on 21-Dec-16.
 */

public class ViewTimeSlot extends CompactFragment {

    private BookASlot booASlot;
    private ArrayList<ConsultancyModel> timeSlotList;
    private RecyclerView gridView;
    private TimeSlotAdapter adapter;
    private LinearLayout llslot;

    public static ViewTimeSlot newInstance(BookASlot bookASlot, ArrayList<ConsultancyModel> timeSlotList) {
        ViewTimeSlot fragment=new ViewTimeSlot();
        fragment.booASlot=bookASlot;
        fragment.timeSlotList=timeSlotList;
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_book_time_slot, container, false);
//        initToolBar(getActivity(), v, "Book A Slot for Consultancy");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        gridView = (RecyclerView) v.findViewById(R.id.grid);
        llslot = (LinearLayout) v.findViewById(R.id.slot);
        gridView.setHasFixedSize(true);

        //set layout manager and adapter for "GridView"
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 4);
        gridView.setLayoutManager(layoutManager);
        if(timeSlotList!=null) {
            adapter = new TimeSlotAdapter(getActivity(), timeSlotList,ViewTimeSlot.this);
            gridView.setAdapter(adapter);
        }
        AppAnimation.setViewAnimation(llslot, AppAnimation.FLIP_HORIZONTAL);

    }


    public void setTimeSlot(String slotTime) {
        dismissFlipAnimation(llslot,slotTime);
    }

    public void dismissFlipAnimation(final View view, final String slotTime) {

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new LinearInterpolator());
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(view, "ScaleY", 1f, 0f, 0f);
        scaleYAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                getFragmentManager().popBackStack();
                booASlot.requestBooking(slotTime);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                getFragmentManager().popBackStack();
                booASlot.requestBooking(slotTime);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        animatorSet.playTogether(scaleYAnimator);
        animatorSet.start();
    }
}
