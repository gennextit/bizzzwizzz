package com.gennext.bizzzwizzz.user;


import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class AppFeedback extends CompactFragment implements ApiCallError.ErrorListener{
    private Button btnOK;
    private FragmentManager manager;
    AssignTask assignTask;
    private EditText etFeedback, etEmail;
    private RatingBar rbRating;
    private AppAnimation anim;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (assignTask != null) {
            assignTask.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static AppFeedback newInstance() {
        AppFeedback appFeedback=new AppFeedback();
        return appFeedback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_app_feedback, container, false);
        manager = getFragmentManager();
//        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(getActivity()));
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
//        tvTitle = (TextView) v.findViewById(R.id.tv_popup_title);
        btnOK = (Button) v.findViewById(R.id.btn_popup);
        ImageView ivImage = (ImageView) v.findViewById(R.id.iv_profile);
        LinearLayout llClose = (LinearLayout) v.findViewById(R.id.ll_whitespace);
        etFeedback = (EditText) v.findViewById(R.id.et_alert_feedback);
        etEmail = (EditText) v.findViewById(R.id.et_alert_email);
        rbRating = (RatingBar) v.findViewById(R.id.ratingBar);

//        tvTitle.setText(title);

        String emailId = AppUser.getEmailAddress(getActivity());
        if (emailId.equals("")) {
            etEmail.setVisibility(View.VISIBLE);
        } else {
            etEmail.setText(emailId);
            etEmail.setVisibility(View.GONE);
        }

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                manager.popBackStack();
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                sendFeedback(etFeedback.getText().toString(),
                        AppUser.getUserId(getActivity()));
            }
        });
    }

    private void sendFeedback(String feedback, String regId) {
        assignTask = new AssignTask(getActivity(), feedback, regId);
        assignTask.execute(AppSettings.SEND_FEEDBACK);
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        sendFeedback( etFeedback.getText().toString(),
                AppUser.getUserId(getActivity()));
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }



    private class AssignTask extends AsyncTask<String, Void, Model> {
        Activity activity;
        private String rating, feedback, regId, emailId;

        public void onAttach(Activity activity) {
            // TODO Auto-generated method stub
            this.activity = activity;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, String feedback, String regId) {
            this.feedback = feedback;
            this.regId = regId;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Processing please wait...");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            String userId = AppUser.getUserId(activity);
            response = ApiCall.upload(urls[0], RequestBuilder.FeedbackDetail(userId, feedback));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.defaultParser(response);
        }

        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        showPopupAlert("Feedback", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Feedback", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),AppFeedback.this)
                                .show(getFragmentManager(),"apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,AppFeedback.this)
                                .show(getFragmentManager(),"apiCallError");
                    }
                }
            }
        }
    }
}