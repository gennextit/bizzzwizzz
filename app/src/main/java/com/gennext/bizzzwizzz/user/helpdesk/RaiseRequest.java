package com.gennext.bizzzwizzz.user.helpdesk;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.bizzzwizzz.DocumentMaster;
import com.gennext.bizzzwizzz.ImageMaster;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.model.HelpdeskModel;
import com.gennext.bizzzwizzz.model.RaiseRequestAdapter;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.FieldValidation;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;
import com.gennext.bizzzwizzz.util.Utility;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.nbsp.materialfilepicker.MaterialFilePicker;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by Abhijit on 26-Dec-16.
 */
public class RaiseRequest extends CompactFragment implements ApiCallError.ErrorFlagListener {

    private static final int LOAD_DATA = 1, RAISE_REQUEST = 2;
    public static final int ATTACHMENT = 0;

    private AssignTask assignTask;
    private Button btnSend;
    private Spinner spMain;
    private EditText etDescription;
    private RaiseRequestAdapter adapter;
    private String sltReqType;
    private Helpdesk helpdesk;
    private ImageButton btnDoc;
    private TextView tvDocStatus;
    private File sltFile;
    private PermissionListener permissionListener;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static RaiseRequest newInstance(Helpdesk helpdesk) {
        RaiseRequest raiseRequest = new RaiseRequest();
        raiseRequest.helpdesk = helpdesk;
        return raiseRequest;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_raise_request, container, false);
        initToolBar(getActivity(), v, "Raise Request");
        InitUI(v);
        setPermissionListener();
        return v;
    }

    private void InitUI(View v) {
        btnSend = (Button) v.findViewById(R.id.btn_send);
        btnDoc = (ImageButton) v.findViewById(R.id.btnDoc);
        spMain = (Spinner) v.findViewById(R.id.sp_field1);
        tvDocStatus = (TextView) v.findViewById(R.id.tvDocStatus);
        etDescription = (EditText) v.findViewById(R.id.et_field1);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeTask(RAISE_REQUEST);
            }
        });


        btnDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TedPermission(getActivity())
                        .setPermissionListener(permissionListener)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setGotoSettingButtonText("setting")
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .check();
            }
        });

        spMain.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> ad, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
//                    ConsultancyModel model=adapter.getItem(position);
                if (adapter != null)
                    sltReqType = adapter.getRequestId(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        executeTask(LOAD_DATA);
    }

    private void executeTask(int task) {
        if(task==RAISE_REQUEST) {
            if (checkValidation()) {
                hideKeybord(getActivity());
                assignTask = new AssignTask(getActivity(), task, etDescription.getText().toString());
                assignTask.execute(AppSettings.RAISE_REQUEST);
            }
        }else{
            assignTask = new AssignTask(getActivity(), task, etDescription.getText().toString());
            assignTask.execute(AppSettings.RAISE_REQUEST);
        }
    }

    private boolean checkValidation() {
        if (sltReqType == null || sltReqType.equals("0")) {
            showToast(getActivity(), "Please Select request type");
            return false;
        } else if (!FieldValidation.validate(getActivity(), etDescription, FieldValidation.STRING, true)) {
            return false;
        }
        return true;
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, int task) {
        executeTask(task);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, int task) {

    }

    private class AssignTask extends AsyncTask<String, Void, HelpdeskModel> {

        private final int task;
        private final String description;
        private Context context;
        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, int task, String description) {
            this.task = task;
            this.context = context;
            this.description = description;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (task == RAISE_REQUEST) {
                showProgressDialog(context, "Processing, Please wait.");
            }
        }

        @Override
        protected HelpdeskModel doInBackground(String... urls) {
            if (task == LOAD_DATA) {
                String response = AppUser.getRequestType(context);
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseRequest(response);
            } else {
                String userId = AppUser.getUserId(context);
                String response = ApiCall.upload(urls[0], RequestBuilder.raiseRequest(userId, sltReqType, description, sltFile));
                JsonParser jsonParser = new JsonParser();
                return jsonParser.parseRaiseReqDetail(response);
            }
        }

        @Override
        protected void onPostExecute(HelpdeskModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (task == RAISE_REQUEST) {
                    hideProgressDialog();
                }
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        if (task == LOAD_DATA) {
                            adapter = new RaiseRequestAdapter(getActivity(), result.getList());
                            spMain.setAdapter(adapter);
                        } else {
                            helpdesk.refreshListMethod();
                            showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT);
                        }
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT);
                    } else if (result.getOutput().equals("internet")) {
                        if (task == RAISE_REQUEST) {
                            hideProgressDialog();
                        }
                        ApiCallError.newInstance(result.getOutputMsg(), task, RaiseRequest.this)
                                .show(getFragmentManager(), "apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        if (task == RAISE_REQUEST) {
                            hideProgressDialog();
                        }
                        ApiCallError.newInstance(result.getOutputMsg(),true, task, RaiseRequest.this)
                                .show(getFragmentManager(), "apiCallError");
                    }
                }
            }
        }

    }
    public void addThemToView(String filePath) {
        sltFile = new File(filePath);
        String string1 = Utility.getColoredSpanned(sltFile.getName(), "#3ab54a");
        String string2 = Utility.getColoredSpanned("\nis selected", "#666666");
        tvDocStatus.setText(Html.fromHtml(string1 + " " + string2));
        Toast.makeText(getActivity(), sltFile.getName(), Toast.LENGTH_LONG).show();
    }


    private void setPermissionListener() {
        permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                onPickDoc();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(getActivity(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }


        };
    }

    public void onPickDoc() {
//        new MaterialFilePicker()
//                .withActivity(getActivity())
//                .withRequestCode(RaiseRequest.ATTACHMENT)
////                .withFilter(Pattern.compile(".*\\.txt$")) // Filtering files and directories by file name using regexp
//                .withFilterDirectories(false) // Set directories filterable (false by default)
//                .withHiddenFiles(true) // Show hidden files and folders
//                .start();
        Intent intent = new Intent(getActivity(), DocumentMaster.class);
        intent.putExtra("from", DocumentMaster.REQ_RAISE_ATTACH);
        startActivityForResult(intent, DocumentMaster.REQ_RAISE_ATTACH);
    }


}
