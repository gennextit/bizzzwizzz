package com.gennext.bizzzwizzz.user.directRefrel;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.databinding.AlertReferralDetailBinding;
import com.gennext.bizzzwizzz.model.DirectReferralModel;
import com.gennext.bizzzwizzz.util.CompactFragment;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class DirectReferralDetail extends CompactFragment {
    private TextView tvTitle, tvDate;
    private Button btnOK;
    private String title, date;
    private FragmentManager manager;
    private LinearLayout llWhitespace;

    private DirectReferralModel model;
    private LinearLayout llslot;
    private Animation animMove;
    private LinearLayout llslotBody;


    public static DirectReferralDetail newInstance(String title, String date, DirectReferralModel model) {
        DirectReferralDetail fragment = new DirectReferralDetail();
        fragment.title = title;
        fragment.date = date;
        fragment.model = model;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AlertReferralDetailBinding binding = DataBindingUtil.inflate(inflater, R.layout.alert_referral_detail, container, false);
        binding.setModel(model);
        View v = binding.getRoot();
        manager = getFragmentManager();
        InitUI(v);
        setAnimation();
        return v;
    }

    private void InitUI(View v) {
        tvTitle = (TextView) v.findViewById(R.id.tv_popup_1);
        tvDate = (TextView) v.findViewById(R.id.tv_popup_2);
        btnOK = (Button) v.findViewById(R.id.btn_popup);
        llWhitespace = (LinearLayout) v.findViewById(R.id.ll_whitespace);
        llslot = (LinearLayout) v.findViewById(R.id.slot);
        llslotBody = (LinearLayout) v.findViewById(R.id.ll_body);

        tvTitle.setText(title);
        tvDate.setText(date);


        llWhitespace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                manager.popBackStack();
                dismissFlipAnimation(llslot);

            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                manager.popBackStack();
                dismissFlipAnimation(llslot);

            }
        });

    }

    //    private void setAnimation() {
//        AppAnimation.setViewAnimation(llslot, AppAnimation.FLIP_HORIZONTAL);
//    }
    private void setAnimation() {
//        AppAnimation.setViewAnimation(llslot,AppAnimation.FLIP_HORIZONTAL);
        animMove = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down);

        llslotBody.startAnimation(animMove);
    }


    public void dismissFlipAnimation(final View view) {

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new LinearInterpolator());
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(view, "ScaleY", 1f, 0f, 0f);
        scaleYAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (manager != null) {
                    manager.popBackStack();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                if (manager != null) {
                    manager.popBackStack();
                }
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        animatorSet.playTogether(scaleYAnimator);
        animatorSet.start();
    }

}
