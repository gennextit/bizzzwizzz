package com.gennext.bizzzwizzz.user.payout;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.databinding.AlertMyPayoutDetailBinding;
import com.gennext.bizzzwizzz.model.MyPayoutModel;
import com.gennext.bizzzwizzz.util.CompactFragment;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class MyPayoutDetail extends CompactFragment {
    private TextView tvTitle, tvDesc;
    private Button btnOK;
    private String title, date;
    private FragmentManager manager;
    private LinearLayout llWhitespace;

    private MyPayoutModel model;
    private LinearLayout llslot;
    private LinearLayout llslotBody;


    public static MyPayoutDetail newInstance(String title, String date, MyPayoutModel model) {
        MyPayoutDetail myPayoutDetail=new MyPayoutDetail();
        myPayoutDetail.title = title;
        myPayoutDetail.model = model;
        myPayoutDetail.date = date;
        return myPayoutDetail;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        Bundle bundle = getArguments();
//        String actionTitle = "DUMMY ACTION";
//        String transText = "";
//
//        if (bundle != null) {
//            actionTitle = bundle.getString("ACTION");
//            transText = bundle.getString("TRANS_TEXT");
//        }

        AlertMyPayoutDetailBinding binding = DataBindingUtil.inflate(inflater, R.layout.alert_my_payout_detail, container, false);
        binding.setModel(model);
        View v = binding.getRoot();
        tvDesc = (TextView) v.findViewById(R.id.tv_popup_2);
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            tvDesc.setTransitionName(transText);
//            tvDesc.setText(actionTitle);
//        }
        tvDesc.setText(date);
        manager = getFragmentManager();
        InitUI(v);
        setAnimation();
        return v;
    }


    private void InitUI(View v) {
        tvTitle = (TextView) v.findViewById(R.id.tv_popup_1);
        btnOK = (Button) v.findViewById(R.id.btn_popup);
        llWhitespace = (LinearLayout) v.findViewById(R.id.ll_whitespace);
        llslot = (LinearLayout) v.findViewById(R.id.slot);
        llslotBody = (LinearLayout) v.findViewById(R.id.ll_body);

        tvTitle.setText(title);
//        tvDesc.setText(DateTimeUtility.convertDate(date));


        llWhitespace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                manager.popBackStack();
                dismissFlipAnimation(llslot);
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                manager.popBackStack();
                dismissFlipAnimation(llslot);
            }
        });

    }

    private void setAnimation() {
//        AppAnimation.setViewAnimation(llslot,AppAnimation.FLIP_HORIZONTAL);
        Animation animMove = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down);

        llslotBody.startAnimation(animMove);
    }

    public void dismissFlipAnimation(final View view) {

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new LinearInterpolator());
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(view, "ScaleY", 1f, 0f, 0f);
        scaleYAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(manager!=null) {
                    manager.popBackStack();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                if(manager!=null) {
                    manager.popBackStack();
                }
            }

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        animatorSet.playTogether(scaleYAnimator);
        animatorSet.start();
    }



}