package com.gennext.bizzzwizzz.user;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.bizzzwizzz.ImageMaster;
import com.gennext.bizzzwizzz.MainActivity;
import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.UpdateCustomerDetail;
import com.gennext.bizzzwizzz.UserBinding;
import com.gennext.bizzzwizzz.common.CityPickerDialog;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.common.StatePickerDialog;
import com.gennext.bizzzwizzz.dashboard.Dashboard;
import com.gennext.bizzzwizzz.model.Model;
import com.gennext.bizzzwizzz.model.ProfileModel;
import com.gennext.bizzzwizzz.user.customerDetail.CustomerDetail;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppAnimation;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.FieldValidation;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;

import jp.wasabeef.glide.transformations.CropCircleTransformation;


/**
 * Created by Abhijit on 08-Dec-16.
 */
public class MyProfile extends CompactFragment implements ApiCallError.ErrorListener,StatePickerDialog.StateDialogListener, CityPickerDialog.CityDialogListener {

    private static final int REQUEST_MYPROFILE = 101;
    private EditText etName, etLandline, etMobile, etEmail, etDob, etAddress,etPinCode;
    private String gender = "";

    private AssignTask assignTask;
    private ProfileModel profile;
    private LinearLayout llState,llCity;
//    private Button btnCity;
    private String sltState, sltCity, sltStateId, sltCityId;
    private CustomerDetail customerDetail;
    private ImageView ivProfile;
    private Uri mImageUri;
    private TextView tvState,tvCity;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static MyProfile newInstance(ProfileModel profile, CustomerDetail customerDetail) {
        MyProfile fragment = new MyProfile();
        fragment.profile = profile;
        fragment.customerDetail=customerDetail;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        UserBinding binding = DataBindingUtil.inflate(inflater, R.layout.frag_myprofile, container, false);
        binding.setProfileModel(profile);

        View view = binding.getRoot();
//        View v = inflater.inflate(R.layout.frag_myprofile, container, false);
        if(customerDetail!=null){
            ((Toolbar)view.findViewById(R.id.toolbar)).setVisibility(View.VISIBLE);
            initToolBar(getActivity(), view, "Profile");
        }else {
            ((Toolbar)view.findViewById(R.id.toolbar)).setVisibility(View.GONE);
        }
//        initToolBar(getActivity(), view, "Profile");
        InitUI(view);
//        return v;
        return view;
    }

    private void InitUI(View v) {
        Button btnSend = (Button) v.findViewById(R.id.btn_send);
        etName = (EditText) v.findViewById(R.id.et_profile_name);
        ivProfile = (ImageView) v.findViewById(R.id.iv_profile);
        etMobile = (EditText) v.findViewById(R.id.et_profile_mobile);
        etLandline = (EditText) v.findViewById(R.id.et_profile_landline);
        etEmail = (EditText) v.findViewById(R.id.et_profile_email);
        etDob = (EditText) v.findViewById(R.id.et_profile_dob);
//        btnState = (Button) v.findViewById(R.id.btn_state);
//        btnCity = (Button) v.findViewById(R.id.btn_city);
        llState = (LinearLayout) v.findViewById(R.id.ll_select_state);
        llCity = (LinearLayout) v.findViewById(R.id.ll_select_city);
        tvState = (TextView) v.findViewById(R.id.tv_select_state);
        tvCity = (TextView) v.findViewById(R.id.tv_select_city);

        etAddress = (EditText) v.findViewById(R.id.et_profile_address);
        etPinCode = (EditText) v.findViewById(R.id.et_profile_pinCode);
        RadioGroup rgGender = (RadioGroup) v.findViewById(R.id.rg_gender);

        Glide.with(getActivity())
                .load(R.drawable.profile)
                .bitmapTransform(new CropCircleTransformation(getActivity()))
                .into(ivProfile);

        if (profile != null) {
            gender = profile.getGender();
            if (gender.equalsIgnoreCase("male")) {
                rgGender.check(R.id.male);
            } else if (gender.equalsIgnoreCase("female")) {
                rgGender.check(R.id.female);
            }
            this.sltStateId = profile.getStateId();
            this.sltCityId = profile.getCityId();
            if(!TextUtils.isEmpty(profile.getImage())) {
                Glide.with(getActivity())
                        .load(profile.getImage())
                        .bitmapTransform(new CropCircleTransformation(getActivity()))
                        .into(ivProfile);
            }else {
                Glide.with(getActivity())
                        .load(R.drawable.profile)
                        .bitmapTransform(new CropCircleTransformation(getActivity()))
                        .into(ivProfile);
            }
        }
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeTask();
            }
        });
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId == R.id.male) {
                    gender = "male";
                } else if (checkedId == R.id.female) {
                    gender = "female";
                }
            }
        });

        llState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showStateDialog();
            }
        });
        llCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(sltStateId)) {
                    showCityDialog(sltStateId, sltState);
                } else {
                    showToast(getActivity(), "Please select state");
                    showStateDialog();
                }
            }
        });

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ImageMaster.class);
                intent.putExtra("from", ImageMaster.REQUEST_MYPROFILE);
                startActivityForResult(intent, ImageMaster.REQUEST_MYPROFILE);
            }
        });
    }



    public void setImageUzRI(Uri uri) {
        mImageUri=uri;
//        ivProfile.setImageURI(uri);
        Glide.with(getActivity())
                .load(uri)
                .bitmapTransform(new CropCircleTransformation(getActivity()))
                .into(ivProfile);
    }


    private void showStateDialog() {
        StatePickerDialog dialog = StatePickerDialog.newInstance(MyProfile.this);
        AppAnimation.setDialogAnimation(getActivity(),dialog);
        dialog.show(getFragmentManager(), "statePickerDialog");
    }

    @Override
    public void onStateClick(DialogFragment dialog, String stateId, String stateName) {
        tvState.setText(stateName);
        this.sltState = stateName;
        this.sltStateId = stateId;
        this.sltCity=null;
        this.sltCity=null;
        tvCity.setText("Select City");
    }

    private void showCityDialog(String stateId, String stateName) {
        CityPickerDialog dialog = CityPickerDialog.newInstance(MyProfile.this, stateId, stateName);
        AppAnimation.setDialogAnimation(getActivity(),dialog);
        dialog.show(getFragmentManager(), "cityPickerDialog");
    }

    @Override
    public void onCityClick(DialogFragment dialog, String cityId, String cityName) {
        tvCity.setText(cityName);
        this.sltCity = cityName;
        this.sltCityId = cityId;
    }




    private boolean checkValidation() {
        if (!FieldValidation.validate(getActivity(), etName, FieldValidation.NAME, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etEmail, FieldValidation.EMAIL, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etMobile, FieldValidation.MOBILE, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etDob, FieldValidation.STRING, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etPinCode, FieldValidation.PIN_CODE, true)) {
            return false;
        } else if (!FieldValidation.validate(getActivity(), etAddress, FieldValidation.STRING, true)) {
            return false;
        } else if (TextUtils.isEmpty(sltStateId)) {
            showToast(getActivity(), "Please Select State");
            return false;
        } else if (TextUtils.isEmpty(sltCityId)) {
            showToast(getActivity(), "Please Select City");
            return false;
        }
//        else if (!FieldValidation.validate(getActivity(), btnState, FieldValidation.BUTTON, "State")) {
//            showToast(getActivity(),"Please Select State");
//            return false;
//        }else if (!FieldValidation.validate(getActivity(), btnCity, FieldValidation.BUTTON, "City")) {
//            showToast(getActivity(),"Please Select City");
//            return false;
//        }
        return true;
    }


    private void executeTask() {
        if (checkValidation()) {
            hideKeybord(getActivity());
            assignTask = new AssignTask(getActivity(), etName.getText().toString(),
                    etMobile.getText().toString(), gender, etEmail.getText().toString(), etDob.getText().toString()
                    , sltStateId, sltCityId, etPinCode.getText().toString(), etAddress.getText().toString(), etLandline.getText().toString(),mImageUri);
            assignTask.execute(AppSettings.UPDATE_PROFILE);

        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog) {

    }

    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final Uri mImageUri;
        private String name, mobile, gender, email, dob, state, city, landline,pinCode, address;
        private Context context;


        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String name, String mobile, String gender, String email, String dob
                , String state, String city, String pinCode, String address, String landline, Uri mImageUri) {
            this.context = context;
            this.name = name;
            this.mobile = mobile;
            this.gender = gender;
            this.email = email;
            this.dob = dob;
            this.state = state;
            this.city = city;
            this.pinCode = pinCode;
            this.address = address;
            this.landline = landline;
            this.mImageUri=mImageUri;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(context, "Processing, please wait");
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response;
            String userId = AppUser.getUserId(context);
            response = ApiCall.upload(urls[0], RequestBuilder.UpdateProfile(userId, name, mobile, gender, email, dob, state, city,pinCode, address, landline,mImageUri));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.defaultParser(response);
        }

        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            hideProgressDialog();
            if (context != null) {
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        AppUser.setUserProfile(context,name, mobile, gender, email, dob, sltState, sltCity,pinCode, address, landline);
//                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.FRAGMENT);
                        if(mImageUri!=null){
                            Dashboard dashboard = (Dashboard)getFragmentManager().findFragmentByTag("dashboard");
                            if (dashboard != null) {
                                ((MainActivity)getActivity()).setProfileImageUri(mImageUri);
                                dashboard.setProfileImage(mImageUri);
                            }
                        }
                        showToast(context,result.getOutputMsg());
                        if(customerDetail!=null){
                            showToast(context,result.getOutputMsg());
                            getFragmentManager().popBackStack();
                            customerDetail.attemptLogin();
                        }else{
                            showToast(context,result.getOutputMsg());
                            AppUser.setCustomerDetail(context);
                            if(AppUser.getPackageType(context).equalsIgnoreCase(AppUser.BUSINESS)){
                                ((UpdateCustomerDetail)getActivity()).setTab(UpdateCustomerDetail.TAB_BUSINESS);
                            }else{
                                ((UpdateCustomerDetail)getActivity()).setTab(UpdateCustomerDetail.TAB_KYC);
                            }
                        }
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),MyProfile.this)
                                .show(getFragmentManager(),"apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,MyProfile.this)
                                .show(getFragmentManager(),"apiCallError");
                    }
                }
            }
        }
    }
}
