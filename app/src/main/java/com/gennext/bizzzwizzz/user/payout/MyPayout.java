package com.gennext.bizzzwizzz.user.payout;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.gennext.bizzzwizzz.R;
import com.gennext.bizzzwizzz.common.FilterCriteria;
import com.gennext.bizzzwizzz.common.PopupAlert;
import com.gennext.bizzzwizzz.intro.IntroPayout;
import com.gennext.bizzzwizzz.intro.IntroTokens;
import com.gennext.bizzzwizzz.model.MyPayoutAdapter;
import com.gennext.bizzzwizzz.model.MyPayoutModel;
import com.gennext.bizzzwizzz.user.sales.MySales;
import com.gennext.bizzzwizzz.util.ApiCall;
import com.gennext.bizzzwizzz.util.ApiCallError;
import com.gennext.bizzzwizzz.util.AppSettings;
import com.gennext.bizzzwizzz.util.AppUser;
import com.gennext.bizzzwizzz.util.CompactFragment;
import com.gennext.bizzzwizzz.util.DateTimeUtility;
import com.gennext.bizzzwizzz.util.JsonParser;
import com.gennext.bizzzwizzz.util.RequestBuilder;

import java.util.ArrayList;

/**
 * Created by Abhijit on 08-Dec-16.
 */

public class MyPayout extends CompactFragment implements ApiCallError.ErrorFlagListener,FilterCriteria.FilterDialogListener{
    private static final int LOAD_LIST_TASK = 1, UPGRADE_TASK = 2;
    private ArrayList<MyPayoutModel> cList,originalList;
    private RecyclerView lvMain;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private MyPayoutAdapter adapter;

    private AssignTask assignTask;
    private ImageView ivFilter;
    private String sltFilterItem, startDate, endDate;
    private LinearLayout llPayCriteria;
    private ImageView ivCriteriaLeft,ivCriteriaRight,ivCriteriaBank,ivCriteriaKyc;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static MyPayout newInstance() {
        MyPayout myPayout=new MyPayout();
        return myPayout;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_mypayout, container, false);
        initToolBar(getActivity(), v, "My Payout");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        llPayCriteria = (LinearLayout) v.findViewById(R.id.ll_payout_criteria);
        ivCriteriaLeft = (ImageView) v.findViewById(R.id.iv_payout_status_left);
        ivCriteriaRight = (ImageView) v.findViewById(R.id.iv_payout_status_right);
        ivCriteriaBank = (ImageView) v.findViewById(R.id.iv_payout_status_bank);
        ivCriteriaKyc = (ImageView) v.findViewById(R.id.iv_payout_status_kyc);
        llPayCriteria.setVisibility(View.GONE);
        ivFilter = (ImageView) v.findViewById(R.id.iv_filter);
        ivFilter.setVisibility(View.GONE);
        ivFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterCriteria.newInstance(MyPayout.this, sltFilterItem, startDate, endDate).show(getFragmentManager(), "filterCriteria");
            }
        });
        lvMain.setHasFixedSize(true);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask(LOAD_LIST_TASK);
            }
        });

        cList=new ArrayList<>();
        adapter = new MyPayoutAdapter(getActivity(), cList,this,getFragmentManager());
        lvMain.setAdapter(adapter);

        executeTask(LOAD_LIST_TASK);
    }

    private void executeTask(int task) {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        assignTask = new AssignTask(getActivity(), task);
        assignTask.execute(AppSettings.MY_PAYOUTS);
    }

    private void showProgressBar(int task) {

    }

    private void hideProgressBar(int task) {
        if (task == LOAD_LIST_TASK) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onErrorRetryClick(DialogFragment dialog, int task) {
        executeTask(task);
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, int flag) {

    }

    @Override
    public void onFilterOkClick(DialogFragment dialog, String sltFilterItem, String startDate, String endDate) {
        this.sltFilterItem = sltFilterItem;
        this.startDate = startDate;
        this.endDate = endDate;
        if (sltFilterItem != null) {
            setMinContentInList(sltFilterItem);
        } else if (startDate != null && endDate != null) {
            filterListByDate(startDate, endDate);
        } else if (startDate != null) {
            filterListByDate(startDate);
        } else if (endDate != null) {
            filterListByDate(endDate);
        }
    }

    private void filterListByDate(String startDate, String endDate) {
        ArrayList<MyPayoutModel> cTempList;
        int startDateTStamp = DateTimeUtility.generateTimeStamp(startDate);
        int endDateTStamp = DateTimeUtility.generateTimeStamp(endDate);
        if (cList != null && originalList != null) {
            cTempList = new ArrayList<>();
            if(originalList.size()!=0){
                for (MyPayoutModel model:originalList) {
                    int sltDateTStamp = DateTimeUtility.generateTimeStamp(model.getPayDate());
                    if (startDateTStamp<=sltDateTStamp && sltDateTStamp<=endDateTStamp) {
                        cTempList.add(model);
                    }
                }
            }
            cList.clear();
            cList.addAll(cTempList);
            adapter.notifyDataSetChanged();
        }
    }


    private void filterListByDate(String date) {
        ArrayList<MyPayoutModel> cTempList;
        if (cList != null && originalList != null) {

            cTempList = new ArrayList<>();
            for (int i = 0; i < originalList.size(); i++) {
                if (originalList.get(i).getPayDate().equals(date)) {
                    cTempList.add(originalList.get(i));
                }
            }
            cList.clear();
            cList.addAll(cTempList);
            adapter.notifyDataSetChanged();
        }
    }

    private void setMinContentInList(String sltFilterItem) {
        if (!TextUtils.isEmpty(sltFilterItem)) {
            switch (sltFilterItem.toLowerCase()) {
                case "all":
                    if (originalList != null) {
                        maxContentInList(originalList.size());
                    }
                    break;
                case "10":
                    maxContentInList(10);
                    break;
                case "20":
                    maxContentInList(20);
                    break;
                case "30":
                    maxContentInList(30);
                    break;
                case "40":
                    maxContentInList(40);
                    break;
            }
        }
    }

    private void maxContentInList(int maxLength) {
        ArrayList<MyPayoutModel> cTempList;
        if (cList != null && originalList != null) {
            int listSize = originalList.size();
            if (listSize >= maxLength) {
                cTempList = new ArrayList<>();
                for (int i = 0; i < maxLength; i++) {
                    cTempList.add(originalList.get(i));
                }
                cList.clear();
                cList.addAll(cTempList);
                adapter.notifyDataSetChanged();
            }
        }
    }


    @Override
    public void onFilterCancelClick(DialogFragment dialog) {

    }

    private class AssignTask extends AsyncTask<String, Void, MyPayoutModel> {
        private Context context;
        int task;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, int task) {
            this.context = context;
            this.task = task;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar(task);
        }

        @Override
        protected MyPayoutModel doInBackground(String... urls) {
            String response;
            String userId= AppUser.getUserId(context);
            response = ApiCall.POST(urls[0], RequestBuilder.Default(userId));
            JsonParser jsonParser = new JsonParser();
            return jsonParser.parseMyPayoutDetail(response);
        }


        @Override
        protected void onPostExecute(MyPayoutModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar(task);
                if (result != null) {
                    if (result.getOutput().equals("success")) {
                        cList.clear();
                        cList.addAll(result.getList());
                        originalList=result.getList();
                        adapter.notifyDataSetChanged();
                        setCriteria(result.getCriteriaLeft(),result.getCriteriaRight(),result.getCriteriaBank(),
                                result.getCriteriaKyc());
                        ivFilter.setVisibility(View.VISIBLE);
                        if(IntroTokens.payoutIntroOpen(context)){
                            addFragment(IntroPayout.newInstance(),"introPayout");
                        }
                    } else if (result.getOutput().equals("failure")) {
                        showPopupAlert("Alert",result.getOutputMsg(), PopupAlert.FRAGMENT);
                    } else if (result.getOutput().equals("internet")) {
                        ApiCallError.newInstance(result.getOutputMsg(),task,MyPayout.this)
                                .show(getFragmentManager(),"apiCallError");
                    } else if (result.getOutput().equals("server")) {
                        ApiCallError.newInstance(result.getOutputMsg(),true,task,MyPayout.this)
                                .show(getFragmentManager(),"apiCallError");
                    }
                }
            }
        }


    }
    private void setCriteria(String criteriaLeft, String criteriaRight, String criteriaBank, String criteriaKyc) {
        if(!criteriaLeft.equals("0")&&!criteriaRight.equals("0")&&!criteriaBank.equals("")&&!criteriaKyc.equals("0")){
            llPayCriteria.setVisibility(View.GONE);
        }else {
            if(criteriaLeft.equals("0")){
                ivCriteriaLeft.setImageResource(R.drawable.ic_fail);
            }else {
                ivCriteriaLeft.setImageResource(R.drawable.ic_success);
            }if(criteriaRight.equals("0")){
                ivCriteriaRight.setImageResource(R.drawable.ic_fail);
            }else {
                ivCriteriaRight.setImageResource(R.drawable.ic_success);
            }if(criteriaBank.equals("0")){
                ivCriteriaBank.setImageResource(R.drawable.ic_fail);
            }else {
                ivCriteriaBank.setImageResource(R.drawable.ic_success);
            }if(criteriaKyc.equals("0")){
                ivCriteriaKyc.setImageResource(R.drawable.ic_fail);
            }else {
                ivCriteriaKyc.setImageResource(R.drawable.ic_success);
            }
            llPayCriteria.setVisibility(View.VISIBLE);
        }
    }
}

